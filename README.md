# ExpressionEngine Code Challenge #

10 hours to build an ExpressionEngine site which covers all the main areas of a CMS including: homepage, category pages, content filtering, EE2 coding standards, and more.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I set this up? ###

* Requirements *
* Installation *
* Database *
* Configuration *



### Requirements ###

Since this is an old code challenge (from 2012), I suggest running this on PHP 5.3 (when 5.3 was actually the current PHP version). Anything 5.4 and later may result in unexpected behavior.
The codebase should run on any traditional LAMP/LEMP stack and should be functional on a Microsoft IIS server if required (although IIS side-effects can occur when it comes to template rendering).

### Installation ###

Drop the files located in './https' into your public directory.

### Database ###

Use the SQL export file located in './sql' and import that into your database.

### Configuration ###

Assuming your environment supports the use of $_SERVER['HTTP_HOST'] and $_SERVER['DOCUMENT_ROOT'], the /library/expressionengine/config/config.php file should configure itself automatically. However, you will likely need to update the database credentials in /library/expressionengine/config/database.php with your environment's database credentials.


Per the ExpressionEngine documentation, you should recurse the suggested read/write/execute permissions on the following directories:

```
#!bash

$ chmod -R 775 /public
$ chmod -R 775 /library/expressionengine/cache
$ chmod 666 /library/expressionengine/config/config.php
$ chmod 666 /library/expressionengine/config/database.php

```

Refer to ExpressionEngine's documentation for additional permissions.





### Questions? ###


Just ask... hello@jarrettbarnett.com