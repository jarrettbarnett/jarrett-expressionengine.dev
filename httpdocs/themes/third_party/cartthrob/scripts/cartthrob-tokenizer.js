var CartthrobTokenizer;
(function(e){
	e.form = null;
	
	e.errorHandler = function(errorMessage){
		alert(errorMessage);
	};
	
	e.setErrorHandler = function(errorHandler){
		this.errorHandler = errorHandler;
		return this;
	};
	
	e.submitHandler = function(){
		CartthrobTokenizer.form.get(0).submit();
	};
	
	e.setSubmitHandler = function(submitHandler){
		this.submitHandler = submitHandler;
		return this;
	};
	
	e.bindHandler = function(){
		return true;
	};
	
	e.setBindHandler = function(bindHandler){
		this.bindHandler = bindHandler;
		return this;
	};
	
	e.beforeSubmit = function(){
		return true;
	};
	
	e.setBeforeSubmit = function(beforeSubmit){
		this.beforeSubmit = beforeSubmit;
		return this;
	};
	
	e.submissionState = function(){
		return CartthrobTokenizer.form.data("submissionState");
	}
	
	e.setSubmissionState = function(submissionState){
		CartthrobTokenizer.form.data("submissionState", submissionState);
		return this;
	};
	
	e.bind = function(){
		jQuery("#checkout_form").bind("submit", function(){
			CartthrobTokenizer.form = jQuery(this);
			if (CartthrobTokenizer.submissionState() === true){
				return false;
			}
			CartthrobTokenizer.setSubmissionState(true);
			if (CartthrobTokenizer.beforeSubmit() !== false){
				CartthrobTokenizer.bindHandler();
			}
			return false;
		});
	};
	
	e.init = function(bindHandler){
		this.setBindHandler(bindHandler);
		
		var interval = 50;
		var elapsed = 0;
		  
		if (typeof jQuery === "function"){
			return CartthrobTokenizer.bind();
		}
		
		var script = document.createElement("script");
		script.setAttribute("type", "text/javascript");
		//maybe we should replace this with a local version
		script.setAttribute("src", window.parent.document.location.protocol+"//ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js");
		document.getElementsByTagName("head")[0].appendChild(script);
		
		var check = setInterval(function(){
			elapsed += interval;
			if (typeof jQuery === "function"){
				clearInterval(check);
				return CartthrobTokenizer.bind();
			}
			//timeout after 5 seconds
			if (elapsed > 5000){
				clearInterval(check);
				return false;
			}
			return null;
		}, interval);
		
		return this;
	};
	
})(CartthrobTokenizer = {});
