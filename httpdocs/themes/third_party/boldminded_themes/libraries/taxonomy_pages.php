<?php

/**
 * ExpressionEngine Taxonomy Helper Class
 *
 * @package     ExpressionEngine
 * @category    Helpers
 * @author      Brian Litzinger
 * @copyright   Copyright 2010 - Brian Litzinger
 * @link        http://boldminded.com/
 */

class Taxonomy_Pages {
    
    static $inst = null;
    static $singleton = 0;

    private $return_data;
    private $EE;

    public function __construct()
    {
        if(self::$singleton == 0)
        {
            throw new Exception('This class cannot be instantiated by the new keyword.');
        }
    }
    
    public function get_pages()
    {
        $this->EE =& get_instance();
        
        // 1.x
        if(file_exists(PATH_THIRD .'taxonomy/libraries/MPTtree.php'))
        {
            require_once PATH_THIRD .'taxonomy/libraries/MPTtree.php';
            $this->EE->mpttree = new MPTtree;
            
            $taxonomy_version = 1;
        }
        // 2.x
        else
        {
            require_once PATH_THIRD .'taxonomy/libraries/Ttree.php';
            $this->EE->ttree = new Ttree;
            
            $taxonomy_version = 2;
        }
        
        //just to prevent any errors
        if ( ! defined('BASE'))
        {
            $s = ($this->EE->config->item('admin_session_type') != 'c') ? $this->EE->session->userdata('session_id') : 0;
            define('BASE', SELF.'?S='.$s.'&amp;D=cp');
        }
        
        $options = array();
        $return = '';
        
        $trees = $this->EE->db->get('taxonomy_trees');
        
        $options['depth']           = 100 ;
        $options['display_root']    = "yes";
        $options['root']            = 1;
        $options['root_entry_id']   = NULL;
        $options['root_node_id']    = NULL;
        $options['entry_id']        = NULL;
        $options['ul_css_id']       = NULL;
        $options['ul_css_class']    = NULL;
        $options['hide_dt_group']   = NULL;
        $options['path']            = NULL;
        $options['url_title']       = NULL;
        
        foreach($trees->result_array() as $tree)
        {
            if($taxonomy_version == 1)
            {
                $this->EE->mpttree->set_opts(array( 
                    'table' => 'exp_taxonomy_tree_'.$tree['id'],
                    'left' => 'lft',
                    'right' => 'rgt',
                    'id' => 'node_id',
                    'title' => 'label'
                ));
                
                $return .= '<h4>'. $tree['label'] .'</h4>';
                $return .= $this->build_list($tree_array, $options);
                
                $tree_array = $this->EE->mpttree->tree2array_v2($options['root'], $options['root_entry_id'], $options['root_node_id']);
            }
            else
            {
                $this->EE->ttree->set_table($tree['id']);
                
                $tree_array = $this->EE->ttree->tree_to_array($options['root'], $options['root_entry_id'], $options['root_node_id']);
                
                $return .= '<h4>'. $tree['label'] .'</h4>';
                $return .= $this->build_list($tree_array, $options);
            }
        }

        return $return;
    }
    
    
    function build_list($array, $options)
    {
        $options['depth']           = 100;
        $options['display_root']    = ($options['display_root']) ? $options['display_root'] : "yes";
                
        $str = '';
        $ul_id = '';
        $ul_class = '';
        
        if(!$array)
            return false;
        
        $str .= '<ul class="structure_pages">';
        
        $closing_ul = '</ul>';
        
        // Added by @nevsie
        $level_count = 0;
        $level_total_count = count($array);
        
        foreach($array as $data)
        {    
            $active_parent = '';
            $level_count ++;
            
            if(($data['level'] == 0) && ($options['display_root'] == "no" && isset($data['children'])))
            {
                $str = $this->build_list($data['children'], $options);
                $closing_ul = '';
            }
            else
            {
                // remove default template group segments
                $template_group = ($data['is_site_default'] == 'y') ? '' : '/'.$data['group_name'];
                $template_name =    '/'.$data['template_name']; 
                $url_title =        '/'.$data['url_title'];
                
                // don't display /index
                if($template_name == '/index')
                {
                    $template_name = '';
                }

                $link_type = 'template';
                $node_url = $template_group.$template_name.$url_title;

                // override template and entry slug with custom url if set
                if($data['custom_url'])
                {
                    $node_url = $data['custom_url'];
                    
                    // if we've got a page_uri set, go fetch the pages uri
                    if($node_url == "[page_uri]")
                    {
                        $link_type = 'page';
                        $site_id = $this->EE->config->item('site_id');
                        $node_url = $this->entry_id_to_page_uri($data['entry_id'], $site_id);
                    }
                    elseif($node_url[0] == "#")
                    {
                        $link_type = 'custom';
                        $node_url = $data['custom_url'];
                    }
                    // if it's a relative url, prepend the site index
                    // otherwise just roll with the user's input
                    else
                    {
                        $link_type = 'custom';
                        // does the custom url start with http://, 
                        // if not we add our site_index as it'll be a relative link
                        // and the nav tag will apply the $active css class to the node
                        $node_url = ((substr(ltrim($node_url), 0, 7) != 'http://') && (substr(ltrim($node_url), 0, 8) != 'https://') ? $this->EE->functions->fetch_site_index() : '') . $node_url;
                    }
                }
                
                // get rid of double slashes, and trailing slash
                $node_url = trim($this->EE->functions->remove_double_slashes($node_url), '/');
                
                $children = '';
                $children_class = '';
                
                if(isset($data['has_children']))
                {
                    $children = 'yes';
                    $children_class = 'has_children';
                }           
                
                $variables = array(
                    'node_id' => $data['node_id'],
                    'node_title' => $data['label'], 
                    'node_url' => $node_url,
                    'node_lft' => $data['lft'],
                    'node_rgt' => $data['rgt'],
                    'node_entry_id' =>  $data['entry_id'],
                    'node_custom_url' => $data['custom_url'],
                    'node_extra' =>  isset($data['extra']) ? $data['extra'] : '',
                    'node_entry_title' => $data['title'],
                    'node_entry_url_title' => $data['url_title'],
                    'node_entry_status' =>  $data['status'],
                    'node_entry_entry_date' => $data['entry_date'],
                    'node_entry_template_name' => $data['template_name'],
                    'node_entry_template_group_name' => $data['group_name'],
                    'node_has_children' => $children,
                    'node_next_child' => $data['lft']+1,
                    'node_level' => $data['level'],
                    'node_level_count' => $level_count,
                    'node_level_total_count' => $level_total_count 
                );
                
                // make sure each node has a unique class
                if($data['entry_id'] == "")
                {
                    $this->EE->load->helper('url');
                    $unique_class = str_replace(".","_", url_title(strtolower($data['label'])));
                }
                else
                {
                    $unique_class = $data['url_title'];
                }
                
                $entry = $this->EE->db->where('entry_id', $variables['node_entry_id'])
                                      ->get('channel_titles');
                
                $level = $data['level'];
                $page_edit_url = BASE .'&C=content_publish&M=entry_form&channel_id='. $entry->row('channel_id') .'&entry_id='. $variables['node_entry_id'];
                
                $str .= '<li id="page-'. $variables['node_entry_id'] . '" class="page-item">';
                $str .= '<div class="item_wrapper round"><a href="'. $page_edit_url .'" data-taxonomy="yes" data-type="'. $link_type .'" data-url="'. $node_url .'" data-id="'. $data['entry_id'] .'" class="round_left round_right">'. $variables['node_title'] .'</a></div>';
                
                if(isset($data['children']) && $data['level'] < $options['depth'])
                {
                    // reset css id and class if going deeper
                    $options['ul_css_id'] = NULL;
                    $options['ul_css_class'] = NULL;
                    
                    // recurse dammit
                    $str .= $this->build_list($data['children'], $options);
                }
                
                $str .= '</li>';
            }
        } 
           
        $str .= $closing_ul;
        
        return $str;
    }
    
    // returns a page_uri from an entry_id
    private function entry_id_to_page_uri($entry_id, $site_id = '1')
    {
        $site_pages = $this->EE->config->item('site_pages');
                                    
        if ($site_pages !== FALSE && isset($site_pages[$site_id]['uris'][$entry_id]))
        {
            $node_url = $site_pages[$site_id]['uris'][$entry_id];
        }
        else
        {
            // not sure what else to do really?
            $node_url = NULL;
        }
        
        return $node_url;
    }
    
    private function _get_styles()
    {
        require 'page_styles.php';
        return preg_replace("/\s+/", " ", $css);
    }
    
    private function debug($str, $die = false)
    {
        echo '<pre>';
        var_dump($str);
        echo '</pre>';
        
        if($die) die('debug terminated');
    }
    
    static function get_instance()
    {
        if(self::$inst == null)
        {
            self::$singleton = 1;
            self::$inst = new Taxonomy_Pages();
        }
    
        return self::$inst;
    }
}
