<?php

/**
 * ExpressionEngine NavEE Helper Class
 *
 * @package     ExpressionEngine
 * @category    Helpers
 * @author      Brian Litzinger
 * @copyright   Copyright 2010 - Brian Litzinger
 * @link        http://boldminded.com/
 */

class NavEE_Pages {
    
    static $inst = null;
    static $singleton = 0;

    private $EE;

    public function __construct()
    {
        if(self::$singleton == 0)
        {
            throw new Exception('This class cannot be instantiated by the new keyword.');
        }
    }
    
    public function get_pages()
    {
        $this->EE =& get_instance();
        $return = '';
        
        $trees = $this->EE->db->where('site_id', $this->EE->config->item('site_id'))
                              ->get('navee_navs');
                              
        if($trees->num_rows() == 0)
            return $return;
        
        foreach($trees->result_array() as $tree)
        {
            $nav = $this->_build_list($tree['navigation_id'], 0);
            
            $return .= '<h4>'. $tree['nav_name'] .'</h4>';
            $return .= $this->_style_list($nav);
        }

        return $return;
    }

    private function _build_list($nav_id, $parent)
    {
        $nav = array();

        $query = $this->EE->db->where("navigation_id", $nav_id)
                              ->where("parent", $parent)
                              ->where("site_id", $this->EE->config->item('site_id'))
                              ->order_by("sort", "asc")
                              ->get('navee');
        
        if($query->num_rows() > 0)
        {
            $count = 0;
            
            foreach($query->result_array() as $node)
            {
                $nav[$count]["navee_id"]    = $node['navee_id'];
                $nav[$count]["parent"]      = $node['parent'];
                $nav[$count]["text"]        = $node['text'];
                $nav[$count]["link"]        = $node['link'];
                $nav[$count]["class"]       = $node['class'];
                $nav[$count]["id"]          = $node['id'];
                $nav[$count]["sort"]        = $node['sort'];
                $nav[$count]["include"]     = $node['include'];
                $nav[$count]["rel"]         = $node['rel'];
                $nav[$count]["name"]        = $node['name'];
                $nav[$count]["target"]      = $node['target'];
                $nav[$count]["regex"]       = $node['regex'];
                $nav[$count]["kids"]        = $this->_build_list($nav_id, $node['navee_id']);
                
                $count++;
            }
        }
        
        return $nav;
    }
    
    private function _style_list($nav, $depth = 0)
    {
        $str = '';

        if(count($nav) == 0)
            return $str;
        
        $str .= $depth == 0 ? '<ul class="structure_pages">' : '<ul>';

        $count = 0;
        $nav_count = count($nav);

        foreach ($nav as $k => $v)
        {
            $count++;
            
            $str .= '<li id="page-'. $v['id'] . '" class="page-item">';
            $str .= '<div class="item_wrapper round"><a href="#" data-type="navee" data-url="'. $v['link'] .'" class="round_left round_right">'. $v['text'] .'</a></div>';

            // If our nav item has kids, let's recurse
            if(count($v["kids"]) > 0)
            {
                $str .= $this->_style_list($v['kids'], $depth + 1);
            }
            
            $str .= '</li>';
        }
        
        $str .= '</ul>';
        
        return $str;
    }
    
    
    private function _get_styles()
    {
        require 'page_styles.php';
        return preg_replace("/\s+/", " ", $css);
    }
    
    private function debug($str, $die = false)
    {
        echo '<pre>';
        var_dump($str);
        echo '</pre>';
        
        if($die) die('debug terminated');
    }
    
    static function get_instance()
    {
        if(self::$inst == null)
        {
            self::$singleton = 1;
            self::$inst = new NavEE_Pages();
        }
    
        return self::$inst;
    }
}
