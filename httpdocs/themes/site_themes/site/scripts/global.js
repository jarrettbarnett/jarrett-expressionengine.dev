var NERD = NERD || {};

$(function(){
    
    NERD.carousel.init();
    NERD.newsfilter.init();
    
});

/* ---------------------------------------------------------------------
Carousel
Author: Angela Norlen

Sets up third party jcarousel
------------------------------------------------------------------------ */
NERD.carousel = {
    init: function() {
        
/*         console.log('hi there'); */// this breaks IE if console has not been init
        if ($('.slider').length != 0) {

            jQuery('.carousel').jcarousel({
                animation: "slow",
                scroll: 1,
                visible: 1,
                size: 3,
                wrap: 'both'
            }); 
        }
    }
}

/* ---------------------------------------------------------------------
News Filter Dropdown
Author: Jarrett Barnett

Super simple AJAX helper... would've preferred JSON, but this works for a quick solution
------------------------------------------------------------------------ */
NERD.newsfilter = {
    init: function()
    {
        var _this = $('#filter');
        
        // only runs where widget exists
        if (_this.length)
        {
            // would normally use on() but site is using older jQuery version
            _this.change(function()
            {
                var _s = $(this).find("option:selected"); // option selector
                var _cid = _s.attr('data-cid'); // cat id
                var _name = _s.attr('data-name'); // cat name
                var _url = '/helpers/entries/news/' + _cid; // helper url
                
                $.ajax({
                    url: _url,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data)
                    {
                        $(".filtertitle").html(_name);
                        $("#news-results").html(data);
                    }
                });
            });
        } 
    }
}