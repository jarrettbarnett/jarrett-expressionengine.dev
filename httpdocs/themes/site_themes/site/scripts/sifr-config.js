var omnes = {
	src: 'assets/flash/omnes.swf', 
	ratios: [9, 1.16, 16, 1.09, 24, 1.06, 37, 1.04, 74, 1.02, 1.01]
};
 
sIFR.activate(omnes);
 
sIFR.replace(omnes, {
	selector: '.bucket h4',
	css: [
	'.sIFR-root { font-size:18px; color: #5b1400; }'
	],
	offsetTop: 6,
	wmode: 'transparent'
});

sIFR.replace(omnes, {
	selector: '.bucket p',
	css: [
	'.sIFR-root { font-size:14px; color: #366009; line-height: 14px}'
	],
	leading: 16,
	wmode: 'transparent'
});


/* Ratios 
Load the sifr-debug.js between sifr.js and sifr-config.js
	<script src="assets/scripts/sifr-debug.js" type="text/javascript"></script>
Comment out the .replace methods above
Uncomment the .debug.ratios method below
*/
// sIFR.debug.ratios({ src: 'assets/flash/omnes.swf', selector: '.bucket p' });