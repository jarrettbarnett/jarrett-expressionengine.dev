<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| ExpressionEngine Config Items
|--------------------------------------------------------------------------
|
| The following items are for use with ExpressionEngine.  The rest of
| the config items are for use with CodeIgniter, some of which are not
| observed by ExpressionEngine, e.g. 'permitted_uri_chars'
|
| Note that we also establish some base upload directories on around line 160
| which saves us time by not having to update the server upload paths and address paths
| between the development and production stages.
|
|   - Jarrett M. Barnett - linkedin.com/in/jarrettbarnett
|
*/
// convenience vars which aide in developing in multiple environments...
$prefix = ($_SERVER['SERVER_PORT'] == '443') ? 'https://' : 'http://';
$address = $prefix . $_SERVER['HTTP_HOST'];
$docroot = $_SERVER['DOCUMENT_ROOT'];

// cms build
$config['app_version']                  =   '253';
$config['is_system_on']                 =   'y';
$config['install_lock']                 =   'y';
$config['allow_extensions']             =   'y';
                                        
// client specific                      
$config['site_label']                   =   '';
$config['site_name']                    =   '';
$config['license_number']               =   '0000-0000-0000-0000';
$config['cookie_prefix']                =   'jb';
                                        
// global paths                         
$config['site_index']                   =   '';
$config['site_url']                     =   $address.'/';
$config['cp_url']                       =   "$address/manage.php";
$config['doc_url']                      =   'http:///www.jarrettbarnett.com/';
$config['base_url']                     =   '/';

// optimizations
$config['send_headers']                 =   'y';
$config['gzip_output']                  =   'y';
$config['compress_output']              =   FALSE;
$config['dynamic_tracking_disabling']   =   'y';
$config['enable_online_user_tracking']  =   'n';
$config['enable_hit_tracking'] = 'n';
$config['enable_entry_view_tracking']   =   'n';
$config['new_version_check']            =   'n';
$config['enable_db_caching']            =   'n'; // EllisLabs has discontinued this in the interim as built-in MySQL caching outperforms EE db caching

// security
$config['encryption_key']               =   '11F3B2600A1EA8448BE10F4A037887EA71D2FC29E6D81611B1B8F1F30CD25AD6';
$config['global_xss_filtering']         =   TRUE;
$config['csrf_protection']              =   FALSE;
$config['xss_clean_uploads']            =   FALSE; // only admins will be uploading AND we've had issues with file uploads w/ clients using Safari/Chrome

// settings
$config['cp_theme']                     =   'jarrettbarnett';
$config['member_theme']                 =   'default';
$config['webmaster_email']              =   'developer@jarrettbarnett.com';

// usually unchanged settings
$config['proxy_ips']                    =   '';
$config['rewrite_short_tags']           =   FALSE;
$config['time_reference']               =   'local';
$config['enable_query_strings']         =   FALSE;
$config['directory_trigger']            =   'D';
$config['controller_trigger']           =   'C';
$config['function_trigger']             =   'M';
$config['permitted_uri_chars']          =   'a-z 0-9~%.:_\\-';
$config['url_suffix']                   =   '';
$config['uri_protocol']                 =   'AUTO';
$config['language']                     =   'english';
$config['charset']                      =   'UTF-8';
$config['enable_hooks']                 =   FALSE;
$config['subclass_prefix']              =   'EE_';

// other path settings
$config['theme_folder_url']             =   "$address/themes/"; // URL to your themes directory
$config['theme_folder_path']            =   "$docroot/themes/"; // Path to your themes directory
$config['avatar_path']                  =   "$docroot/public/avatars/";
$config['avatar_url']                   =   "$address/public/avatars/";
$config['enable_avatars']               =   'y';
$config['allow_avatar_uploads']         =   'y';
$config['avatar_max_width']             =   '500';
$config['avatar_max_height']            =   '500';
$config['avatar_max_kb']                =   '350';
$config['captcha_path']                 =   "$docroot/public/captchas/";
$config['captcha_url']                  =   "$address/public/captchas/";
$config['photo_path']                   =   "$docroot/public/member_photos/";
$config['photo_url']                    =   "$address/public/member_photos/";
$config['sig_img_path']                 =   "$docroot/public/signature_attachments/";
$config['sig_img_url']                  =   "$address/public/signature_attachments/";
$config['prv_msg_upload_path']          =   "$docroot/public/pm_attachments/";
$config['prv_msg_upload_url']           =   "$address/public/pm_attachments/";
$config['prv_msg_auto_links']           =   'n';
$config['emoticon_path']                =   "$docroot/public/smileys/";
$config['emoticon_url']                 =   "$address/public/smileys/";
$config['cp_session_ttl']               =   3600; // sets session timeout length in seconds

/* logging */
$config['log_threshold']                =   1; // 0 = disable, 1 = error, 2 = debug, 3 = info, 4 = all
$config['log_path']                     =   FCPATH.'cache/logs/';
$config['log_date_format']              =   'Y-m-d H:i:s';
$config['cache_path']                   =   FCPATH.'cache/sql/';

/* templates */
$config['tmpl_file_basepath']           =   "$docroot/views/templates/";
                                            
/* snippet sync addon */                    
$config['snippet_file_basepath']        =   "$docroot/views/snippets/"; // filepath
$config['snippets_sync_prefix']         =   'snippet:'; // prefix
$config['snippets_sync_suffix']         =   '_'; // snippet folder snippet delimiter
$config['msm_shared_folder']            =   'shared'; // multisite shared

/* stash addon*/
$config['stash_file_basepath']          =   'views/stash/';
$config['stash_file_sync']              =   TRUE; // set to FALSE for production
$config['stash_cookie']                 =   'stashid'; // the stash cookie name
$config['stash_cookie_expire']          =   0; // seconds - 0 means expire at end of session
$config['stash_default_scope']          =   'user'; // default variable scope if not specified
$config['stash_limit_bots']             =   TRUE; // stop database writes by bots to reduce load on busy sites
$config['stash_bots']                   =   array('bot', 'crawl', 'spider', 'archive', 'search', 'java', 'yahoo', 'teoma');

/** 
*   Quick debugging via query string

*   Notes: Allows use of ?jb_debug=value to perform quick debugging where the value being one of the cases below:
*       911 --> disable extensions and enable debugging messages (for you only); useful if '1' isnt providing enough clarity
*       1   --> enable debugging messages (for you only); can also supply 'y' or 'yes' as an alternative
*       0   --> the fallback where debugging is disabled and extensions are enabled.
*/
if( isset($_GET['jb_debug']) ) :
    switch($_GET['jb_debug']) :
        case '911':
            $config['allow_extensions'] = 'n';
        case '1':
        case 'y':
        case 'yes':
            $config['debug'] = '1';
            $config['show_profiler'] = 'y';
            $config['template_debugging'] = 'y';
            break;
        
        default:
            $config['allow_extensions'] = 'y';
            $config['debug'] = '0';
            break;
endswitch;endif;

/*
 *  Uploads Path Overrides
 *
 *  one upload path declaration to rule all environments
 *  with the help of PHP super globals...
 */
// upload preferences override
if ( !empty($docroot) )
{
    // Note: FCPATH is defined in index.php and manage.php -- where the app and view are init.
    $uploadpath = $address.'/public/uploads';

    $config['upload_preferences'] = array(
        1 => array( # database id number
            'name'        => 'Default', # display name in control panel
            'server_path' => FCPATH.$uploadpath.'/default/', # server path
            'url'         => "$uploadpath/default/" # url path
        ),
        2 => array(
            'name'        => 'Pages',
            'server_path' => FCPATH.$uploadpath.'/pages/',
            'url'         => "$uploadpath/pages/"
        ),
        3 => array(
            'name'        => 'News > Preview',
            'server_path' => FCPATH.$uploadpath.'/news/preview/',
            'url'         => "$uploadpath/news/preview/"
        ),
        4 => array(
            'name'        => 'Store',
            'server_path' => FCPATH.$uploadpath.'/store/',
            'url'         => "$uploadpath/store/"
        ),
        5 => array(
            'name'        => 'Product Images',
            'server_path' => FCPATH.$uploadpath.'/store/products/',
            'url'         => "$uploadpath/store/products/"
        ),
        6 => array(
            'name'        => 'Module > Get Involved',
            'server_path' => FCPATH.$uploadpath.'/getinvolved/',
            'url'         => "$uploadpath/getinvolved/"
        ),
        7 => array(
            'name'        => 'Module > Carousel',
            'server_path' => FCPATH.$uploadpath.'/carousel/',
            'url'         => "$uploadpath/carousel/"
        ),
        8 => array(
            'name'        => 'News > Feature',
            'server_path' => FCPATH.$uploadpath.'/news/feature/',
            'url'         => "$uploadpath/news/feature/"
        ),
        9 => array(
            'name'        => 'Related Content > Callout',
            'server_path' => FCPATH.$uploadpath.'/related/callout/',
            'url'         => "$uploadpath/related/callout/"
        ),
        10 => array(
            'name'        => 'Related Content > Preview',
            'server_path' => FCPATH.$uploadpath.'/related/preview/',
            'url'         => "$uploadpath/related/preview/"
        ),

    );
}
// END EE config items