<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *      Jarrett Barnett | developer@jarrettbarnett.com | linkedin.com/in/jarrettbarnett
 */

$active_group = $_SERVER['HTTP_HOST'];
$active_record = TRUE;

// GLOBAL DB SETTINGS
$db[$active_group]['hostname'] = 'localhost';
$db[$active_group]['dbdriver'] = "mysql"; //  The database type. ie: mysql, postgres, odbc, etc. Must be specified in lower case.
$db[$active_group]['pconnect'] = FALSE; // Whether to use a persistent connection.
$db[$active_group]['swap_pre'] = "exp_";
$db[$active_group]['db_debug'] = FALSE; // Whether database errors should be displayed.
$db[$active_group]['cache_on'] = FALSE; // Whether database query caching is enabled, see also Database Caching Class: http://codeigniter.com/user_guide/database/caching.html
$db[$active_group]['autoinit'] = FALSE; // Whether or not to automatically connect to the database when the library loads. If set to false, the connection will take place prior to executing the first query.
$db[$active_group]['char_set'] = "utf8";
$db[$active_group]['dbcollat'] = "utf8_general_ci";
$db[$active_group]['cachedir'] = FCPATH . "cache/mysql/";
/* this is in the event that the current http_host doesnt match any below;
 * its just insurance that we dont get an accidental connection to anyone else's database */
$db[$active_group]['username'] = 'disabled';
$db[$active_group]['password'] = 'disabled';
$db[$active_group]['database'] = 'disabled';
$db[$active_group]['dbprefix'] = 'ee2_'; // allows you to not have to specify the dbprefix below. so long as you use ee2_

/**
 *  PRODUCTION SETUP
 */




/**
 *  STAGING SETUP
 */
$db['schmo.pro']['username'] = 'db142189';
$db['schmo.pro']['password'] = 'C53F63821CA6F980321F86F84BECF4E9122FA295A2A7C6CA026BF53903C8924C';
$db['schmo.pro']['database'] = 'db142189_nerdery';
$db['schmo.pro']['hostname'] = 'internal-db.s142189.gridserver.com';

/**
 *      LOCAL SETUP
 */
// - jarrett's local db
$db['nerdery.loc']['username'] = "root";
$db['nerdery.loc']['password'] = "devaccess";
$db['nerdery.loc']['database'] = "nerdery.loc"; // The name of the database you want to connect to.



/* End of file database.php */
/* Location: ./system/expressionengine/config/database.php */