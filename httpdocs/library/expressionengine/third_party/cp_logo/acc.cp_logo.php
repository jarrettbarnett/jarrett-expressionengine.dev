<?php
/**
 * 
 * @package		CP Logo
 * @version			1.0.0
 * @author			exp design, llc
 * @copyright 		n/a
 * @license 		Freeware, please provide attribution
 * @link				http://expdesign.net
 * @see				n/a
 
 // Thanks to Alex Kendrick and Aidann Bowley for starting points and inspiration
 */

class cp_logo_acc 
{
	var $name	 		= 'CP Logo';
	var $id	 			= 'cp_logo';
	var $version	 	= '1.1';
	var $description	= 'Add a logo to the control panel sidebar. Your clients will wet themselves in excitement.';
	var $sections	 	= array();

	/**
	* Set Sections
	*
	* Set content for the accessory
	*
	* @access	public
	* @return	void
	*/
	function set_sections()
	{
		$EE =& get_instance();

		$theme_folder_url = $EE->config->item('theme_folder_url');

		if (substr($theme_folder_url, -1) != '/')
		$theme_folder_url .= '/';

		$theme_folder_url .= "third_party/cp_logo/";

		// Call your own stylesheet if you want, but we don't need it.
		//$EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.$theme_folder_url.'styles/screen.css" />');
		
		// jQuery adds a div before #activeUser in the sidebar and displays a logo of your choice. 
		//  Lots of crazy escaping of quotes so we don't get the dreaded jQuery errors
		// Also remove this accessory's tab from the footer -- there is nothing to configure
		$this->sections[] = '	<script type="text/javascript" charset="utf-8">	$("#sidebarContent #notePad").before("<div id=\'cp_logo\' style=\'margin-top:10px\'><img src=\''.$theme_folder_url.'images/cp_logo.png\' />");$("#accessoryTabs a.cp_logo").parent().remove();</script>';
		}
}