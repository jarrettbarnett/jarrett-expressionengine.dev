<?php

/**
 *  MX Mobile Detect Class for ExpressionEngine2
 *
 * @package  ExpressionEngine
 * @subpackage Plugins
 * @category Plugins
 * @author    Max Lazar <max@eec.ms>
 * @Commercial - please see LICENSE file included with this distribution
 */




$plugin_info = array(
	'pi_name' => 'MX Mobile Detect',
	'pi_version' => '2.8.4',
	'pi_author' => 'Max Lazar',
	'pi_author_url' => 'http:/eec.ms/',
	'pi_description' => 'Detect Mobile Device Requests',
	'pi_usage' => mobile_detect::usage()
);


class Mobile_detect
{
	var $return_data = "";

	function Mobile_detect()
	{

		$this->EE =& get_instance();

		$this->EE->load->helper('url');

		$uri_string = $this->EE->uri->uri_string();

		$location       = (!$this->EE->TMPL->fetch_param('location')) ? false : str_replace("{uri}", $this->EE->uri->uri_string(), $this->EE->TMPL->fetch_param('location'));

		$redirect       = (!$this->EE->TMPL->fetch_param('redirect')) ? ('mobile') : $this->EE->TMPL->fetch_param('redirect');

		$client_request = (!$this->EE->TMPL->fetch_param('client')) ? false : $this->EE->TMPL->fetch_param('client');

		$cookie_name = (!$this->EE->TMPL->fetch_param('cookie_name')) ? 'mobile_redirect' : $this->EE->TMPL->fetch_param('cookie_name');

		$cookie_value = (!$this->EE->TMPL->fetch_param('cookie_value')) ? true : $this->EE->TMPL->fetch_param('cookie_value');
		
		$custom_agents = (!$this->EE->TMPL->fetch_param('custom_agents')) ? false : explode('|', strtolower($this->EE->TMPL->fetch_param('custom_agents')));

		$enable = (!$this->EE->TMPL->fetch_param('enable')) ? false : (($this->EE->TMPL->fetch_param('enable') == 'yes') ? 'on' : 'off');

		if ($enable) {

			if ($enable == 'on') {
				$this->EE->functions->set_cookie($cookie_name, '', '');
			} else {
				$this->EE->functions->set_cookie($cookie_name, 'off', 86500);
			}

			$this->EE->functions->redirect(str_replace('&#47;', '/', $location));

		} else {

			$agent = $_SERVER['HTTP_USER_AGENT'];

			if ($this->EE->input->cookie($cookie_name) != $cookie_value) {

				$client  = new Client();
				$tagdata         = $this->EE->TMPL->tagdata;
				$conds['mobile'] = ($client_request) ? ((strpos($agent, $client_request) != 0) ? true : false) : $client->isMobileClient($agent, $custom_agents);
				$conds['not_mobile'] = ($conds['mobile'] == true) ? false : true;
				$conds['device']  = ($conds['mobile']) ? $client->device : '';
				//$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
			
				if ($this->EE->TMPL->fetch_param($conds['device']) == 'no') {return;}
								
				$location = (!$this->EE->TMPL->fetch_param($conds['device'])) ? $location : str_replace("{uri}", $this->EE->uri->uri_string(), $this->EE->TMPL->fetch_param($conds['device']));

				//if ($location == $url) { return;};
				
				if ($tagdata == '') {

					if ($location && ($conds['mobile'] == true || ($conds['mobile'] == false && $redirect == "not_mobile"))) {
						
						$this->EE->functions->redirect(str_replace('&#47;', '/', $location));
						return;
					}
				}
				else {
					$tagdata = $this->EE->functions->prep_conditionals($tagdata, $conds);
					$tagdata = $this->EE->TMPL->parse_variables_row($tagdata,  $conds['mobile']);
					return $this->return_data = $tagdata;
				}
			}

		}

		return;
	}

	// ----------------------------------------
	//  Plugin Usage
	// ----------------------------------------

	// This function describes how the plugin is used.
	//  Make sure and use output buffering

	function usage()
	{
		ob_start();
?>

Place the following tag in your templates (it would be better if your place it close to the header);

{exp:mobile_detect location="http://m.site.com/"}

{exp:mobile_detect location="http://site.com/" redirect="not_mobile" client=""}

"location" - its your url for mobile/not mobile devices
"client"(optional) - the string which you want to find in HTTP_USER_AGENT header. It could be the OS name/version, Browser name etc..
For example you can redirect all users with Internet Explorer 6.0 to specific page.
NOTE: this option is overwriting the mobile detect functionality.

Or you can use that way:

{exp:mobile_detect}
	{if mobile}
		Is mobile device
	{/if}
	{if not_mobile}
		Is not mobile device
	{/if}
{/exp:mobile_detect}


you can also specify redirection/action per device:

{exp:mobile_detect location="http://site.com/" ipad="http://site.com/ipad/" android="http://site.com/android/"}

-or-

{exp:mobile_detect}
	{if device == "ipad"}
		Hello Steve
	{/if}
	{if device == "android" }
		Hello Bender
	{/if}
	{if not_mobile}
		Hello ...
	{/if}
{/exp:mobile_detect}

You can also to turned off plugin action with this:
{exp:mobile_detect enable="off"  location="http://site.com/"}

List of devices which you can use for conditionals or redirection

android
acer
asus
alcatel
blackberry
htc
hp
lg
motorola
nokia
palm
samsung
sonyericsson
zte
mobile
iphone
ipod
ipad
mini
playstation
docomo
benq
vodafone
sharp
kindle
nexus
windows_phone
midp
240x320
netfront
nokia
panasonic
portalmmm
sie-
symbian
mda
mot-
opera_mini
philips
pocket_pc
sagem
sda
sgh-
xda
iphone

more information - http://www.eec.ms/add-ons/mobile-device-detect/tags

<?php
		$buffer = ob_get_contents();

		ob_end_clean();

		return $buffer;
	}
	/* END */

}


class Client
{

	public $agent;
	public $device;

	/**
	 * Available Mobile Clients
	 *  http://www.zytrax.com/tech/web/mobile_ids.html
	 * @var array
	 */
	
	private $_mobileClients = array("android", "acer", "asus", "alcatel",  "blackberry", "htc", "hp ", "lg", "motorola", "nokia", "palm", "samsung", "sonyericsson", "zte","iphone", "ipod", "ipad", "mini", "playstation", "docomo", "benq", "vodafone", "sharp", "kindle", "nexus", "windows phone","midp", "240x320", "netfront", "nokia", "panasonic", "portalmmm", "sie-",  "symbian", "mda", "mot-", "opera mini", "philips", "pocket pc", "sagem", "sda", "sgh-", "xda", "iphone", "mobile", "ppc");
         
	public function isMobileClient($userAgent,  $custom_agents = false)
	{
		$userAgent = strtolower($userAgent);
		
		if ($custom_agents) {
			$this->_mobileClients = array_merge((array)$custom_agents, $this->_mobileClients);
		}		
	
		foreach ($this->_mobileClients as $mobileClient) {
			if (strstr($userAgent, $mobileClient)) {
	
				$this->device = trim(str_replace(' ', '_', $mobileClient));
				return  true;
			}
		}

		return false;
	}

}

/* End of file pi.mobile_detect.php */
/* Location: ./system/expressionengine/third_party/mobile_detect/pi.mobile_detect.php */