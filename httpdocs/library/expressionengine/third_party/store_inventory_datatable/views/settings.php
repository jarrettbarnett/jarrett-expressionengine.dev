<?=form_open('C=addons_extensions'.AMP.'M=save_extension_settings'.AMP.'file=store_inventory_datatable');?>
<?php

$this->table->set_template($cp_pad_table_template);
$this->table->set_heading(
    array('data' => lang('colomn')),
    lang('show_hide')
);
		$cols = array('expand', 'store_products.entry_id', 'sku', 'title', 'total_stock', 'regular_price', 'sale_price', 'sale_price_enabled', 'options', 'stock_table');
foreach ($current as $key => $val)
{
	$continue = false;
	switch ($key) {
		case 'expand':
		case 'stock_table':
		case 'regular_price':
			$continue = true;
			break;
	}
	if ($continue) {
		echo form_hidden($key,"1");
	} else {
		$radio = lang('Yes'). NBS . form_radio($key, 1,  $val). NBS .NBS .NBS .
				 lang('No') . NBS . form_radio($key, 0, !$val);
    	$this->table->add_row(lang($key, $key), $radio);
	}
}

echo $this->table->generate();

?>

<p><?=form_submit('submit', lang('submit'), 'class="submit"')?></p>
<?php $this->table->clear()?>


<?=form_close()?>