<?php
/*
 * Author: Percipio Ltd
 * http://www.percipio.me
 * 
 * package		Store - Custom Inventory Columns
 * version		Version 1.0.0
 * copyright		Copyright (c) 2011 Percipio.Me Ltd
 * Last Update	22 November 2011
 * 
 * License: Store - Custom Inventory Columns by Alex Khavdiy (Percipio.Me Ltd) is licensed under a Creative Commons Attribution-NoDerivs 3.0 Unported License (http://creativecommons.org/licenses/by-nd/3.0/). Permissions
 * beyond the scope of this license may be available by contacting us at addons@percipio.me.
 * 
 * Store - Custom Inventory Columns is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and
 * noninfringement.
 * 
 * You assume all risk associated with the installation and use of Store - Custom Inventory Columns
*/
class  Store_inventory_datatable_ext {
	var $name       	= '';
	var $version        = '1.0.1';
	var $description    = '';
	var $settings_exist = 'y';
	var $docs_url       = 'http://addons.percipio.me';
	var $settings       = array();
	var $not_sorting    = array();
	var $cols        	= array();
	var $cols_f       	= array(); //flipped

	/**
     * Constructor
     *
     * @param   mixed   Settings array or empty string if none exist.
     */
	function __construct($settings='')
	{
		$this->EE =& get_instance();
		$this->EE->lang->loadfile('store_inventory_datatable');

		$this->name 	= lang('sccname');
		$this->description = lang('sccdescription');

		$cols = array('expand', 'entry_id', 'sku', 'entry_date', 'title', 'total_stock', 'regular_price', 'sale_price', 'sale_price_enabled', 'options', 'stock_table');

		//
		$this->cols_f = array_flip($cols);
		
		//
		foreach ($cols as $col) {
			$this->cols[$col] = 1;
		}

		$this->settings = $settings;
	}

	/**
	 * Activate Extension
	 *
	 * This function enters the extension into the exp_extensions table
	 *
	 * @see http://codeigniter.com/user_guide/database/index.html for
	 * more information on the db class.
	 *
	 * @return void
	 */
	function activate_extension()
	{

		$data = array(
		'class'     => __CLASS__,
		'method'    => 'modify_table',
		'hook'      => 'store_inventory_datatable',
		'settings'  => serialize($this->cols),
		'priority'  => 10,
		'version'   => $this->version,
		'enabled'   => 'y'
		);

		$this->EE->db->insert('extensions', $data);
	}

	/**
	 * Update Extension
	 *
	 * This function performs any necessary db updates when the extension
	 * page is visited
	 *
	 * @return  mixed   void on update / false if none
	 */
	function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}

		if ($current < '1.0')
		{
			// Update to version 1.0
		}

		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->from('extensions');

		$result = $this->EE->db->get()->row_array();
		$settings = unserialize($result['settings']);
		$new_settings = array();
		foreach ($this->cols as $col => $val) {
			if (!isset($settings[$col])) {
				$new_settings[$col] = 1;
			} elseif ($settings[$col]) {
				$new_settings[$col] = 1;
			} else {
				$new_settings[$col] = 0;
			}
		}

		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update(
		'extensions',
		array('settings'  => serialize($new_settings), 'version' => $this->version)
		);
	}

	function settings_form($current)
	{
		$vars['current'] = $current;
		return $this->EE->load->view('settings', $vars, TRUE);
	}

	/**
	 * Save Settings
	 *
	 * This function provides a little extra processing and validation
	 * than the generic settings form.
	 *
	 * @return void
	 */
	function save_settings()
	{
		if (empty($_POST))
		{
			show_error($this->EE->lang->line('unauthorized_access'));
		}

		unset($_POST['submit']);

		foreach ($this->cols as $col => $val) {
			if ($this->EE->input->post($col)) {
				$settings[$col] = 1;
			} else {
				$settings[$col] = 0;
			}
		}

		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update('extensions', array('settings' => serialize($settings)));

		$this->EE->session->set_flashdata(
		'message_success',
		$this->EE->lang->line('preferences_updated')
		);
	}

	/**
	 * Disable Extension
	 *
	 * This method removes information from the exp_extensions table
	 *
	 * @return void
	 */
	function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}

	function _hide_colomn_js($col){

		$js =<<<JS
		$('body').append('<script>$(document).ready(function(){ fnShowHide2($col);}) </scr' + 'ipt>');
JS;
		return $js;
	}
	
	function _insert_colomn_after_js($cNum, $val=""){
		$js =<<<JS
		$("#store_inventory_table tr:first th:eq($cNum)").after("<th >$val</th>");
JS;
		return $js;
	}	
	
	function _add_colomn_js(){
		$js = "";

		$c = 0;
		foreach ($this->settings as $col => $val) {
			if(!$val) {
				$js .= "\$('#store_inventory_table tr:first th:eq(" . $c .  ")').remove();";
				$c--;
			}
			$c++;
		}
		return $js;
	}
	
	function _common_js(){
		$js =<<<JS
		$('body').append('<script>function fnShowHide2( iCol ){var oTable = $("#store_inventory_table").dataTable();var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;oTable.fnSetColumnVis( iCol, bVis ? false : true );}</scr' + 'ipt>');
JS;
		return $js;
	}

	function modify_table($response){
		//return $response;
		$this->EE->load->model('store_inventory_datatable_model');

		// fixing view path
		$installed = $this->EE->addons->get_installed();
		$this->EE->load->add_package_path($installed['store']['path'], true);

		// setting default page size value
		eval("\$DATATABLES_PAGE_SIZE = " . $installed['store']['class'] . "::DATATABLES_PAGE_SIZE ;");

		// 
		$this->EE->javascript->output($this->_common_js());
		
		// init new colomns
		$this->EE->javascript->output($this->_insert_colomn_after_js($this->cols_f['sku']-1, lang('sku')));
		$this->EE->javascript->output($this->_insert_colomn_after_js($this->cols_f['entry_date']-1, lang('entry_date')));
		
		// replace table header in the page
		$this->EE->javascript->output($this->_add_colomn_js());

		// inventory_datatable code with changes
		$col_map = array();
		$response['non_sortable_columns'] = array();
		$response['clickable_columns'] = array();
		$response['default_sort'] = array(1, 'asc');
		
		foreach ($this->settings as $key => $val) {
			if ($val) {
	
				$cnt_col_map = count($col_map);
				if ($cnt_col_map < 4 ) {
					$response['clickable_columns'][] = count($col_map);
				}
				
				if ($cnt_col_map == 7 ) {
					$col_map[] = "";
					$col_map[] = "";
					$this->EE->javascript->output($this->_insert_colomn_after_js(6));
					$this->EE->javascript->output($this->_insert_colomn_after_js(6));					
					$this->EE->javascript->output($this->_hide_colomn_js(7));
					$this->EE->javascript->output($this->_hide_colomn_js(8));
				}	
				
				switch ($key) {
					case 'total_stock':
						$col_map[] = $key;
						break;
					case 'sku':
						$col_map[] = $key;
						break;
					case 'entry_date':
						$col_map[] = $key;
						break;
					case 'entry_id':
						$col_map[] = 'store_products.entry_id';
						break;
					case 'title':
						$response['default_sort'] = array(count($col_map), 'asc');
						$col_map[] = $key;
						break;
					case 'expand':
						$response['non_sortable_columns'][] = count($col_map);
						$col_map[] = $key;
						break;
					case 'options':
						$response['non_sortable_columns'][] = count($col_map);
						$col_map[] = $key;
						break;
					case 'stock_table':
						$response['non_sortable_columns'][] = count($col_map);
						$col_map[] = $key;
						break;

					default:
						$col_map[] = "p.$key";
						break;
				}
				

			}
			
		}

		$keywords = $this->EE->input->get_post('keywords');

		$filters = array(
		'perpage' 		=> $this->EE->input->get_post('perpage') ? (int)$this->EE->input->get_post('perpage') : $DATATABLES_PAGE_SIZE,
		'category_id'	=> (int)$this->EE->input->get_post('product_categories'),
		'keywords'		=> $keywords,
		'exact_match'	=> $this->EE->input->get_post('exact_match')
		);

		$filters['limit'] = $filters['perpage'];
		$filters['offset'] = (int)$this->EE->input->get_post('iDisplayStart');

		/* Ordering */
		if (($order_by = $this->EE->input->get('iSortCol_0')) !== FALSE or ($order_by = $this->EE->input->get('imySortCol_0')) !== FALSE)
		{
			if (isset($col_map[$order_by]))
			{
				$filters['order_by'] = $col_map[$order_by];
				$filters['sort'] = $this->EE->input->get('sSortDir_0');
			}
		}

		$query = $this->EE->store_inventory_datatable_model->find_all($filters);
		$total_filtered = $this->EE->store_inventory_datatable_model->find_all(array_merge($filters, array('count_all_results' => TRUE)));
		
		$total = $this->EE->store_products_model->total_products();

		// restore keyword
		$filters['keywords'] = $keywords;
		
		$response ['aaData'] = array();
		$response ['sEcho'] = (int)$this->EE->input->get_post('sEcho');
		$response ['iTotalRecords'] = $total;
		$response ['iTotalDisplayRecords'] = $total_filtered;
		
		$c = 0; foreach ($query as $row)
		{

			$row['regular_price'] = store_cp_format_currency($row['regular_price_val']);
			$row['sale_price'] = store_cp_format_currency($row['sale_price_val']);
			$settings = $this->settings;
			$response['aaData'][$c] = array();

			while (list($key, $val) = each($settings)) {
				switch ($key) {
					case 'expand':
						if($val) {
							$response['aaData'][$c][] = '<a href="#"><img src="'.$this->EE->config->item('theme_folder_url').'cp_global_images/expand.gif"></a>';
						}						
						break;
						
					case 'entry_id':
						if($val) {
							$response['aaData'][$c][] = $row['entry_id'];
						}
						break;
						
					case 'sku':
						if($val) {
							$response['aaData'][$c][] = $row['sku'];
						}
						break;
						
					case 'entry_date':
						if($val) {
							$response['aaData'][$c][] = date("m/d/y h:i a", $row['entry_date']);	
						}
						break;
						
					case 'title':
						if($val) {
							$response['aaData'][$c][] = $row['title'].NBS.NBS.'('.(substr_count($row['stock_table'], '<tr>')-1).' variations)';
						}
						break;
						
					case 'total_stock':
						if($val) {
							$response['aaData'][$c][] = $row['total_stock'];
						}
						break;
						
					case 'regular_price':
						if($val) {
							$response['aaData'][$c][] = form_input('store_product_field['.$row['entry_id'].'][regular_price]', $row['regular_price'], 'style="text-align: right"');
						}
						break;
						
					case 'sale_price':
						if($val) {
							$response['aaData'][$c][] = form_input('store_product_field['.$row['entry_id'].'][sale_price]', $row['sale_price'], 'style="text-align: right"');
						}
						break;
						
					case 'sale_price_enabled':
						if($val) {
							$response['aaData'][$c][] = form_checkbox('store_product_field['.$row['entry_id'].'][sale_price_enabled]', 'y', $row['sale_price_enabled'] == 'y');
						}
						break;
						
					case 'options':
						if($val) {
							$response['aaData'][$c][] = '<strong><a href="'.$row['channel_edit_link'].'">'.lang('edit_entry').'</a></strong>';
						}
						break;
						
					case 'stock_table':
						if($val) {
							$response['aaData'][$c][] = $row['stock_table'] ;
						}
						break;
						
					default:
						break;
				}
			}
			
			// we insert empty fields (7,8) so will not generate sorting error in parent extension
			if (count($response['aaData'][$c]) >= 8) {
				$p1 = array_slice($response['aaData'][$c],0,7);
				$p2 = array_slice($response['aaData'][$c],7);
				$response['aaData'][$c] = array_merge($p1, array("", ""));
				$response['aaData'][$c] = array_merge($response['aaData'][$c], $p2 );
			}

//
			$c++;
		}
		$c--;


		$response = array_merge($response, $filters);

		return $response;
	}
}
/* End of file ext.store_inventory_datatable.php */