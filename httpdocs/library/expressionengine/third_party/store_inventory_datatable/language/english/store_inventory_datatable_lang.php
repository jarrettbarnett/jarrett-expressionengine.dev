<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(
	// module details
	"expand" => "Expand",
	"entry_id" => "#",
	"regular_price" => lang("price"),
	"colomn" => lang("Colomn"),
	"show_hide" => lang("Show / Hide"),
	"stock_table" => "Stock",
	"entry_date" => "Entry Date",
	
	//
	"sccname" => "Store - Custom Columns",
	"sccdescription" => "Custom columns for store invantory table",
);

/* End of file store_lang.php */