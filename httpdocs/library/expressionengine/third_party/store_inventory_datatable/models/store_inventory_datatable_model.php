<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Exp:Store module for ExpressionEngine 2.x by Crescendo (support@crescendo.net.nz)
 * Copyright (c) 2010 Crescendo Multimedia Ltd
 * All rights reserved.
 */

class Store_inventory_datatable_model extends Store_products_model {

	public function __construct()
	{
		parent::__construct();
	}

	// overwrite parent method
	public function find_all($options)
	{
		$options = array_merge(array(
			'count_all_results' => FALSE,
			'fetch_modifiers' => TRUE,
			'fetch_stock' => TRUE,
			'on_sale' => NULL,
			'order_by' => NULL,
			'sort' => NULL,
			'limit' => 50,
			'offset' => 0,
			'category_id' => NULL,
			'keywords' => FALSE,
			'exact_match' => FALSE,
		), $options);

		$this->db->select('*, sum(s.stock_level) as total_stock');
		$this->db->from('store_products p');
		$this->db->join('channel_titles t', 'p.entry_id = t.entry_id');
		$this->db->where('t.site_id', $this->config->item('site_id'));

		if ( ! empty($options['category_id']))
		{
			$this->db->join('category_posts', 'category_posts.entry_id = p.entry_id');
			$this->db->where('cat_id', $options['category_id']);
		}

		if ( ! empty($options['keywords']))
		{
			if ($options['exact_match'])
			{
				$this->db->where('p.entry_id', $options['keywords']);
				$this->db->or_where('t.title', $options['keywords']);
				$this->db->or_where('sku'    , $options['keywords']);
			}
			else
			{
				$this->db->like('p.entry_id', $options['keywords']);
				$this->db->or_like('t.title', $options['keywords']);
				$this->db->or_like('sku'    , $options['keywords']);
			}
		}

		$this->db->join('store_stock s', 's.entry_id = p.entry_id');
		
		
		if ($options['count_all_results'])
		{
			return $this->db->count_all_results();
		}

		if ( ! empty($options['order_by']))
		{
			$order_by = str_replace('store_products.', 'p.', $options['order_by']);
			$sort = strtoupper($options['sort']) == 'ASC' ? 'ASC' : 'DESC';
			$this->db->order_by($order_by, $sort);
		}
		else
		{
			$this->db->order_by('t.title', 'ASC');
		}

		$this->db->group_by('p.entry_id');
		
		$this->db->limit($options['limit'], $options['offset']);
		$products = $this->db->get()->result_array();

		foreach ($products as $key => $product)
		{
			$product = $this->_process_product($product, $options['fetch_modifiers'], $options['fetch_stock']);
			$product['stock_table'] = $this->generate_stock_matrix_html($product, 'store_product_field['.$product['entry_id'].']', FALSE);
			$product['channel_edit_link'] = BASE.AMP.'C=content_publish'.AMP.'M=entry_form'.AMP.'channel_id='.$product['channel_id'].AMP.'entry_id='.$product['entry_id'];
			$products[$key] = $product;
		}

		return $products;
	}
	
	private function _process_product($product, $fetch_modifiers = FALSE, $fetch_stock = FALSE)
	{
		$product['regular_price_val'] = (float)$product['regular_price'];
		$product['regular_price'] = store_format_currency($product['regular_price_val']);
		$product['sale_price_val'] = (float)$product['sale_price'];
		$product['sale_price'] = store_format_currency($product['sale_price_val']);
		$product['handling_val'] = (float)$product['handling'];
		$product['handling'] = store_format_currency($product['handling_val']);
		$product['price_val'] = $product['regular_price_val'];
		$product['price'] = $product['regular_price'];
		$product['free_shipping'] = $product['free_shipping'] == 'y';
		$product['tax_exempt'] = $product['tax_exempt'] == 'y';
		$product['on_sale'] = FALSE;

		// determine if the product is on sale
		if (($product['sale_price_enabled'] == 'y') AND
			(empty($product['sale_start_date']) OR $product['sale_start_date'] <= $this->localize->now) AND
			(empty($product['sale_end_date']) OR $product['sale_end_date'] >= $this->localize->now))
		{
			$product['price_val'] = $product['sale_price_val'];
			$product['price'] = $product['sale_price'];
			$product['on_sale'] = TRUE;
		}

		$product['sale_start_date_val'] = $product['sale_start_date'];
		$product['sale_start_date'] = empty($product['sale_start_date_val']) ? '' : $this->localize->set_human_time($product['sale_start_date_val']);
		$product['sale_end_date_val'] = $product['sale_end_date'];
		$product['sale_end_date'] = empty($product['sale_end_date_val']) ? '' : $this->localize->set_human_time($product['sale_end_date_val']);

		if ($fetch_modifiers) $product['modifiers'] = $this->get_product_modifiers($product['entry_id']);

		if ($fetch_stock) $product['stock'] = $this->get_stock($product['entry_id']);

		return $product;
	}
}
/* End of file ./models/store_inventory_datatable_model.php */