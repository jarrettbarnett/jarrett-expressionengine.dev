<?php
function rss_aggregator_cmp_pubDate($a, $b)
{		
	return date('U', strtotime($a['pub_date'])) < date('U', strtotime($b['pub_date'])) ? 1 : -1;			
}

function rss_aggregator_cmp_title($a, $b)
{
	return $a['title'] < $b['title'] ? 1 : -1;
}