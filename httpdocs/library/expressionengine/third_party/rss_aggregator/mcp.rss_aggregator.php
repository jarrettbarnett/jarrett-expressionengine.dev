<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lets you sort and display multiple RSS feeds.
 *
 * @package		Rss_aggregator
 * @subpackage	ThirdParty
 * @category	Modules
 * @author		Jason Pancake
 * @link		http://www.flapjacklabs.com
 */
class Rss_aggregator_mcp
{
	var $base;			// the base url for this module			
	var $form_base;		// base url for forms
	var $module_name = "rss_aggregator";	
	var $perpage     = '10';
	var $pipe_length = 1;

    /**
     * @var Devkit_code_completion
     */
    var $EE;

	function Rss_aggregator_mcp( $switch = TRUE )
	{
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance(); 
		$this->base	 	 = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;

		$this->EE->cp->set_right_nav(array(
	        'All RSS Feeds'    => $this->base,
	        'Add RSS Feed'     => $this->base.AMP.'method=add_feed',			
		));	
	}

	function index() 
	{
		$this->EE->load->dbforge();				
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		$this->EE->load->library('javascript');
		
		if ( ! $rownum = $this->EE->input->get_post('rownum'))
		{
		    $rownum = 0;
		}		
		
		$query = $this->EE->db->get('rss_aggregator_feed', $this->perpage, $rownum);
		
		$vars['feeds'] = array();
		foreach($query->result_array() as $row)
		{
			$var               = array();
			$var['id']         = $row['id'];
			$var['name']       = $row['name'];
			$var['url']        = $row['url'];
			$var['updated_at'] = $row['updated_at'];
			$var['created_at'] = $row['created_at'];
			$var['toggle']     = array(
									'name'      => 'toggle[]',
									'id'        => 'edit_box_'.$row['id'],
									'value'     => $row['id'],
									'class'     =>'toggle'
									);
			$vars['feeds'][]   = $var;
		}								

		$vars['options'] = array(                
                'delete'  => 'Delete Selected',				
		);
		$vars['action_url']  = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=rss_aggregator'.AMP.'method=edit_selected';
    	$vars['form_hidden'] = NULL;   
    	$vars['sort_by_options'] = array(
    		'id'   => 'Id',
    		'name' => 'Name',
    		'url'  => 'URL',
    		'updated_at' => 'Last Updated',
    		'created_at' => 'Created',
    	); 	
		
		//  Check for pagination
	    $total = $this->EE->db->count_all('rss_aggregator_feed');
	
	    // Pass the relevant data to the paginate class so it can display the "next page" links
	    $this->EE->load->library('pagination');
	    $p_config = $this->pagination_config('index', $total);
	
	    $this->EE->pagination->initialize($p_config);
	    
		$this->EE->cp->add_js_script(array('plugin' => 'dataTables'));
	    
	    $vars['pagination'] = $this->EE->pagination->create_links();
	    
	    $this->EE->javascript->output(array(
		    '$(".toggle_all").toggle(
		        function(){
		            $("input.toggle").each(function() {
		                this.checked = true;
		            });
		        }, function (){
		            var checked_status = this.checked;
		            $("input.toggle").each(function() {
		                this.checked = false;
		            });
		        }
		    );'
		)
		);
		
		$this->EE->javascript->output($this->ajax_filters('edit_items_ajax_filter', 6));
		
		$this->EE->javascript->compile();
		
		return $this->content_wrapper('index', 'RSS Aggregator', $vars);
	}
	
	function add_feed()
	{
		$vars = array();		
		return $this->content_wrapper('add_feed', 'Add RSS Feed', $vars);
	}
	
	function _is_url($url)
	{
		return preg_match('/^http:\/\/.*/', $url, $matches) > 0 ? true : false;
	}
	
	function _delete_feed($feed_id)
	{
		$result = $this->EE->db->query('DELETE FROM exp_rss_aggregator_feed WHERE id = ' . $feed_id);
	}
	
	function edit_selected()
	{
		$vars = array();
		$feed_ids = $this->EE->input->post('toggle');
		switch($this->EE->input->post('action'))
		{
			case 'delete':
				$vars['num_deleted'] = 0;				
				foreach($feed_ids as $feed_id)
				{
					$this->_delete_feed($feed_id);
					$vars['num_deleted']++;
				}
				break;			
			default:
				show_error('Invalid option');
				break;
		}
		$vars['return_url'] = $this->base;
		return $this->content_wrapper('edit_selected', 'Edit Selected', $vars);
	}
	
	function create_feed()
	{
		$name = $this->EE->input->post('name');
		$url  = $this->EE->input->post('url');		
		
		if($name == '' || $url == '')
		{
			show_error('Please fill out all required fields.');
		}
		
		if(!$this->_is_url($url))
		{
			show_error('Invalid URL.');
		}
		
		$result = file_get_contents($url);	
		
		if(!$result)
		{
			show_error('Invalid RSS feed.');
		}
		
		$data = array(
			'name'       => $name,			
			'url'        => $url,
			'cache'      => htmlspecialchars($result),
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		);
		
		$sql = $this->EE->db->insert_string('rss_aggregator_feed', $data);
		
		if(!$this->EE->db->query($sql))
		{
			show_error('There was an error creating the RSS feed.');
		}
		
		$vars['return_url'] = $this->base;		
		
		return $this->content_wrapper('create_feed', 'Add RSS Feed', $vars);
	}
	
	function edit_items_ajax_filter()
	{
		$this->EE->output->enable_profiler(FALSE);
		$this->EE->load->helper('text');		

		$col_map = array('id', 'name', 'url', 'updated_at', 'created_at');

		$id = ($this->EE->input->get_post('id')) ? $this->EE->input->get_post('id') : '';		

		// Note- we pipeline the js, so pull more data than are displayed on the page		
		$perpage = $this->EE->input->get_post('iDisplayLength');
		$offset  = ($this->EE->input->get_post('iDisplayStart')) ? $this->EE->input->get_post('iDisplayStart') : 0; // Display start point
		$sEcho   = $this->EE->input->get_post('sEcho');

		/* Ordering */
		$order = array();

		if ($this->EE->input->get('iSortCol_0') !== FALSE)
		{
			for ( $i=0; $i < $this->EE->input->get('iSortingCols'); $i++ )
			{
				if (isset($col_map[$this->EE->input->get('iSortCol_'.$i)]))
				{
					$order[$col_map[$this->EE->input->get('iSortCol_'.$i)]] = ($this->EE->input->get('sSortDir_'.$i) == 'asc') ? 'asc' : 'desc';
				}
			}
		}

		$total = $this->EE->db->count_all('rss_aggregator_feed');

		$j_response['sEcho'] = $sEcho;
		$j_response['iTotalRecords'] = $total;
		$j_response['iTotalDisplayRecords'] = $total;

		$tdata = array();
		$i = 0;

		if (count($order) > 0)
		{
			foreach ($order as $key => $val)
			{
				$this->EE->db->order_by($key, $val);
			}
		}
		else
		{
			$this->EE->db->order_by('id');
		}

		$query = $this->EE->db->get('rss_aggregator_feed', $perpage, $offset);

		// Note- empty string added because otherwise it will throw a js error
		foreach ($query->result_array() as $feed)
		{
			$m[] = $feed['id'];
			$m[] = $feed['name'];
			$m[] = $feed['url'];
			$m[] = $feed['updated_at'];
			$m[] = $feed['created_at'];
			$m[] = '<input class="toggle" id="edit_box_'.$feed['id'].'" type="checkbox" name="toggle[]" value="'.$feed['id'].'" />';		

			$tdata[$i] = $m;
			$i++;
			unset($m);
		}		

		$j_response['aaData'] = $tdata;	
		$sOutput = $this->EE->javascript->generate_json($j_response, TRUE);

		die($sOutput);
	}
	
	function ajax_filters($ajax_method = '', $cols = '')
	{	
	if ($ajax_method == '')
		{
			return;
		}

		$col_defs = '';
		if ($cols != '')
		{
			$col_defs .= '"aoColumns": [ ';
			$i = 1;

			while ($i < $cols)
			{
				$col_defs .= 'null, ';
				$i++;
			}

			$col_defs .= '{ "bSortable" : false } ],';
		}

		$js = '
var oCache = {
	iCacheLower: -1
};

function fnSetKey( aoData, sKey, mValue )
{
	for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
	{
		if ( aoData[i].name == sKey )
		{
			aoData[i].value = mValue;
		}
	}
}

function fnGetKey( aoData, sKey )
{
	for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
	{
		if ( aoData[i].name == sKey )
		{
			return aoData[i].value;
		}
	}
	return null;
}

function fnDataTablesPipeline ( sSource, aoData, fnCallback ) {
	var iPipe = '.$this->pipe_length.';  /* Ajust the pipe size */
	
	var bNeedServer = false;
	var sEcho = fnGetKey(aoData, "sEcho");
	var iRequestStart = fnGetKey(aoData, "iDisplayStart");
	var iRequestLength = fnGetKey(aoData, "iDisplayLength");
	var iRequestEnd = iRequestStart + iRequestLength;
	oCache.iDisplayStart = iRequestStart;
	
	/* outside pipeline? */
	if ( oCache.iCacheLower < 0 || iRequestStart < oCache.iCacheLower || iRequestEnd > oCache.iCacheUpper )
	{
		bNeedServer = true;
	}
	
	/* sorting etc changed? */
	if ( oCache.lastRequest && !bNeedServer )
	{
		for( var i=0, iLen=aoData.length ; i<iLen ; i++ )
		{
			if ( aoData[i].name != "iDisplayStart" && aoData[i].name != "iDisplayLength" && aoData[i].name != "sEcho" )
			{
				if ( aoData[i].value != oCache.lastRequest[i].value )
				{
					bNeedServer = true;
					break;
				}
			}
		}
	}
	
	/* Store the request for checking next time around */
	oCache.lastRequest = aoData.slice();
	
	if ( bNeedServer )
	{
		if ( iRequestStart < oCache.iCacheLower )
		{
			iRequestStart = iRequestStart - (iRequestLength*(iPipe-1));
			if ( iRequestStart < 0 )
			{
				iRequestStart = 0;
			}
		}
		
		oCache.iCacheLower = iRequestStart;
		oCache.iCacheUpper = iRequestStart + (iRequestLength * iPipe);
		oCache.iDisplayLength = fnGetKey( aoData, "iDisplayLength" );
		fnSetKey( aoData, "iDisplayStart", iRequestStart );
		fnSetKey( aoData, "iDisplayLength", iRequestLength*iPipe );
		
		$.getJSON( sSource, aoData, function (json) { 
			/* Callback processing */
			oCache.lastJson = jQuery.extend(true, {}, json);
			
			if ( oCache.iCacheLower != oCache.iDisplayStart )
			{
				json.aaData.splice( 0, oCache.iDisplayStart-oCache.iCacheLower );
			}
			json.aaData.splice( oCache.iDisplayLength, json.aaData.length );
			
			fnCallback(json)
		} );
	}
	else
	{
		json = jQuery.extend(true, {}, oCache.lastJson);
		json.sEcho = sEcho; /* Update the echo for each response */
		json.aaData.splice( 0, iRequestStart-oCache.iCacheLower );
		json.aaData.splice( iRequestLength, json.aaData.length );
		fnCallback(json);
		return;
	}
}

	oTable = $(".mainTable").dataTable( {	
			"sPaginationType": "full_numbers",
			"bLengthChange": false,
			"bFilter": false,
			"sWrapper": false,
			"sInfo": false,
			"bAutoWidth": false,
			"iDisplayLength": '.$this->perpage.', 
			
			'.$col_defs.'
					
		"oLanguage": {
			"sZeroRecords": "'.$this->EE->lang->line('invalid_entries').'",
			
			"oPaginate": {
				"sFirst": "<img src=\"'.$this->EE->cp->cp_theme_url.'images/pagination_first_button.gif\" width=\"13\" height=\"13\" alt=\"&lt; &lt;\" />",
				"sPrevious": "<img src=\"'.$this->EE->cp->cp_theme_url.'images/pagination_prev_button.gif\" width=\"13\" height=\"13\" alt=\"&lt; &lt;\" />",
				"sNext": "<img src=\"'.$this->EE->cp->cp_theme_url.'images/pagination_next_button.gif\" width=\"13\" height=\"13\" alt=\"&lt; &lt;\" />", 
				"sLast": "<img src=\"'.$this->EE->cp->cp_theme_url.'images/pagination_last_button.gif\" width=\"13\" height=\"13\" alt=\"&lt; &lt;\" />"
			}
		},
		
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": EE.BASE+"&C=addons_modules&M=show_module_cp&module=rss_aggregator&method='.$ajax_method.'",
			"fnServerData": fnDataTablesPipeline

	} );';

		return $js;
	}
	
	function content_wrapper($content_view, $lang_key, $vars = array())
	{
		$vars['content_view'] = $content_view;
		$vars['_base'] = $this->base;
		$vars['_form_base'] = $this->form_base;
		$this->EE->cp->set_variable('cp_page_title', lang($lang_key));
		$this->EE->cp->set_breadcrumb($this->base, lang('rss_aggregator_module_name'));

		return $this->EE->load->view('_wrapper', $vars, TRUE);
	}
	
	function pagination_config($method, $total_rows)
	{
	    // Pass the relevant data to the paginate class
	    $config['base_url']   = $this->base.AMP.'method=index';
	    $config['total_rows'] = $total_rows;
	    $config['per_page']   = $this->perpage;
	    $config['page_query_string']    = TRUE;
	    $config['query_string_segment'] = 'rownum';
	    $config['full_tag_open']        = '<p id="paginationLinks">';
	    $config['full_tag_close']       = '</p>';
	    $config['prev_link']            = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_prev_button.gif" width="13" height="13" alt="<" />';
	    $config['next_link']            = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_next_button.gif" width="13" height="13" alt=">" />';
	    $config['first_link']           = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_first_button.gif" width="13" height="13" alt="< <" />';
	    $config['last_link']            = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_last_button.gif" width="13" height="13" alt="> >" />';
	
	    return $config;
	}
}

/* End of file mcp.rss_aggregator.php */ 
/* Location: ./system/expressionengine/third_party/rss_aggregator/mcp.rss_aggregator.php */
/* Generated by DevKit for EE - develop addons faster! */