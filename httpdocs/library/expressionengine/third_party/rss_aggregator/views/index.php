<?php
if(count($feeds) > 0)
{
	echo form_open($action_url, '', $form_hidden);	
    $this->table->set_template($cp_table_template);
    $this->table->set_heading(
    	'Id',
    	'Name',
    	'URL',
    	'Last Updated',
    	'Created',      
        form_checkbox('select_all', 'true', FALSE, 'class="toggle_all" id="select_all"'));

    foreach($feeds as $feed)
    {
        $this->table->add_row(
                $feed['id'],
                $feed['name'],
                $feed['url'],
                $feed['updated_at'],
                $feed['created_at'],                
                form_checkbox($feed['toggle'])
            );
    }

	echo $this->table->generate();
	
	?>
	<div class="tableFooter">
	    <div class="tableSubmit">
	        <?php echo form_submit(array('name' => 'submit', 'value' => 'Submit', 'class' => 'submit')).NBS.NBS.form_dropdown('action', $options)?>
	    </div>	
	    <span class="js_hide"><?php echo $pagination?></span>
	    <span class="pagination" id="filter_pagination"></span>
	</div>
	<?php 
	echo form_close();
}
else 
{
	echo 'No Feeds Found';
}