<?php echo form_open($_form_base."&method=create_feed", '') ?>
<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th class='translatePhrase'>
				Attribute</th><th>Value
			</th>
		</tr>
	</thead>
	<tbody>
		<tr class="even">
			<td><label for="name"><em class="required">*</em>Name</label></td>
			<td><input type="text" name="name" id="name" value="" class="fullfield"/></td>
		</tr>
		<tr class="odd">
			<td><label for="url"><em class="required">*</em>URL</label></td>
			<td><input type="text" name="url" id="url" value="" class="fullfield"/></td>
		</tr>
	</tbody>
</table>
<div class="tableSubmit">
	<?php echo form_submit(array('name' => 'submit', 'value' => 'Add RSS Feed', 'class' => 'submit'));?>	
</div>
</form>