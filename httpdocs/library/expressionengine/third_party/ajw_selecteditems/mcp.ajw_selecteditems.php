<?php

class Ajw_selecteditems_mcp {
	
	var $version = '3.0';
	
	var $base;
	var $form_base;
	var $module_name = "ajw_selecteditems";
	
	/*
	TODO:
	
	1. Comment this file
	2. Make title, name and description available as variables
	3. Remove extra db calls being made to fetch all items
	
	*/
	
	function Ajw_selecteditems_mcp() {
		// todo: mpve stuff back into here
		// todo: refactor common controller functions
	}
	
	/**
	 * Default controller to display list of saved selections
	 *
	 * @author Andrew Weaver
	 */
	function index() {
		
		$this->EE =& get_instance();
		$this->base = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->theme_url = $this->EE->config->item('theme_folder_url') . 'third_party/ajw_selecteditems/';
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'. $this->theme_url .'css/ajw_selecteditems.css" />');
		
		$this->EE->cp->set_breadcrumb($this->base, $this->EE->lang->line('ajw_selecteditems_module_name') );

		$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('ajw_selecteditems_selections') );
		$this->EE->cp->set_right_nav( array(
			'ajw_selecteditems_create' => $this->base . AMP . 'method=edit'
		));
		
		// Load helpers
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();
		$data["content"] = "index";
		
		$this->EE->db->where( "site_id", $this->EE->config->item('site_id') );
		$query = $this->EE->db->get( "exp_ajw_selecteditems" );
		$data["items"] = $query->result_array();
		
		$data["base"] = $this->base;
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}

	/**
	 * Edit controller to create and edit selections
	 *
	 * @param string $id 
	 * @author Andrew Weaver
	 */
	function edit( $id = NULL ) {

		$this->EE =& get_instance();
		$this->base = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->theme_url = $this->EE->config->item('theme_folder_url') . 'third_party/ajw_selecteditems/';
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'. $this->theme_url .'css/ajw_selecteditems.css" />');
		
		$this->EE->cp->set_breadcrumb($this->base, $this->EE->lang->line('ajw_selecteditems_module_name') );

		if( $id == NULL ) {
			$id = $this->EE->input->get("id");
		}

		$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('ajw_selecteditems_edit') );
		$this->EE->cp->set_right_nav( array(
			'ajw_selecteditems_selections' => $this->base . AMP . 'method=index'
		));
		
		// Load helpers
		$this->EE->load->library('table');
		$this->EE->load->helper('form');
		
		$data = array();
		$data["content"] = "edit";
		
		$data["selected_items"] = array();
		$data["filter"] = array();
		if( $id !== FALSE ) {
			$this->EE->db->where( "id", $id );
			$query = $this->EE->db->get( "exp_ajw_selecteditems" );
			$data["settings"] = $query->row_array();
			$data["filter"] = unserialize( $data["settings"]["filter"]);
			
			$this->EE->db->select( "entry_id, title" );
			$this->EE->db->where_in( "entry_id", explode("|", $data["settings"]["items"]) );
			$query = $this->EE->db->get( "exp_channel_titles" );
			$data["selected_items"] = $query->result_array();
		}

		$data["all_items"] = array();
		$this->EE->db->select( "entry_id, title" );
		$query = $this->EE->db->get( "exp_channel_titles" );
		$data["all_items"] = $query->result_array();
		
		$data["form_action"] = $this->form_base . AMP . "method=update";
		
		$url = $this->EE->functions->fetch_site_index(0, 0) . QUERY_MARKER . 'ACT=' . $this->EE->cp->fetch_action_id('Ajw_selecteditems_mcp', 'refresh');

		$this->EE->javascript->output(
			array('
			
			function refresh() {
				$("#all").html("Loading...")
				$.ajax({
					type: "POST",
						url: "'.$url.'",
						data: $("#update_form").serialize(),
						success: function(data){
							$("#all").html(data)
					}
				});
			}
			
			var delay = (function(){
				var timer = 0;
				return function(callback, ms){
					clearTimeout (timer);
					timer = setTimeout(callback, ms);
				};
			})();
			
			$(function(){
				
				refresh();
				
				$("input[name=filter_keywords]").keyup(function() {
					delay(function(){
						refresh();
					}, 300 );
				});

				$("select").change( function() {
					refresh();
				})

				$("#items").hide();
				$("#all, #added").sortable({
					connectWith: ".dnd_list",
					placeholder: "highlight",
					update: function() {
						var order = $("#added").sortable("toArray");
						var added = new Array();
						for( o in order ) {
							added.push(order[o].substr(5));
						}
						$("#items").val(added.join("|"));
					}
				}).disableSelection();

				$("#refresh").click( function() {
					refresh();
				})

			});
			')
		); 
		$this->EE->javascript->compile();
		
		$this->EE->db->select( "channel_id, channel_title" );
		$query = $this->EE->db->get( "exp_channels" );
		$data["channels"] = array();
		$data["channels"][] = "All";
		foreach( $query->result_array() as $row ) {
			$data["channels"][ $row["channel_id"] ] = $row["channel_title"];
		}

		$this->EE->db->select( "cat_id, cat_name" );
		$query = $this->EE->db->get( "exp_categories" );
		$data["categories"] = array();
		$data["categories"][] = "All";
		foreach( $query->result_array() as $row ) {
			$data["categories"][ $row["cat_id"] ] = $row["cat_name"];
		}

		$data["order"] = array(
			"t.entry_id desc" => "Entry ID",
			"t.title asc" => "Title",
			"t.entry_date desc" => "Entry Date"
		);
		
		$data["limit"] = array(
			"25" => "25",
			"50" => "50",
			"100" => "100",
			"250" => "250",
			"0" => "All",
		);
		
		return $this->EE->load->view('_wrapper', $data, TRUE);
	}
	
	/**
	 * Save selection to database
	 *
	 * @author Andrew Weaver
	 */
	function update() {
		
		$this->EE =& get_instance();
		$this->base = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->theme_url = $this->EE->config->item('theme_folder_url') . 'third_party/ajw_selecteditems/';
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'. $this->theme_url .'css/ajw_selecteditems.css" />');
		
		$this->EE->cp->set_breadcrumb($this->base, $this->EE->lang->line('ajw_selecteditems_module_name') );
		
		$this->EE->load->library('form_validation');
		$this->EE->form_validation->set_error_delimiters('<p class="notice">', '</p>');
		$this->EE->form_validation->set_rules('title', lang('ajw_selecteditems_title'), 'required');
		$this->EE->form_validation->set_rules('name', lang('ajw_selecteditems_name'), 'required|alpha_dash');
		
		$filter_fields = array(
			"filter_keywords",
			"filter_channel",
			"filter_category",
			"filter_order",
			"filter_limit"
		);
		$filter = array();
		foreach( $filter_fields as $f ) {
			$filter[ $f ] = $this->EE->input->post( $f, "" );
		}
		
		$data = array(
			"title" => $this->EE->input->post("title"),
			"name" => $this->EE->input->post("name"),
			"description" => $this->EE->input->post("description"),
			"items" => $this->EE->input->post("items"),
			"filter" => serialize($filter)
		);

		if( $this->EE->input->post("id") === FALSE ) {

			if ($this->EE->form_validation->run() === FALSE) {
				return $this->edit();
			}

			$this->EE->db->insert('exp_ajw_selecteditems', $data);
			$this->EE->session->set_flashdata('message_success', lang("ajw_selecteditems_selection_added") );
			$this->EE->functions->redirect( $this->base );

		} else {

			if ($this->EE->form_validation->run() === FALSE) {
				return $this->edit( $this->EE->input->post("id") );
			}

			$this->EE->db->where('id', $this->EE->input->post("id") );
			$this->EE->db->update('exp_ajw_selecteditems', $data);
			$this->EE->session->set_flashdata('message_success', lang("ajw_selecteditems_selection_updated") );
			$this->EE->functions->redirect( $this->base . AMP . 'method=edit' . AMP . 'id=' . $this->EE->input->post("id"));
			
		}
		
	}

	/**
	 * Confirm and delete selection
	 *
	 * @author Andrew Weaver
	 */
	function delete() {

		$this->EE =& get_instance();
		$this->base = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name;
		$this->theme_url = $this->EE->config->item('theme_folder_url') . 'third_party/ajw_selecteditems/';
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'. $this->theme_url .'css/ajw_selecteditems.css" />');
		
		$this->EE->cp->set_breadcrumb($this->base, $this->EE->lang->line('ajw_selecteditems_module_name') );

		$vars = array();

		if( $this->EE->input->post('confirm') != 'confirm' ) {

			// Change this depending on required action

			$data["content"] = 'delete';

			$data["id"] = $this->EE->input->get('id');
			$data["delete_action"] = $this->form_base . AMP . "method=delete";

			$data["confirm_message"] = $this->EE->lang->line('ajw_selecteditems_delete_confirm');

			return $this->EE->load->view('_wrapper', $data, TRUE);

		} else {

			$id = $this->EE->input->post('id');

			$this->EE->db->where( "id", $this->EE->input->post('id') );
			$query = $this->EE->db->delete( "exp_ajw_selecteditems" );
			
			$message = $this->EE->lang->line('ajw_selecteditems_delete_success');
			$this->EE->session->set_flashdata('message_success', $message);
			$this->EE->functions->redirect($this->base . AMP . "method=index");
			
		}

	}

	/**
	 * AJAX-called method to fill available items select box
	 *
	 * @author Andrew Weaver
	 */
	function refresh() {
		
		$this->EE =& get_instance();

		$channel_id = $this->EE->input->post("filter_channel", "0");
		$category_id = $this->EE->input->post("filter_category", "0");
		$keywords = $this->EE->input->post("filter_keywords", "");
		$order = $this->EE->input->post("filter_order", "t.entry_id desc");
		$limit = $this->EE->input->post("filter_limit", "0");
		
		$this->EE->db->select( "t.entry_id, title" );
		$this->EE->db->from( "exp_channel_titles t" );
		$this->EE->db->join( "exp_category_posts p", "p.entry_id = t.entry_id", "left outer" );
		$this->EE->db->where_not_in( "t.entry_id", explode("|", $this->EE->input->post("items") ) );
		if( $keywords != "" ) {
			$this->EE->db->like( "t.title", $keywords );
		}
		if( $channel_id != 0 ) {
			$this->EE->db->where( "t.channel_id", $channel_id );
		}
		if( $category_id != 0 ) {
			$this->EE->db->where( "p.cat_id", $category_id );
		}
		$this->EE->db->group_by( "t.entry_id" );
		$this->EE->db->order_by( $order );
		if( $limit != 0 ) {
			$this->EE->db->limit( $limit );
		}
		$query = $this->EE->db->get();
		
		foreach( $query->result_array() as $row ) {
			print '<li class="item" id="item-'.$row["entry_id"].'">'.$row["title"].'</li>';
		}
		exit;
	}

}

/* End of file mcp.ajw_selecteditems.php */