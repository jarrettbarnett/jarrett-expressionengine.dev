<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajw_selecteditems_upd { 

	var $version        = '3.0'; 

	function Ajw_selecteditems_upd( $switch = TRUE ) 
	{ 
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
	} 

	function install() 
	{
		$this->EE->load->dbforge();

		$data = array(
			'module_name' => 'Ajw_selecteditems' ,
			'module_version' => $this->version,
			'has_cp_backend' => 'y'
			);

		$fields = array(
			'id' => array(
				'type' => 'int',
				'constraint' => '6',
				'unsigned' => TRUE,
				'auto_increment'=> TRUE
			),
			'site_id' => array(
				'type' => 'int',
				'constraint' => '4',
				'unsigned' => TRUE,
				'null' => FALSE,
				'default' => '1'
			),
			'title' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'null' => FALSE
			), 
			'name' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'null' => FALSE
			), 
			'description' => array(
				'type' => 'text',
				'null' => FALSE
			), 
			'items' => array(
				'type' => 'text'
			), 
			'filter' => array(
				'type' => 'text'
			)
		);

		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('id', TRUE);
		$this->EE->dbforge->create_table('ajw_selecteditems');

		$this->EE->db->insert('modules', $data); 

		$data = array(
			'class' => 'Ajw_selecteditems_mcp',
			'method' => 'refresh'
		);
		$this->EE->db->insert('actions', $data);

		return TRUE;
	}

	function uninstall() 
	{ 
		$this->EE->load->dbforge();

		$this->EE->db->select('module_id');
		$query = $this->EE->db->get_where('modules', array('module_name' => 'Ajw_selecteditems'));

		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');

		$this->EE->db->where('module_name', 'Ajw_selecteditems');
		$this->EE->db->delete('modules');

		$this->EE->db->where('class', 'Ajw_selecteditems');
		$this->EE->db->delete('actions');

		$this->EE->db->where('class', 'Ajw_selecteditems_mcp');
		$this->EE->db->delete('actions');

		$this->EE->dbforge->drop_table('ajw_selecteditems');

		return TRUE;
	}

	function update($current = '')
	{
		if ($current == $this->version) {
			return FALSE;
		}
		$this->EE->load->dbforge();

		if ( $current < "2.1" )  {

			// Add AJAX action
			$data = array(
				'class' => 'Ajw_selecteditems_mcp',
				'method' => 'refresh'
			);
			$this->EE->db->insert('actions', $data);
			
		} 

		if ( $current < "2.2" )  {

			$this->EE->load->dbforge();
			$fields = array(
				'title' => array(
					'type' => 'varchar',
					'constraint' => '255',
					'null' => FALSE
				)
			);
			$this->EE->dbforge->add_column('ajw_selecteditems', $fields, 'site_id');
			$fields = array(
				'filter' => array(
					'type' => 'text'
				),
			);
			$this->EE->dbforge->add_column('ajw_selecteditems', $fields);
			
			$this->EE->dbforge->drop_column('ajw_selecteditems', 'orderby');
			$this->EE->dbforge->drop_column('ajw_selecteditems', 'channels');
			$fields = array(
			'cache' => array(
				'name' => 'items',
				'type' => 'text',
				),
			);
			$this->EE->dbforge->modify_column('ajw_selecteditems', $fields);
		}
		
		return TRUE;
	}

}

/* End of file upd.ajw_selecteditems.php */