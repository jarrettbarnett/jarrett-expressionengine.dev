INSTALLATION

1) Download and extract the add-on
2) Copy the ajw_selecteditems folder to your system/expressionengine/third_party folder
3) Copy the themes/third_party/ajw_selecteditems folder to your themes/third_party folder
4) In the Control Panel, got Add-Ons > Modules and click on Install on the AJW Selected Items line.


USAGE

NOTE: CHANGE OF USAGE BETWEEN v2.2 and v3

You no longer wrap the {exp:ajw_selecteditems} tag around a {exp:channel:entries} tag.

A new tag {exp:ajw_selecteditems:entries} has been introduced that allows 
all the standard exp:channel:entries parameters, but only displays the 
selected items (in the selected order).

The name= parameter should correspond to the id or the name of a selection created in the module’s Control Panel.

<ul>
{exp:ajw_selecteditems:entries name="Test"
	channel="default_site|feeds|twitter" fixed_order="{ajw_selecteditems}"  
	dynamic="off"
	disable="pagination|custom_fields|categories|member_data|trackbacks"}
	<li><a href="{comment_url_title_auto_path}">{title}</a></li>
{/exp:ajw_selecteditems:entries}
</ul>

