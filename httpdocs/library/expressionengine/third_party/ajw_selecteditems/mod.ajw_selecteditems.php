<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! class_exists('Channel') )
{
	require_once( APPPATH.'modules/channel/mod.channel'.EXT );
}

class Ajw_selecteditems extends Channel {

	var $return_data    = '';

	/**
	 * Tag to display list of selected items
	 *
	 * @author Andrew Weaver
	 */
	function Ajw_selecteditems() {
		// Call parent class constructor
		parent::Channel();
	}
	
	function entries() {
		
		// Fetch parameters
		$id = $this->EE->TMPL->fetch_param('id'); 
		$name = $this->EE->TMPL->fetch_param('name'); 

		// todo: validate parameters

		// Fetch items from database
		$this->EE->db->select( "items" );
		$this->EE->db->where( "id", $id );
		$this->EE->db->or_where( "name", $name );
		$query = $this->EE->db->get( "exp_ajw_selecteditems" );

		// Run channel entries tag with corect parameters
		if ($query->num_rows() > 0) {
			
			// Swap ajw_selecteditems variable for list of entries
			$row = $query->row_array();
			$this->EE->TMPL->tagparams['fixed_order'] = $row["items"];

		} else {

			// No selected items so use non-existant entry zero
			$this->EE->TMPL->tagparams['fixed_order'] = '0';

		}

		$this->EE->TMPL->tagparams['dynamic'] = 'off';
		
		$this->return_data = parent::entries();
		return $this->return_data;
	}

}

/* End of file mod.ajw_selecteditems.php */