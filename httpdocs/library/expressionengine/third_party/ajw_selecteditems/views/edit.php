<?php echo form_open( $form_action, array('id' => 'update_form') ); ?>

<?php 

$data["selected_items"] = array();


if( isset( $settings["id"] ) ) {
	echo form_hidden("id", $settings["id"] );
}

$this->table->set_template($cp_table_template);
$this->table->set_heading("Settings", "Value");

$this->table->add_row(
	'<em class="required">*</em> ' .
		form_label(lang('ajw_selecteditems_title'), 'title') . 
		'<div class="subtext">' . lang('ajw_selecteditems_title_info') . '</div>' . 
		form_error("title"), 
		form_input("title",  
		isset($settings["title"]) ? $settings["title"] : '' )
);

$this->table->add_row(
	'<em class="required">*</em> ' .
		form_label(lang('ajw_selecteditems_name'), 'name') .
		'<div class="subtext">' . lang('ajw_selecteditems_name_info') . '</div>' . 
		form_error("name"), 
		form_input("name",  
		isset($settings["name"]) ? $settings["name"] : '' )
);

$this->table->add_row(
		form_label(lang('ajw_selecteditems_description'), 'description') . 
		form_error("description"), 
		form_textarea("description",  
		isset($settings["description"]) ? $settings["description"] : '' )
);

$available = '<div class="dnd_list_container">' . 
	form_label(lang("ajw_selecteditems_available_items")) . 
	'<ul id="all" class="dnd_list">';
$added = '<div class="dnd_list_container">' . 
	form_label(lang("ajw_selecteditems_selected_items")) . 
	'<ul id="added" class="dnd_list">';

if( !isset($settings["items"]) ) {
	$settings["items"] = "";
}
$selection = explode("|", $settings["items"]);
foreach( $all_items as $item) {
	$id = $item["entry_id"];
	if( ! in_array( $id, $selection ) ) {
		$available .= '  <li class="item" id="item-'. $id . '">' . $item["title"] . '</li>';
	}
}
foreach( $selection as $id ) {
	foreach( $selected_items as $item ) {
		$entry_id = $item["entry_id"];
		if( $entry_id == $id ) {
			$added .= '  <li class="item" id="item-'. $entry_id . '">' . $item["title"] . '</li>';
		}
	}
}

$available .= '</ul>' . 
	"<p>Search: " . form_input("filter_keywords", (isset($filter["filter_keywords"]) ? $filter["filter_keywords"] : '' ), 'style="width:120px"') . "</p>" .
	"<p>Channel: " . form_dropdown("filter_channel", $channels, (isset($filter["filter_channel"]) ? $filter["filter_channel"] : '' )) . "</p>" .
	"<p>Categories: " . form_dropdown("filter_category", $categories, (isset($filter["filter_category"]) ? $filter["filter_category"] : '' )) . "</p>" .
	"<p>Order: " . form_dropdown("filter_order", $order, (isset($filter["filter_order"]) ? $filter["filter_order"] : '' )) . "</p>" .
	"<p>Display: " . form_dropdown("filter_limit", $limit, (isset($filter["filter_limit"]) ? $filter["filter_limit"] : '' )) . "</p>" .
	form_button("refresh", "Refresh", 'id="refresh"') .
	'</div>';

$added .= '</ul></div>';

$this->table->add_row(
	form_label(lang("ajw_selecteditems_items"), 'items'), 
	form_input(
		"items",  
		isset($settings["items"]) ? $settings["items"] : '',
		'id="items"'
	) . $available . $added
);

echo $this->table->generate();
echo $this->table->clear();

?>

<input type="submit" value="Submit" class="submit" />

<?php echo form_close(); ?>