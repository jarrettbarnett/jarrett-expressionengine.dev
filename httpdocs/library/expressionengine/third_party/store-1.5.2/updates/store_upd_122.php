<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Exp:Store module for ExpressionEngine 2.x by Crescendo (support@crescendo.net.nz)
 * Copyright (c) 2010-2012 Crescendo Multimedia Ltd
 * All rights reserved.
 */

class Store_upd_122
{
	/**
	 * Add new logout hook
	 */
	public function up()
	{
		Store_upd::register_hook('member_member_logout');
	}
}
