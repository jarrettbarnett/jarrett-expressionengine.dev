<?php
$config['name'] = 'Blueprints';
$config['version'] = '1.3.7.4';
$config['description'] = 'Assign Publish Layouts to Structure templates.';
$config['docs_url'] = 'http://boldminded.com/add-ons/blueprints';
$config['nsm_addon_updater']['versions_xml'] = 'http://boldminded.com/assets/add-ons/versions/blueprints.xml';