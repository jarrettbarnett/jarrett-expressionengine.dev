<script type="text/javascript" src="<?=$theme_base?>js/plates.js" ></script>
<link rel="stylesheet" href="<?=$theme_base?>css/plates.css" type="text/css" media="screen" />
<script type="text/javascript">
	$(function() {
		$("ul#plates_<?=$field_id?> a").click(function() { //check for the first selection
			var $column = $(this).attr('rel'); // assign the ID of the column
			$('ul#plates_<?=$field_id?>').find("li").removeClass("highlight") //forget the last highlighted column
			$('ul#plates_<?=$field_id?>').find("li."+$column).addClass("highlight"); //highlight the selected column
			$('ul#plates_<?=$field_id?>').find("li."+$column).find(":radio").attr("checked","checked");
			return false;
		});	
	});
</script>
<div id="plates_wrapper">
	<ul id="plates_<?=$field_id?>" class="plates_list">
	<?php
		$i = 1;	
		
		
		foreach($options['templates'] as $option):
	
			$thumnb 	= ($options['thumbs'][$i]) ? "<img src='".$theme_base."thumbs/".$options['thumbs'][$i]."' width='200' height='160' />" : '';
			$tip 		= ($options['notes'][$i] || $options['thumbs'][$i]) ? 'class="has_tip" tip="'.$thumnb.htmlspecialchars($options['notes'][$i]).'"' : '';
			$selected 	= (($i == 1 && $selected_template == '') || $selected_template == $options['templates'][$i]) ? ' selected="selected"' : '';
	?>
			<li class="<? echo $i; if(($i == 1 && $selected_template == '') || $selected_template == $options['templates'][$i]) echo ' highlight'?>">
				<input class="hidden_radio" name="<?=$field_name?>" value="<?=$options['templates'][$i] ?>" type="radio"<?=$selected?> />
				<h4><a href="#" rel="<?=$i?>"<?=$tip?>><?=$options['labels'][$i] ?></a></h4>
			</li>
	<?php
		$i++;
		endforeach;
	?>
	</ul>
</div>