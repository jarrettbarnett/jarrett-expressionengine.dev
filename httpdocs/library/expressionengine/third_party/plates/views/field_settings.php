<style type="text/css">
	tr#plates_default_row {display: none;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#plates_add_row').live('click', function() {
			$("#plates_default_row").clone().removeAttr("id").appendTo( $("#plates_default_row").parent() );
			return false;
		});
		$('.plates_delete_row').live('click', function() {
			$(this).parent().parent().remove();
			return false;
		});
		
		$("a.plates_up, a.plates_down").live('click', function() {
	        var row = $(this).parents("tr:first");
	        if ($(this).is("a.plates_up")) {
	            row.insertBefore(row.prev());
	        } else {
	            row.insertAfter(row.next());
	        }
	        return false;
	    });
	});
</script>

<table cellspacing="0" style="width: 100%; margin-top: 10px;">
	<tr>
		<th style="width: 40px;"><?=lang('sort');?></th>
		<th style="width: 100px;"><?=lang('template');?></th>
		<th style="width: 150px;"><?=lang('display_label');?></th>
		<th style="width: 100px;"><?=lang('thumb_filename');?></th>
		<th><?=lang('publisher_notes');?></th>
		<th></th>
	</tr>
	<tbody>
	<tr id="plates_default_row">
		<td>
			<a href="#" class="plates_up">
				<img src="<?=PATH_CP_GBL_IMG?>arrow_up.gif" border="0"  width="16" height="16" alt="Up" title="<?=lang('up');?>" />
			</a>
            <a href="#" class="plates_down">
            	<img src="<?=PATH_CP_GBL_IMG?>arrow_down.gif" border="0"  width="16" height="16" alt="Down" title="<?=lang('down');?>" />
            </a>
        </td>
		<td><?= form_dropdown('options[templates][]', $template_options, '') ?></td>
		<td><?= form_input("options[labels][]", '') ?></td>
		<td><?= form_input("options[thumbs][]", '') ?></td>
		<td><?= form_input("options[notes][]", '') ?></td>
		<td><a href="#" class="plates_delete_row">Delete</a></td>
	</tr>
<?php 
	$count = 1;
	if(isset($options['templates']))
	{
	foreach($options['templates'] as $key => $option): 
?>	
	<tr>
		<td>
			<a href="#" class="plates_up">
				<img src="<?=PATH_CP_GBL_IMG?>arrow_up.gif" border="0"  width="16" height="16" alt="Up" title="Up" />
			</a>
            <a href="#" class="plates_down">
            	<img src="<?=PATH_CP_GBL_IMG?>arrow_down.gif" border="0"  width="16" height="16" alt="Down" title="Down" />
            </a>
         </td>
		<td><?= form_dropdown("options[templates][]", $template_options, $options['templates'][$count] ) ?></td>
		<td><?= form_input("options[labels][]", (isset($options['labels'][$count])) ? $options['labels'][$count] : '') ?></td>
		<td><?= form_input("options[thumbs][]", (isset($options['thumbs'][$count])) ? $options['thumbs'][$count] : '') ?></td>
		<td><?= form_input("options[notes][]", (isset($options['notes'][$count])) ? $options['notes'][$count] : '') ?></td>
		<td><a href="#" class="plates_delete_row"><?=lang('delete');?></a></td>
	</tr>
<?php 
	$count++; 
	endforeach;
	}
?>
</tbody>
</table>
<p><a href="#" id="plates_add_row"><?=lang('add_a_template');?></a></p>