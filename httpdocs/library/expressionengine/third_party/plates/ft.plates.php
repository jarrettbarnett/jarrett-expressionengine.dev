<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 'Plates Fieldtype for ExpressionEngine 2
 *
 * @package		ExpressionEngine
 * @subpackage	Fieldtypes
 * @category	Fieldtypes
 * @author    	Iain Urquhart <shout@iain.co.nz>
 * @copyright 	Copyright (c) 2011 Iain Urquhart
 * @license   	Commercial, All Rights Reserved: http://devot-ee.com/add-ons/license/plates/
*/


class Plates_ft extends EE_Fieldtype {
	
	var $info = array(
		'name'		=> 'Plates',
		'version'	=> '1.0.2'
	);

	// --------------------------------------------------------------------
	
	/**
	 * Display Field on Publish
	 *
	 * @access	public
	 * @param	existing data
	 * @return	field html
	 *
	 */
	function display_field($data)
	{

		$vars = array();
		
		$vars['field_name'] = ($this->structure_exists()) ? 'structure__template_id' : 'pages__pages_template_id';
		
		$pages = $this->EE->config->item('site_pages');
		$entry_id = $this->EE->input->get('entry_id');
		$site_id = $this->EE->config->item('site_id');

		$vars['options'] = (isset($this->settings['plates_options']) ? unserialize($this->settings['plates_options']) : '');
		$vars['selected_template'] = (isset($pages[$site_id]['templates'][$entry_id])) ? $pages[$site_id]['templates'][$entry_id] : '';
		$vars['field_id'] = $this->settings['field_id'];
		$vars['theme_base'] = $this->EE->config->item('theme_folder_url').'third_party/plates_assets/';
		
		return $this->EE->load->view('field', $vars, TRUE);

	}
	
	// --------------------------------------------------------------------
	
	// we're not saving anything
	function save($data)
	{
		return NULL;
	}
	
	// --------------------------------------------------------------------
		
	/**
	 * Replace tag
	 *
	 * @access	public
	 * @param	field contents
	 * @return	replacement text
	 *
	 */
	function replace_tag($data, $params = array(), $tagdata = FALSE)
	{
		return NULL;
	}
	
	// --------------------------------------------------------------------

	
	/**
	 * Display Settings Screen
	 *
	 * @access	public
	 * @return	default global settings
	 *
	 */
	function display_settings($data)
	{

		$site_id = $this->EE->config->item('site_id');
		$template_options = array();
		
		$this->EE->cp->add_js_script(array('plugin' => 'sortable'));
		$this->EE->lang->loadfile('plates');
		
		// resolve conflict with PT fields who store settings in 'options'
		// change namespace to plates_options
		if(isset($data['options']))
		{
			$vars['options'] = (isset($data['options'])) ? unserialize($data['options']) : '';
		}
		else
		{
			$vars['options'] = (isset($data['plates_options'])) ? unserialize($data['plates_options']) : '';
		}

		$templates = $this->EE->template_model->get_templates($site_id);
		foreach ($templates->result() as $template)
		{
			$vars['template_options'][$template->template_id] = $template->group_name.'/'.$template->template_name;
		}
		
		$this->EE->table->add_row(
			array('data' => $this->EE->load->view('field_settings', $vars, TRUE), 'colspan' => 2)				
		);
	}
	
	// --------------------------------------------------------------------

	/**
	 * Save Settings
	 *
	 * @access	public
	 * @return	field settings
	 *
	 */
	function save_settings($data)
	{
		$options = $this->EE->input->post('options');
		
		// print_r($options);
		
		if(is_array($options))
		{
			unset($options['templates'][0]); // remove our template option
			unset($options['labels'][0]); // remove our template option
			unset($options['thumbs'][0]); // remove our template option
			unset($options['notes'][0]); // remove our template option
			$options = $this->EE->security->xss_clean($options);
			$options = serialize($options);
		}
				
		return array(
			'plates_options' => $options,
		);
	}

	// --------------------------------------------------------------------
	
	/**
	 * Install Fieldtype
	 *
	 * @access	public
	 * @return	default global settings
	 *
	 */
	function install()
	{
		return NULL;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check for Structure module
	 *
	 * @access	public
	 * @return	TRUE/FALSE
	 *
	 */
	private function structure_exists()
	{
		if ( ! isset($this->EE->session->cache['plates_structure_check']))
		{
			if (! $this->EE->db->table_exists('structure'))
				return FALSE;

			$this->EE->session->cache['plates_structure_check'] = 1;
		}
		return TRUE;
	}

}

/* End of file ft.plates.php */
/* Location: ./system/expressionengine/third_party/plates/ft.plates.php */