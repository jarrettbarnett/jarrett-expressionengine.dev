<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Exp:Store module for ExpressionEngine 2.x by Crescendo (support@crescendo.net.nz)
 * Copyright (c) 2010-2012 Crescendo Multimedia Ltd
 * All rights reserved.
 */

class Store_upd_124
{
	/**
	 * Register new add to cart action
	 */
	public function up()
	{
		Store_upd::register_action('act_add_to_cart');
	}
}
