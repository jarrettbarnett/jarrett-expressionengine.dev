<?php

if ( ! defined('STORE_NAME'))
{
	define('STORE_NAME', 'Store');
	define('STORE_CLASS', 'Store');
	define('STORE_VERSION', '1.5.3.1');
	define('STORE_DESCRIPTION', 'Fully featured e-commerce for ExpressionEngine');
	define('STORE_DOCS', 'http://exp-resso.com/store/docs');
}

$config['name'] = STORE_NAME;
$config['version'] = STORE_VERSION;
$config['nsm_addon_updater']['versions_xml'] = 'http://exp-resso.com/rss/store/versions.rss';
