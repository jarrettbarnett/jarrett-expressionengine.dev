$(document).ready(function() {
	$(".import_list_item").click(function() {
		var id = $(this).attr('id');
		$(this).closest('td').append('<div id="edit_import_'+id+'" class="edit_form"><input name="title" value="'+$(this).text()+'" class="edit_form"><input type="hidden" name="import_id" value="'+id+'"> <input type="button" class="submit edit_form" value="Save"></div>');
		$(this).hide();
		return false;
	});
	
	$('input.edit_form[type="button"]').live('click', function(){
		var url = baseUrl+"&method=update_import_item&import_id="+$('input[type=hidden][name="import_id"]').val()+'&title='+$('input.edit_form[name="title"]').val();
		$.get(url, function(data){
			$('body').append(data);
		})
	})
	$('form').live('submit', function(){
		$('.submit').attr('disabled','disabled');
		return true;
	})
	
	$('.delete_link').live('click', function(){
		if( ! confirm($(this).text()+' this?')) return false;
		
		var url = $(this).attr('href');
		$.get(url, function(data){
			$('body').append(data);
		})
		$(this).closest('tr').hide();
		return false;		
	});
});