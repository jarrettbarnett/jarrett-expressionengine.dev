<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Db_lib extends CI_Model
{
	public  $table;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get($options=array(),$table='')
	{
		$table = $table ? $table : $this->table;
		
		$this->db->from($table);
		if(@$options['where'])
		{
			$this->db->where($options['where']);
		}
		
		if(isset($options['count']))
		{
			return $this->db->count_all_results();
		}	
			
		if(@$options['order_by'])
		{
			$this->db->order_by($options['order_by'], @$options['dir']);
		}
		
		
		return $this->db->get()->result_array();
	}
		
	public function get_row($options=array(), $table='')
	{
		$option_keys = array('where', 'order_by');
		
		// remove that in future versions//////
		if( ! is_array($options))
		{
			$tmp = $table;
			$table = $options;
			$options = array();
			if(in_array(key($tmp),$option_keys))
				$options = $table;
			else 
				$options['where'] = $tmp;
		}
		////////////////////////////////////////
		$table = $table ? $table : $this->table;
		
		$this->db->from($table);
		if(@$options['where'])
		{
			$this->db->where($options['where']);
		}
		
		if(@$options['order_by'])
		{
			$this->db->order_by($options['order_by'], @$options['dir']);
		}
				
		return  $this->db->get()->row_array();
	}
	
	public function delete($where=array(), $table='')
	{
		$table = $table ? $table : $this->table;
		if($where)
		$this->db->where($where)->delete($table);
	}
	
	public function save($data, $where=array(), $table='')
	{
		$table = $table ? $table : $this->table;
		$primary_key = $this->_get_primary_key($table);
		if(isset($data[$primary_key]))
		{
			$where[$primary_key] = $data[$primary_key];
			unset($data[$primary_key]);
		}
		
		if($where)
		{
			$this->db->where($where)->update($table, $data);
		}
		else 
		{
			$this->db->insert($table, $data);
			return $this->db->insert_id();
		}
	}
	
	public function _get_primary_key($table)
	{
		$fields = $this->db->field_data($table);
		foreach ($fields as $f) 
		{
			if($f->primary_key)
				return $f->name;
		}
	}
	
	public function get_tables()
	{
		$tables = $this->db->list_tables();
		foreach ($tables as $t) 
		{
			$ret[$t] = $t;
		}
		
		return $ret;
	}

	public function __call($func, $arg=array())
	{
		if(strstr($func, 'get_'))
		{
			preg_match("!get_(.*)!", $func, $m);
			if($this->table_exists($m[1]))
			{
				
				return $this->get($arg, $m[1]);
			}
		}
	}	
	
	public function table_exists($table)
	{
		return $this->db->table_exists($table);
	}	
}

/* End of file db_lib.php */