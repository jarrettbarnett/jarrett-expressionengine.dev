<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2003 - 2011, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------

/**
 * Data Import Module Front End File
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Module
 * @author		addonlabs
 * @link		http://addonlabs.com		
 */

class Data_import {
	
	public $return_data;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
	}
	
	// ----------------------------------------------------------------

	public function start()
	{
    	$this->EE =& get_instance();
    	$this->EE->load->model(array('data_import_remote_model'));
    	$this->EE->load->library(array('data_import_config', 'db_lib'));
    	$this->settings = $this->EE->data_import_config->items();
    	$this->EE->load->model(array('data_import_model', 'data_import_list_model'));
    	$this->EE->data_import_remote_model->connect($this->settings);
    	$import_list = $this->EE->data_import_list_model->get();
    	
    	foreach ($import_list as $import_row) 
    	{
	    	$this->settings = array_merge($this->settings, $this->EE->data_import_list_model->get_settings($import_row['import_id']));
	    	$entries_to_import = $this->EE->data_import_remote_model->get_from($this->settings['remote_table']);
	    	$channel_fields = $this->EE->data_import_model->get_channel_fields($this->settings['channel']);
	    	foreach ($entries_to_import as $row) 
	    	{
	    		$title = $this->settings['new_entry_title'];
	    		$entry_id = $this->_get_remote_data($row);
	    		if( ! $entry_id) continue;
	    		$where['entry_id'] = $entry_id;
	    		$channel_titles_update = $update_data = array();
	    		foreach ($this->settings['assigned_fields'] as $key => $field_row)
	    		{
	    			if( ! $field_row) continue;
	    			if(is_array($field_row) and $key == 'matrix')
	    			{
	    				$update_matrix_data = array();
	    				foreach ($field_row as $key1 => $field_row1) {
	    					if( ! $field_row1) continue;
	    					$update_matrix_data['col_id_'.$key1] = $row[$field_row1];
	    					$title = str_replace('{'.$channel_fields['matrix'][$key1].'}', $row[$field_row1], $title);
	    					$update_matrix_data['field_id'] = $this->EE->data_import_model->get_matrix_field_id($key1);
	    				}
	    				$this->EE->data_import_model->update('matrix_data', $where, $update_matrix_data);
	    				continue;
	    			}
	    			if(is_array($field_row) and $key == 'store')
	    			{
	    				$update_product_data = $update_stock_data = array();
	    				foreach ($field_row as $key1 => $field_row1) {
	    					if( ! $field_row1) continue;
	    					switch ($key1) {
	    						case 'sku':
	    							if( ! $this->EE->data_import_model->get_row(array('where' => array('sku'=>$row[$field_row1])),'store_stock'))
	     							$update_stock_data[$key1] = $row[$field_row1];
	    							break;   							
	    						case 'stock_level':
	    							$update_stock_data[$key1] = $row[$field_row1];
	    							break;    						
	    						case 'regular_price':
	    						case 'sale_price':
	    							$update_product_data[$key1] = $row[$field_row1];
	    							break;
	    					
	    						default:
	    							break;
	    					}
	    					$title = str_replace('{'.$channel_fields['store'][$key1].'}', $row[$field_row1], $title);
	    				}
	    				if($update_stock_data)
	    				$this->EE->data_import_model->update('store_stock', $where, $update_stock_data);
	    				$this->EE->data_import_model->update('store_products', $where, $update_product_data);
	    				
	    				continue;
	    			}
	    			$title = str_replace('{'.$channel_fields[$key].'}', $row[$field_row], $title);
	    			$update_data['field_id_'.$key] = $row[$field_row];
	    		}
	    		$channel_titles_update['title'] = $title;
	    		$this->EE->data_import_model->update('channel_data', $where, $update_data);
	    		$this->EE->data_import_model->update('channel_titles', $where, $channel_titles_update);
	    	}
    	}
	}
	
	private function _get_remote_data($row)
	{
		if(strstr($this->settings['match_key'], 'matrix'))
		{
			$fid = substr($this->settings['match_key'], strpos($this->settings['match_key'], 'matrix')+6);
		
			$key_data = $this->EE->data_import_model->get_row('matrix_data', array('col_id_'.$fid=>$row[$this->settings['assigned_fields']['matrix'][$fid]]));
		}
		
		elseif(strstr($this->settings['match_key'], 'store'))
		{
			$fid = substr($this->settings['match_key'], strpos($this->settings['match_key'], 'store')+5);
			
			switch ($fid) {
				case 'sku':
				case 'stock_level':
					$key_data = $this->EE->data_import_model->get_row('store_stock', array($fid=>$row[$this->settings['assigned_fields']['store'][$fid]]));
					break;
				case 'regular_price':
				case 'sale_price':
					$key_data = $this->EE->data_import_model->get_row('store_products', array($fid=>$row[$this->settings['assigned_fields']['store'][$fid]]));
					break;

				default:
					break;
			}
		}

		else 
		$key_data = $this->EE->data_import_model->get_row('channel_data', array('field_id_'.$this->settings['match_key']=>$row[$this->settings['assigned_fields']['matrix'][$this->settings['match_key']]]));
		if( ! $key_data)
		{
			return $this->_create_entry($row);
		}

		return $key_data['entry_id'];
	}
	
	private function _create_entry($row)
	{
		if($this->settings['create_if_not_exists'] == 'N' ) return false;
		$this->EE->load->library('api');
		$this->EE->api->instantiate('channel_entries');
		$this->EE->api->instantiate('channel_fields');
		$data = array(
		        'title'    => 'Temp title',
		        'entry_date'    => mktime(),
		);	
		$this->EE->api_channel_fields->setup_entry_settings($this->settings['channel'], $data);

		if ($this->EE->api_channel_entries->submit_new_entry($this->settings['channel'], $data) === FALSE)
		{
			show_error('An Error Occurred Creating the Entry');
		}
		$entry_id = $this->EE->api_channel_entries->entry_id;
		// create matrix entry
		if($this->EE->db->table_exists('matrix_data'))
		{
			$this->EE->data_import_model->insert('matrix_data', array('entry_id'=>$entry_id));
		}
		// create store entry
		if($this->EE->db->table_exists('store_stock'))
		{
			$this->EE->data_import_model->insert('store_stock', array('sku'=>rand(),'entry_id'=>$entry_id));
		}
		return $entry_id;
	}
}
/* End of file mod.data_import.php */
/* Location: /system/expressionengine/third_party/data_import/mod.data_import.php */