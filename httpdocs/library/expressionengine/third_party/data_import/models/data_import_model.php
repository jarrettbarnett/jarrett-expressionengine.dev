<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Data_import_model extends Db_lib
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_channels()
	{
		$data = $this->get('','channels');
		$ret  = array("" => $this->lang->line('select_channel'));
		foreach ($data as $row) 
		{
			$ret[$row['channel_id']] = $row['channel_title'];
		}
		asort($ret);
		return $ret;
	}
	
	public function get_channel_fields($channel_id)
	{
		$options['where'] = array('channel_id' => $channel_id);
		$channel_data = $this->get_row($options, 'channels');
		$options['where'] = array('group_id' => $channel_data['field_group']);
		$data = $this->get($options,'channel_fields');

		foreach ($data as $row) 
		{
			if($row['field_type'] == 'matrix')
			{
				$ret[$row['field_type']] = $this->get_matrix_fields($row['field_id']);
				continue;
			}
			if($row['field_type'] == 'store')
			{
				$ret[$row['field_type']] = $this->get_store_fields($row['field_id']);
				continue;
			}
			$ret[$row['field_id']] = $row['field_label'];
		}

		asort($ret);
		return $ret;
	}
	
	public function get_matrix_field_id($col_id)
	{
		$options['where'] = array('col_id' => $col_id);
		$data = $this->get_row($options, 'matrix_cols');
		return @$data['field_id'];
	}
	
	public function get_matrix_fields($field_id)
	{
		$options['where'] = array('field_id' => $field_id);
		$data = $this->get_matrix_cols($options);
		foreach ($data as $row) 
		{
			$ret[$row['col_id']] = $row['col_label'];
		}
		asort($ret);
		return $ret;
	}
	
	public function get_store_fields($field_id)
	{
		return array(
			'sku' => 'sku',
			'stock_level' => 'stock_level',
			'regular_price' => 'regular_price',
			'sale_price' => 'sale_price',
		);
	}
	
	public function get_table_keys($table)
	{
		$fields = $this->db->list_fields($table);
		sort($fields);
		foreach ($fields as $f) 
		{
			$ret[$f] = $f;
		}
		
		return $ret;
	}
}

/* End of file data_import_model.php */