<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Data_import_remote_model extends CI_Model
{
	public $rdb;
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public function connect($vars)
	{
		if( ! @$vars['username'] or ! @$vars['hostname']) return false;
		
		$dsn = "mysql://$vars[username]:$vars[password]@$vars[hostname]/$vars[database]?db_debug=0";
		$this->rdb = '';
		$this->rdb =& $this->load->database($dsn, true);
		$this->rdb->dbprefix = $vars['dbprefix'];
		if( ! $this->rdb->conn_id) return false;
		
		return true;
	}
	
	public function get_from($table, $where=array())
	{
		return  $this->rdb->from($table)->where($where)->get()->result_array();
	}
	
	public function table_exists($table)
	{
		return $this->rdb->table_exists($table);
	}
	
	public function get_tables()
	{
		$tables = $this->rdb->list_tables();
		foreach ($tables as $t) 
		{
			$ret[$t] = $t;
		}
		
		return $ret;
	}
	
	public function get_table_keys($table)
	{
		if( ! $this->table_exists($table)) return array();
		$fields = $this->rdb->list_fields($table);
		sort($fields);
		foreach ($fields as $f) 
		{
			$ret[$f] = $f;
		}
		
		return $ret;
	}
			
	public function get($table, $where=array())
	{
		return  $this->rdb->from($table)->where($where)->get()->result_array();
	}
	
	public function get_row($table, $where=array())
	{
		return  $this->rdb->from($table)->where($where)->get()->row_array();
	}
}

/* End of file data_import_remote_model.php */