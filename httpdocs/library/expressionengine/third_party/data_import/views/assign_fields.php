<?php
if($channel_fields)
{	
	$this->table->clear();
	$this->table->set_template($cp_table_template);
	$this->table->set_heading(
		array('data' => lang('field_label')),
		array('data' => lang('match_key'), 'width'=>'5%'),
		array('data' => lang('remote_table_field')));

	foreach ($channel_fields as $key => $field) 
	{
		if(is_array($field))
		{
			$table = "<table border=0 class='mainTable' style='width:40%'>
			<thead>
				<tr>
					<th>".lang('field_label')."</th>
					<th>".lang('match_key')."</th>
					<th>".lang('remote_table_field')."</th>
				</tr>
			</thead>";
			foreach ($field as $key1 => $field1)
			{
					$table .= "<tr><td>{$field1}</td><td>".form_radio("match_key", $key.$key1,  ($match_key == $key.$key1))."</td><td>".form_dropdown("assigned_fields[{$key}][{$key1}]", $remote_table_keys, @$assigned_fields[$key][$key1])."</td></tr>";
			}
			$table .= "</table>";
			$this->table->add_row(
		        $key,
		        '', 
		        $table
		    );
		    continue;			
		}
		$this->table->add_row(
	        $field, 
	        form_radio("match_key", $key,  ($match_key == $key)), 
	        form_dropdown("assigned_fields[{$key}]", $remote_table_keys, @$assigned_fields[$key])
	    );		
	}
   
	echo $this->table->generate();
}
?>