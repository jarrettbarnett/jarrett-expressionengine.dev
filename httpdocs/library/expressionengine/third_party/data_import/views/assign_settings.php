<?= form_open($action_url.AMP.'method=settings'); ?>
<?= form_hidden('import_id', $import_id); ?>
<?php
	$this->table->clear();
	$this->table->set_template($cp_table_template);
	$this->table->set_heading(
		array('data' => lang('preference'), 'width' => "40%"),
		array('data' => ''));

		$this->table->add_row(
	        $this->lang->line('tables'), 
	        form_dropdown('remote_table', $remote_tables, $remote_table)
	    );
	    
	    $this->table->add_row(
	        $this->lang->line('channel'), 
	        array('data' => form_dropdown('channel', $channels, $channel))
	    );
	    
	    $this->table->add_row(
	        $this->lang->line('create_if_not_exists'), 
	        lang('yes').NBS. form_radio('create_if_not_exists', 'Y', $create_if_not_exists == 'Y').NBS. lang('no').NBS.
	        form_radio('create_if_not_exists', 'N', $create_if_not_exists == 'N') 
	    );	    
	    $this->table->add_row(
	        $this->lang->line('new_entry_title'), 
	        form_input('new_entry_title', $new_entry_title) 
	    );
	    
	echo $this->table->generate();
?>
<div id='assign_fields'><?php echo $view_assign_fields ?></div>
<div style="text-align: center;">
	<?= form_submit(array('name' => 'submit', 'value' => lang('submit'), 'class' => 'submit')); ?>
</div>
<?= form_close(); ?>