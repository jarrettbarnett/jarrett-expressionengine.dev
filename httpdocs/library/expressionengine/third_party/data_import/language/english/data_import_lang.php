<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(
	'import' 								=> 'Import',
	'channel' 								=> '<label for=tables>Channel</label>',
	'tables' 								=> '<label for=tables>Remote Tables</label><br>Select a table where is field with the same value for both sides',
	'table_keys'							=> '<label for=key>Match by field</label><br>Select a fields with the same value for both sides',
	'import_tables'							=> '<label for=key>Import Tables</label><br>Select tables for import(each table must have entry_id)',
	'new_entry_title'							=> '<label for=new_entry_title>New Entry\'s Title</label><br>Use {Field Lablel} in the title template(eg: New product {Some Field})',
	'select_key'							=> '- Select a key -',
	'database_settings'						=> 'Remote DB settings',
	'hostname'								=> 'Hostname',
	'username'								=> 'Username',
	'password'								=> 'Password',
	'database'								=> 'Database',
	'dbprefix'								=> 'Dbprefix',
	'local'									=> 'Local',
	'remote'								=> 'Remote',
	'loading'								=> 'Loading...',
	'field_label'							=> 'Field Lablel',
	'remote_table_field'					=> 'Remote Table Field',
	'module_home'							=> 'Remote Table Field',
	'data_import_module_name' 				=>	'Data Import',
	'data_import_usage' 					=>	'Data Import Usage',
	'match_key' 							=>	'Key',
	'create_if_not_exists' 					=>	'<b>Create new entry if record doesn\'t exist</b>',
	'data_import_settings' 					=>	'Data import settings',
	'select_channel' 						=>	'- Select a channel -',
	'new_import_item' 						=>	'New Import Item',
	'import_item_saved' 					=>	'Import Item has been saved',
	'import_item_deleted' 					=>	'Import Item has been deleted',
	'import_list' 							=>	'Import list',

	'data_import_module_description' => 
	'Imports data from remote host',	
	//errors
	'invalid_database_settings'				=> 'Invalid database settings. Please check it and try again',
);

/* End of file store_vendor_notification_lang.php */