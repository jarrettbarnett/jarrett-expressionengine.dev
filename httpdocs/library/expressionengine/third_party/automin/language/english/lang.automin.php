<?php

$lang['automin_enabled'] = 'Enable AutoMin?';
$lang['automin_module_description'] = 'Automatic CSS, Javascript, and LESS compilation and caching';
$lang['automin_module_name'] = 'AutoMin';
$lang['automin_preferences'] = 'AutoMin Preferences';
$lang['cache_enabled'] = 'Enable Caching?';
$lang['cache_server_path'] = 'Server Path to Cache Directory';
$lang['cache_server_path_instructions'] = 'Something like: /var/www/vhosts/yourdomain.com/httpdocs/automin/<br><br>Note: you <strong>must</strong> create this folder and give it writable permissions';
$lang['cache_url'] = 'URL to Cache Directory';
$lang['cache_url_instructions'] = 'Something like: /automin/<br><br>Used when creating the file path for AutoMin\'s output.';
$lang['compress_markup'] = 'Compress HTML Markup?';
$lang['compress_markup_instructions'] = 'Requires that you install the CodeIgniter hook. See the installation guide for details.';
$lang['preferences_saved'] = 'AutoMin preferences have been saved successfully';
$lang['setting'] = 'Setting';
$lang['value'] = 'Value';
$lang['welcome'] = 'Welcome to AutoMin';
