<?php   
$lang = array(
	// EWAY ERRORS & CONTENT
	
	'eway_title' => 'eWay.au Payment Gateway',
	'eway_overview' => "<p>eWAY provides a leading payment gateway solution for processing payments in real time. eWAY’s high level of standards in technology, innovations and customer care has helped them become Australia’s award winning payment gateway.</p>",
	'eway_customer_id'	=> 'Customer ID',
	'eway_payment_method' => 'Payment Method',
	'eway_test_mode'	=> 'Test Mode?',
	'eway_cant_connect' => "Can't connect using using cURL",
	'eway_transaction_error' => "Transaction Error: ",
	'eway_invalid_response' => "Error: An invalid response was recieved from the payment gateway."
);