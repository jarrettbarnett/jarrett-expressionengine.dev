<?php   
$lang = array(
		

		//PAYPAL EXPRESS ERRORS & CONTENT
		'paypal_express_title' => 'PayPal',
		'paypal_express_sandbox_mode' => 'Sandbox Mode?',
		'paypal_express_signature' => 'Signature',
		'paypal_express_api_password' => 'API Password <a href="https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_ECAPICredentials">(obtaining API credentials...)</a>',
		'paypal_express_api_username' => 'API Username',
		'paypal_express_sandbox_signature' => 'Sandbox Signature',
		'paypal_express_sandbox_api_password' => 'Sandbox API Password',
		'paypal_express_sandbox_api_username' => 'Sandbox API Username',
		
		'paypal_express_overview' => '<p>PayPal Express allows you to take PayPal and Credit Card payments and receive instant notifcation of success or failure, making it an attractive option for selling digital and downloadable goods, or merchandise that needs to be immediately shipped. Using PayPal Express requires that you have a verified PayPal Business account. If you have not upgraded your personal account to a business account, you can do so at the following URL: <a href="https://www.paypal.com/us/cgi-bin/webscr?cmd=_product-go&product=additional_payment">Upgrade to a business account here.</a></p> 
		<p>Please note: PayPal only displays shipping addresses on their payment capture page. If a shipping address is captured on your site, it will be sent to PayPal. If a shipping address is not captured on your site, the billing address will be assumed to be the shipping address and will be sent to PayPal instead.</p>
		',
		'paypal_express_affiliate' => '<p><A HREF="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A" target="_blank"><IMG  SRC="http://images.paypal.com/en_US/i/bnr/paypal_mrb_banner.gif" BORDER="0" ALT="Sign up for PayPal and start accepting credit card payments instantly."></A></p><p><a href="https://www.paypal.com/us/mrb/pal=FXG36C7JSD58A">Click here to sign up now!</a></p>', 
		'paypal_express_payment_action'	=> 'Payment Action Type',
		'paypal_express_did_not_respond'	=> 'PayPal did not respond',
		'paypal_express_you_cancelled'	=> 'You cancelled your payment at PayPal',
		'paypal_express_advanced_settings_header'	=> 'Advanced settings. Only edit these settings if there is a problem.',
		'paypal_express_shipping_settings'	=> 'Shipping settings.',
		'paypal_express_no_shipping_note'		=> '*customers may receive the following error: "Paypal does not allow your country of residence to ship to the country you wish to" when using the setting marked with an asterisk. Unless you are attempting to restrict shipments only to customers based in your specific country, do not use this setting. Choose instead to allow customer to edit shipping address, or do not show shipping address at all. ',
		'paypal_sole'	=> 'Buyer does not need to create a PayPal account to check out. PayPal referrs to this as "PayPal Account Optional"', 
		'paypal_mark'	=> 'Buyer must have a PayPal account to check out.',
		'paypal_account_preferences'	=> 'PayPal customer account preferences',
		'paypal_express_allow_note'		=> 'Allow customer to leave a note at PayPal? ',
		'paypal_express_allow_note_note'		=> 'Customer notes left at PayPal are not stored by CartThrob. They are stored with order data in your PayPal account.',
		'paypal_express_show_item_id'	=> 'Show items entry_id in line item summary at PayPal?',
		'paypal_express_show_item_options'	=> 'Show item options in line item summary at PayPal?',
		'paypal_express_hide_shipping_address'	=> 'Do not show shipping address at PayPal',
		'paypal_express_editable_shipping'		=> 'Show editable shipping address at PayPal',
		'paypal_express_static_shipping'		=> '*Show non-editable shipping address at PayPal',
		'paypal_express_paypal_shipping'		=> 'Let PayPal handle shipping addresses',
   );