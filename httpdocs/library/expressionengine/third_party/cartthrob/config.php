<?php
if ( ! defined('CARTTHROB_VERSION'))
{
	define('CARTTHROB_VERSION', '2.1.1');
}
$config['name'] = 'CartThrob';
$config['version'] = CARTTHROB_VERSION;
$config['nsm_addon_updater']['versions_xml'] = 'http://cartthrob.com/versions.xml';