<?php

/*
=====================================================
Stand-alone member register
-----------------------------------------------------
 http://www.intoeetive.com/
-----------------------------------------------------
 Copyright (c) 2010-2011 Yuri Salimovskiy
=====================================================
 This software is based upon and derived from
 ExpressionEngine software protected under
 copyright dated 2004 - 2010. Please see
 http://expressionengine.com/docs/license.html
=====================================================
 File: mod.sa_member_register.php
-----------------------------------------------------
 Purpose: Display members registration form within EE templates
=====================================================
*/


if ( ! defined('EXT'))
{
    exit('Invalid file request');
}


class Sa_member_register {

    var $return_data	= ''; 						// Bah!

    /** ----------------------------------------
    /**  Constructor
    /** ----------------------------------------*/

    function __construct()
    {        
    	$this->EE =& get_instance(); 
    	$this->EE->lang->loadfile('member');
    	$this->EE->lang->loadfile('sa_member_register');
    }
    /* END */
    

	/** -----------------------------------------
    /**  USAGE: Display the form
    /** -----------------------------------------*/
    
    function form()
    {

        $this->EE =& get_instance(); 
    
        $TMPL = $this->EE->TMPL;
        $DB = $this->EE->db;
        $PREFS = $this->EE->config;
        $FNS = $this->EE->functions;
        $LANG = $this->EE->lang;
        $SESS = $this->EE->session;
        $OUT = $this->EE->output;
		
		if ( $PREFS->item('allow_member_registration') != 'y' )
		{
			return $OUT->show_user_error('general', array($LANG->line('mbr_registration_not_allowed')));
		}
        
		if ( $SESS->userdata('member_id') != 0)
		{ 
            return $OUT->show_user_error('general', array($LANG->line('mbr_you_are_registered')));
		}

        $cond['captcha'] = ($PREFS->item('use_membership_captcha') == 'y') ? TRUE : FALSE;
        
		$tagdata = $FNS->prep_conditionals( $TMPL->tagdata, $cond );
        
        if ( $cond['captcha']===TRUE )
		{
			$tagdata = $TMPL->swap_var_single('captcha', $FNS->create_captcha(), $tagdata);
		}
        
        $query = $DB->query("SELECT * FROM  exp_member_fields WHERE m_field_reg = 'y' ORDER BY m_field_order");
        $row_chunk = '';
        $row_inner = '';
        
        preg_match_all("/".LD."custom_fields".RD."(.*?)".LD."\/custom_fields".RD."/s", $tagdata, $custom_fields);

        foreach($custom_fields[0] as $row_key => $row_tag)
        {
        	$row_chunk = $custom_fields[0][$row_key];
        
        	$row_chunk_content = $custom_fields[1][$row_key];
        
        	$row_inner = '';
        
        	// loop over the row_data
        	foreach ($query->result_array() as $row)
        	{
        	   
                $row_template = $row_chunk_content;
                $cond = array();
                
                $row_template = $TMPL->swap_var_single('field_name', "m_field_id_".$row['m_field_id'], $row_template);

                $row_template = $TMPL->swap_var_single('field_label', $row['m_field_label'], $row_template);
                
                $cond['field_description'] = ($row['m_field_description'] != '') ? TRUE : FALSE;
                $row_template = $TMPL->swap_var_single('field_description', $row['m_field_description'], $row_template);
                
                $cond['required'] = ($row['m_field_required'] == 'y') ? TRUE : FALSE;        

                $width = ( ! stristr($row['m_field_width'], 'px')  AND strpos($row['m_field_width'], '%') === FALSE) ? $row['m_field_width'].'px' : $row['m_field_width'];
                $row_template = $TMPL->swap_var_single('width', $width, $row_template);
                
                $cond['textarea'] = ($row['m_field_type'] == 'textarea') ? TRUE : FALSE;   
                $rows = ( ! isset($row['m_field_ta_rows'])) ? '10' : $row['m_field_ta_rows']; 
                $row_template = $TMPL->swap_var_single('rows', $rows, $row_template);
                
                $cond['text'] = ($row['m_field_type'] == 'text') ? TRUE : FALSE;   
                $maxlength = ($row['m_field_maxl'] == 0) ? '100' : $row['m_field_maxl'];  
                $row_template = $TMPL->swap_var_single('maxlength', $maxlength, $row_template);
                
                $cond['select'] = ($row['m_field_type'] == 'select') ? TRUE : FALSE;   
                $select_list = trim($row['m_field_list_items']);
                $options = '';
                foreach (explode("\n", $select_list) as $v)
                {   
                    $v = trim($v);
                    $options .= "<option value=\"$v\">$v</option>\n";
                }
                $row_template = $TMPL->swap_var_single('options', $options, $row_template);

                $row_template = $FNS->prep_conditionals($row_template, $cond);
        		
        		$row_inner .= $row_template;
            }  
         }

         $tagdata = str_replace($row_chunk, $row_inner, $tagdata);
        
        
        
        $return = ($TMPL->fetch_param('return')=='')?$FNS->fetch_site_index():(strpos($TMPL->fetch_param('return'), "http://")!==FALSE || strpos($TMPL->fetch_param('return'), "https://")!==FALSE)?$TMPL->fetch_param('return'):$FNS->create_url($TMPL->fetch_param('return'));

                
        $data['hidden_fields'] = array(
										'ACT'	=> $FNS->fetch_action_id('Sa_member_register', 'process'),
										'RET'	=> $return
									  );        
        if ($TMPL->fetch_param('ajax')=='yes') { $data['hidden_fields']['ajax'] = 'y'; }                                          
        if ($PREFS->item('secure_forms') == 'y') { $data['hidden_fields']['XID']=$FNS->add_form_security_hash('{XID_HASH}'); }
		$data['id']		= ($TMPL->fetch_param('id')!='') ? $TMPL->fetch_param('id') : 'sa_member_register_form';
        $data['name']		= ($TMPL->fetch_param('name')!='') ? $TMPL->fetch_param('name') : 'sa_member_register_form';
        $data['class']		= ($TMPL->fetch_param('class')!='') ? $TMPL->fetch_param('class') : 'sa_member_register_form';

        $out = $FNS->form_declaration($data).$tagdata."\n"."</form>";
        
        if ($TMPL->fetch_param('ajax')=='yes') 
        {
        
        $post_process_a = ($this->EE->TMPL->fetch_param('post_process')!='') ? explode("|", $this->EE->TMPL->fetch_param('post_process')) : array();    
        $out = $this->EE->TMPL->swap_var_single('error_container', '<div id="sa_member_register_error_container" style="display: none"></div>', $out); 
        $has_loader = preg_match_all("/".LD."loader".RD."(.*?)".LD."\/loader".RD."/s", $out, $loader);
        if ($has_loader > 0)
        {
            $out = str_replace($loader[0][0], "<div id=\"sa_member_register_loader\" class=\"sa_member_register_loader\" style=\"display: none\">".$loader[1][0]."</div>", $out);
        }
        
        $out .= "<script type=\"text/javascript\">
$(document).ready(function(){
    $('#".$data['id']."').live('submit', function(event){
        event.preventDefault();
        $('#sa_member_register_error_container').hide();
        ";
        if ($has_loader > 0)
        {
            $out .= "$('#sa_member_register_loader').show();";
        }
        $out .= "
        $.post(
            '/?ACT=".$data['hidden_fields']['ACT']."',
            $('#".$data['id']."').serialize(),
            function(msg) {
                ";
        if ($has_loader > 0)
        {
            $out .= "$('#sa_member_register_loader').hide();";
        }
        $out .= "
                if (msg.indexOf('".$this->EE->lang->line('mbr_registration_complete')."') >= 0)
                {
                    
                ";
        if ($this->EE->TMPL->fetch_param('return')!='')
        {
            $out .= "
                    $.get('".$this->EE->functions->create_url($this->EE->TMPL->fetch_param('return'))."', 
                    function(ret){
                        $('#".$data['id']."').replaceWith(ret);
                    });";
        }
        else
        {
            $out .= "
                    var out = '' + /<body>[\s\S]*<\/body>/.exec(msg);
                    $('#".$data['id']."').replaceWith(out);
                    ";
        }
        foreach ($post_process_a as $post_process)
        {
            $out .= "
                    ".$post_process."();
                    ";
        }
        $out .= "
                    
                } else {
                    var out = /<ul>[\s\S]*<\/ul>/.exec(msg);
                    $('#sa_member_register_error_container').html(''+out);
                    $('#sa_member_register_error_container').show();
                }
                return false;
            }
        );
        return false;
    });
});
</script>";
        }

        return $out;
	}
    /* END */
    



    
    /** -----------------------------------------
    /**  USAGE: process registration
    /** -----------------------------------------*/
    
    function process()
    { 
        if ( ! class_exists('Member'))
    	{
    		require_once PATH_MOD.'member/mod.member.php';
    	}
        
        if ( ! class_exists('Member_register'))
    	{
    		require_once PATH_MOD.'member/mod.member_register.php';
    	}
    	
    	$MR = new Member_register();
    	
    	foreach(get_object_vars($this) as $key => $value)
		{
			$MR->{$key} = $value;
		}
    	
    	$MR->register_member();
    }
    /* END */
    
    

}
/* END */
?>