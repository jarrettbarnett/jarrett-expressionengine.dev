<?php

/*
=====================================================
Stand-alone member register
-----------------------------------------------------
 http://www.intoeetive.com/
-----------------------------------------------------
 Copyright (c) 2010-2011 Yuri Salimovskiy
=====================================================
 This software is based upon and derived from
 ExpressionEngine software protected under
 copyright dated 2004 - 2010. Please see
 http://expressionengine.com/docs/license.html
=====================================================
 File: mcp.sa_member_register.php
-----------------------------------------------------
 Purpose: Display members registration form within EE templates
=====================================================
*/

if ( ! defined('EXT'))
{
    exit('Invalid file request');
}



class Sa_member_register_mcp {

    var $version = '2.3';
    
    function __construct() { 
        // Make a local reference to the ExpressionEngine super object 
        $this->EE =& get_instance(); 
    } 

}
/* END */
?>