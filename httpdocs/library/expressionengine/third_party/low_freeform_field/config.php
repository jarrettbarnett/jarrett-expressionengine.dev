<?php

/**
* Low Freeform Field config file
*
* @package		low-freeform-field
* @author		Lodewijk Schutte ~ Low <low@loweblog.com>
* @link			http://github.com/lodewijk/low_freeform_field
* @copyright	Copyright (c) 2010, Low
*/

if ( ! defined('LOW_FFF_NAME'))
{
	define('LOW_FFF_NAME',         'Low Freeform Field');
	define('LOW_FFF_VERSION',      '1.0.1');
	define('LOW_FFF_DOCS',         'http://github.com/lodewijk/low_freeform_field');
}

$config['name']    = LOW_FFF_NAME;
$config['version'] = LOW_FFF_VERSION;

// No support just yet
// $config['nsm_addon_updater']['versions_xml'] = LOW_REORDER_DOCS.'feed/';
