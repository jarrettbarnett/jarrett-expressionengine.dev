# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.25)
# Database: nerdery.loc
# Generation Time: 2012-11-01 20:53:40 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ee2_accessories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_accessories`;

CREATE TABLE `ee2_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_accessories` WRITE;
/*!40000 ALTER TABLE `ee2_accessories` DISABLE KEYS */;

INSERT INTO `ee2_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`)
VALUES
	(4,'Editee_acc','1|5','myaccount|addons_extensions|tools_utilities|tools_communicate|design|content_files|members|tools|addons_plugins|homepage|content|tools_data|addons_accessories|addons_fieldtypes|addons_modules|tools_logs|content_edit|content_publish|content_files_modal|admin_content|admin_system|addons','1.0.5'),
	(11,'Blueprints_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.3.7.4'),
	(10,'Structure_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','3.2.3'),
	(14,'Template_variables_acc','1','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.2'),
	(7,'Cp_logo_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.1'),
	(13,'Draggable_acc','1|5|6|7|8','addons_fieldtypes|addons|addons_accessories|addons_extensions|admin_content|admin_system|content|content_edit|addons_modules|addons_plugins|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','1.3'),
	(12,'Low_nospam_acc','1|5','addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities','2.2.1');

/*!40000 ALTER TABLE `ee2_accessories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_actions`;

CREATE TABLE `ee2_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_actions` WRITE;
/*!40000 ALTER TABLE `ee2_actions` DISABLE KEYS */;

INSERT INTO `ee2_actions` (`action_id`, `class`, `method`)
VALUES
	(1,'Comment','insert_new_comment'),
	(2,'Comment_mcp','delete_comment_notification'),
	(3,'Comment','comment_subscribe'),
	(4,'Comment','edit_comment'),
	(5,'Email','send_email'),
	(6,'Safecracker','submit_entry'),
	(7,'Safecracker','combo_loader'),
	(8,'Search','do_search'),
	(9,'Channel','insert_new_entry'),
	(10,'Channel','filemanager_endpoint'),
	(11,'Channel','smiley_pop'),
	(12,'Member','registration_form'),
	(13,'Member','register_member'),
	(14,'Member','activate_member'),
	(15,'Member','member_login'),
	(16,'Member','member_logout'),
	(17,'Member','retrieve_password'),
	(18,'Member','reset_password'),
	(19,'Member','send_member_email'),
	(20,'Member','update_un_pw'),
	(21,'Member','member_search'),
	(22,'Member','member_delete'),
	(54,'Structure','ajax_move_set_data'),
	(37,'Playa_mcp','filter_entries'),
	(65,'Zoo_flexible_admin','ajax_copy_tree'),
	(64,'Zoo_flexible_admin','ajax_remove_tree'),
	(63,'Zoo_flexible_admin','ajax_save_tree'),
	(62,'Zoo_flexible_admin','ajax_load_settings'),
	(61,'Zoo_flexible_admin','ajax_load_tree'),
	(60,'Zoo_flexible_admin','ajax_preview'),
	(55,'Minicp','search'),
	(56,'Store','act_download_file'),
	(57,'Store','act_add_to_cart'),
	(58,'Store','act_field_stock'),
	(59,'Store','act_payment_return'),
	(66,'Freeform','insert_new_entry'),
	(67,'Freeform','retrieve_entries'),
	(68,'Freeform','delete_freeform_notification');

/*!40000 ALTER TABLE `ee2_actions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_automin_preferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_automin_preferences`;

CREATE TABLE `ee2_automin_preferences` (
  `row_id` int(10) NOT NULL,
  `automin_enabled` varchar(1) NOT NULL,
  `cache_enabled` varchar(1) NOT NULL,
  `compress_markup` varchar(1) NOT NULL,
  `cache_server_path` varchar(255) NOT NULL,
  `cache_url` varchar(255) NOT NULL,
  PRIMARY KEY (`row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_automin_preferences` WRITE;
/*!40000 ALTER TABLE `ee2_automin_preferences` DISABLE KEYS */;

INSERT INTO `ee2_automin_preferences` (`row_id`, `automin_enabled`, `cache_enabled`, `compress_markup`, `cache_server_path`, `cache_url`)
VALUES
	(0,'y','y','y','/var/www/vhosts/ee2.pleaseproof.com/httpdocs/cache/minified/','/cache/minified/');

/*!40000 ALTER TABLE `ee2_automin_preferences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_captcha
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_captcha`;

CREATE TABLE `ee2_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_cartthrob_cart
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_cart`;

CREATE TABLE `ee2_cartthrob_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart` text,
  `timestamp` int(11) DEFAULT '0',
  `url` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_cartthrob_cart` WRITE;
/*!40000 ALTER TABLE `ee2_cartthrob_cart` DISABLE KEYS */;

INSERT INTO `ee2_cartthrob_cart` (`id`, `cart`, `timestamp`, `url`)
VALUES
	(12,'lIjQgttc285Ej01lzl9A8989XJ4Hp5YT918o/DGhEp4HzlVZXMOUjnrHhPceG2BSAzUQQ0JZbBOkpi0iYsgetg==',1339091253,NULL),
	(11,'jEw6lbiI3SKnwxodDmeXgSU2+SXTOFzm89A00iVW0IPto/8j8gr20MGeWXcubgMeB+aEIjYYC4prOMDQM8bYOw==',1338395127,NULL),
	(9,'9RWYA2TqMMJbEQ5j/XmPGqutFxoNdicM2LmU82xpdn+my4pDMuAhjJDhqufykGqyzkPaMfuktLcVx9xCwPgwZA==',1336412183,NULL),
	(10,'75V2vDTNIiNsSNOykh0ITPhCVMdSJP3GA8s5jvr7hr8GM8i4g64SeOW0tFv/1t3tDk5AGd4+8qEntEiLcKeIqQ==',1336413046,NULL),
	(13,'pvngfumF8mlXukvB/A5H90BVSxJCbEWPXD5Y+/JxEbWHvhDTh8cJDbbgUTFSSY9GSIfOgyCAiwSNPKCtRlF78w==',1351401367,NULL),
	(14,'zK6B9Ff1u/dqndqXTbB3tWpi+2+9eW2BVYu5NXLn32QqexNR5zhkRVqiYws8FVOZSZzyAEIK90+Vl+JD1BnC0w==',1351542710,NULL);

/*!40000 ALTER TABLE `ee2_cartthrob_cart` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_cartthrob_order_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_order_items`;

CREATE TABLE `ee2_cartthrob_order_items` (
  `row_id` int(10) NOT NULL AUTO_INCREMENT,
  `row_order` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `quantity` varchar(10) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `weight` varchar(100) DEFAULT NULL,
  `shipping` varchar(100) DEFAULT NULL,
  `no_tax` tinyint(1) DEFAULT '0',
  `no_shipping` tinyint(1) DEFAULT '0',
  `extra` text,
  `price_plus_tax` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `order_id` (`order_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_cartthrob_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_permissions`;

CREATE TABLE `ee2_cartthrob_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT NULL,
  `sub_id` varchar(100) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `item_id` int(10) DEFAULT NULL,
  `permission` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_cartthrob_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_sessions`;

CREATE TABLE `ee2_cartthrob_sessions` (
  `session_id` varchar(32) NOT NULL,
  `cart_id` int(10) DEFAULT NULL,
  `fingerprint` varchar(40) DEFAULT '',
  `expires` int(11) DEFAULT '0',
  `member_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`session_id`),
  KEY `cart_id` (`cart_id`),
  KEY `fingerprint` (`fingerprint`),
  KEY `expires` (`expires`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_cartthrob_sessions` WRITE;
/*!40000 ALTER TABLE `ee2_cartthrob_sessions` DISABLE KEYS */;

INSERT INTO `ee2_cartthrob_sessions` (`session_id`, `cart_id`, `fingerprint`, `expires`, `member_id`)
VALUES
	('03696620cd6b2f0272bb0db38cbcc589',12,'fbbebd8e4b1ed34f27bb91d22ac3edfa5f1ac878',1339696053,2),
	('f44b995434a95b6744654b8a854d5ec6',13,'9ab1226b48a03b195ced8221cf82776c55e18357',1352006167,2),
	('763af1ebb7530d38a500d401222bb915',14,'9ab1226b48a03b195ced8221cf82776c55e18357',1352147510,2);

/*!40000 ALTER TABLE `ee2_cartthrob_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_cartthrob_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_settings`;

CREATE TABLE `ee2_cartthrob_settings` (
  `site_id` int(4) DEFAULT '1',
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  `serialized` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_cartthrob_settings` WRITE;
/*!40000 ALTER TABLE `ee2_cartthrob_settings` DISABLE KEYS */;

INSERT INTO `ee2_cartthrob_settings` (`site_id`, `key`, `value`, `serialized`)
VALUES
	(1,'last_order_number','0',0),
	(1,'notifications','a:5:{i:0;a:9:{s:13:\"email_subject\";s:25:\"Thank you for your order.\";s:15:\"email_from_name\";s:11:\"{site_name}\";s:10:\"email_from\";s:28:\"developer@jarrettbarnett.com\";s:8:\"email_to\";s:16:\"{customer_email}\";s:14:\"email_template\";s:14:\"email/customer\";s:11:\"email_event\";s:9:\"completed\";s:12:\"status_start\";s:3:\"ANY\";s:10:\"status_end\";s:3:\"ANY\";s:10:\"email_type\";s:4:\"html\";}i:1;a:9:{s:13:\"email_subject\";s:25:\"An order has been placed.\";s:15:\"email_from_name\";s:15:\"{customer_name}\";s:10:\"email_from\";s:16:\"{customer_email}\";s:8:\"email_to\";s:18:\"you@yourdomain.com\";s:14:\"email_template\";s:11:\"email/admin\";s:11:\"email_event\";s:9:\"completed\";s:12:\"status_start\";s:3:\"ANY\";s:10:\"status_end\";s:3:\"ANY\";s:10:\"email_type\";s:4:\"html\";}i:3;a:9:{s:13:\"email_subject\";s:30:\"Order status has been updated.\";s:15:\"email_from_name\";s:11:\"Store Owner\";s:10:\"email_from\";s:28:\"developer@jarrettbarnett.com\";s:8:\"email_to\";s:16:\"{customer_email}\";s:14:\"email_template\";s:18:\"email/statuschange\";s:11:\"email_event\";s:0:\"\";s:12:\"status_start\";s:3:\"ANY\";s:10:\"status_end\";s:3:\"ANY\";s:10:\"email_type\";s:4:\"html\";}i:4;a:9:{s:13:\"email_subject\";s:18:\"Low stock warning!\";s:15:\"email_from_name\";s:7:\"Jarrett\";s:10:\"email_from\";s:28:\"developer@jarrettbarnett.com\";s:8:\"email_to\";s:28:\"developer@jarrettbarnett.com\";s:14:\"email_template\";s:14:\"email/lowstock\";s:11:\"email_event\";s:9:\"low_stock\";s:12:\"status_start\";s:3:\"ANY\";s:10:\"status_end\";s:3:\"ANY\";s:10:\"email_type\";s:4:\"html\";}i:5;a:9:{s:13:\"email_subject\";s:22:\"Incomplete Transaction\";s:15:\"email_from_name\";s:7:\"Jarrett\";s:10:\"email_from\";s:28:\"developer@jarrettbarnett.com\";s:8:\"email_to\";s:28:\"developer@jarrettbarnett.com\";s:14:\"email_template\";s:14:\"email/lowstock\";s:11:\"email_event\";s:10:\"processing\";s:12:\"status_start\";s:3:\"ANY\";s:10:\"status_end\";s:3:\"ANY\";s:10:\"email_type\";s:4:\"html\";}}',1),
	(1,'license_number','jb',0),
	(1,'logged_in','0',0),
	(1,'default_member_id','',0),
	(1,'session_expire','604800',0),
	(1,'clear_cart_on_logout','1',0),
	(1,'clear_session_on_logout','0',0),
	(1,'allow_empty_cart_checkout','0',0),
	(1,'global_item_limit','0',0),
	(1,'enable_logging','0',0),
	(1,'cp_menu','0',0),
	(1,'cp_menu_label','Store',0),
	(1,'session_use_fingerprint','1',0),
	(1,'session_fingerprint_method','3',0),
	(1,'garbage_collection_cron','0',0),
	(1,'checkout_form_captcha','0',0),
	(1,'number_format_defaults_decimals','2',0),
	(1,'number_format_defaults_dec_point','.',0),
	(1,'number_format_defaults_thousands_sep',',',0),
	(1,'number_format_defaults_prefix','$',0),
	(1,'number_format_defaults_currency_code','USD',0),
	(1,'rounding_default','standard',0),
	(1,'default_location','a:8:{s:5:\"state\";s:2:\"CA\";s:3:\"zip\";s:5:\"95928\";s:12:\"country_code\";s:3:\"USA\";s:6:\"region\";s:5:\"Butte\";s:14:\"shipping_state\";s:2:\"CA\";s:12:\"shipping_zip\";s:5:\"95928\";s:21:\"shipping_country_code\";s:3:\"USA\";s:15:\"shipping_region\";s:5:\"Butte\";}',1),
	(1,'locales_countries','a:1:{i:0;s:3:\"USA\";}',1),
	(1,'product_channels','a:2:{i:0;s:1:\"7\";i:1;s:1:\"2\";}',1),
	(1,'product_channel_fields','a:2:{i:7;a:5:{s:5:\"price\";s:2:\"70\";s:8:\"shipping\";s:0:\"\";s:6:\"weight\";s:0:\"\";s:9:\"inventory\";s:0:\"\";s:12:\"global_price\";s:0:\"\";}i:2;a:5:{s:5:\"price\";s:1:\"2\";s:8:\"shipping\";s:1:\"5\";s:6:\"weight\";s:1:\"6\";s:9:\"inventory\";s:1:\"7\";s:12:\"global_price\";s:0:\"\";}}',1),
	(1,'coupon_code_field','title',0),
	(1,'coupon_code_channel','3',0),
	(1,'coupon_code_type','20',0),
	(1,'discount_channel','6',0),
	(1,'discount_type','67',0),
	(1,'save_orders','1',0),
	(1,'orders_channel','4',0),
	(1,'orders_items_field','21',0),
	(1,'orders_total_field','22',0),
	(1,'orders_subtotal_field','23',0),
	(1,'orders_subtotal_plus_tax_field','24',0),
	(1,'orders_shipping_field','25',0),
	(1,'orders_shipping_plus_tax_field','26',0),
	(1,'orders_tax_field','27',0),
	(1,'orders_coupon_codes','28',0),
	(1,'orders_shipping_option','29',0),
	(1,'orders_discount_field','30',0),
	(1,'orders_customer_name','31',0),
	(1,'orders_customer_phone','32',0),
	(1,'orders_customer_email','33',0),
	(1,'orders_full_billing_address','35',0),
	(1,'orders_full_shipping_address','36',0),
	(1,'orders_billing_first_name','37',0),
	(1,'orders_billing_address','38',0),
	(1,'orders_billing_address2','39',0),
	(1,'orders_billing_state','40',0),
	(1,'orders_billing_zip','41',0),
	(1,'orders_billing_city','42',0),
	(1,'orders_billing_last_name','43',0),
	(1,'orders_billing_company','44',0),
	(1,'orders_billing_country','45',0),
	(1,'orders_country_code','46',0),
	(1,'orders_shipping_first_name','47',0),
	(1,'orders_shipping_last_name','48',0),
	(1,'orders_shipping_address','49',0),
	(1,'orders_shipping_address2','50',0),
	(1,'orders_shipping_city','51',0),
	(1,'orders_shipping_state','52',0),
	(1,'orders_shipping_zip','53',0),
	(1,'orders_shipping_company','54',0),
	(1,'orders_shipping_country','55',0),
	(1,'orders_shipping_country_code','56',0),
	(1,'orders_error_message_field','57',0),
	(1,'orders_transaction_id','58',0),
	(1,'orders_last_four_digits','59',0),
	(1,'orders_customer_ip_address','60',0),
	(1,'orders_payment_gateway','61',0),
	(1,'save_purchased_items','1',0),
	(1,'purchased_items_channel','5',0),
	(1,'purchased_items_id_field','62',0),
	(1,'purchased_items_quantity_field','63',0),
	(1,'purchased_items_price_field','64',0),
	(1,'purchased_items_order_id_field','65',0),
	(1,'purchased_items_license_number_field','66',0),
	(1,'tax_use_shipping_address','0',0),
	(1,'tax_plugin','Cartthrob_tax_default',0),
	(1,'Cartthrob_tax_default_settings','a:2:{s:13:\"use_tax_table\";s:2:\"no\";s:12:\"tax_settings\";a:1:{i:0;a:4:{s:4:\"name\";s:10:\"California\";s:4:\"rate\";s:4:\"7.25\";s:5:\"state\";s:2:\"CA\";s:3:\"zip\";s:5:\"95926\";}}}',1),
	(1,'Cartthrob_tax_default_plus_quebec_settings','a:6:{s:7:\"tax_gst\";s:1:\"5\";s:7:\"tax_qst\";s:3:\"8.5\";s:19:\"tax_quebec_shipping\";s:2:\"no\";s:15:\"tax_quebec_name\";s:27:\"Consumption Tax (GST & QST)\";s:25:\"tax_quebec_effective_rate\";s:6:\"13.925\";s:12:\"tax_settings\";a:1:{i:0;a:4:{s:4:\"name\";s:0:\"\";s:4:\"rate\";s:0:\"\";s:5:\"state\";s:0:\"\";s:3:\"zip\";s:0:\"\";}}}',1),
	(1,'Cartthrob_tax_standard_settings','a:1:{s:11:\"default_tax\";s:1:\"8\";}',1),
	(1,'save_member_data','1',0),
	(1,'member_first_name_field','1',0),
	(1,'member_last_name_field','2',0),
	(1,'member_address_field','3',0),
	(1,'member_address2_field','4',0),
	(1,'member_city_field','5',0),
	(1,'member_state_field','6',0),
	(1,'member_zip_field','7',0),
	(1,'member_country_field','8',0),
	(1,'member_country_code_field','9',0),
	(1,'member_company_field','10',0),
	(1,'member_phone_field','11',0),
	(1,'member_email_address_field','email',0),
	(1,'member_use_billing_info_field','12',0),
	(1,'member_shipping_first_name_field','13',0),
	(1,'member_shipping_last_name_field','14',0),
	(1,'member_shipping_address_field','15',0),
	(1,'member_shipping_address2_field','16',0),
	(1,'member_shipping_city_field','17',0),
	(1,'member_shipping_state_field','18',0),
	(1,'member_shipping_zip_field','19',0),
	(1,'member_shipping_country_field','20',0),
	(1,'member_shipping_country_code_field','21',0),
	(1,'member_shipping_company_field','22',0),
	(1,'member_language_field','23',0),
	(1,'member_shipping_option_field','24',0),
	(1,'member_region_field','25',0),
	(1,'checkout_registration_options','auto-login',0),
	(1,'payment_gateway','Cartthrob_authorize_net',0),
	(1,'last_edited_gateway','Cartthrob_authorize_net',0),
	(1,'Cartthrob_anz_egate_settings','a:3:{s:11:\"access_code\";s:0:\"\";s:11:\"merchant_id\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_authorize_net_settings','a:9:{s:9:\"api_login\";s:0:\"\";s:15:\"transaction_key\";s:0:\"\";s:14:\"email_customer\";s:2:\"no\";s:4:\"mode\";s:4:\"test\";s:13:\"dev_api_login\";s:0:\"\";s:19:\"dev_transaction_key\";s:0:\"\";s:10:\"hash_value\";s:0:\"\";s:20:\"transaction_settings\";s:12:\"AUTH_CAPTURE\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_authorize_net_sim_settings','a:17:{s:14:\"email_customer\";s:2:\"no\";s:4:\"mode\";s:4:\"test\";s:13:\"tax_inclusive\";s:1:\"Y\";s:9:\"api_login\";s:0:\"\";s:15:\"transaction_key\";s:0:\"\";s:13:\"dev_api_login\";s:0:\"\";s:19:\"dev_transaction_key\";s:0:\"\";s:10:\"hash_value\";s:0:\"\";s:20:\"transaction_settings\";s:12:\"AUTH_CAPTURE\";s:26:\"x_header_html_payment_form\";s:17:\"account/dashboard\";s:26:\"x_footer_html_payment_form\";s:17:\"account/dashboard\";s:18:\"x_color_background\";s:7:\"#FFFFFF\";s:12:\"x_color_link\";s:7:\"#FF0000\";s:12:\"x_color_text\";s:7:\"#000000\";s:10:\"x_logo_url\";s:0:\"\";s:16:\"x_background_url\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_beanstream_direct_settings','a:2:{s:11:\"merchant_id\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_thirdparty_nab_transact_settings','a:5:{s:11:\"merchant_id\";s:7:\"XYZ0010\";s:8:\"password\";s:8:\"abcd1234\";s:9:\"test_mode\";s:4:\"test\";s:13:\"test_response\";s:3:\"200\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_ct_pay_by_account_settings','a:2:{s:17:\"processing_status\";s:8:\"complete\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_cartthrob_direct_settings','a:6:{s:8:\"username\";s:0:\"\";s:8:\"password\";s:0:\"\";s:12:\"dev_username\";s:0:\"\";s:12:\"dev_password\";s:0:\"\";s:4:\"mode\";s:10:\"no_account\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_quantum_settings','a:4:{s:13:\"gateway_login\";s:0:\"\";s:12:\"restrict_key\";s:0:\"\";s:14:\"email_customer\";s:1:\"0\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_dev_template_settings','a:2:{s:4:\"mode\";s:14:\"always_succeed\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_echo_nvp_settings','a:5:{s:16:\"merchant_echo_id\";s:11:\"123>1234567\";s:17:\"merchant_echo_pin\";s:8:\"12345678\";s:16:\"transaction_type\";s:2:\"ES\";s:4:\"mode\";s:4:\"test\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_eway_settings','a:5:{s:11:\"customer_id\";s:8:\"87654321\";s:14:\"payment_method\";s:9:\"REAL-TIME\";s:9:\"test_mode\";s:4:\"test\";s:13:\"test_response\";s:3:\"100\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_linkpoint_settings','a:6:{s:12:\"store_number\";s:0:\"\";s:7:\"keyfile\";s:22:\"yourcert_file_name.pem\";s:17:\"test_store_number\";s:0:\"\";s:12:\"test_keyfile\";s:22:\"yourcert_file_name.pem\";s:9:\"test_mode\";s:4:\"good\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_moneris_direct_settings','a:7:{s:8:\"store_id\";s:0:\"\";s:9:\"api_token\";s:0:\"\";s:3:\"avs\";s:2:\"no\";s:4:\"mode\";s:4:\"test\";s:13:\"test_store_id\";s:6:\"store1\";s:10:\"test_total\";s:5:\"10.00\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_ct_offline_payments_settings','a:2:{s:17:\"processing_status\";s:10:\"processing\";s:23:\"gateway_fields_template\";s:29:\"store/checkout_customerfields\";}',1),
	(1,'Cartthrob_ogone_direct_settings','a:7:{s:4:\"mode\";s:4:\"test\";s:10:\"pspid_live\";s:0:\"\";s:10:\"pspid_test\";s:0:\"\";s:10:\"api_userid\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:10:\"passphrase\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_ct_pay_by_check_settings','a:2:{s:17:\"processing_status\";s:8:\"complete\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_ct_pay_by_phone_settings','a:2:{s:17:\"processing_status\";s:8:\"complete\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_paypal_express_settings','a:14:{s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:13:\"test_username\";s:0:\"\";s:13:\"test_password\";s:0:\"\";s:14:\"test_signature\";s:0:\"\";s:4:\"mode\";s:4:\"test\";s:10:\"allow_note\";s:2:\"no\";s:12:\"show_item_id\";s:3:\"yes\";s:17:\"show_item_options\";s:2:\"no\";s:12:\"solutiontype\";s:4:\"Mark\";s:17:\"shipping_settings\";s:17:\"editable_shipping\";s:14:\"payment_action\";s:4:\"Sale\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_paypal_pro_settings','a:11:{s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:13:\"test_username\";s:0:\"\";s:13:\"test_password\";s:0:\"\";s:14:\"test_signature\";s:0:\"\";s:14:\"payment_action\";s:4:\"Sale\";s:9:\"test_mode\";s:4:\"test\";s:7:\"country\";s:2:\"us\";s:11:\"api_version\";s:4:\"60.0\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_thirdparty_psigate_settings','a:4:{s:4:\"mode\";s:4:\"test\";s:18:\"test_mode_response\";s:1:\"R\";s:9:\"store_key\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_worldpay_redirect_settings','a:4:{s:15:\"installation_id\";s:0:\"\";s:9:\"test_mode\";s:4:\"test\";s:23:\"order_complete_template\";s:17:\"account/dashboard\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_realex_remote_settings','a:3:{s:16:\"your_merchant_id\";s:0:\"\";s:11:\"your_secret\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_sage_settings','a:3:{s:4:\"mode\";s:4:\"test\";s:11:\"vendor_name\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_sage_s_settings','a:4:{s:7:\"profile\";s:6:\"NORMAL\";s:4:\"mode\";s:4:\"test\";s:11:\"vendor_name\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_sage_us_settings','a:6:{s:9:\"test_mode\";s:4:\"test\";s:4:\"m_id\";s:0:\"\";s:5:\"m_key\";s:0:\"\";s:9:\"m_test_id\";s:0:\"\";s:10:\"m_test_key\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_samurai_settings','a:5:{s:4:\"mode\";s:7:\"sandbox\";s:12:\"merchant_key\";s:0:\"\";s:17:\"merchant_password\";s:0:\"\";s:15:\"processor_token\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_ct_save_order_settings','a:2:{s:17:\"processing_status\";s:8:\"complete\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_stripe_settings','a:6:{s:4:\"mode\";s:4:\"test\";s:24:\"api_key_test_publishable\";s:0:\"\";s:19:\"api_key_test_secret\";s:0:\"\";s:24:\"api_key_live_publishable\";s:0:\"\";s:19:\"api_key_live_secret\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'Cartthrob_transaction_central_settings','a:3:{s:11:\"merchant_id\";s:0:\"\";s:7:\"reg_key\";s:0:\"\";s:23:\"gateway_fields_template\";s:0:\"\";}',1),
	(1,'allow_gateway_selection','1',0),
	(1,'available_gateways','a:3:{s:23:\"Cartthrob_authorize_net\";s:1:\"1\";s:29:\"Cartthrob_ct_offline_payments\";s:1:\"1\";s:24:\"Cartthrob_paypal_express\";s:1:\"1\";}',1),
	(1,'modulus_10_checking','1',0),
	(1,'allow_products_more_than_once','1',0),
	(1,'product_split_items_by_quantity','0',0),
	(1,'low_stock_level','5',0),
	(1,'orders_sequential_order_numbers','1',0),
	(1,'orders_title_prefix','Order #',0),
	(1,'orders_title_suffix','',0),
	(1,'orders_url_title_prefix','order-',0),
	(1,'orders_url_title_suffix','',0),
	(1,'orders_convert_country_code','1',0),
	(1,'orders_default_status','Processing',0),
	(1,'orders_processing_status','Payment Processing',0),
	(1,'orders_failed_status','Payment Failed',0),
	(1,'orders_declined_status','Payment Declined',0),
	(1,'orders_status_pending','Payment Pending',0),
	(1,'orders_status_expired','Payment Expired',0),
	(1,'orders_status_canceled','Payment Cancelled',0),
	(1,'orders_status_voided','Payment Voided',0),
	(1,'orders_status_refunded','Payment Refunded',0),
	(1,'orders_status_reversed','Payment Reversed',0),
	(1,'orders_status_offsite','Payment Processing',0),
	(1,'orders_language_field','',0),
	(1,'purchased_items_default_status','Processing',0),
	(1,'purchased_items_processing_status','Payment Processing',0),
	(1,'purchased_items_declined_status','Payment Declined',0),
	(1,'purchased_items_failed_status','Payment Failed',0),
	(1,'purchased_items_status_pending','Payment Pending',0),
	(1,'purchased_items_status_expired','Payment Expired',0),
	(1,'purchased_items_status_canceled','Payment Cancelled',0),
	(1,'purchased_items_status_voided','Payment Voided',0),
	(1,'purchased_items_status_refunded','Payment Refunded',0),
	(1,'purchased_items_status_reversed','Payment Reversed',0),
	(1,'purchased_items_status_offsite','Payment Processing',0),
	(1,'purchased_items_title_prefix','',0),
	(1,'purchased_items_license_number_type','uuid',0);

/*!40000 ALTER TABLE `ee2_cartthrob_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_cartthrob_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_status`;

CREATE TABLE `ee2_cartthrob_status` (
  `entry_id` int(10) NOT NULL DEFAULT '0',
  `session_id` varchar(32) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'processing',
  `inventory_processed` int(2) DEFAULT '0',
  `discounts_processed` int(2) DEFAULT '0',
  `error_message` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `cart` text,
  `cart_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`entry_id`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_cartthrob_tax
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_tax`;

CREATE TABLE `ee2_cartthrob_tax` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_name` text,
  `percent` varchar(5) DEFAULT NULL,
  `shipping_is_taxable` tinyint(1) DEFAULT '0',
  `special` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_cartthrob_vault
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cartthrob_vault`;

CREATE TABLE `ee2_cartthrob_vault` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `sub_id` varchar(100) DEFAULT NULL,
  `vault_id` int(10) DEFAULT NULL,
  `timestamp` int(11) DEFAULT '0',
  `expires` int(11) DEFAULT '0',
  `status` varchar(10) DEFAULT 'closed',
  `description` text,
  `gateway` varchar(32) DEFAULT NULL,
  `total_occurrences` int(5) DEFAULT NULL,
  `trial_occurrences` int(5) DEFAULT NULL,
  `total_intervals` int(4) DEFAULT NULL,
  `interval_units` varchar(32) DEFAULT NULL,
  `allow_modification` tinyint(1) DEFAULT '1',
  `price` varchar(100) DEFAULT NULL,
  `trial_price` varchar(100) DEFAULT NULL,
  `error_message` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_categories`;

CREATE TABLE `ee2_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_categories` WRITE;
/*!40000 ALTER TABLE `ee2_categories` DISABLE KEYS */;

INSERT INTO `ee2_categories` (`cat_id`, `site_id`, `group_id`, `parent_id`, `cat_name`, `cat_url_title`, `cat_description`, `cat_image`, `cat_order`)
VALUES
	(1,1,2,0,'Volunteer Opportunities','volunteer-opportunities','','0',3),
	(2,1,2,0,'Stories','stories','','0',4),
	(3,1,2,0,'Local News','local-news','','0',1),
	(4,1,2,0,'News Abroad','news-abroad','','0',2);

/*!40000 ALTER TABLE `ee2_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_category_field_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_category_field_data`;

CREATE TABLE `ee2_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_category_field_data` WRITE;
/*!40000 ALTER TABLE `ee2_category_field_data` DISABLE KEYS */;

INSERT INTO `ee2_category_field_data` (`cat_id`, `site_id`, `group_id`)
VALUES
	(1,1,2),
	(2,1,2),
	(3,1,2),
	(4,1,2);

/*!40000 ALTER TABLE `ee2_category_field_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_category_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_category_fields`;

CREATE TABLE `ee2_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_category_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_category_groups`;

CREATE TABLE `ee2_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_category_groups` WRITE;
/*!40000 ALTER TABLE `ee2_category_groups` DISABLE KEYS */;

INSERT INTO `ee2_category_groups` (`group_id`, `site_id`, `group_name`, `sort_order`, `exclude_group`, `field_html_formatting`, `can_edit_categories`, `can_delete_categories`)
VALUES
	(1,1,'Pages','a',0,'all','',''),
	(2,1,'News','a',0,'all','6|7|8','6|7|8'),
	(3,1,'Store','a',0,'all','','');

/*!40000 ALTER TABLE `ee2_category_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_category_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_category_posts`;

CREATE TABLE `ee2_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_category_posts` WRITE;
/*!40000 ALTER TABLE `ee2_category_posts` DISABLE KEYS */;

INSERT INTO `ee2_category_posts` (`entry_id`, `cat_id`)
VALUES
	(8,2),
	(8,4),
	(9,1),
	(9,3),
	(10,2),
	(10,3),
	(10,4),
	(24,1),
	(24,2),
	(25,1),
	(25,2),
	(25,3),
	(25,4);

/*!40000 ALTER TABLE `ee2_category_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_channel_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_channel_data`;

CREATE TABLE `ee2_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text NOT NULL,
  `field_ft_1` tinytext,
  `field_id_11` text NOT NULL,
  `field_ft_11` tinytext,
  `field_id_12` text NOT NULL,
  `field_ft_12` tinytext,
  `field_id_19` text NOT NULL,
  `field_ft_19` tinytext,
  `field_id_24` text NOT NULL,
  `field_ft_24` tinytext,
  `field_id_26` text NOT NULL,
  `field_ft_26` tinytext,
  `field_id_28` text NOT NULL,
  `field_ft_28` tinytext,
  `field_id_29` text NOT NULL,
  `field_ft_29` tinytext,
  `field_id_31` text NOT NULL,
  `field_ft_31` tinytext,
  `field_id_32` text NOT NULL,
  `field_ft_32` tinytext,
  `field_id_33` text NOT NULL,
  `field_ft_33` tinytext,
  `field_id_34` text NOT NULL,
  `field_ft_34` tinytext,
  `field_id_35` text NOT NULL,
  `field_ft_35` tinytext,
  `field_id_36` text NOT NULL,
  `field_ft_36` tinytext,
  `field_id_37` text NOT NULL,
  `field_ft_37` tinytext,
  `field_id_38` text NOT NULL,
  `field_ft_38` tinytext,
  `field_id_39` text NOT NULL,
  `field_ft_39` tinytext,
  `field_id_40` text NOT NULL,
  `field_ft_40` tinytext,
  `field_id_41` text NOT NULL,
  `field_ft_41` tinytext,
  `field_id_42` text NOT NULL,
  `field_ft_42` tinytext,
  `field_id_43` text NOT NULL,
  `field_ft_43` tinytext,
  `field_id_44` text NOT NULL,
  `field_ft_44` tinytext,
  `field_id_45` text NOT NULL,
  `field_ft_45` tinytext,
  `field_id_46` text NOT NULL,
  `field_ft_46` tinytext,
  `field_id_47` text NOT NULL,
  `field_ft_47` tinytext,
  `field_id_48` text NOT NULL,
  `field_ft_48` tinytext,
  `field_id_49` text NOT NULL,
  `field_ft_49` tinytext,
  `field_id_50` text NOT NULL,
  `field_ft_50` tinytext,
  `field_id_51` text NOT NULL,
  `field_ft_51` tinytext,
  `field_id_52` text NOT NULL,
  `field_ft_52` tinytext,
  `field_id_53` text NOT NULL,
  `field_ft_53` tinytext,
  `field_id_54` text NOT NULL,
  `field_ft_54` tinytext,
  `field_id_55` text NOT NULL,
  `field_ft_55` tinytext,
  `field_id_56` text NOT NULL,
  `field_ft_56` tinytext,
  `field_id_57` text NOT NULL,
  `field_ft_57` tinytext,
  `field_id_58` text NOT NULL,
  `field_ft_58` tinytext,
  `field_id_59` text NOT NULL,
  `field_ft_59` tinytext,
  `field_id_60` text NOT NULL,
  `field_ft_60` tinytext,
  `field_id_61` text NOT NULL,
  `field_ft_61` tinytext,
  `field_id_72` text,
  `field_ft_72` tinytext,
  `field_id_73` text,
  `field_ft_73` tinytext,
  `field_id_74` text,
  `field_ft_74` tinytext,
  `field_id_75` text,
  `field_ft_75` tinytext,
  `field_id_76` text,
  `field_ft_76` tinytext,
  `field_id_77` text,
  `field_ft_77` tinytext,
  `field_id_78` text,
  `field_ft_78` tinytext,
  `field_id_79` text,
  `field_ft_79` tinytext,
  `field_id_80` text,
  `field_ft_80` tinytext,
  `field_id_81` text,
  `field_ft_81` tinytext,
  `field_id_82` text,
  `field_ft_82` tinytext,
  `field_id_83` text,
  `field_ft_83` tinytext,
  `field_id_84` text,
  `field_ft_84` tinytext,
  `field_id_85` text,
  `field_ft_85` tinytext,
  `field_id_86` text,
  `field_ft_86` tinytext,
  `field_id_87` text,
  `field_ft_87` tinytext,
  `field_id_88` text,
  `field_ft_88` tinytext,
  `field_id_89` text,
  `field_ft_89` tinytext,
  `field_id_90` text,
  `field_ft_90` tinytext,
  `field_id_91` text,
  `field_ft_91` tinytext,
  `field_id_92` text,
  `field_ft_92` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_channel_data` WRITE;
/*!40000 ALTER TABLE `ee2_channel_data` DISABLE KEYS */;

INSERT INTO `ee2_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_19`, `field_ft_19`, `field_id_24`, `field_ft_24`, `field_id_26`, `field_ft_26`, `field_id_28`, `field_ft_28`, `field_id_29`, `field_ft_29`, `field_id_31`, `field_ft_31`, `field_id_32`, `field_ft_32`, `field_id_33`, `field_ft_33`, `field_id_34`, `field_ft_34`, `field_id_35`, `field_ft_35`, `field_id_36`, `field_ft_36`, `field_id_37`, `field_ft_37`, `field_id_38`, `field_ft_38`, `field_id_39`, `field_ft_39`, `field_id_40`, `field_ft_40`, `field_id_41`, `field_ft_41`, `field_id_42`, `field_ft_42`, `field_id_43`, `field_ft_43`, `field_id_44`, `field_ft_44`, `field_id_45`, `field_ft_45`, `field_id_46`, `field_ft_46`, `field_id_47`, `field_ft_47`, `field_id_48`, `field_ft_48`, `field_id_49`, `field_ft_49`, `field_id_50`, `field_ft_50`, `field_id_51`, `field_ft_51`, `field_id_52`, `field_ft_52`, `field_id_53`, `field_ft_53`, `field_id_54`, `field_ft_54`, `field_id_55`, `field_ft_55`, `field_id_56`, `field_ft_56`, `field_id_57`, `field_ft_57`, `field_id_58`, `field_ft_58`, `field_id_59`, `field_ft_59`, `field_id_60`, `field_ft_60`, `field_id_61`, `field_ft_61`, `field_id_72`, `field_ft_72`, `field_id_73`, `field_ft_73`, `field_id_74`, `field_ft_74`, `field_id_75`, `field_ft_75`, `field_id_76`, `field_ft_76`, `field_id_77`, `field_ft_77`, `field_id_78`, `field_ft_78`, `field_id_79`, `field_ft_79`, `field_id_80`, `field_ft_80`, `field_id_81`, `field_ft_81`, `field_id_82`, `field_ft_82`, `field_id_83`, `field_ft_83`, `field_id_84`, `field_ft_84`, `field_id_85`, `field_ft_85`, `field_id_86`, `field_ft_86`, `field_id_87`, `field_ft_87`, `field_id_88`, `field_ft_88`, `field_id_89`, `field_ft_89`, `field_id_90`, `field_ft_90`, `field_id_91`, `field_ft_91`, `field_id_92`, `field_ft_92`)
VALUES
	(1,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','','none','','none','1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','1','none','y\ny\ny\n','none','[10] [minnesota-outreach-women-how-it-works] Minnesota Outreach Women: How it Works','none','','none','','none','','none','','none','','none','','xhtml'),
	(2,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','','none','','none','1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','none','','none','','none','','none','','none','1','none','','none','','none','','xhtml'),
	(3,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'','none','1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(4,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','1','xhtml'),
	(5,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','none','','none','','none','{filedir_2}img-interior-banner-fpo.jpeg','none','','none','1','none','','none','','none','','xhtml'),
	(6,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(7,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(8,1,8,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet.</p>\n','none','{filedir_3}home.jpeg\n{filedir_8}img_event-fpo.jpeg\n','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','','none','','none','','none','<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n','none','','none','','none','1','none','','xhtml'),
	(9,1,8,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Pellent cursus fringilla lectus id magna facilisis vel tristique facilisis vel tristique dolor semper</p>\n','none','1','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','','none','','none','','none','<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n','none','','none','','none','1','none','','xhtml'),
	(10,1,8,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Ras in blandit libero. In hendrerit interdum sem, cursus auctor tellus pulvinar ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent mattis cursus Pellent mattis pulvinar. Nulla a est in <a href=\"#\">turpis fermentum luctus</a>. Nulla ut nunc consectetur neque suscipit pharetra id quis urna. Morbi justo nisi, egestas in blandit id, tristique sed risus. Nulla a est in turpis fermentum luctus.&nbsp;</p>\n','none','1','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n','none','','none','','none','','none','','xhtml'),
	(11,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','{filedir_2}fpo-library.jpg','none','','none','','none','','none','','none','','xhtml'),
	(12,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis odio eleifend elit rhoncus sodales sit amet non nibh. Nulla eleifend nisi vel purus facilisis elementum. Aenean posuere ante quis dui aliquam elementum. Maecenas est turpis, hendrerit eu suscipit ut, aliquet mattis urna. Suspendisse potenti. Aenean sagittis ornare ligula a lobortis. Duis in felis eros, et consequat purus. Suspendisse potenti. Maecenas blandit, neque sit amet cursus placerat, tortor arcu sagittis ipsum, vel accumsan elit eros ut turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean accumsan aliquet rutrum. Morbi pellentesque ullamcorper congue. Fusce et justo sit amet leo volutpat hendrerit. Etiam mattis, risus vel cursus hendrerit, lacus diam aliquet lacus, vestibulum hendrerit justo ligula vitae risus.</p>\n<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n<p>\n	Integer pretium lacus vitae metus imperdiet sed commodo nisl convallis. Proin ipsum ipsum, lacinia at porta eu, elementum sed ante. Cras ultrices sem laoreet justo dictum eu volutpat orci pellentesque. In pharetra suscipit scelerisque. Curabitur pharetra velit non mauris varius consectetur sit amet sit amet diam. Vivamus quis ante sed sem pretium malesuada nec eget erat. Sed vel sapien non eros pretium convallis nec quis dolor. Aenean nec eros vitae eros egestas condimentum. Morbi facilisis, dui sed ultrices pulvinar, ipsum est porttitor elit, et sollicitudin lorem quam a odio. Mauris mauris libero, malesuada sed blandit non, consectetur vitae nunc.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','{filedir_2}bg-banner-overlay-home.png','none','','none','','none','','none','','none','','xhtml'),
	(13,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(14,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(15,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(16,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','y\n','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(17,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod sem sed velit pretium interdum. Aenean feugiat iaculis ligula, ut scelerisque diam commodo in. Phasellus tincidunt leo iaculis quam dictum ultricies. In vehicula mi eget magna convallis egestas. Cras volutpat justo et leo sollicitudin eget malesuada neque dictum. Maecenas venenatis, libero nec accumsan feugiat, metus neque venenatis leo, vitae pharetra orci orci eu lacus. Maecenas eros purus, congue sed venenatis accumsan, tincidunt vitae velit. Suspendisse eu magna auctor ligula feugiat vulputate. Donec sit amet laoreet dui. Sed hendrerit tempor elit, eget egestas arcu ornare sit amet.</p>\n<p>\n	Nullam feugiat mattis turpis. Aenean vel magna mi. Sed sed elit felis. Sed commodo commodo facilisis. Proin mauris ipsum, rutrum et accumsan ac, bibendum eu nisl. Maecenas lorem ante, dignissim eu sodales in, hendrerit a mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus odio enim, euismod suscipit feugiat sed, commodo eu eros. In eget sem imperdiet augue sollicitudin mattis. Vestibulum posuere porta elementum.</p>\n<p>\n	Pellentesque aliquet, ipsum sed tempus malesuada, dolor lorem mattis orci, sit amet ornare nisl purus non magna. Morbi non nunc diam. Cras cursus sollicitudin dui, nec aliquam odio feugiat ac. Sed tincidunt tempus placerat. Nullam tincidunt, est nec faucibus facilisis, nisl lectus euismod libero, in eleifend quam ipsum a erat. Fusce erat arcu, tincidunt vel sodales in, dapibus euismod metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fermentum suscipit sapien, at scelerisque purus consequat at. Nullam pellentesque posuere diam eget aliquam. Morbi tristique fermentum iaculis. Sed semper vehicula lectus eu aliquam. Praesent porta urna sed sem aliquet at porttitor enim feugiat. Proin dignissim luctus nulla, vel interdum tellus dapibus at. Fusce a nibh egestas neque laoreet hendrerit.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(18,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(19,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(20,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod sem sed velit pretium interdum. Aenean feugiat iaculis ligula, ut scelerisque diam commodo in. Phasellus tincidunt leo iaculis quam dictum ultricies. In vehicula mi eget magna convallis egestas. Cras volutpat justo et leo sollicitudin eget malesuada neque dictum. Maecenas venenatis, libero nec accumsan feugiat, metus neque venenatis leo, vitae pharetra orci orci eu lacus. Maecenas eros purus, congue sed venenatis accumsan, tincidunt vitae velit. Suspendisse eu magna auctor ligula feugiat vulputate. Donec sit amet laoreet dui. Sed hendrerit tempor elit, eget egestas arcu ornare sit amet.</p>\n<p>\n	Nullam feugiat mattis turpis. Aenean vel magna mi. Sed sed elit felis. Sed commodo commodo facilisis. Proin mauris ipsum, rutrum et accumsan ac, bibendum eu nisl. Maecenas lorem ante, dignissim eu sodales in, hendrerit a mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus odio enim, euismod suscipit feugiat sed, commodo eu eros. In eget sem imperdiet augue sollicitudin mattis. Vestibulum posuere porta elementum.</p>\n<p>\n	Pellentesque aliquet, ipsum sed tempus malesuada, dolor lorem mattis orci, sit amet ornare nisl purus non magna. Morbi non nunc diam. Cras cursus sollicitudin dui, nec aliquam odio feugiat ac. Sed tincidunt tempus placerat. Nullam tincidunt, est nec faucibus facilisis, nisl lectus euismod libero, in eleifend quam ipsum a erat. Fusce erat arcu, tincidunt vel sodales in, dapibus euismod metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fermentum suscipit sapien, at scelerisque purus consequat at. Nullam pellentesque posuere diam eget aliquam. Morbi tristique fermentum iaculis. Sed semper vehicula lectus eu aliquam. Praesent porta urna sed sem aliquet at porttitor enim feugiat. Proin dignissim luctus nulla, vel interdum tellus dapibus at. Fusce a nibh egestas neque laoreet hendrerit.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(21,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar rutrum ornare. Cras eu neque eget sapien dictum placerat a vel justo. Phasellus malesuada auctor volutpat. Vestibulum mattis neque at nunc luctus a dictum mi malesuada. Ut elementum mattis rutrum. Ut suscipit tellus quis lectus mollis congue. Duis viverra nulla a tortor rhoncus ut lobortis nulla egestas. Aenean rutrum, risus vel hendrerit suscipit, velit nisl dapibus arcu, in sodales purus massa non enim. Aliquam vulputate mattis diam, vitae scelerisque urna ullamcorper nec. Vivamus consequat magna eget tellus feugiat porta.</p>\n<p>\n	Curabitur turpis tellus, lacinia in fermentum pulvinar, imperdiet at orci. Fusce fermentum, quam hendrerit dapibus dictum, tortor sapien molestie nulla, lacinia luctus nulla sem at risus. Integer vitae bibendum nunc. Ut pharetra neque sit amet purus imperdiet interdum. Curabitur vel tellus urna. In sagittis, diam a pretium mattis, lectus nulla ultrices libero, sed accumsan sapien mi eget sem. Phasellus rhoncus leo sit amet nisl bibendum tincidunt. Duis auctor odio at justo euismod vestibulum.</p>\n<p>\n	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce nunc nisi, pretium nec elementum ut, elementum consequat neque. Sed facilisis porta erat, eu luctus ligula vestibulum in. In non massa ut nisl tempor hendrerit quis ut tortor. Nullam adipiscing nulla vel tellus vestibulum in adipiscing dui laoreet. Nam vitae mauris risus. Curabitur id tellus dui, eu convallis ante. Nunc pellentesque orci quis dui rhoncus mattis.</p>\n<p>\n	Maecenas non arcu lacus. Sed quis eros enim, sit amet bibendum eros. Morbi quis nisl elit. Vivamus rutrum malesuada vulputate. Duis ut leo lectus, ut fermentum purus. Aliquam erat volutpat. Aliquam diam sem, malesuada at semper quis, accumsan ut erat. Quisque sed sem nunc. Cras nec leo massa. Maecenas vulputate orci tempor quam viverra feugiat. Cras ac elit nec eros volutpat lacinia et a odio. Cras molestie egestas libero vitae bibendum.</p>\n<p>\n	Mauris orci eros, ultricies rhoncus lacinia eget, tincidunt id dui. Praesent nibh erat, dictum posuere pellentesque ac, fermentum vel felis. Donec orci ante, ultricies a viverra id, aliquet et odio. Praesent neque leo, varius et consequat non, dignissim sit amet odio. Nullam ut consequat lacus. Pellentesque mattis, dui non mollis dapibus, nisi tortor egestas mi, vel auctor est dolor non magna. Nullam aliquam rutrum est eget sollicitudin. Curabitur vulputate malesuada tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc imperdiet consequat risus, et laoreet mauris condimentum ut. Quisque sed est urna, ac posuere dui. Praesent vestibulum lorem in dui tempor facilisis. Morbi sit amet sem at elit convallis lobortis sit amet quis eros.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(22,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar rutrum ornare. Cras eu neque eget sapien dictum placerat a vel justo. Phasellus malesuada auctor volutpat. Vestibulum mattis neque at nunc luctus a dictum mi malesuada. Ut elementum mattis rutrum. Ut suscipit tellus quis lectus mollis congue. Duis viverra nulla a tortor rhoncus ut lobortis nulla egestas. Aenean rutrum, risus vel hendrerit suscipit, velit nisl dapibus arcu, in sodales purus massa non enim. Aliquam vulputate mattis diam, vitae scelerisque urna ullamcorper nec. Vivamus consequat magna eget tellus feugiat porta.</p>\n<p>\n	Curabitur turpis tellus, lacinia in fermentum pulvinar, imperdiet at orci. Fusce fermentum, quam hendrerit dapibus dictum, tortor sapien molestie nulla, lacinia luctus nulla sem at risus. Integer vitae bibendum nunc. Ut pharetra neque sit amet purus imperdiet interdum. Curabitur vel tellus urna. In sagittis, diam a pretium mattis, lectus nulla ultrices libero, sed accumsan sapien mi eget sem. Phasellus rhoncus leo sit amet nisl bibendum tincidunt. Duis auctor odio at justo euismod vestibulum.</p>\n<p>\n	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce nunc nisi, pretium nec elementum ut, elementum consequat neque. Sed facilisis porta erat, eu luctus ligula vestibulum in. In non massa ut nisl tempor hendrerit quis ut tortor. Nullam adipiscing nulla vel tellus vestibulum in adipiscing dui laoreet. Nam vitae mauris risus. Curabitur id tellus dui, eu convallis ante. Nunc pellentesque orci quis dui rhoncus mattis.</p>\n<p>\n	Maecenas non arcu lacus. Sed quis eros enim, sit amet bibendum eros. Morbi quis nisl elit. Vivamus rutrum malesuada vulputate. Duis ut leo lectus, ut fermentum purus. Aliquam erat volutpat. Aliquam diam sem, malesuada at semper quis, accumsan ut erat. Quisque sed sem nunc. Cras nec leo massa. Maecenas vulputate orci tempor quam viverra feugiat. Cras ac elit nec eros volutpat lacinia et a odio. Cras molestie egestas libero vitae bibendum.</p>\n<p>\n	Mauris orci eros, ultricies rhoncus lacinia eget, tincidunt id dui. Praesent nibh erat, dictum posuere pellentesque ac, fermentum vel felis. Donec orci ante, ultricies a viverra id, aliquet et odio. Praesent neque leo, varius et consequat non, dignissim sit amet odio. Nullam ut consequat lacus. Pellentesque mattis, dui non mollis dapibus, nisi tortor egestas mi, vel auctor est dolor non magna. Nullam aliquam rutrum est eget sollicitudin. Curabitur vulputate malesuada tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc imperdiet consequat risus, et laoreet mauris condimentum ut. Quisque sed est urna, ac posuere dui. Praesent vestibulum lorem in dui tempor facilisis. Morbi sit amet sem at elit convallis lobortis sit amet quis eros.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(23,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','','none','','none','','none','','none','','xhtml'),
	(24,1,8,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat.</p>\n','none','1','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n','none','','none','','none','','none','','xhtml'),
	(25,1,8,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','1','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n','none','','none','','none','','none','','xhtml'),
	(26,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	<img alt=\"\" src=\"{filedir_2}how-to-add-related-content.png\" style=\"font-size: 16px; width: 408px; height: 198px; float: left; margin-left: 10px; margin-right: 10px; \" /></p>\n<p>\n	<span style=\"font-size:16px;\"><em>This page is not publically visible.</em></span></p>\n<p>\n	<strong><span style=\"font-size:16px;\">To add \"Related Content\":</span></strong></p>\n<p>\n	<span style=\"font-size:16px;\">1) Click the \"<strong>Pages</strong>\" tab in the top navigation.</span></p>\n<p>\n	<span style=\"font-size:16px;\">2) Click the \"<strong>Add</strong>\" next to this page in the navigation list as illustrated to the left.</span></p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','',NULL,'','none','','none','','none','','xhtml'),
	(27,1,9,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<h2>\n	People against Malaria (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','',NULL,'','none','1','none','','none','','xhtml'),
	(28,1,9,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	United Methodist fundraiser to be held June 9, 2011.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','',NULL,'','none','1','none','','none','','xhtml'),
	(29,1,9,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	We offer a paper newsletter for those interested.</p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','',NULL,'','none','1','none','','none','','xhtml'),
	(30,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','Page Not Found (Oh no!)','none','','none','',NULL,'','none','','none','','none','','none','',NULL,'','none','','none','',NULL,'','none'),
	(31,1,1,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n	Success! Your request for more information was successful. We will contact you shortly.</p>\n<p>\n	&nbsp;</p>\n<p>\n	<a href=\"{page_4}\">Click here to submit another request.</a></p>\n','none','',NULL,'',NULL,'1','none','','none','','none','','none','','none','','none','','none','','none','',NULL,'','none','','none','','none','','none','',NULL,'','none','','none','',NULL,'','none');

/*!40000 ALTER TABLE `ee2_channel_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_channel_entries_autosave
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_channel_entries_autosave`;

CREATE TABLE `ee2_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_channel_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_channel_fields`;

CREATE TABLE `ee2_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_channel_fields` WRITE;
/*!40000 ALTER TABLE `ee2_channel_fields` DISABLE KEYS */;

INSERT INTO `ee2_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`)
VALUES
	(1,1,2,'product_description','Description','','textarea','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',1,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToieSI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJ5IjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToieSI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToieSI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(75,1,1,'og_pages','Meta - Facebook','','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','y','none','n',12,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjU6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO2k6MjtzOjE6IjMiO2k6MztzOjE6IjQiO2k6NDtzOjE6IjUiO319'),
	(11,1,2,'product_thumbnail','Product Thumbnail','','file','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',11,'image','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(12,1,2,'product_detail_image','Product Detail Image','','file','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',12,'image','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(83,1,2,'product_options','Product Options','','store','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','xhtml','n',5,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(19,1,2,'product_download_url','Download URL','If the product has an associated download URL, add it here. ','text','','0',0,0,'channel',8,'title','desc',0,6,300,'n','ltr','n','n','none','n',19,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(24,1,4,'order_subtotal_plus_tax','Order Subtotal Plus Tax','','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',4,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(26,1,4,'order_shipping_plus_tax','Order Shipping Plus Tax','','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',6,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(28,1,4,'order_coupons','Order Coupon Codes','','text','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',8,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(29,1,4,'order_shipping_option','Order Shipping Method','','text','','0',0,0,'channel',8,'date','desc',0,8,128,'n','ltr','n','n','none','n',9,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(31,1,4,'order_customer_full_name','Customer Full Name','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',11,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(32,1,4,'order_customer_phone','Customer Phone','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',12,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(33,1,4,'order_customer_email','Customer Email','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',13,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(34,1,4,'order_language','Customer Language Code','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',14,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(35,1,4,'order_full_billing_address','Full Billing Address','','textarea','','0',0,0,'channel',5,'title','desc',0,4,128,'n','ltr','n','n','none','n',15,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(36,1,4,'order_full_shipping_address','Full Shipping Address','','textarea','','0',0,0,'channel',5,'title','desc',0,4,128,'n','ltr','n','n','none','n',16,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(37,1,4,'order_billing_first_name','Billing First Name','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',17,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(38,1,4,'order_billing_address','Billing Address','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',18,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(39,1,4,'order_billing_address2','Billing Address 2','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',19,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(40,1,4,'order_billing_state','Billing State','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',20,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(41,1,4,'order_billing_zip','Billing Zip','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',21,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(42,1,4,'order_billing_city','Billing City','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',22,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(43,1,4,'order_billing_last_name','Billing Last Name','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',23,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(44,1,4,'order_billing_company','Billing Company','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','y',24,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(45,1,4,'order_billing_country','Billing Country','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','y',25,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(46,1,4,'order_country_code','Billing Country Code','','text','','0',0,0,'channel',5,'title','desc',0,6,3,'n','ltr','n','n','none','y',26,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(47,1,4,'order_shipping_first_name','Shipping First Name','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',27,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(48,1,4,'order_shipping_last_name','Shipping Last Name','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',28,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(49,1,4,'order_shipping_address','Shipping Address','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',29,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(50,1,4,'order_shipping_address2','Shipping Address 2','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',30,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(51,1,4,'order_shipping_city','Shipping City','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',31,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(52,1,4,'order_shipping_state','Shipping State','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',32,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(53,1,4,'order_shipping_zip','Shipping Zip','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',33,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(54,1,4,'order_shipping_company','Shipping Company','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',34,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(55,1,4,'order_shipping_country','Shipping Country','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',35,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(56,1,4,'order_shipping_country_code','Shipping Country Code','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',36,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(57,1,4,'order_error_message','Payment: Error Message','','text','','0',0,0,'channel',8,'title','desc',0,6,255,'n','ltr','n','n','none','n',37,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(58,1,4,'order_transaction_id','Payment: Transaction ID','','text','','0',0,0,'channel',5,'date','desc',0,8,128,'n','ltr','n','n','none','n',38,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(59,1,4,'order_last_four','Payment: CC Last Four Digits','','text','','0',0,0,'channel',8,'title','desc',0,6,4,'n','ltr','n','n','none','n',39,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(60,1,4,'order_ip_address','Payment: IP Address','','text','','0',0,0,'channel',5,'title','desc',0,6,128,'n','ltr','n','n','none','n',40,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3RleHQiO2I6MDtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO3M6MTg6ImZpZWxkX2NvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
	(61,1,4,'order_payment_gateway','Payment: Payment Gateway','','text','','0',0,0,'channel',2,'title','desc',0,6,255,'n','ltr','n','n','none','n',41,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(89,1,1,'relatedcontent','Related Content','Note: <em><strong>only the first (3) active selections will be displayed</strong></em>. Please contact The Nerdery if you would like this limit to be increased.\n(Hint: You can also schedule callouts to be shown at a future date by modifying the \"Entry Date\" of the callout entry and then assigning it below.)','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',14,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMjMiO2k6MTtzOjI6IjI0Ijt9fQ=='),
	(90,1,1,'callout','Callout','To display just an image, use the \"Callout Image\" field below.<br />\nTo include an image along with a short description and link, use the remaining fields.','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',15,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtzOjI6IjE5IjtpOjE7czoyOiIyMCI7aToyO3M6MjoiMjEiO2k6MztzOjI6IjIyIjt9fQ=='),
	(91,1,8,'newsrelatedcontent','Related Content','Note: <em><strong>only the first (3) active selections will be displayed</strong></em>. Please contact The Nerdery if you would like this limit to be increased.\n(Hint: You can also schedule callouts to be shown at a future date by modifying the \"Entry Date\" of the callout entry and then assigning it below.)','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',4,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMjUiO2k6MTtzOjI6IjI2Ijt9fQ=='),
	(92,1,1,'form','Custom Form','','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',16,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Njp7aTowO3M6MjoiMjkiO2k6MTtzOjI6IjMwIjtpOjI7czoyOiIzNCI7aTozO3M6MjoiMzEiO2k6NDtzOjI6IjMzIjtpOjU7czoyOiIzMiI7fX0='),
	(72,1,1,'content','Content','','wygwam','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',2,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(73,1,8,'news_preview','Preview','','wygwam','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',3,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(74,1,8,'news_images','Images','','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','y','y','none','n',1,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MjoiMTUiO2k6MTtzOjI6IjE2IjtpOjI7czoyOiIyNyI7aTozO3M6MjoiMjgiO319'),
	(88,1,8,'news_summary','Summary','','wygwam','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',2,'any','YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(76,1,1,'meta_title','Meta - Title','Overrides the default meta title.','text','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','y','n','none','n',9,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(77,1,1,'meta_description','Meta - Description','','text','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','y','n','none','n',10,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(78,1,1,'meta_keywords','Meta - Keywords','','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',11,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MToiNiI7fX0='),
	(79,1,1,'navigation_excludeentry','Navigation - Exclude From Navigation','\'Yes\' will exclude this entry from navigation','pt_switch','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','y','none','n',5,'any','YToxMTp7czo5OiJvZmZfbGFiZWwiO3M6MjoiTk8iO3M6Nzoib2ZmX3ZhbCI7czowOiIiO3M6ODoib25fbGFiZWwiO3M6MzoiWUVTIjtzOjY6Im9uX3ZhbCI7czoxOiJ5IjtzOjc6ImRlZmF1bHQiO3M6Mzoib2ZmIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(80,1,1,'navigation_titleoverride','Navigation - Title Override','Overrides the title as displayed in the navigation menu.','text','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','y','y','none','n',6,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(81,1,1,'pagetitle_override','Page Title Overide','This content will override the Page Title but not the title in the Navigation','text','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','y','y','none','n',8,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(82,1,1,'navigation_urloverride','Navigation - URL Override','','text','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','y','none','n',7,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(84,1,1,'getinvolved','Get Involved','','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',3,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NTp7aTowO3M6MToiNyI7aToxO3M6MToiOCI7aToyO3M6MToiOSI7aTozO3M6MjoiMTAiO2k6NDtzOjI6IjExIjt9fQ=='),
	(85,1,1,'carousel','Carousel','','matrix','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','y','n','none','n',1,'any','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MjoiMTIiO2k6MTtzOjI6IjEzIjtpOjI7czoyOiIxNCI7fX0='),
	(86,1,1,'spotlight_article','Spotlight Article','If more than one article selected, the homepage will randomly choose an article on an hourly basis.','playa','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',4,'any','YToxMzp7czo1OiJtdWx0aSI7czoxOiJuIjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJjaGFubmVscyI7YToxOntpOjA7czoxOiI4Ijt9czo4OiJzdGF0dXNlcyI7YToxOntpOjA7czo0OiJvcGVuIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(87,1,1,'pagebanner','Page Banner Image','','file','','0',0,0,'channel',8,'title','desc',0,6,128,'n','ltr','n','n','none','n',13,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');

/*!40000 ALTER TABLE `ee2_channel_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_channel_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_channel_member_groups`;

CREATE TABLE `ee2_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_channel_member_groups` WRITE;
/*!40000 ALTER TABLE `ee2_channel_member_groups` DISABLE KEYS */;

INSERT INTO `ee2_channel_member_groups` (`group_id`, `channel_id`)
VALUES
	(6,1),
	(6,2),
	(6,3),
	(6,4),
	(6,5),
	(6,6),
	(6,7),
	(6,8),
	(7,1),
	(7,2),
	(7,3),
	(7,4),
	(7,5),
	(7,6),
	(7,7),
	(7,8),
	(8,1),
	(8,2),
	(8,3),
	(8,4),
	(8,5),
	(8,6),
	(8,7),
	(8,8);

/*!40000 ALTER TABLE `ee2_channel_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_channel_titles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_channel_titles`;

CREATE TABLE `ee2_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_channel_titles` WRITE;
/*!40000 ALTER TABLE `ee2_channel_titles` DISABLE KEYS */;

INSERT INTO `ee2_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`)
VALUES
	(1,1,1,2,0,NULL,'127.0.0.1','Home','homepage','open','y',0,0,0,0,'n','n',1324579628,'n','2011','12','22',0,0,20121029202209,0,0),
	(2,1,1,2,0,NULL,'127.0.0.1','News & Events','news','open','y',0,0,0,0,'y','n',1332438915,'n','2012','03','22',0,0,20121030062716,0,0),
	(3,1,1,2,0,NULL,'127.0.0.1','Store','store','closed','y',0,0,0,0,'y','n',1332439286,'n','2012','03','22',0,0,20121029203227,0,0),
	(4,1,1,2,0,NULL,'127.0.0.1','Request Information','request-information','open','y',0,0,0,0,'y','n',1337205506,'n','2012','05','16',0,0,20121030103027,0,0),
	(5,1,1,2,0,NULL,'127.0.0.1','About Us','about','open','y',0,0,0,0,'y','n',1337206698,'n','2012','05','16',0,0,20121030115319,0,0),
	(6,1,1,2,0,NULL,'67.182.163.29','Orders','orders','open','y',0,0,0,0,'y','n',1337206924,'n','2012','05','16',0,0,20120516152204,0,0),
	(7,1,1,2,0,NULL,'67.182.163.29','Products','products1','open','y',0,0,0,0,'y','n',1337206979,'n','2012','05','16',0,0,20120516152259,0,0),
	(8,1,8,2,0,NULL,'127.0.0.1','Pacific Earthquake Relief','pacific-earthquake-relief','open','y',0,0,0,0,'y','n',1351469565,'n','2012','10','28',0,1354061565,20121030075946,1351584673,1),
	(9,1,8,2,0,NULL,'127.0.0.1','Northern Minnesota Campuses are Fertile Mission Fields','northern-minnesota-campuses-are-fertile-mission-fields','open','y',0,0,0,0,'y','n',1351469757,'n','2012','10','28',0,1354061757,20121030073358,1351800071,6),
	(10,1,8,2,0,NULL,'127.0.0.1','Minnesota Outreach Women: How it Works','minnesota-outreach-women-how-it-works','open','y',0,0,0,0,'y','n',1351321267,'n','2012','10','27',0,1353913267,20121029215608,0,0),
	(11,1,1,2,0,NULL,'127.0.0.1','Staff Directory','staff-directory','open','y',0,0,0,0,'y','n',1351498140,'n','2012','10','29',0,0,20121029082201,0,0),
	(12,1,1,2,0,NULL,'127.0.0.1','History','history','open','y',0,0,0,0,'y','n',1351498163,'n','2012','10','29',0,0,20121029082224,0,0),
	(13,1,1,2,0,NULL,'127.0.0.1','Districts','districts','open','y',0,0,0,0,'y','n',1351526644,'n','2012','10','29',0,0,20121029090404,0,0),
	(14,1,1,2,0,NULL,'127.0.0.1','Events','events','open','y',0,0,0,0,'y','n',1351526744,'n','2012','10','29',0,0,20121029160545,0,0),
	(15,1,1,2,0,NULL,'127.0.0.1','Classifieds','classifieds','open','y',0,0,0,0,'y','n',1351526720,'n','2012','10','29',0,0,20121029090520,0,0),
	(16,1,1,2,0,NULL,'127.0.0.1','Conference Session','conference-session','open','y',0,0,0,0,'y','n',1351526773,'n','2012','10','29',0,0,20121029090613,0,0),
	(17,1,1,2,0,NULL,'127.0.0.1','Get Involved','get-involved','open','y',0,0,0,0,'y','n',1351541356,'n','2012','10','29',0,0,20121029201017,0,0),
	(18,1,1,2,0,NULL,'127.0.0.1','Our Programs','our-programs','open','y',0,0,0,0,'y','n',1351541556,'n','2012','10','29',0,0,20121029131236,0,0),
	(19,1,1,2,0,NULL,'127.0.0.1','Resources','resources','open','y',0,0,0,0,'y','n',1351541519,'n','2012','10','29',0,0,20121029201300,0,0),
	(20,1,1,2,0,NULL,'127.0.0.1','Program 1','program-1','open','y',0,0,0,0,'y','n',1351542041,'n','2012','10','29',0,0,20121029132041,0,0),
	(21,1,1,2,0,NULL,'127.0.0.1','Womens Clinic','womens-clinic','open','y',0,0,0,0,'y','n',1351542004,'n','2012','10','29',0,0,20121029132004,0,0),
	(22,1,1,2,0,NULL,'127.0.0.1','Learn More','learn-more','open','y',0,0,0,0,'y','n',1351542086,'n','2012','10','29',0,0,20121029132126,0,0),
	(23,1,1,2,0,NULL,'127.0.0.1','News','news1','closed','y',0,0,0,0,'y','n',1351542125,'n','2012','10','29',0,0,20121029202406,0,0),
	(24,1,8,2,0,NULL,'127.0.0.1','Consectetur adipiscing elit','consectetur-adipiscing-elit','open','y',0,0,0,0,'y','n',1349211325,'n','2012','10','02',0,1354136125,20121029215626,0,0),
	(25,1,8,2,0,NULL,'127.0.0.1','Donec vel sapien quam','donec-vel-sapien-quam','open','y',0,0,0,0,'y','n',1345669117,'n','2012','08','22',0,1354136257,20121029215638,0,0),
	(26,1,1,2,0,NULL,'127.0.0.1','Related Content','related-content','open','y',0,0,0,0,'y','n',1351575431,'n','2012','10','29',0,0,20121030054312,0,0),
	(27,1,9,2,0,NULL,'127.0.0.1','Imagine No Malaria','imagine-no-malaria','open','y',0,0,0,0,'y','n',1351576467,'n','2012','10','29',0,0,20121030062428,0,0),
	(28,1,9,2,0,NULL,'127.0.0.1','Donate to Japan','donate-to-japan','open','y',0,0,0,0,'y','n',1351578302,'n','2012','10','29',0,0,20121029232502,0,0),
	(29,1,9,2,0,NULL,'127.0.0.1','Prefer Print?','prefer-print','open','y',0,0,0,0,'y','n',1351581270,'n','2012','10','30',0,0,20121030001430,0,0),
	(30,1,1,2,0,NULL,'127.0.0.1','404','fourofour','open','y',0,0,0,0,'y','n',1351593746,'n','2012','10','30',0,0,20121030104427,0,0),
	(31,1,1,2,0,NULL,'127.0.0.1','Success','success','open','y',0,0,0,0,'y','n',1351594132,'n','2012','10','30',0,0,20121030104953,0,0);

/*!40000 ALTER TABLE `ee2_channel_titles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_channels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_channels`;

CREATE TABLE `ee2_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_channels` WRITE;
/*!40000 ALTER TABLE `ee2_channels` DISABLE KEYS */;

INSERT INTO `ee2_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`)
VALUES
	(1,1,'pages','Pages','http://nerdery.loc/','','en',21,0,1351594132,0,'1',1,'open',1,75,'','y','y',NULL,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','y',25,'','',0),
	(2,1,'products','Products','http://nerdery.loc/store','','en',0,0,0,0,'3',1,'open',2,1,'','n','y',0,'all','y','n','n','','','n','n','n','n',0,0,'y','xhtml','safe','n','n','n','n','',0,'','','y','','y',25,'','',0),
	(4,1,'orders','Orders','http://nerdery.loc/account/orders','','en',0,0,0,0,NULL,2,'open',4,21,'','y','y',0,'all','y','n','n','','','y','n','n','n',0,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','y',25,'','',20),
	(8,1,'news','News','http://nerdery.loc/blog','','en',5,7,1351469757,1351800071,'2',1,'open',8,73,'','y','y',NULL,'all','y','n','n','','','y','n','n','n',5000,30,'n','xhtml','safe','n','y','n','n','',30,'','','y','','y',25,'','',0),
	(9,1,'related','Related','http://nerdery.loc/','','en',2,0,1351578302,0,'1',1,'open',1,75,'','y','y',NULL,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','y',25,'','',0);

/*!40000 ALTER TABLE `ee2_channels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_comment_subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_comment_subscriptions`;

CREATE TABLE `ee2_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_comments`;

CREATE TABLE `ee2_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_comments` WRITE;
/*!40000 ALTER TABLE `ee2_comments` DISABLE KEYS */;

INSERT INTO `ee2_comments` (`comment_id`, `site_id`, `entry_id`, `channel_id`, `author_id`, `status`, `name`, `email`, `url`, `location`, `ip_address`, `comment_date`, `edit_date`, `comment`)
VALUES
	(1,1,8,8,2,'o','Jarrett Barnett','me@jarrettbarnett.com','http://www.jarrettbarnett.com','Chico, CA','127.0.0.1',1351584673,NULL,'This is some great lipsum!!!!'),
	(3,1,9,8,2,'o','Jarrett Barnett','me@jarrettbarnett.com','http://www.jarrettbarnett.com','Chico, CA','127.0.0.1',1351585063,NULL,'First comment for this post.'),
	(4,1,9,8,2,'o','Jarrett Barnett','me@jarrettbarnett.com','http://www.jarrettbarnett.com','Chico, CA','127.0.0.1',1351585079,NULL,'Second Comment'),
	(5,1,9,8,2,'o','Jarrett Barnett','me@jarrettbarnett.com','http://www.jarrettbarnett.com','Chico, CA','127.0.0.1',1351585193,NULL,'Third comment...'),
	(6,1,9,8,0,'o','Jarrett Barnett','nogravatar@jarrettbarnett.com','','','127.0.0.1',1351585710,NULL,'Testing the gravatar logic.'),
	(9,1,9,8,0,'o','Random Person','random@person.com','','','127.0.0.1',1351586960,NULL,'No website supplied, bad gravatar email supplied.'),
	(10,1,9,8,0,'o','Jarrett Barnett','test@jarrettbarnett.com','','','127.0.0.1',1351800071,NULL,'some test comment.\n');

/*!40000 ALTER TABLE `ee2_comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_cp_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cp_log`;

CREATE TABLE `ee2_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_cp_log` WRITE;
/*!40000 ALTER TABLE `ee2_cp_log` DISABLE KEYS */;

INSERT INTO `ee2_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`)
VALUES
	(1,1,1,'admin','67.174.62.230',1324429332,'Logged in'),
	(2,1,1,'admin','67.174.62.230',1324430301,'Channel Created:&nbsp;&nbsp;Pages'),
	(3,1,1,'admin','24.10.80.250',1324460624,'Logged in'),
	(4,1,1,'admin','67.174.62.230',1324490527,'Logged in'),
	(5,1,1,'admin','67.174.62.230',1324490669,'Member profile created:&nbsp;&nbsp;Brent.Boyd'),
	(6,1,1,'admin','67.174.62.230',1324490693,'Logged out'),
	(7,1,2,'Brent.Boyd','67.174.62.230',1324490702,'Logged in'),
	(8,1,1,'admin','67.174.62.230',1324508665,'Logged in'),
	(9,1,1,'admin','67.174.62.230',1324511477,'Member profile created:&nbsp;&nbsp;Brent.Temp'),
	(10,1,1,'admin','67.174.62.230',1324511765,'Member profile created:&nbsp;&nbsp;michael.coogan'),
	(11,1,1,'admin','67.174.62.230',1324511835,'Member profile created:&nbsp;&nbsp;bcurtis'),
	(12,1,1,'admin','67.174.62.230',1324511868,'Member profile created:&nbsp;&nbsp;bvargas'),
	(13,1,3,'brent.boyd','67.174.62.230',1324512070,'Logged in'),
	(14,1,1,'admin','67.174.62.230',1324512095,'Logged out'),
	(15,1,2,'jbarnett','67.174.62.230',1324512099,'Logged in'),
	(16,1,3,'brent.boyd','67.174.62.230',1324571821,'Logged in'),
	(17,1,3,'brent.boyd','67.174.62.230',1324572814,'Logged out'),
	(18,1,3,'brent.boyd','67.174.62.230',1324572832,'Logged in'),
	(19,1,3,'brent.boyd','67.174.62.230',1324578260,'Logged in'),
	(20,1,2,'jbarnett','67.174.62.230',1324578870,'Logged in'),
	(21,1,3,'brent.boyd','67.174.62.230',1324592754,'Logged in'),
	(22,1,2,'jbarnett','67.174.62.230',1324592824,'Logged in'),
	(23,1,2,'jbarnett','67.174.62.230',1324593051,'Logged in'),
	(24,1,2,'jbarnett','67.174.62.230',1324593823,'Screen name was changed to:&nbsp;&nbsp;MIchael Coogan'),
	(25,1,2,'jbarnett','67.174.62.230',1324593849,'Screen name was changed to:&nbsp;&nbsp;Brian Curtis'),
	(26,1,2,'jbarnett','67.174.62.230',1324593867,'Screen name was changed to:&nbsp;&nbsp;Brian Vargas'),
	(27,1,2,'jbarnett','67.174.62.230',1324599908,'Logged in'),
	(28,1,2,'jbarnett','67.174.62.230',1324600274,'Logged in'),
	(29,1,3,'brent.boyd','67.174.62.230',1324656441,'Logged in'),
	(30,1,3,'brent.boyd','67.174.62.230',1325013071,'Logged in'),
	(31,1,2,'jbarnett','127.0.0.1',1325092385,'Logged in'),
	(32,1,2,'jbarnett','127.0.0.1',1325871832,'Logged in'),
	(33,1,2,'jbarnett','127.0.0.1',1326144417,'Logged in'),
	(34,1,2,'jbarnett','127.0.0.1',1332211037,'Logged in'),
	(35,1,2,'jbarnett','127.0.0.1',1332402759,'Logged in'),
	(36,1,2,'jbarnett','127.0.0.1',1332406093,'Field Group Created:&nbsp;Primary Fields'),
	(37,1,2,'jbarnett','127.0.0.1',1332407015,'Field Group Created:&nbsp;Products - Channel Fields'),
	(38,1,2,'jbarnett','127.0.0.1',1332407022,'Field Group Created:&nbsp;Orders - Channel Fields'),
	(39,1,2,'jbarnett','127.0.0.1',1332407448,'Field Group Created:&nbsp;Coupons - Channel Fields'),
	(40,1,2,'jbarnett','127.0.0.1',1332407505,'Field Group Created:&nbsp;Discounts - Channel Fields'),
	(41,1,2,'jbarnett','127.0.0.1',1332432785,'Logged in'),
	(42,1,2,'jbarnett','127.0.0.1',1332439030,'Channel Created:&nbsp;&nbsp;Blog'),
	(43,1,2,'jbarnett','127.0.0.1',1332439077,'Field Group Created:&nbsp;Blog'),
	(44,1,2,'jbarnett','127.0.0.1',1332448636,'Logged in'),
	(45,1,2,'jbarnett','127.0.0.1',1332454769,'Logged in'),
	(46,1,2,'jbarnett','127.0.0.1',1332459158,'Logged in'),
	(47,1,2,'jbarnett','127.0.0.1',1332518397,'Logged in'),
	(48,1,2,'jbarnett','127.0.0.1',1332806292,'Logged in'),
	(49,1,2,'jbarnett','127.0.0.1',1332866820,'Logged in'),
	(50,1,2,'jbarnett','127.0.0.1',1332879790,'Logged in'),
	(51,1,2,'jbarnett','127.0.0.1',1332880513,'Logged out'),
	(52,1,2,'jbarnett','127.0.0.1',1332883934,'Category Group Created:&nbsp;&nbsp;Pages'),
	(53,1,2,'jbarnett','127.0.0.1',1332883941,'Category Group Created:&nbsp;&nbsp;Blog'),
	(54,1,2,'jbarnett','127.0.0.1',1332883947,'Category Group Created:&nbsp;&nbsp;Store'),
	(55,1,2,'jbarnett','127.0.0.1',1332885003,'Status Group Created:&nbsp;Orders Channel'),
	(56,1,2,'jbarnett','127.0.0.1',1334014962,'Logged out'),
	(57,1,2,'jbarnett','127.0.0.1',1334093459,'Member profile created:&nbsp;&nbsp;storeadmin'),
	(58,1,2,'jbarnett','127.0.0.1',1334703190,'Member Group Created:&nbsp;&nbsp;Administrators'),
	(59,1,2,'jbarnett','127.0.0.1',1334703643,'Member Group Created:&nbsp;&nbsp;Publishers'),
	(60,1,2,'jbarnett','127.0.0.1',1334703810,'Member Group Created:&nbsp;&nbsp;Editors'),
	(61,1,2,'jbarnett','127.0.0.1',1334703919,'Member Group Updated:&nbsp;&nbsp;Editors'),
	(62,1,2,'jbarnett','127.0.0.1',1334703977,'Member Group Updated:&nbsp;&nbsp;Publishers'),
	(63,1,2,'jbarnett','127.0.0.1',1334704055,'Member Group Updated:&nbsp;&nbsp;Administrators'),
	(64,1,2,'jbarnett','127.0.0.1',1334764881,'Logged in'),
	(65,1,2,'jbarnett','127.0.0.1',1334776315,'Logged in'),
	(66,1,2,'jbarnett','127.0.0.1',1334785615,'Logged in'),
	(67,1,2,'jbarnett','127.0.0.1',1334944209,'Logged in'),
	(68,1,2,'jbarnett','127.0.0.1',1335461095,'Logged in'),
	(69,1,2,'jbarnett','127.0.0.1',1335461817,'Member Field Deleted:&nbsp;&nbsp;Last Name'),
	(70,1,2,'jbarnett','127.0.0.1',1335461935,'Member Field Deleted:&nbsp;&nbsp;Shipping | Last Name'),
	(71,1,2,'jbarnett','67.182.163.29',1335543213,'Logged in'),
	(72,1,3,'brent.boyd','67.182.163.29',1335543226,'Logged in'),
	(73,1,2,'jbarnett','67.182.163.29',1335545696,'Logged out'),
	(74,1,3,'brent.boyd','67.182.163.29',1335568149,'Logged in'),
	(75,1,2,'jbarnett','67.182.163.29',1335568291,'Logged in'),
	(76,1,2,'jbarnett','67.182.163.29',1336412183,'Logged in'),
	(77,1,3,'brent.boyd','67.182.163.29',1336413046,'Logged in'),
	(78,1,3,'brent.boyd','67.182.163.29',1336423728,'Logged in'),
	(79,1,2,'jbarnett','67.182.163.29',1336577343,'Logged in'),
	(80,1,2,'jbarnett','67.182.163.29',1336586121,'Logged in'),
	(81,1,2,'jbarnett','67.182.163.29',1336594736,'Logged in'),
	(82,1,2,'jbarnett','67.182.163.29',1336594782,'Logged out'),
	(83,1,2,'jbarnett','67.182.163.29',1336675295,'Logged in'),
	(84,1,3,'brent.boyd','67.182.163.29',1337010102,'Logged in'),
	(85,1,3,'brent.boyd','67.182.163.29',1337034137,'Logged in'),
	(86,1,2,'jbarnett','67.182.163.29',1337188180,'Logged in'),
	(87,1,2,'jbarnett','67.182.163.29',1337204109,'Logged in'),
	(88,1,2,'jbarnett','67.182.163.29',1337206835,'Channel Deleted:&nbsp;&nbsp;Store > Coupon Codes'),
	(89,1,2,'jbarnett','67.182.163.29',1337206843,'Channel Deleted:&nbsp;&nbsp;Store > Discounts'),
	(90,1,2,'jbarnett','67.182.163.29',1337206850,'Channel Deleted:&nbsp;&nbsp;Store > Product Packages'),
	(91,1,2,'jbarnett','67.182.163.29',1337206857,'Channel Deleted:&nbsp;&nbsp;Store > Purchased Items'),
	(92,1,2,'jbarnett','67.182.163.29',1337207819,'Custom Field Deleted:&nbsp;Price'),
	(93,1,2,'jbarnett','67.182.163.29',1337207823,'Custom Field Deleted:&nbsp;Retail Price'),
	(94,1,2,'jbarnett','67.182.163.29',1337207828,'Custom Field Deleted:&nbsp;Wholesale Cost'),
	(95,1,2,'jbarnett','67.182.163.29',1337207833,'Custom Field Deleted:&nbsp;Shipping Price'),
	(96,1,2,'jbarnett','67.182.163.29',1337207837,'Custom Field Deleted:&nbsp;Weight'),
	(97,1,2,'jbarnett','67.182.163.29',1337207841,'Custom Field Deleted:&nbsp;Inventory'),
	(98,1,2,'jbarnett','67.182.163.29',1337207848,'Custom Field Deleted:&nbsp;SKU'),
	(99,1,2,'jbarnett','67.182.163.29',1337207853,'Custom Field Deleted:&nbsp;Disable tax for this item?'),
	(100,1,2,'jbarnett','67.182.163.29',1337207857,'Custom Field Deleted:&nbsp;Disable shipping for this item?'),
	(101,1,2,'jbarnett','67.182.163.29',1337207877,'Custom Field Deleted:&nbsp;Options - Size'),
	(102,1,2,'jbarnett','67.182.163.29',1337207881,'Custom Field Deleted:&nbsp;Options - Other'),
	(103,1,2,'jbarnett','67.182.163.29',1337207887,'Custom Field Deleted:&nbsp;Options - Color'),
	(104,1,2,'jbarnett','67.182.163.29',1337207896,'Custom Field Deleted:&nbsp;Related Product 1'),
	(105,1,2,'jbarnett','67.182.163.29',1337207901,'Custom Field Deleted:&nbsp;Related Product 2'),
	(106,1,2,'jbarnett','67.182.163.29',1337207907,'Custom Field Deleted:&nbsp;Related Product 3'),
	(107,1,2,'jbarnett','67.182.163.29',1337270617,'Logged in'),
	(108,1,2,'jbarnett','67.182.163.29',1337791485,'Logged in'),
	(109,1,2,'jarrett.barnett','67.182.163.29',1338395104,'Logged in'),
	(110,1,2,'jarrett.barnett','67.182.163.29',1339089557,'Logged in'),
	(111,1,2,'jarrett.barnett','67.182.163.29',1339089943,'Logged in'),
	(112,1,2,'jarrett.barnett','67.182.163.29',1339089944,'Logged in'),
	(113,1,2,'jarrett.barnett','67.182.163.29',1339094416,'Logged in'),
	(114,1,2,'jarrett.barnett','67.182.163.29',1339094418,'Logged out'),
	(115,1,2,'jarrett.barnett','67.182.163.29',1339102278,'Logged in'),
	(116,1,2,'jarrett.barnett','67.182.163.29',1339452458,'Logged in'),
	(117,1,2,'jarrett.barnett','67.182.163.29',1339525734,'Logged in'),
	(118,1,2,'jarrett.barnett','67.182.163.29',1339530238,'Logged in'),
	(119,1,2,'jarrett.barnett','67.182.163.29',1339605498,'Logged in'),
	(120,1,2,'jarrett.barnett','67.182.163.29',1339624580,'Logged in'),
	(121,1,2,'jarrett.barnett','67.182.163.29',1339773231,'Logged in'),
	(122,1,2,'jarrett.barnett','67.182.163.29',1340379443,'Logged in'),
	(123,1,2,'jarrett.barnett','24.10.81.212',1342196342,'Logged in'),
	(124,1,2,'jarrett.barnett','67.182.163.29',1344877628,'Logged in'),
	(125,1,2,'jarrett.barnett','67.182.163.29',1346447601,'Logged in'),
	(126,1,2,'jarrett.barnett','67.182.163.29',1347046562,'Logged in'),
	(127,1,3,'brent.boyd','67.182.163.29',1347059529,'Logged in'),
	(128,1,2,'jarrett.barnett','67.182.163.29',1348006165,'Logged in'),
	(129,1,2,'jarrett.barnett','67.182.163.29',1348271273,'Logged out'),
	(130,1,2,'jarrett.barnett','67.182.163.29',1350414462,'Username was changed to:&nbsp;&nbsp;jarrettbarnett'),
	(131,1,6,'bvargas','67.182.163.29',1350923510,'Screen name was changed to:&nbsp;&nbsp;Bobby Vargas'),
	(132,1,6,'bvargas','67.182.163.29',1350923530,'Logged out'),
	(133,1,2,'jarrettbarnett','127.0.0.1',1351208527,'Logged in'),
	(134,1,2,'jarrettbarnett','127.0.0.1',1351209217,'Logged in'),
	(135,1,2,'jarrettbarnett','127.0.0.1',1351314114,'Logged in'),
	(136,1,2,'jarrettbarnett','127.0.0.1',1351369968,'Logged in'),
	(137,1,2,'jarrettbarnett','127.0.0.1',1351410771,'Logged out'),
	(138,1,2,'jarrettbarnett','127.0.0.1',1351411134,'Logged out'),
	(139,1,2,'jarrettbarnett','127.0.0.1',1351411134,'Logged out'),
	(140,1,2,'jarrettbarnett','127.0.0.1',1351542697,'Field group Deleted:&nbsp;&nbsp;Coupon Codes'),
	(141,1,2,'jarrettbarnett','127.0.0.1',1351542701,'Field group Deleted:&nbsp;&nbsp;Discounts'),
	(142,1,2,'jarrettbarnett','127.0.0.1',1351542709,'Field group Deleted:&nbsp;&nbsp;Packages'),
	(143,1,2,'jarrettbarnett','127.0.0.1',1351542721,'Field group Deleted:&nbsp;&nbsp;Purchased Items'),
	(144,1,2,'jarrettbarnett','127.0.0.1',1351551769,'Logged out'),
	(145,1,2,'jarrettbarnett','127.0.0.1',1351553204,'Logged out'),
	(146,1,2,'jarrettbarnett','127.0.0.1',1351574751,'Channel Created:&nbsp;&nbsp;Related'),
	(147,1,2,'jarrettbarnett','127.0.0.1',1351585942,'Logged out'),
	(148,1,2,'jarrettbarnett','127.0.0.1',1351609608,'Member profile created:&nbsp;&nbsp;nerdery'),
	(149,1,2,'jarrettbarnett','127.0.0.1',1351627755,'Logged out'),
	(150,1,2,'jarrettbarnett','127.0.0.1',1351800357,'Logged out'),
	(151,1,2,'jarrettbarnett','127.0.0.1',1351800376,'Logged out');

/*!40000 ALTER TABLE `ee2_cp_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_cp_search_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_cp_search_index`;

CREATE TABLE `ee2_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_developer_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_developer_log`;

CREATE TABLE `ee2_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned DEFAULT NULL,
  `viewed` char(1) DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_developer_log` WRITE;
/*!40000 ALTER TABLE `ee2_developer_log` DISABLE KEYS */;

INSERT INTO `ee2_developer_log` (`log_id`, `timestamp`, `viewed`, `description`, `function`, `line`, `file`, `deprecated_since`, `use_instead`)
VALUES
	(1,1334694593,'y',NULL,'get_upload_preferences()',61,'/Volumes/Development/local/ee2.local/root/library/expressionengine/third_party/matrix/celltypes/file.php','2.4','File_upload_preferences_model::get_file_upload_preferences() to support config variable overrides'),
	(2,1334706055,'y',NULL,'get_upload_preferences()',658,'/Volumes/Development/local/ee2.local/root/library/expressionengine/third_party/wygwam/ft.wygwam.php','2.2','File_upload_preferences_model::get_file_upload_preferences()'),
	(3,1334706055,'y',NULL,'get_upload_preferences()',522,'/Volumes/Development/local/ee2.local/root/library/expressionengine/models/tools_model.php','2.4','File_upload_preferences_model::get_file_upload_preferences() to support config variable overrides'),
	(4,1334706055,'y',NULL,'get_upload_preferences()',494,'/Volumes/Development/local/ee2.local/root/library/expressionengine/third_party/wygwam/ft.wygwam.php','2.2','File_upload_preferences_model::get_file_upload_preferences()'),
	(5,1334706055,'y',NULL,'get_upload_preferences()',120,'/Volumes/Development/local/ee2.local/root/library/expressionengine/third_party/matrix/celltypes/file.php','2.2','File_upload_preferences_model::get_file_upload_preferences()');

/*!40000 ALTER TABLE `ee2_developer_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_edit_alarm_ext
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_edit_alarm_ext`;

CREATE TABLE `ee2_edit_alarm_ext` (
  `alert_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `alert_type` char(1) NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `resource_id` int(10) unsigned NOT NULL,
  `time_of_edit` int(10) unsigned NOT NULL,
  PRIMARY KEY (`alert_id`),
  KEY `alert_type` (`alert_type`),
  KEY `author_id` (`author_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `ee2_edit_alarm_ext` WRITE;
/*!40000 ALTER TABLE `ee2_edit_alarm_ext` DISABLE KEYS */;

INSERT INTO `ee2_edit_alarm_ext` (`alert_id`, `alert_type`, `author_id`, `resource_id`, `time_of_edit`)
VALUES
	(304,'t',2,57,1351609719),
	(303,'t',2,60,1351609690);

/*!40000 ALTER TABLE `ee2_edit_alarm_ext` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_email_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_email_cache`;

CREATE TABLE `ee2_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_email_cache_mg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_email_cache_mg`;

CREATE TABLE `ee2_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_email_cache_ml
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_email_cache_ml`;

CREATE TABLE `ee2_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_email_console_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_email_console_cache`;

CREATE TABLE `ee2_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_email_tracker
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_email_tracker`;

CREATE TABLE `ee2_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_entry_ping_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_entry_ping_status`;

CREATE TABLE `ee2_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_entry_versioning
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_entry_versioning`;

CREATE TABLE `ee2_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_entry_versioning` WRITE;
/*!40000 ALTER TABLE `ee2_entry_versioning` DISABLE KEYS */;

INSERT INTO `ee2_entry_versioning` (`version_id`, `entry_id`, `channel_id`, `author_id`, `version_date`, `version_data`)
VALUES
	(1,2,1,2,1351469194,'a:30:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:22:\"structure__template_id\";s:1:\"5\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"News\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:4:\"news\";s:9:\"url_title\";s:4:\"news\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:10:\"entry_date\";i:1332438933;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"8\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";}'),
	(2,8,8,2,1351469617,'a:22:{s:8:\"entry_id\";i:8;s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_73\";s:2861:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:21:\"field_id_74_directory\";s:0:\"\";s:10:\"entry_date\";i:1351469556;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061556;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"18\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_74\";N;}'),
	(3,8,8,2,1351469717,'a:22:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_73\";s:2861:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:21:\"field_id_74_directory\";s:0:\"\";s:10:\"entry_date\";i:1351469536;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061536;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"18\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_74\";N;}'),
	(4,9,8,2,1351469749,'a:22:{s:8:\"entry_id\";i:9;s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_73\";s:3313:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n<p>\n	&nbsp;</p>\n\";s:21:\"field_id_74_directory\";s:0:\"\";s:10:\"entry_date\";i:1351469748;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061748;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"18\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_74\";N;}'),
	(5,1,1,2,1351483078,'a:29:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:10:\"entry_date\";i:1324579677;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(6,1,1,2,1351488802,'a:29:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:10:\"entry_date\";i:1324579641;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(7,1,1,2,1351493212,'a:30:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:10:\"entry_date\";i:1324579671;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(8,1,1,2,1351493283,'a:30:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:10:\"entry_date\";i:1324579622;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";}'),
	(9,1,1,2,1351493421,'a:30:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:10:\"entry_date\";i:1324579640;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(10,10,8,2,1351494240,'a:22:{s:8:\"entry_id\";i:10;s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:38:\"Minnesota Outreach Women: How it Works\";s:9:\"url_title\";s:37:\"minnesota-outreach-women-how-it-works\";s:21:\"field_id_74_directory\";s:0:\"\";s:11:\"field_id_73\";s:419:\"<p>\n	Ras in blandit libero. In hendrerit interdum sem, cursus auctor tellus pulvinar ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent mattis cursus Pellent mattis pulvinar. Nulla a est in <a href=\"#\">turpis fermentum luctus</a>. Nulla ut nunc consectetur neque suscipit pharetra id quis urna. Morbi justo nisi, egestas in blandit id, tristique sed risus. Nulla a est in turpis fermentum luctus.</p>\n\";s:10:\"entry_date\";i:1351321319;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354086119;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"18\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_74\";N;}'),
	(11,10,8,2,1351494395,'a:22:{s:8:\"entry_id\";s:2:\"10\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:38:\"Minnesota Outreach Women: How it Works\";s:9:\"url_title\";s:37:\"minnesota-outreach-women-how-it-works\";s:21:\"field_id_74_directory\";s:1:\"3\";s:11:\"field_id_73\";s:425:\"<p>\n	Ras in blandit libero. In hendrerit interdum sem, cursus auctor tellus pulvinar ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent mattis cursus Pellent mattis pulvinar. Nulla a est in <a href=\"#\">turpis fermentum luctus</a>. Nulla ut nunc consectetur neque suscipit pharetra id quis urna. Morbi justo nisi, egestas in blandit id, tristique sed risus. Nulla a est in turpis fermentum luctus.&nbsp;</p>\n\";s:10:\"entry_date\";i:1351321294;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354086094;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:37:\"minnesota-outreach-women-how-it-works\";s:22:\"structure__template_id\";s:2:\"18\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_74\";s:29:\"{filedir_3}fpo-library-1.jpeg\";}'),
	(12,10,8,2,1351494406,'a:22:{s:8:\"entry_id\";s:2:\"10\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:38:\"Minnesota Outreach Women: How it Works\";s:9:\"url_title\";s:37:\"minnesota-outreach-women-how-it-works\";s:21:\"field_id_74_directory\";s:1:\"3\";s:11:\"field_id_73\";s:425:\"<p>\n	Ras in blandit libero. In hendrerit interdum sem, cursus auctor tellus pulvinar ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent mattis cursus Pellent mattis pulvinar. Nulla a est in <a href=\"#\">turpis fermentum luctus</a>. Nulla ut nunc consectetur neque suscipit pharetra id quis urna. Morbi justo nisi, egestas in blandit id, tristique sed risus. Nulla a est in turpis fermentum luctus.&nbsp;</p>\n\";s:10:\"entry_date\";i:1351321305;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354086105;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:37:\"minnesota-outreach-women-how-it-works\";s:22:\"structure__template_id\";s:2:\"18\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_74\";s:29:\"{filedir_3}fpo-library-1.jpeg\";}'),
	(13,1,1,2,1351494458,'a:31:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:11:\"field_id_86\";s:83:\"[10] [minnesota-outreach-women-how-it-works] Minnesota Outreach Women: How it Works\";s:10:\"entry_date\";i:1324579657;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(14,9,8,2,1351495360,'a:22:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:21:\"field_id_74_directory\";s:0:\"\";s:11:\"field_id_73\";s:3318:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n<p>\n	&nbsp;</p>\n\";s:10:\"entry_date\";i:1351469739;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061739;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"18\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_74\";N;}'),
	(15,8,8,2,1351495364,'a:22:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:21:\"field_id_74_directory\";s:0:\"\";s:11:\"field_id_73\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:10:\"entry_date\";i:1351469563;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061563;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"18\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_74\";N;}'),
	(16,8,8,2,1351495534,'a:22:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:21:\"field_id_74_directory\";s:1:\"3\";s:11:\"field_id_73\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:10:\"entry_date\";i:1351469553;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061553;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"18\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_74\";s:20:\"{filedir_3}home.jpeg\";}'),
	(17,9,8,2,1351495777,'a:22:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:1:\"1\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:21:\"field_id_74_directory\";s:1:\"3\";s:11:\"field_id_73\";s:3318:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n<p>\n	&nbsp;</p>\n\";s:10:\"entry_date\";i:1351469736;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061736;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"18\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_74\";s:22:\"{filedir_3}home-1.jpeg\";}'),
	(18,1,1,2,1351495901,'a:31:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:11:\"field_id_86\";s:83:\"[10] [minnesota-outreach-women-how-it-works] Minnesota Outreach Women: How it Works\";s:10:\"entry_date\";i:1324579660;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(19,5,1,2,1351497085,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1337206704;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(20,5,1,2,1351497179,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1337206738;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(21,5,1,2,1351497445,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1036:\"<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1337206704;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(22,5,1,2,1351497508,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1337206707;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(23,5,1,2,1351497538,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1243:\"<h2>Secondary Title (H2)</h2>\n                        <p>The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n                        <h3>Headline Three (H3)</h3>\n                        <ul>\n                            <li>Suspendisse ut ante leo, id blandit tortor.</li>\n                            <li>Aenean luctus lacinia quam, in porta odio auctor at.</li>\n                            <li>Sed dictum ligula interdum nisl euismod porta.</li>\n                            <li>Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n                        </ul>\n                        <p>The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1337206737;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(24,11,1,2,1351498153,'a:32:{s:8:\"entry_id\";i:11;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Staff Directory\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:15:\"staff-directory\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"5\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351498152;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(25,12,1,2,1351498179,'a:32:{s:8:\"entry_id\";i:12;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:7:\"History\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:7:\"history\";s:11:\"field_id_72\";s:3781:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis odio eleifend elit rhoncus sodales sit amet non nibh. Nulla eleifend nisi vel purus facilisis elementum. Aenean posuere ante quis dui aliquam elementum. Maecenas est turpis, hendrerit eu suscipit ut, aliquet mattis urna. Suspendisse potenti. Aenean sagittis ornare ligula a lobortis. Duis in felis eros, et consequat purus. Suspendisse potenti. Maecenas blandit, neque sit amet cursus placerat, tortor arcu sagittis ipsum, vel accumsan elit eros ut turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean accumsan aliquet rutrum. Morbi pellentesque ullamcorper congue. Fusce et justo sit amet leo volutpat hendrerit. Etiam mattis, risus vel cursus hendrerit, lacus diam aliquet lacus, vestibulum hendrerit justo ligula vitae risus.</p>\n<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n<p>\n	Integer pretium lacus vitae metus imperdiet sed commodo nisl convallis. Proin ipsum ipsum, lacinia at porta eu, elementum sed ante. Cras ultrices sem laoreet justo dictum eu volutpat orci pellentesque. In pharetra suscipit scelerisque. Curabitur pharetra velit non mauris varius consectetur sit amet sit amet diam. Vivamus quis ante sed sem pretium malesuada nec eget erat. Sed vel sapien non eros pretium convallis nec quis dolor. Aenean nec eros vitae eros egestas condimentum. Morbi facilisis, dui sed ultrices pulvinar, ipsum est porttitor elit, et sollicitudin lorem quam a odio. Mauris mauris libero, malesuada sed blandit non, consectetur vitae nunc.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"5\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351498178;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(26,11,1,2,1351498817,'a:32:{s:8:\"entry_id\";s:2:\"11\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Staff Directory\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:15:\"staff-directory\";s:9:\"url_title\";s:15:\"staff-directory\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"5\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351498156;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(27,11,1,2,1351498881,'a:32:{s:8:\"entry_id\";s:2:\"11\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Staff Directory\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:15:\"staff-directory\";s:9:\"url_title\";s:15:\"staff-directory\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"5\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1351498160;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";s:37:\"{filedir_2}bg-banner-overlay-home.png\";}'),
	(28,11,1,2,1351498921,'a:32:{s:8:\"entry_id\";s:2:\"11\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Staff Directory\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:15:\"staff-directory\";s:9:\"url_title\";s:15:\"staff-directory\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"5\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1351498140;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";s:26:\"{filedir_2}fpo-library.jpg\";}'),
	(29,12,1,2,1351498944,'a:32:{s:8:\"entry_id\";s:2:\"12\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:7:\"History\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:7:\"history\";s:9:\"url_title\";s:7:\"history\";s:11:\"field_id_72\";s:3781:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis odio eleifend elit rhoncus sodales sit amet non nibh. Nulla eleifend nisi vel purus facilisis elementum. Aenean posuere ante quis dui aliquam elementum. Maecenas est turpis, hendrerit eu suscipit ut, aliquet mattis urna. Suspendisse potenti. Aenean sagittis ornare ligula a lobortis. Duis in felis eros, et consequat purus. Suspendisse potenti. Maecenas blandit, neque sit amet cursus placerat, tortor arcu sagittis ipsum, vel accumsan elit eros ut turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean accumsan aliquet rutrum. Morbi pellentesque ullamcorper congue. Fusce et justo sit amet leo volutpat hendrerit. Etiam mattis, risus vel cursus hendrerit, lacus diam aliquet lacus, vestibulum hendrerit justo ligula vitae risus.</p>\n<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n<p>\n	Integer pretium lacus vitae metus imperdiet sed commodo nisl convallis. Proin ipsum ipsum, lacinia at porta eu, elementum sed ante. Cras ultrices sem laoreet justo dictum eu volutpat orci pellentesque. In pharetra suscipit scelerisque. Curabitur pharetra velit non mauris varius consectetur sit amet sit amet diam. Vivamus quis ante sed sem pretium malesuada nec eget erat. Sed vel sapien non eros pretium convallis nec quis dolor. Aenean nec eros vitae eros egestas condimentum. Morbi facilisis, dui sed ultrices pulvinar, ipsum est porttitor elit, et sollicitudin lorem quam a odio. Mauris mauris libero, malesuada sed blandit non, consectetur vitae nunc.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"5\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1351498163;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";s:37:\"{filedir_2}bg-banner-overlay-home.png\";}'),
	(30,13,1,2,1351526705,'a:32:{s:8:\"entry_id\";i:13;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:9:\"Districts\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:9:\"districts\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351526644;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(31,14,1,2,1351526730,'a:32:{s:8:\"entry_id\";i:14;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:6:\"Events\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:6:\"events\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351526729;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(32,14,1,2,1351526745,'a:32:{s:8:\"entry_id\";s:2:\"14\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:6:\"Events\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:6:\"events\";s:9:\"url_title\";s:6:\"events\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351526744;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(33,15,1,2,1351526781,'a:32:{s:8:\"entry_id\";i:15;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:11:\"Classifieds\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:11:\"classifieds\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351526720;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(34,16,1,2,1351526834,'a:33:{s:8:\"entry_id\";i:16;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:18:\"Conference Session\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:18:\"conference-session\";s:11:\"field_id_72\";s:2248:\"<p>\n	Vestibulum accumsan diam a massa vestibulum ornare. Phasellus ac neque ut lectus rutrum facilisis. Nunc malesuada interdum eros, non varius mi adipiscing ac. Mauris tellus neque, lobortis id fringilla sit amet, sollicitudin nec metus. Aliquam at leo nec sapien venenatis egestas. Proin dolor enim, pharetra ut pulvinar a, pretium a felis. In lectus erat, semper sit amet dictum et, luctus id augue. Donec sed est purus. Fusce sollicitudin sapien nec sapien iaculis pharetra. Duis luctus nulla eu enim sodales sit amet malesuada elit condimentum. Sed et nisi nec lorem rutrum ullamcorper. Cras at urna nec erat tristique viverra.</p>\n<p>\n	Quisque id ante sed risus elementum euismod. Praesent pharetra tristique justo. Curabitur scelerisque orci ut justo tincidunt venenatis. Vestibulum ac nunc quam. Nam fringilla ligula erat, quis volutpat lacus. Pellentesque quis dui erat. In rutrum nunc a arcu consequat aliquet. Etiam nec diam velit. Sed elit dolor, fringilla non elementum sagittis, accumsan in tellus. Nulla placerat consectetur faucibus. Vestibulum purus sapien, accumsan eu aliquet quis, semper in massa. Cras consequat, diam et tempus feugiat, nibh nisl imperdiet tortor, nec pellentesque turpis purus sit amet eros. Cras nibh quam, adipiscing ut sodales non, semper eget nisi. Aenean vel velit tellus. Curabitur quis diam vel nunc auctor auctor.</p>\n<p>\n	Sed ut dolor dolor, eu interdum diam. Vestibulum magna lorem, laoreet vitae blandit et, suscipit et sapien. In sit amet dapibus justo. Donec pharetra scelerisque bibendum. Donec sem diam, sollicitudin ut porta vel, gravida vitae turpis. Quisque tortor nisl, consectetur in pretium at, ultrices sed arcu. Aliquam risus turpis, malesuada id condimentum ut, congue eu elit. Nullam porttitor interdum urna, quis tempor neque ornare nec. Fusce nisi lacus, scelerisque id posuere in, aliquam eget ipsum. Aliquam pellentesque, elit id posuere viverra, massa sem bibendum mi, non ornare odio libero at lacus. Proin quis elit urna, non semper velit. Vivamus pretium mi et justo dignissim scelerisque. Maecenas sodales semper nibh, ac iaculis nibh ullamcorper vel. Sed non tellus nec velit viverra bibendum gravida nec magna. Cras congue venenatis erat, id lacinia nibh fringilla a.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_85\";s:0:\"\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351526773;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(35,9,8,2,1351533490,'a:23:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:21:\"field_id_74_directory\";s:1:\"3\";s:11:\"field_id_73\";s:3318:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n<p>\n	&nbsp;</p>\n\";s:10:\"entry_date\";i:1351469709;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061709;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_74\";s:22:\"{filedir_3}home-1.jpeg\";}'),
	(36,9,8,2,1351533499,'a:23:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:21:\"field_id_74_directory\";s:1:\"3\";s:11:\"field_id_73\";s:3318:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n<p>\n	&nbsp;</p>\n\";s:10:\"entry_date\";i:1351469718;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061718;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_74\";s:22:\"{filedir_3}home-1.jpeg\";}'),
	(37,2,1,2,1351533940,'a:33:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:22:\"structure__template_id\";s:2:\"53\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"News\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:4:\"news\";s:9:\"url_title\";s:4:\"news\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1332438939;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"8\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(38,2,1,2,1351541356,'a:33:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"2005\";s:22:\"structure__template_id\";s:2:\"53\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:13:\"News & Events\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:4:\"news\";s:9:\"url_title\";s:4:\"news\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1332438915;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"8\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(39,17,1,2,1351541368,'a:32:{s:8:\"entry_id\";i:17;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:12:\"Get Involved\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:12:\"get-involved\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351541367;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(40,17,1,2,1351541417,'a:32:{s:8:\"entry_id\";s:2:\"17\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:12:\"Get Involved\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:12:\"get-involved\";s:9:\"url_title\";s:12:\"get-involved\";s:11:\"field_id_72\";s:1970:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod sem sed velit pretium interdum. Aenean feugiat iaculis ligula, ut scelerisque diam commodo in. Phasellus tincidunt leo iaculis quam dictum ultricies. In vehicula mi eget magna convallis egestas. Cras volutpat justo et leo sollicitudin eget malesuada neque dictum. Maecenas venenatis, libero nec accumsan feugiat, metus neque venenatis leo, vitae pharetra orci orci eu lacus. Maecenas eros purus, congue sed venenatis accumsan, tincidunt vitae velit. Suspendisse eu magna auctor ligula feugiat vulputate. Donec sit amet laoreet dui. Sed hendrerit tempor elit, eget egestas arcu ornare sit amet.</p>\n<p>\n	Nullam feugiat mattis turpis. Aenean vel magna mi. Sed sed elit felis. Sed commodo commodo facilisis. Proin mauris ipsum, rutrum et accumsan ac, bibendum eu nisl. Maecenas lorem ante, dignissim eu sodales in, hendrerit a mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus odio enim, euismod suscipit feugiat sed, commodo eu eros. In eget sem imperdiet augue sollicitudin mattis. Vestibulum posuere porta elementum.</p>\n<p>\n	Pellentesque aliquet, ipsum sed tempus malesuada, dolor lorem mattis orci, sit amet ornare nisl purus non magna. Morbi non nunc diam. Cras cursus sollicitudin dui, nec aliquam odio feugiat ac. Sed tincidunt tempus placerat. Nullam tincidunt, est nec faucibus facilisis, nisl lectus euismod libero, in eleifend quam ipsum a erat. Fusce erat arcu, tincidunt vel sodales in, dapibus euismod metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fermentum suscipit sapien, at scelerisque purus consequat at. Nullam pellentesque posuere diam eget aliquam. Morbi tristique fermentum iaculis. Sed semper vehicula lectus eu aliquam. Praesent porta urna sed sem aliquet at porttitor enim feugiat. Proin dignissim luctus nulla, vel interdum tellus dapibus at. Fusce a nibh egestas neque laoreet hendrerit.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351541356;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(41,1,1,2,1351541430,'a:33:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:11:\"field_id_86\";s:83:\"[10] [minnesota-outreach-women-how-it-works] Minnesota Outreach Women: How it Works\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1324579649;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(42,18,1,2,1351541557,'a:32:{s:8:\"entry_id\";i:18;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:12:\"Our Programs\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:12:\"our-programs\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351541556;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(43,19,1,2,1351541569,'a:32:{s:8:\"entry_id\";i:19;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"Research\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:8:\"research\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351541568;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(44,19,1,2,1351541580,'a:32:{s:8:\"entry_id\";s:2:\"19\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:9:\"Resources\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:9:\"resources\";s:9:\"url_title\";s:9:\"resources\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351541519;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(45,3,1,2,1351541680,'a:33:{s:8:\"entry_id\";s:1:\"3\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:22:\"structure__template_id\";s:2:\"53\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:5:\"Store\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:5:\"store\";s:9:\"url_title\";s:5:\"store\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1332439299;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(46,20,1,2,1351542042,'a:32:{s:8:\"entry_id\";i:20;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:9:\"Program 1\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:9:\"program-1\";s:11:\"field_id_72\";s:1970:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod sem sed velit pretium interdum. Aenean feugiat iaculis ligula, ut scelerisque diam commodo in. Phasellus tincidunt leo iaculis quam dictum ultricies. In vehicula mi eget magna convallis egestas. Cras volutpat justo et leo sollicitudin eget malesuada neque dictum. Maecenas venenatis, libero nec accumsan feugiat, metus neque venenatis leo, vitae pharetra orci orci eu lacus. Maecenas eros purus, congue sed venenatis accumsan, tincidunt vitae velit. Suspendisse eu magna auctor ligula feugiat vulputate. Donec sit amet laoreet dui. Sed hendrerit tempor elit, eget egestas arcu ornare sit amet.</p>\n<p>\n	Nullam feugiat mattis turpis. Aenean vel magna mi. Sed sed elit felis. Sed commodo commodo facilisis. Proin mauris ipsum, rutrum et accumsan ac, bibendum eu nisl. Maecenas lorem ante, dignissim eu sodales in, hendrerit a mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus odio enim, euismod suscipit feugiat sed, commodo eu eros. In eget sem imperdiet augue sollicitudin mattis. Vestibulum posuere porta elementum.</p>\n<p>\n	Pellentesque aliquet, ipsum sed tempus malesuada, dolor lorem mattis orci, sit amet ornare nisl purus non magna. Morbi non nunc diam. Cras cursus sollicitudin dui, nec aliquam odio feugiat ac. Sed tincidunt tempus placerat. Nullam tincidunt, est nec faucibus facilisis, nisl lectus euismod libero, in eleifend quam ipsum a erat. Fusce erat arcu, tincidunt vel sodales in, dapibus euismod metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fermentum suscipit sapien, at scelerisque purus consequat at. Nullam pellentesque posuere diam eget aliquam. Morbi tristique fermentum iaculis. Sed semper vehicula lectus eu aliquam. Praesent porta urna sed sem aliquet at porttitor enim feugiat. Proin dignissim luctus nulla, vel interdum tellus dapibus at. Fusce a nibh egestas neque laoreet hendrerit.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"18\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351542041;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(47,21,1,2,1351542065,'a:32:{s:8:\"entry_id\";i:21;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:13:\"Womens Clinic\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:13:\"womens-clinic\";s:11:\"field_id_72\";s:2840:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar rutrum ornare. Cras eu neque eget sapien dictum placerat a vel justo. Phasellus malesuada auctor volutpat. Vestibulum mattis neque at nunc luctus a dictum mi malesuada. Ut elementum mattis rutrum. Ut suscipit tellus quis lectus mollis congue. Duis viverra nulla a tortor rhoncus ut lobortis nulla egestas. Aenean rutrum, risus vel hendrerit suscipit, velit nisl dapibus arcu, in sodales purus massa non enim. Aliquam vulputate mattis diam, vitae scelerisque urna ullamcorper nec. Vivamus consequat magna eget tellus feugiat porta.</p>\n<p>\n	Curabitur turpis tellus, lacinia in fermentum pulvinar, imperdiet at orci. Fusce fermentum, quam hendrerit dapibus dictum, tortor sapien molestie nulla, lacinia luctus nulla sem at risus. Integer vitae bibendum nunc. Ut pharetra neque sit amet purus imperdiet interdum. Curabitur vel tellus urna. In sagittis, diam a pretium mattis, lectus nulla ultrices libero, sed accumsan sapien mi eget sem. Phasellus rhoncus leo sit amet nisl bibendum tincidunt. Duis auctor odio at justo euismod vestibulum.</p>\n<p>\n	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce nunc nisi, pretium nec elementum ut, elementum consequat neque. Sed facilisis porta erat, eu luctus ligula vestibulum in. In non massa ut nisl tempor hendrerit quis ut tortor. Nullam adipiscing nulla vel tellus vestibulum in adipiscing dui laoreet. Nam vitae mauris risus. Curabitur id tellus dui, eu convallis ante. Nunc pellentesque orci quis dui rhoncus mattis.</p>\n<p>\n	Maecenas non arcu lacus. Sed quis eros enim, sit amet bibendum eros. Morbi quis nisl elit. Vivamus rutrum malesuada vulputate. Duis ut leo lectus, ut fermentum purus. Aliquam erat volutpat. Aliquam diam sem, malesuada at semper quis, accumsan ut erat. Quisque sed sem nunc. Cras nec leo massa. Maecenas vulputate orci tempor quam viverra feugiat. Cras ac elit nec eros volutpat lacinia et a odio. Cras molestie egestas libero vitae bibendum.</p>\n<p>\n	Mauris orci eros, ultricies rhoncus lacinia eget, tincidunt id dui. Praesent nibh erat, dictum posuere pellentesque ac, fermentum vel felis. Donec orci ante, ultricies a viverra id, aliquet et odio. Praesent neque leo, varius et consequat non, dignissim sit amet odio. Nullam ut consequat lacus. Pellentesque mattis, dui non mollis dapibus, nisi tortor egestas mi, vel auctor est dolor non magna. Nullam aliquam rutrum est eget sollicitudin. Curabitur vulputate malesuada tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc imperdiet consequat risus, et laoreet mauris condimentum ut. Quisque sed est urna, ac posuere dui. Praesent vestibulum lorem in dui tempor facilisis. Morbi sit amet sem at elit convallis lobortis sit amet quis eros.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"19\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351542004;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(48,22,1,2,1351542087,'a:32:{s:8:\"entry_id\";i:22;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:10:\"Learn More\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:10:\"learn-more\";s:11:\"field_id_72\";s:2840:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar rutrum ornare. Cras eu neque eget sapien dictum placerat a vel justo. Phasellus malesuada auctor volutpat. Vestibulum mattis neque at nunc luctus a dictum mi malesuada. Ut elementum mattis rutrum. Ut suscipit tellus quis lectus mollis congue. Duis viverra nulla a tortor rhoncus ut lobortis nulla egestas. Aenean rutrum, risus vel hendrerit suscipit, velit nisl dapibus arcu, in sodales purus massa non enim. Aliquam vulputate mattis diam, vitae scelerisque urna ullamcorper nec. Vivamus consequat magna eget tellus feugiat porta.</p>\n<p>\n	Curabitur turpis tellus, lacinia in fermentum pulvinar, imperdiet at orci. Fusce fermentum, quam hendrerit dapibus dictum, tortor sapien molestie nulla, lacinia luctus nulla sem at risus. Integer vitae bibendum nunc. Ut pharetra neque sit amet purus imperdiet interdum. Curabitur vel tellus urna. In sagittis, diam a pretium mattis, lectus nulla ultrices libero, sed accumsan sapien mi eget sem. Phasellus rhoncus leo sit amet nisl bibendum tincidunt. Duis auctor odio at justo euismod vestibulum.</p>\n<p>\n	Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce nunc nisi, pretium nec elementum ut, elementum consequat neque. Sed facilisis porta erat, eu luctus ligula vestibulum in. In non massa ut nisl tempor hendrerit quis ut tortor. Nullam adipiscing nulla vel tellus vestibulum in adipiscing dui laoreet. Nam vitae mauris risus. Curabitur id tellus dui, eu convallis ante. Nunc pellentesque orci quis dui rhoncus mattis.</p>\n<p>\n	Maecenas non arcu lacus. Sed quis eros enim, sit amet bibendum eros. Morbi quis nisl elit. Vivamus rutrum malesuada vulputate. Duis ut leo lectus, ut fermentum purus. Aliquam erat volutpat. Aliquam diam sem, malesuada at semper quis, accumsan ut erat. Quisque sed sem nunc. Cras nec leo massa. Maecenas vulputate orci tempor quam viverra feugiat. Cras ac elit nec eros volutpat lacinia et a odio. Cras molestie egestas libero vitae bibendum.</p>\n<p>\n	Mauris orci eros, ultricies rhoncus lacinia eget, tincidunt id dui. Praesent nibh erat, dictum posuere pellentesque ac, fermentum vel felis. Donec orci ante, ultricies a viverra id, aliquet et odio. Praesent neque leo, varius et consequat non, dignissim sit amet odio. Nullam ut consequat lacus. Pellentesque mattis, dui non mollis dapibus, nisi tortor egestas mi, vel auctor est dolor non magna. Nullam aliquam rutrum est eget sollicitudin. Curabitur vulputate malesuada tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc imperdiet consequat risus, et laoreet mauris condimentum ut. Quisque sed est urna, ac posuere dui. Praesent vestibulum lorem in dui tempor facilisis. Morbi sit amet sem at elit convallis lobortis sit amet quis eros.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351542086;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(49,1,1,2,1351542110,'a:33:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:11:\"field_id_86\";s:83:\"[10] [minnesota-outreach-women-how-it-works] Minnesota Outreach Women: How it Works\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1324579669;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(50,1,1,2,1351542129,'a:33:{s:8:\"entry_id\";s:1:\"1\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"NULL\";s:22:\"structure__template_id\";s:1:\"1\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"Home\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:8:\"homepage\";s:9:\"url_title\";s:8:\"homepage\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_84\";s:0:\"\";s:11:\"field_id_85\";s:0:\"\";s:11:\"field_id_86\";s:83:\"[10] [minnesota-outreach-women-how-it-works] Minnesota Outreach Women: How it Works\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1324579628;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(51,23,1,2,1351542240,'a:32:{s:8:\"entry_id\";i:23;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"News\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:4:\"news\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351542179;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(52,23,1,2,1351542246,'a:32:{s:8:\"entry_id\";s:2:\"23\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"NULL\";s:22:\"structure__template_id\";s:1:\"1\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:4:\"News\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:4:\"news\";s:9:\"url_title\";s:5:\"news1\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"2\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351542125;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(53,3,1,2,1351542747,'a:33:{s:8:\"entry_id\";s:1:\"3\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"2005\";s:22:\"structure__template_id\";s:2:\"53\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:6:\"closed\";s:5:\"title\";s:5:\"Store\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:5:\"store\";s:9:\"url_title\";s:5:\"store\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1332439286;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(54,9,8,2,1351544119,'a:21:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_73\";s:3318:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n<p>\n	&nbsp;</p>\n\";s:10:\"entry_date\";i:1351469718;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061718;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(55,8,8,2,1351544138,'a:22:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_73\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:10:\"entry_date\";i:1351469557;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061557;s:8:\"category\";a:2:{i:0;s:1:\"4\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(56,10,8,2,1351544164,'a:22:{s:8:\"entry_id\";s:2:\"10\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:5:\"title\";s:38:\"Minnesota Outreach Women: How it Works\";s:9:\"url_title\";s:37:\"minnesota-outreach-women-how-it-works\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_73\";s:425:\"<p>\n	Ras in blandit libero. In hendrerit interdum sem, cursus auctor tellus pulvinar ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent mattis cursus Pellent mattis pulvinar. Nulla a est in <a href=\"#\">turpis fermentum luctus</a>. Nulla ut nunc consectetur neque suscipit pharetra id quis urna. Morbi justo nisi, egestas in blandit id, tristique sed risus. Nulla a est in turpis fermentum luctus.&nbsp;</p>\n\";s:10:\"entry_date\";i:1351321263;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1353913263;s:8:\"category\";a:3:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:37:\"minnesota-outreach-women-how-it-works\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(57,24,8,2,1351544211,'a:21:{s:8:\"entry_id\";i:24;s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:5:\"title\";s:27:\"Consectetur adipiscing elit\";s:9:\"url_title\";s:27:\"consectetur-adipiscing-elit\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_73\";s:3384:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:10:\"entry_date\";i:1349211410;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354136210;s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(58,24,8,2,1351544220,'a:21:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:27:\"Consectetur adipiscing elit\";s:9:\"url_title\";s:27:\"consectetur-adipiscing-elit\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_73\";s:3384:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:10:\"entry_date\";i:1349211359;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354136159;s:8:\"category\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:27:\"consectetur-adipiscing-elit\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(59,25,8,2,1351544236,'a:21:{s:8:\"entry_id\";i:25;s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"NULL\";s:5:\"title\";s:21:\"Donec vel sapien quam\";s:9:\"url_title\";s:21:\"donec-vel-sapien-quam\";s:11:\"field_id_73\";s:1461:\"<p>\n	&nbsp;</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:10:\"entry_date\";i:1351544235;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354136235;s:8:\"category\";a:4:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"1\";i:3;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(60,25,8,2,1351544322,'a:21:{s:8:\"entry_id\";s:2:\"25\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:21:\"Donec vel sapien quam\";s:9:\"url_title\";s:21:\"donec-vel-sapien-quam\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_73\";s:1461:\"<p>\n	&nbsp;</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:10:\"entry_date\";i:1345669121;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354136261;s:8:\"category\";a:4:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"1\";i:3;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:21:\"donec-vel-sapien-quam\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(61,9,8,2,1351546358,'a:22:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3302:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n\";s:11:\"field_id_73\";s:87:\"<p>\n	Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper</p>\n\";s:10:\"entry_date\";i:1351469737;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061737;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(62,9,8,2,1351546376,'a:22:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3302:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n\";s:11:\"field_id_73\";s:111:\"<p>\n	Pellent cursus fringilla lectus id magna facilisis vel tristique facilisis vel tristique dolor semper</p>\n\";s:10:\"entry_date\";i:1351469755;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061755;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(63,8,8,2,1351546404,'a:23:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:11:\"field_id_73\";s:91:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet.</p>\n\";s:10:\"entry_date\";i:1351469543;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061543;s:8:\"category\";a:2:{i:0;s:1:\"4\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"51\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(64,8,8,2,1351546565,'a:22:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:11:\"field_id_73\";s:91:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet.</p>\n\";s:10:\"entry_date\";i:1351469524;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061524;s:8:\"category\";a:2:{i:0;s:1:\"4\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(65,10,8,2,1351547768,'a:23:{s:8:\"entry_id\";s:2:\"10\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:12:\"member_group\";a:1:{i:0;s:1:\"1\";}s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:38:\"Minnesota Outreach Women: How it Works\";s:9:\"url_title\";s:37:\"minnesota-outreach-women-how-it-works\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3384:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:11:\"field_id_73\";s:425:\"<p>\n	Ras in blandit libero. In hendrerit interdum sem, cursus auctor tellus pulvinar ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent mattis cursus Pellent mattis pulvinar. Nulla a est in <a href=\"#\">turpis fermentum luctus</a>. Nulla ut nunc consectetur neque suscipit pharetra id quis urna. Morbi justo nisi, egestas in blandit id, tristique sed risus. Nulla a est in turpis fermentum luctus.&nbsp;</p>\n\";s:10:\"entry_date\";i:1351321267;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1353913267;s:8:\"category\";a:3:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:37:\"minnesota-outreach-women-how-it-works\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(66,24,8,2,1351547786,'a:22:{s:8:\"entry_id\";s:2:\"24\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:27:\"Consectetur adipiscing elit\";s:9:\"url_title\";s:27:\"consectetur-adipiscing-elit\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3384:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:11:\"field_id_73\";s:113:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat.</p>\n\";s:10:\"entry_date\";i:1349211325;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354136125;s:8:\"category\";a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:27:\"consectetur-adipiscing-elit\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(67,25,8,2,1351547798,'a:22:{s:8:\"entry_id\";s:2:\"25\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:21:\"Donec vel sapien quam\";s:9:\"url_title\";s:21:\"donec-vel-sapien-quam\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3384:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel lectus in ipsum malesuada consequat vitae a mauris. Nunc accumsan venenatis augue, eleifend tincidunt augue adipiscing sed. Ut venenatis metus non purus ultricies eleifend. Nullam lacinia felis sed est tristique ullamcorper. Etiam aliquam mi id eros cursus aliquet. Maecenas auctor leo a metus mattis tempor. Ut ut nisi orci, vel imperdiet nunc. Praesent tristique risus sit amet risus mollis auctor. Nunc nibh leo, pharetra varius ultricies in, sagittis eget libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi orci mauris, tempor eget blandit eu, pretium ac augue. Nulla eu mauris et tortor vehicula consequat vel vel lacus. Nam id nisl urna. Pellentesque porttitor ante id augue aliquet egestas. Sed quis orci et erat mollis dignissim a vitae libero.</p>\n<p>\n	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec at purus a risus varius congue. Cras sit amet tortor nec dolor consequat tempor eget vitae mi. Curabitur tincidunt diam ut purus ullamcorper in aliquet nibh ultricies. Donec non scelerisque mauris. In vel est tincidunt tortor dapibus commodo pretium ac eros. Suspendisse potenti. In vitae enim a eros vehicula pretium eu nec nunc. Aenean tincidunt vehicula ligula, blandit tristique tortor molestie convallis.</p>\n<p>\n	Nunc luctus gravida velit ut scelerisque. Sed quis mi quis dolor eleifend lobortis vel quis tellus. Nulla facilisi. Nulla non ligula ac risus adipiscing laoreet at eu nisi. Vestibulum rutrum ipsum ac velit pharetra sagittis eget a sapien. Aliquam sapien nunc, pharetra sit amet ultricies id, varius ac ante. Proin quis sem et lacus lacinia consectetur. Nulla quis ipsum non purus porta tempor. Duis ac sapien lacus. Cras commodo luctus dignissim. Fusce ornare lobortis turpis id porta. Nam sollicitudin commodo lacus vitae pulvinar.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras aliquam enim non sem hendrerit laoreet. Sed tristique, orci vitae ullamcorper congue, turpis nunc cursus felis, vitae sodales sapien neque ac enim. Sed fermentum augue nec nisl tincidunt consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dui nibh, tincidunt id condimentum sit amet, faucibus non diam. Maecenas mollis, massa ut posuere rutrum, sapien velit mollis diam, sit amet faucibus metus nisl eget enim. Nunc faucibus vehicula massa, id eleifend risus consequat at. In suscipit varius nisl eget faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id nunc ac purus tempor malesuada ut eu est. Sed sollicitudin enim sed libero gravida interdum. Duis convallis auctor commodo. Praesent non tortor at lorem sodales blandit. Ut in urna non turpis accumsan congue ut et velit.</p>\n<p>\n	Donec vel sapien quam, in mollis enim. Aliquam erat volutpat. Ut nec purus sem, sit amet mollis lacus. Duis sem nunc, vehicula a commodo quis, adipiscing nec metus. Etiam cursus congue enim quis congue. Praesent placerat elementum luctus. Pellentesque interdum rutrum metus non semper. Integer egestas tristique leo, nec ultrices neque lobortis mollis. Nam placerat, ligula sagittis vestibulum porta, nibh nibh facilisis nisi, non tincidunt sapien nulla adipiscing risus.</p>\n\";s:11:\"field_id_73\";s:0:\"\";s:10:\"entry_date\";i:1345669117;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354136257;s:8:\"category\";a:4:{i:0;s:1:\"3\";i:1;s:1:\"4\";i:2;s:1:\"1\";i:3;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:21:\"donec-vel-sapien-quam\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(68,5,1,2,1351550562,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1036:\"<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1337206721;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(69,5,1,2,1351550616,'a:32:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1036:\"<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:10:\"entry_date\";i:1337206715;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(70,26,1,2,1351575593,'a:32:{s:8:\"entry_id\";i:26;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"2\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Related Content\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:15:\"related-content\";s:11:\"field_id_72\";s:397:\"<p>\n	<em><span style=\"font-size:16px;\">This page is not publically visible. </span></em></p>\n<p>\n	<u><span style=\"font-size:16px;\">To add \"Related Content\":</span></u></p>\n<p>\n	<span style=\"font-size:16px;\">1) Click the \"Pages\" tab at the top of this page</span></p>\n<p>\n	<span style=\"font-size:16px;\">2) Click the \"+ Add\" next to this page in the navigation list as illustrated below.</span></p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351575472;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(71,26,1,2,1351575607,'a:32:{s:8:\"entry_id\";s:2:\"26\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Related Content\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:15:\"related-content\";s:9:\"url_title\";s:15:\"related-content\";s:11:\"field_id_72\";s:397:\"<p>\n	<em><span style=\"font-size:16px;\">This page is not publically visible. </span></em></p>\n<p>\n	<u><span style=\"font-size:16px;\">To add \"Related Content\":</span></u></p>\n<p>\n	<span style=\"font-size:16px;\">1) Click the \"Pages\" tab at the top of this page</span></p>\n<p>\n	<span style=\"font-size:16px;\">2) Click the \"+ Add\" next to this page in the navigation list as illustrated below.</span></p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351575426;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"9\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(72,26,1,2,1351575792,'a:32:{s:8:\"entry_id\";s:2:\"26\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:15:\"Related Content\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:15:\"related-content\";s:9:\"url_title\";s:15:\"related-content\";s:11:\"field_id_72\";s:622:\"<p>\n	<img alt=\"\" src=\"{filedir_2}how-to-add-related-content.png\" style=\"font-size: 16px; width: 408px; height: 198px; float: left; margin-left: 10px; margin-right: 10px; \" /></p>\n<p>\n	<span style=\"font-size:16px;\"><em>This page is not publically visible.</em></span></p>\n<p>\n	<strong><span style=\"font-size:16px;\">To add \"Related Content\":</span></strong></p>\n<p>\n	<span style=\"font-size:16px;\">1) Click the \"<strong>Pages</strong>\" tab in the top navigation.</span></p>\n<p>\n	<span style=\"font-size:16px;\">2) Click the \"<strong>Add</strong>\" next to this page in the navigation list as illustrated to the left.</span></p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351575431;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"9\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(73,27,9,2,1351576619,'a:30:{s:8:\"entry_id\";i:27;s:10:\"channel_id\";s:1:\"9\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:4:{i:0;s:1:\"1\";i:1;s:1:\"6\";i:2;s:1:\"7\";i:3;s:1:\"8\";}s:14:\"layout_preview\";s:4:\"2006\";s:5:\"title\";s:18:\"Imagine No Malaria\";s:9:\"url_title\";s:18:\"imagine-no-malaria\";s:11:\"field_id_72\";s:1043:\"<h2>\n	People against Malaria (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351576498;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"9\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"60\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(74,27,9,2,1351577158,'a:29:{s:8:\"entry_id\";s:2:\"27\";s:10:\"channel_id\";s:1:\"9\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2006\";s:5:\"title\";s:18:\"Imagine No Malaria\";s:9:\"url_title\";s:18:\"imagine-no-malaria\";s:11:\"field_id_72\";s:1043:\"<h2>\n	People against Malaria (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:10:\"entry_date\";i:1351576497;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"9\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:18:\"imagine-no-malaria\";s:22:\"structure__template_id\";s:2:\"60\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(75,27,9,2,1351577949,'a:31:{s:8:\"entry_id\";s:2:\"27\";s:10:\"channel_id\";s:1:\"9\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2006\";s:5:\"title\";s:18:\"Imagine No Malaria\";s:9:\"url_title\";s:18:\"imagine-no-malaria\";s:11:\"field_id_72\";s:1043:\"<h2>\n	People against Malaria (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351576448;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"9\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:18:\"imagine-no-malaria\";s:22:\"structure__template_id\";s:2:\"60\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(76,27,9,2,1351578268,'a:30:{s:8:\"entry_id\";s:2:\"27\";s:10:\"channel_id\";s:1:\"9\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjkiO30=\";s:14:\"layout_preview\";s:4:\"2006\";s:5:\"title\";s:18:\"Imagine No Malaria\";s:9:\"url_title\";s:18:\"imagine-no-malaria\";s:11:\"field_id_72\";s:1043:\"<h2>\n	People against Malaria (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351576467;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"9\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:18:\"imagine-no-malaria\";s:22:\"structure__template_id\";s:2:\"60\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(77,2,1,2,1351578279,'a:34:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2005\";s:5:\"title\";s:13:\"News & Events\";s:9:\"url_title\";s:4:\"news\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1332438938;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:20:\"structure__parent_id\";s:1:\"0\";s:14:\"structure__uri\";s:4:\"news\";s:22:\"structure__template_id\";s:2:\"53\";s:17:\"structure__hidden\";s:1:\"n\";s:26:\"structure__listing_channel\";s:1:\"8\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(78,28,9,2,1351578423,'a:30:{s:8:\"entry_id\";i:28;s:10:\"channel_id\";s:1:\"9\";s:17:\"autosave_entry_id\";s:1:\"3\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2006\";s:5:\"title\";s:15:\"Donate to Japan\";s:9:\"url_title\";s:15:\"donate-to-japan\";s:11:\"field_id_72\";s:62:\"<p>\n	United Methodist fundraiser to be held June 9, 2011.</p>\n\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351578302;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"9\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"60\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(79,2,1,2,1351578436,'a:34:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2005\";s:5:\"title\";s:13:\"News & Events\";s:9:\"url_title\";s:4:\"news\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1332438915;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:20:\"structure__parent_id\";s:1:\"0\";s:14:\"structure__uri\";s:4:\"news\";s:22:\"structure__template_id\";s:2:\"53\";s:17:\"structure__hidden\";s:1:\"n\";s:26:\"structure__listing_channel\";s:1:\"8\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(80,5,1,2,1351580227,'a:34:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1036:\"<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1337206686;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(81,29,9,2,1351581331,'a:30:{s:8:\"entry_id\";i:29;s:10:\"channel_id\";s:1:\"9\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2006\";s:5:\"title\";s:13:\"Prefer Print?\";s:9:\"url_title\";s:12:\"prefer-print\";s:11:\"field_id_72\";s:59:\"<p>\n	We offer a paper newsletter for those interested.</p>\n\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351581270;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"new_channel\";s:1:\"9\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:22:\"structure__template_id\";s:2:\"60\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(82,9,8,2,1351581664,'a:23:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3302:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n\";s:11:\"field_id_73\";s:111:\"<p>\n	Pellent cursus fringilla lectus id magna facilisis vel tristique facilisis vel tristique dolor semper</p>\n\";s:11:\"field_id_91\";s:0:\"\";s:10:\"entry_date\";i:1351469703;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061703;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(83,9,8,2,1351581690,'a:23:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3302:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n\";s:11:\"field_id_73\";s:111:\"<p>\n	Pellent cursus fringilla lectus id magna facilisis vel tristique facilisis vel tristique dolor semper</p>\n\";s:11:\"field_id_91\";s:0:\"\";s:10:\"entry_date\";i:1351469729;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061729;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:13:\"save_revision\";s:13:\"Save Revision\";}'),
	(84,9,8,2,1351582438,'a:23:{s:8:\"entry_id\";s:1:\"9\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:54:\"Northern Minnesota Campuses are Fertile Mission Fields\";s:9:\"url_title\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:3302:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus fringilla lectus id magna facilisis vel tristique dolor semper. Nam vitae urna lorem. Curabitur et porttitor magna. Proin enim urna, fringilla at feugiat ac, dictum vel augue. Donec tincidunt varius ultricies. Fusce cursus, purus sed ornare venenatis, neque risus imperdiet libero, sed facilisis nisi nibh sit amet velit. Integer suscipit tincidunt leo, in porta erat tempus quis. In sagittis posuere ipsum, luctus vestibulum orci ullamcorper molestie. Sed ornare ullamcorper rhoncus. Ut eleifend lectus in dui gravida vel accumsan metus malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at elit ligula. Nulla viverra ante sit amet leo fermentum dignissim pharetra lorem feugiat. In neque augue, volutpat at adipiscing vitae, hendrerit in purus. Ut lectus turpis, sodales id ullamcorper eu, rutrum non dolor.</p>\n<p>\n	Donec adipiscing augue vel lectus laoreet ornare. Integer blandit mattis nisi, vel auctor velit rhoncus eget. Curabitur dapibus suscipit sapien, non faucibus lorem mattis quis. Nullam id sollicitudin elit. Curabitur sapien ligula, varius sit amet tincidunt id, dapibus ac lectus. In a ipsum mauris, malesuada placerat libero. Donec dui nisl, tristique vitae vehicula a, ultrices ut felis. In hac habitasse platea dictumst. Etiam et odio id lacus auctor ultricies. Duis dolor leo, adipiscing at cursus in, sagittis id magna. Aenean a urna sit amet quam posuere auctor. Suspendisse potenti. Duis blandit viverra risus non euismod. Proin mi quam, euismod vestibulum venenatis id, auctor sed eros. Sed ultrices hendrerit diam in adipiscing.</p>\n<p>\n	Morbi tempus ornare eros, non eleifend dolor porta vitae. Cras ut velit vel ante ultricies euismod. Maecenas consectetur tellus non lacus vehicula id aliquam enim viverra. In sit amet arcu lorem. Aliquam id odio diam. Nullam blandit cursus blandit. Integer enim nunc, bibendum a hendrerit id, lobortis eleifend dolor.</p>\n<p>\n	Integer est purus, placerat ut interdum ac, porttitor et odio. Duis commodo pretium nunc eu iaculis. Sed sit amet sapien elit, vitae vestibulum lectus. Suspendisse urna ligula, egestas ut luctus sed, bibendum sit amet nulla. Praesent eget enim sem, ac euismod elit. Sed semper, justo sed iaculis malesuada, nisl nunc tincidunt sem, eu ornare nisi mi in orci. Vestibulum ac tortor ac ligula convallis malesuada in eu lorem. Proin eget ligula a massa eleifend consequat et sed ligula. Curabitur eu est non massa venenatis semper.</p>\n<p>\n	Phasellus nunc turpis, commodo nec semper ac, luctus sit amet mauris. Integer eu purus sed ipsum pretium porta nec porttitor dui. Etiam eu luctus velit. Phasellus diam est, pharetra in pharetra vehicula, laoreet vitae nulla. Etiam dapibus sodales metus eu dictum. Vivamus volutpat porttitor urna at molestie. Sed eleifend dictum ligula sed lacinia. Morbi vulputate varius ligula, vitae viverra nulla blandit sed. Morbi vehicula euismod eros sed eleifend. Nulla a orci aliquet turpis eleifend sagittis. Donec orci elit, iaculis vitae sollicitudin eu, sagittis eget nibh. Nulla semper purus et nulla vestibulum vel gravida dolor facilisis. Sed sit amet nibh lacus, vel consequat erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\n\";s:11:\"field_id_73\";s:111:\"<p>\n	Pellent cursus fringilla lectus id magna facilisis vel tristique facilisis vel tristique dolor semper</p>\n\";s:11:\"field_id_91\";s:0:\"\";s:10:\"entry_date\";i:1351469757;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061757;s:8:\"category\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"1\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:54:\"northern-minnesota-campuses-are-fertile-mission-fields\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(85,8,8,2,1351583970,'a:23:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:11:\"field_id_73\";s:91:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet.</p>\n\";s:11:\"field_id_91\";s:0:\"\";s:10:\"entry_date\";i:1351469549;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061549;s:8:\"category\";a:2:{i:0;s:1:\"4\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(86,8,8,2,1351583986,'a:23:{s:8:\"entry_id\";s:1:\"8\";s:10:\"channel_id\";s:1:\"8\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:44:\"YToxOntzOjEwOiJjaGFubmVsX2lkIjtzOjE6IjgiO30=\";s:14:\"layout_preview\";s:4:\"2002\";s:5:\"title\";s:25:\"Pacific Earthquake Relief\";s:9:\"url_title\";s:25:\"pacific-earthquake-relief\";s:11:\"field_id_74\";s:0:\"\";s:11:\"field_id_88\";s:2866:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet dui quis purus venenatis dictum quis et magna. Quisque a neque nibh, sit amet ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse id porttitor tortor. Integer imperdiet euismod dictum. Cras vehicula ultrices diam, eget imperdiet ante cursus sed. Quisque accumsan, leo quis euismod mollis, elit dolor convallis risus, sed malesuada nunc enim ut odio. Aliquam erat volutpat. Maecenas pharetra, diam laoreet pharetra volutpat, massa augue posuere tellus, vel rhoncus lorem elit eu neque. In leo nibh, pulvinar consectetur viverra eu, interdum vitae justo.</p>\n<p>\n	Sed interdum tempus augue posuere tristique. Fusce id urna quis tellus interdum adipiscing. Pellentesque quam enim, blandit ut ultricies a, interdum porta augue. Nulla laoreet nulla id ante congue at iaculis purus sodales. Morbi quis odio risus. Proin quis leo non risus laoreet ullamcorper eget non odio. Nam consequat, purus in imperdiet consequat, nibh arcu porta justo, vel lobortis massa dui ac arcu. Donec iaculis malesuada condimentum.</p>\n<p>\n	Suspendisse dictum libero ac orci ultrices adipiscing. Cras bibendum tristique elit quis tempus. Mauris mollis gravida nisl, et ornare quam pellentesque id. Phasellus a neque ac nulla cursus pretium vel ut sem. Duis a mi in ligula blandit fringilla in ac dolor. Proin sit amet arcu mauris. Fusce ultricies fermentum facilisis.</p>\n<p>\n	Donec vitae libero in est iaculis mattis at quis felis. Suspendisse rutrum ullamcorper ipsum, non pharetra urna feugiat sit amet. Nunc leo urna, rhoncus eleifend laoreet id, bibendum in lorem. Etiam dui mi, posuere nec aliquet viverra, sagittis id lacus. Mauris felis enim, elementum nec sollicitudin et, fringilla ut lacus. Nullam elit mauris, convallis viverra eleifend quis, rutrum eu justo. Nunc quis nunc ligula, et placerat dolor. Nam condimentum viverra libero in aliquam. Ut elementum suscipit ullamcorper. Nam vitae lorem id tortor semper bibendum.</p>\n<p>\n	Donec rutrum eleifend nunc id fermentum. Vivamus eget metus neque. Sed vitae leo felis. Praesent ut sapien urna. Nam malesuada ante sed risus aliquam pulvinar faucibus nunc placerat. Aliquam volutpat massa non nunc fringilla et imperdiet felis sollicitudin. Nulla ut turpis id mi molestie rutrum a ac dolor. Vestibulum ligula ante, sagittis quis pulvinar eu, porttitor in massa. Donec mauris mi, gravida non mattis in, bibendum vitae augue. Cras sodales volutpat justo id mattis. Morbi imperdiet euismod facilisis. Donec cursus aliquet est, vitae semper velit dictum sit amet. Sed eu eros ut ligula auctor ornare. Nunc odio orci, convallis et faucibus quis, viverra sit amet nibh. Phasellus semper accumsan nunc, eget eleifend ante adipiscing vel. Cras faucibus erat et sem ultrices laoreet.</p>\n\";s:11:\"field_id_73\";s:91:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellent cursus sit amet.</p>\n\";s:11:\"field_id_91\";s:0:\"\";s:10:\"entry_date\";i:1351469565;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";i:1354061565;s:8:\"category\";a:2:{i:0;s:1:\"4\";i:1;s:1:\"2\";}s:11:\"new_channel\";s:1:\"8\";s:6:\"status\";s:4:\"open\";s:6:\"author\";s:1:\"2\";s:14:\"allow_comments\";s:1:\"y\";s:18:\"versioning_enabled\";s:1:\"y\";s:14:\"structure__uri\";s:25:\"pacific-earthquake-relief\";s:22:\"structure__template_id\";s:2:\"51\";s:6:\"submit\";s:6:\"Submit\";}'),
	(87,4,1,2,1351587535,'a:33:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1337205534;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(88,4,1,2,1351588345,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205504;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(89,4,1,2,1351588367,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205526;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(90,4,1,2,1351588428,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205527;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(91,4,1,2,1351588448,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205487;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(92,4,1,2,1351589211,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205530;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(93,4,1,2,1351589388,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205527;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(94,4,1,2,1351590869,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205508;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(95,4,1,2,1351591517,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205496;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(96,4,1,2,1351591627,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205486;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(97,4,1,2,1351592057,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205496;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(98,4,1,2,1351592078,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205517;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:13:\"save_revision\";s:13:\"Save Revision\";s:11:\"field_id_87\";N;}'),
	(99,4,1,2,1351593027,'a:34:{s:8:\"entry_id\";s:1:\"4\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:19:\"Request Information\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:19:\"request-information\";s:9:\"url_title\";s:19:\"request-information\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:2:\"17\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:11:\"field_id_92\";s:0:\"\";s:10:\"entry_date\";i:1337205506;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(100,30,1,2,1351593750,'a:33:{s:8:\"entry_id\";i:30;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:3:\"404\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:9:\"fourofour\";s:9:\"url_title\";s:9:\"fourofour\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351593749;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(101,30,1,2,1351593867,'a:33:{s:8:\"entry_id\";s:2:\"30\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:3:\"404\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:9:\"fourofour\";s:9:\"url_title\";s:9:\"fourofour\";s:11:\"field_id_72\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351593746;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:23:\"Page Not Found (Oh no!)\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";N;}'),
	(102,31,1,2,1351594165,'a:33:{s:8:\"entry_id\";i:31;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:7:\"Success\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:0:\"\";s:9:\"url_title\";s:7:\"success\";s:11:\"field_id_72\";s:97:\"<p>\n	Success! Your request for more information was successful. We will contact you shortly.</p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"4\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351594104;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(103,31,1,2,1351594193,'a:33:{s:8:\"entry_id\";s:2:\"31\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:7:\"Success\";s:17:\"structure__hidden\";s:1:\"y\";s:14:\"structure__uri\";s:7:\"success\";s:9:\"url_title\";s:7:\"success\";s:11:\"field_id_72\";s:183:\"<p>\n	Success! Your request for more information was successful. We will contact you shortly.</p>\n<p>\n	&nbsp;</p>\n<p>\n	<a href=\"{page_4}\">Click here to submit another request.</a></p>\n\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"4\";s:11:\"field_id_86\";s:0:\"\";s:21:\"field_id_87_directory\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1351594132;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";N;}'),
	(104,5,1,2,1351597317,'a:35:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:12:\"member_group\";a:5:{i:0;s:4:\"2000\";i:1;s:1:\"1\";i:2;s:1:\"6\";i:3;s:1:\"7\";i:4;s:1:\"8\";}s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:0:\"\";s:21:\"field_id_87_directory\";s:1:\"2\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1337206736;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(105,5,1,2,1351597847,'a:34:{s:8:\"entry_id\";i:5;s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:13:\"save_revision\";s:13:\"Save Revision\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1036:\"<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:21:\"field_id_87_directory\";s:1:\"2\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1337206726;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}'),
	(106,5,1,2,1351597999,'a:34:{s:8:\"entry_id\";s:1:\"5\";s:10:\"channel_id\";s:1:\"1\";s:17:\"autosave_entry_id\";s:1:\"0\";s:6:\"filter\";s:0:\"\";s:14:\"layout_preview\";s:4:\"2000\";s:22:\"structure__template_id\";s:2:\"10\";s:6:\"status\";s:4:\"open\";s:5:\"title\";s:8:\"About Us\";s:17:\"structure__hidden\";s:1:\"n\";s:14:\"structure__uri\";s:5:\"about\";s:9:\"url_title\";s:5:\"about\";s:11:\"field_id_72\";s:1036:\"<h2>\n	Secondary Title (H2)</h2>\n<p>\n	The Minnesota Annual Conference of The United Methodist Church is the regional organization of United Methodist congregations in Minnesota. The purpose of United Methodist annual conferences is to make disciples for Jesus Christ by equipping their local congregations for ministry and by providing a connection for ministry beyond the local church, all to the glory of God. The United Methodist Church is the third-largest Christian body in the United States.</p>\n<h3>\n	Headline Three (H3)</h3>\n<ul>\n	<li>\n		Suspendisse ut ante leo, id blandit tortor.</li>\n	<li>\n		Aenean luctus lacinia quam, in porta odio auctor at.</li>\n	<li>\n		Sed dictum ligula interdum nisl euismod porta.</li>\n	<li>\n		Suspendisse vitae nunc eu orci malesuada vehicula.</li>\n</ul>\n<p>\n	The annual conference offices are located at 122 W. Franklin Ave., Room 400, Minneapolis, MN 55404; telephone: 612-870-0058. Need to direct something to a specific staff member? No problem! Check out our <a href=\"#\">Staff Directory</a>.</p>\n\";s:21:\"field_id_87_directory\";s:1:\"2\";s:11:\"field_id_89\";s:0:\"\";s:11:\"field_id_79\";s:0:\"\";s:11:\"field_id_80\";s:0:\"\";s:11:\"field_id_82\";s:0:\"\";s:20:\"structure__parent_id\";s:1:\"0\";s:11:\"field_id_86\";s:0:\"\";s:11:\"field_id_90\";s:0:\"\";s:10:\"entry_date\";i:1337206698;s:15:\"expiration_date\";s:0:\"\";s:23:\"comment_expiration_date\";s:0:\"\";s:11:\"field_id_81\";s:0:\"\";s:11:\"new_channel\";s:1:\"1\";s:26:\"structure__listing_channel\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:6:\"author\";s:1:\"2\";s:11:\"field_id_76\";s:0:\"\";s:11:\"field_id_77\";s:0:\"\";s:11:\"field_id_75\";s:0:\"\";s:18:\"versioning_enabled\";s:1:\"y\";s:6:\"submit\";s:6:\"Submit\";s:11:\"field_id_87\";s:39:\"{filedir_2}img-interior-banner-fpo.jpeg\";}');

/*!40000 ALTER TABLE `ee2_entry_versioning` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_extensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_extensions`;

CREATE TABLE `ee2_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_extensions` WRITE;
/*!40000 ALTER TABLE `ee2_extensions` DISABLE KEYS */;

INSERT INTO `ee2_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`)
VALUES
	(41,'Edit_alarm_ext','display_template_alert','edit_template_start','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(32,'Structure_ext','wygwam_config','wygwam_config','',10,'3.3.3','y'),
	(31,'Structure_ext','channel_module_create_pagination','channel_module_create_pagination','',9,'3.3.3','y'),
	(30,'Structure_ext','sessions_start','sessions_start','',10,'3.3.3','y'),
	(10,'Matrix_ext','channel_entries_tagdata','channel_entries_tagdata','',10,'2.4.3','y'),
	(11,'Pt_field_pack_ext','channel_entries_tagdata','channel_entries_tagdata','',10,'1.0.3','y'),
	(12,'Playa_ext','channel_entries_tagdata','channel_entries_tagdata','',9,'4.3.3','y'),
	(13,'Twomile_login_redirect_ext','process_login','member_member_login_single','a:7:{s:33:\"display_confirmation_after_logout\";s:2:\"no\";s:27:\"logout_lastpage_destination\";s:2:\"no\";s:18:\"logout_destination\";s:0:\"\";s:32:\"display_confirmation_after_login\";s:2:\"no\";s:20:\"lastpage_destination\";s:2:\"no\";s:17:\"login_destination\";s:8:\"/account\";s:14:\"login_page_url\";s:0:\"\";}',7,'2.1.1','n'),
	(14,'Twomile_login_redirect_ext','process_logout','member_member_logout','a:7:{s:33:\"display_confirmation_after_logout\";s:2:\"no\";s:27:\"logout_lastpage_destination\";s:2:\"no\";s:18:\"logout_destination\";s:0:\"\";s:32:\"display_confirmation_after_login\";s:2:\"no\";s:20:\"lastpage_destination\";s:2:\"no\";s:17:\"login_destination\";s:8:\"/account\";s:14:\"login_page_url\";s:0:\"\";}',7,'2.1.1','n'),
	(39,'Edit_alarm_ext','display_entry_alert','publish_form_entry_data','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(40,'Edit_alarm_ext','delete_entry_alert','entry_submission_absolute_end','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(29,'Structure_ext','cp_member_login','cp_member_login','',10,'3.3.3','y'),
	(28,'Structure_ext','entry_submission_redirect','entry_submission_redirect','',10,'3.3.3','y'),
	(33,'Structure_ext','entry_submission_end','entry_submission_end','',10,'3.3.3','y'),
	(34,'Structure_ext','safecracker_submit_entry_end','safecracker_submit_entry_end','',10,'3.3.3','y'),
	(38,'Republic_analytics_ext','cp_member_login','cp_member_login','',10,'2.2.2','y'),
	(35,'Blueprints_ext','publish_form_channel_preferences','publish_form_channel_preferences','a:1:{i:1;a:12:{s:30:\"enable_publish_layout_takeover\";s:1:\"y\";s:23:\"enable_edit_menu_tweaks\";s:1:\"y\";s:29:\"enable_template_multi_channel\";b:0;s:8:\"template\";a:8:{i:0;s:2:\"10\";i:1;s:2:\"45\";i:2;s:2:\"51\";i:3;s:2:\"20\";i:4;s:2:\"17\";i:5;s:2:\"53\";i:6;s:2:\"60\";i:7;s:1:\"1\";}s:10:\"thumbnails\";a:8:{i:0;s:9:\"pages.jpg\";i:1;s:0:\"\";i:2;s:16:\"newsarticles.jpg\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:8:\"news.jpg\";i:6;s:11:\"related.jpg\";i:7;s:12:\"homepage.jpg\";}s:16:\"layout_group_ids\";a:8:{i:0;s:4:\"2000\";i:1;s:4:\"2001\";i:2;s:4:\"2002\";i:3;s:4:\"2003\";i:4;s:4:\"2004\";i:5;s:4:\"2005\";i:6;s:4:\"2006\";i:7;s:4:\"2007\";}s:18:\"layout_group_names\";a:8:{i:0;s:5:\"Pages\";i:1;s:7:\"Contact\";i:2;s:12:\"News Article\";i:3;s:6:\"Orders\";i:4;s:8:\"Products\";i:5;s:4:\"News\";i:6;s:7:\"Related\";i:7;s:8:\"Homepage\";}s:14:\"thumbnail_path\";s:17:\"themes/templates/\";s:8:\"channels\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"8\";i:2;s:1:\"9\";}s:21:\"channel_show_selected\";a:3:{i:0;s:1:\"y\";i:1;s:1:\"y\";i:2;s:1:\"y\";}s:17:\"channel_templates\";a:3:{i:0;a:5:{i:0;s:2:\"53\";i:1;s:2:\"48\";i:2;s:2:\"45\";i:3;s:1:\"1\";i:4;s:2:\"10\";}i:1;a:1:{i:0;s:2:\"51\";}i:2;a:1:{i:0;s:2:\"60\";}}s:15:\"template_layout\";a:23:{i:1;a:2:{s:11:\"template_id\";s:1:\"1\";s:15:\"layout_group_id\";s:4:\"NULL\";}i:3;a:2:{s:11:\"template_id\";s:2:\"53\";s:15:\"layout_group_id\";s:4:\"2005\";}i:0;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:4;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:6;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:7;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:2;a:2:{s:11:\"template_id\";s:2:\"53\";s:15:\"layout_group_id\";s:4:\"2005\";}i:8;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:10;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:9;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:5;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:11;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:12;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:14;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:17;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:19;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:23;a:2:{s:11:\"template_id\";s:1:\"1\";s:15:\"layout_group_id\";s:4:\"NULL\";}i:24;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:25;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:26;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:27;a:2:{s:11:\"template_id\";s:2:\"60\";s:15:\"layout_group_id\";s:4:\"2006\";}i:30;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:31;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}}}}',8,'1.3.7.4','y'),
	(36,'Blueprints_ext','sessions_end','sessions_end','a:1:{i:1;a:12:{s:30:\"enable_publish_layout_takeover\";s:1:\"y\";s:23:\"enable_edit_menu_tweaks\";s:1:\"y\";s:29:\"enable_template_multi_channel\";b:0;s:8:\"template\";a:8:{i:0;s:2:\"10\";i:1;s:2:\"45\";i:2;s:2:\"51\";i:3;s:2:\"20\";i:4;s:2:\"17\";i:5;s:2:\"53\";i:6;s:2:\"60\";i:7;s:1:\"1\";}s:10:\"thumbnails\";a:8:{i:0;s:9:\"pages.jpg\";i:1;s:0:\"\";i:2;s:16:\"newsarticles.jpg\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:8:\"news.jpg\";i:6;s:11:\"related.jpg\";i:7;s:12:\"homepage.jpg\";}s:16:\"layout_group_ids\";a:8:{i:0;s:4:\"2000\";i:1;s:4:\"2001\";i:2;s:4:\"2002\";i:3;s:4:\"2003\";i:4;s:4:\"2004\";i:5;s:4:\"2005\";i:6;s:4:\"2006\";i:7;s:4:\"2007\";}s:18:\"layout_group_names\";a:8:{i:0;s:5:\"Pages\";i:1;s:7:\"Contact\";i:2;s:12:\"News Article\";i:3;s:6:\"Orders\";i:4;s:8:\"Products\";i:5;s:4:\"News\";i:6;s:7:\"Related\";i:7;s:8:\"Homepage\";}s:14:\"thumbnail_path\";s:17:\"themes/templates/\";s:8:\"channels\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"8\";i:2;s:1:\"9\";}s:21:\"channel_show_selected\";a:3:{i:0;s:1:\"y\";i:1;s:1:\"y\";i:2;s:1:\"y\";}s:17:\"channel_templates\";a:3:{i:0;a:5:{i:0;s:2:\"53\";i:1;s:2:\"48\";i:2;s:2:\"45\";i:3;s:1:\"1\";i:4;s:2:\"10\";}i:1;a:1:{i:0;s:2:\"51\";}i:2;a:1:{i:0;s:2:\"60\";}}s:15:\"template_layout\";a:23:{i:1;a:2:{s:11:\"template_id\";s:1:\"1\";s:15:\"layout_group_id\";s:4:\"NULL\";}i:3;a:2:{s:11:\"template_id\";s:2:\"53\";s:15:\"layout_group_id\";s:4:\"2005\";}i:0;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:4;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:6;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:7;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:2;a:2:{s:11:\"template_id\";s:2:\"53\";s:15:\"layout_group_id\";s:4:\"2005\";}i:8;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:10;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:9;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:5;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:11;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:12;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:14;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:17;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:19;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:23;a:2:{s:11:\"template_id\";s:1:\"1\";s:15:\"layout_group_id\";s:4:\"NULL\";}i:24;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:25;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:26;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:27;a:2:{s:11:\"template_id\";s:2:\"60\";s:15:\"layout_group_id\";s:4:\"2006\";}i:30;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:31;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}}}}',8,'1.3.7.4','y'),
	(37,'Blueprints_ext','entry_submission_ready','entry_submission_ready','a:1:{i:1;a:12:{s:30:\"enable_publish_layout_takeover\";s:1:\"y\";s:23:\"enable_edit_menu_tweaks\";s:1:\"y\";s:29:\"enable_template_multi_channel\";b:0;s:8:\"template\";a:8:{i:0;s:2:\"10\";i:1;s:2:\"45\";i:2;s:2:\"51\";i:3;s:2:\"20\";i:4;s:2:\"17\";i:5;s:2:\"53\";i:6;s:2:\"60\";i:7;s:1:\"1\";}s:10:\"thumbnails\";a:8:{i:0;s:9:\"pages.jpg\";i:1;s:0:\"\";i:2;s:16:\"newsarticles.jpg\";i:3;s:0:\"\";i:4;s:0:\"\";i:5;s:8:\"news.jpg\";i:6;s:11:\"related.jpg\";i:7;s:12:\"homepage.jpg\";}s:16:\"layout_group_ids\";a:8:{i:0;s:4:\"2000\";i:1;s:4:\"2001\";i:2;s:4:\"2002\";i:3;s:4:\"2003\";i:4;s:4:\"2004\";i:5;s:4:\"2005\";i:6;s:4:\"2006\";i:7;s:4:\"2007\";}s:18:\"layout_group_names\";a:8:{i:0;s:5:\"Pages\";i:1;s:7:\"Contact\";i:2;s:12:\"News Article\";i:3;s:6:\"Orders\";i:4;s:8:\"Products\";i:5;s:4:\"News\";i:6;s:7:\"Related\";i:7;s:8:\"Homepage\";}s:14:\"thumbnail_path\";s:17:\"themes/templates/\";s:8:\"channels\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"8\";i:2;s:1:\"9\";}s:21:\"channel_show_selected\";a:3:{i:0;s:1:\"y\";i:1;s:1:\"y\";i:2;s:1:\"y\";}s:17:\"channel_templates\";a:3:{i:0;a:5:{i:0;s:2:\"53\";i:1;s:2:\"48\";i:2;s:2:\"45\";i:3;s:1:\"1\";i:4;s:2:\"10\";}i:1;a:1:{i:0;s:2:\"51\";}i:2;a:1:{i:0;s:2:\"60\";}}s:15:\"template_layout\";a:23:{i:1;a:2:{s:11:\"template_id\";s:1:\"1\";s:15:\"layout_group_id\";s:4:\"NULL\";}i:3;a:2:{s:11:\"template_id\";s:2:\"53\";s:15:\"layout_group_id\";s:4:\"2005\";}i:0;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:4;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:6;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:7;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:2;a:2:{s:11:\"template_id\";s:2:\"53\";s:15:\"layout_group_id\";s:4:\"2005\";}i:8;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:10;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:9;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:5;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:11;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:12;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:14;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:17;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:19;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:23;a:2:{s:11:\"template_id\";s:1:\"1\";s:15:\"layout_group_id\";s:4:\"NULL\";}i:24;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:25;a:2:{s:11:\"template_id\";s:2:\"51\";s:15:\"layout_group_id\";s:4:\"2002\";}i:26;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:27;a:2:{s:11:\"template_id\";s:2:\"60\";s:15:\"layout_group_id\";s:4:\"2006\";}i:30;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}i:31;a:2:{s:11:\"template_id\";s:2:\"10\";s:15:\"layout_group_id\";s:4:\"2000\";}}}}',8,'1.3.7.4','y'),
	(42,'Edit_alarm_ext','delete_template_alert','update_template_end','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(43,'Edit_alarm_ext','display_wiki_alert','edit_wiki_article_form_end','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(44,'Edit_alarm_ext','display_wiki_alert','wiki_article_end','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(45,'Edit_alarm_ext','delete_wiki_alert','edit_wiki_article_end','a:4:{s:8:\"apply_to\";a:3:{i:0;s:1:\"e\";i:1;s:1:\"t\";i:2;s:1:\"w\";}s:6:\"expire\";i:10;s:14:\"disable_submit\";s:2:\"no\";s:9:\"alert_css\";s:745:\".editAlarm {\n    color: inherit;\n    font-size: 12px;\n    line-height: 1.5em;\n    margin: 0;\n    padding: 7px 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    color: inherit;\n\n    background-color: #fffcbf;\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0%, rgba(255, 255, 255, 0.3)), color-stop(100%, rgba(255, 255, 255, 0)));\n    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%);\n    background-image: linear-gradient(top, rgba(255, 255, 255, 0.3) 0%, rgba(255, 255, 255, 0) 100%); \n}\n\n.editAlarm ul {\n    margin: 0;\n    padding-left: 1.5em;\n    list-style: disc !important; \n}\n\n.editAlarm span {\n    font-size: small;\n    color: rgba(0, 0, 0, 0.5);\n}\";}',10,'1.2','y'),
	(46,'Low_nospam_ext','sessions_start','sessions_start','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(47,'Low_nospam_ext','insert_comment_insert_array','insert_comment_insert_array','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(48,'Low_nospam_ext','delete_comment_additional','delete_comment_additional','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(49,'Low_nospam_ext','forum_submit_post_start','forum_submit_post_start','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(50,'Low_nospam_ext','edit_wiki_article_end','edit_wiki_article_end','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(51,'Low_nospam_ext','member_member_register_start','member_member_register_start','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(52,'Store_ext','channel_entries_query_result','channel_entries_query_result','',10,'1.5.3.1','y'),
	(53,'Store_ext','cp_menu_array','cp_menu_array','',10,'1.5.3.1','y'),
	(54,'Store_ext','sessions_end','sessions_end','',10,'1.5.3.1','y'),
	(55,'Store_ext','member_member_logout','member_member_logout','',10,'1.5.3.1','y'),
	(56,'Draggable_ext','update_order','sessions_end','',10,'1.3','y'),
	(57,'Low_variables_ext','sessions_end','sessions_end','a:7:{s:11:\"license_key\";s:36:\"2df01ab3-60c7-44d0-b83d-b9e688323aaf\";s:10:\"can_manage\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"1\";}s:16:\"register_globals\";s:1:\"y\";s:20:\"register_member_data\";s:1:\"y\";s:13:\"save_as_files\";s:1:\"n\";s:9:\"file_path\";s:0:\"\";s:13:\"enabled_types\";a:19:{i:0;s:12:\"low_checkbox\";i:1;s:18:\"low_checkbox_group\";i:2;s:15:\"low_radio_group\";i:3;s:14:\"low_reorder_vt\";i:4;s:10:\"low_select\";i:5;s:21:\"low_select_categories\";i:6;s:19:\"low_select_channels\";i:7;s:18:\"low_select_entries\";i:8;s:16:\"low_select_files\";i:9;s:9:\"low_table\";i:10;s:14:\"low_text_input\";i:11;s:6:\"matrix\";i:12;s:5:\"playa\";i:13;s:7:\"pt_list\";i:14;s:7:\"pt_pill\";i:15;s:9:\"pt_switch\";i:16;s:9:\"structure\";i:17;s:6:\"wygwam\";i:18;s:12:\"low_textarea\";}}',2,'2.3.0','y'),
	(58,'Low_variables_ext','template_fetch_template','template_fetch_template','a:7:{s:11:\"license_key\";s:36:\"2df01ab3-60c7-44d0-b83d-b9e688323aaf\";s:10:\"can_manage\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"1\";}s:16:\"register_globals\";s:1:\"y\";s:20:\"register_member_data\";s:1:\"y\";s:13:\"save_as_files\";s:1:\"n\";s:9:\"file_path\";s:0:\"\";s:13:\"enabled_types\";a:19:{i:0;s:12:\"low_checkbox\";i:1;s:18:\"low_checkbox_group\";i:2;s:15:\"low_radio_group\";i:3;s:14:\"low_reorder_vt\";i:4;s:10:\"low_select\";i:5;s:21:\"low_select_categories\";i:6;s:19:\"low_select_channels\";i:7;s:18:\"low_select_entries\";i:8;s:16:\"low_select_files\";i:9;s:9:\"low_table\";i:10;s:14:\"low_text_input\";i:11;s:6:\"matrix\";i:12;s:5:\"playa\";i:13;s:7:\"pt_list\";i:14;s:7:\"pt_pill\";i:15;s:9:\"pt_switch\";i:16;s:9:\"structure\";i:17;s:6:\"wygwam\";i:18;s:12:\"low_textarea\";}}',2,'2.3.0','y'),
	(59,'Structure_ext','core_template_route','core_template_route','',10,'3.3.3','y'),
	(60,'Structure_ext','template_post_parse','template_post_parse','',10,'3.3.3','y'),
	(61,'Authenticate_ext','member_member_logout','member_member_logout','',10,'1.1.1','y'),
	(62,'Zoo_flexible_admin_ext','cp_css_end','cp_css_end','',1,'1.4','y'),
	(63,'Zoo_flexible_admin_ext','cp_js_end','cp_js_end','',1,'1.4','y'),
	(64,'Zoo_flexible_admin_ext','sessions_end','sessions_end','',1,'1.4','y'),
	(65,'Low_reorder_ext','entry_submission_end','entry_submission_end','a:0:{}',5,'2.0.2','y'),
	(66,'Low_reorder_ext','channel_entries_query_result','channel_entries_query_result','a:0:{}',5,'2.0.2','y'),
	(67,'Low_nospam_ext','zoo_visitor_register_validation_start','zoo_visitor_register_validation_start','a:11:{s:7:\"service\";s:4:\"tpas\";s:7:\"api_key\";s:32:\"892704c5bf543fd0d8c4e4688755b5c0\";s:13:\"check_members\";a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"5\";i:3;s:1:\"4\";}s:14:\"check_comments\";s:1:\"y\";s:15:\"caught_comments\";s:1:\"p\";s:17:\"check_forum_posts\";s:1:\"n\";s:19:\"check_wiki_articles\";s:1:\"n\";s:26:\"check_member_registrations\";s:1:\"y\";s:23:\"moderate_if_unreachable\";s:1:\"y\";s:14:\"zero_tolerance\";s:1:\"n\";s:26:\"check_visitor_registration\";s:1:\"n\";}',1,'2.2.3','y'),
	(68,'Republic_analytics_ext','cp_js_end','cp_js_end','',10,'2.2.2','y'),
	(69,'Republic_analytics_ext','sessions_end','sessions_end','',10,'2.2.2','y'),
	(70,'Stash_ext','template_fetch_template','template_fetch_template','',10,'2.3.2','y'),
	(71,'Stash_ext','template_post_parse','template_post_parse','',10,'2.3.2','y'),
	(72,'Snippet_sync_ext','sessions_end','sessions_end','a:6:{s:6:\"hashes\";a:1:{i:1;a:22:{s:21:\"snippet:global_footer\";a:2:{i:0;s:32:\"c083de7270bff1d8755552486d551f45\";i:1;s:10:\"1351620810\";}s:21:\"snippet:global_header\";a:2:{i:0;s:32:\"5c870e8989b3c1fe76c3843fe5b44c28\";i:1;s:10:\"1351610310\";}s:25:\"snippet:global_navigation\";a:2:{i:0;s:32:\"b1b68f8763f1a5afdae0d16cb4bc24f7\";i:1;s:10:\"1351375061\";}s:21:\"snippet:blog_comments\";a:2:{i:0;s:32:\"2b556a8e74c1331dc50744afaba9ad44\";i:1;s:10:\"1351375103\";}s:19:\"snippet:global_head\";a:2:{i:0;s:32:\"d22d4f7fc2cc6d74ed9408ffbcc4d934\";i:1;s:10:\"1351540679\";}s:24:\"snippet:modules_carousel\";a:2:{i:0;s:32:\"9c22f115695ee8063ddfd25ee8ca7d40\";i:1;s:10:\"1351533320\";}s:27:\"snippet:modules_getinvolved\";a:2:{i:0;s:32:\"b00f8df21c51e021a0c9e72694ab77b6\";i:1;s:10:\"1351495912\";}s:26:\"snippet:modules_latestnews\";a:2:{i:0;s:32:\"011f4d45bf1578522c04df58962eb5e4\";i:1;s:10:\"1351493718\";}s:25:\"snippet:modules_spotlight\";a:2:{i:0;s:32:\"e2b597def149ba3a4268f0eeaeb7fdb5\";i:1;s:10:\"1351546759\";}s:21:\"snippet:news_comments\";a:2:{i:0;s:32:\"127e5ddfbe14eda31e39c2dc2cc44d1d\";i:1;s:10:\"1351586927\";}s:20:\"snippet:news_entries\";a:2:{i:0;s:32:\"8327315ec7a31bcb66bc609e8b4eb252\";i:1;s:10:\"1351546171\";}s:20:\"snippet:stash_footer\";a:2:{i:0;s:32:\"5a443a84ebae4216f0ea794f3c1deb35\";i:1;s:10:\"1351484431\";}s:18:\"snippet:stash_head\";a:2:{i:0;s:32:\"d22d4f7fc2cc6d74ed9408ffbcc4d934\";i:1;s:10:\"1351540679\";}s:20:\"snippet:stash_header\";a:2:{i:0;s:32:\"ae4ee18de5e30ef5de98e9bee5d8e1f0\";i:1;s:10:\"1351542329\";}s:22:\"snippet:layouts_footer\";a:2:{i:0;s:32:\"5a443a84ebae4216f0ea794f3c1deb35\";i:1;s:10:\"1351484431\";}s:20:\"snippet:layouts_head\";a:2:{i:0;s:32:\"d22d4f7fc2cc6d74ed9408ffbcc4d934\";i:1;s:10:\"1351540679\";}s:22:\"snippet:layouts_header\";a:2:{i:0;s:32:\"c417159a6cb1ca0d60105954009f30b3\";i:1;s:10:\"1351596843\";}s:22:\"snippet:global_sidebar\";a:2:{i:0;s:32:\"f22b5d2800a625243d3acd2f8b36fc4d\";i:1;s:10:\"1351582044\";}s:18:\"snippet:if_sidebar\";a:2:{i:0;s:32:\"e0fe5f731c9a375df92733da99ed693d\";i:1;s:10:\"1351580394\";}s:25:\"snippet:global_pagebanner\";a:2:{i:0;s:32:\"a6f22e05e2a1dd73bff452f0ae9a7e36\";i:1;s:10:\"1351580458\";}s:24:\"snippet:if_titleoverride\";a:2:{i:0;s:32:\"d8b859a76f62223f6ea3e42546083067\";i:1;s:10:\"1351580618\";}s:20:\"snippet:modules_form\";a:2:{i:0;s:32:\"04209fb0bc327f4f106f363555a14c8f\";i:1;s:10:\"1351594042\";}}}s:6:\"prefix\";s:8:\"snippet:\";s:16:\"subfolder_suffix\";s:1:\"_\";s:17:\"msm_shared_folder\";s:6:\"shared\";s:9:\"hide_menu\";s:1:\"n\";s:14:\"enable_cleanup\";s:1:\"n\";}',8,'1.3','y'),
	(73,'Snippet_sync_ext','show_full_control_panel_end','show_full_control_panel_end','a:6:{s:6:\"hashes\";a:1:{i:1;a:22:{s:21:\"snippet:global_footer\";a:2:{i:0;s:32:\"c083de7270bff1d8755552486d551f45\";i:1;s:10:\"1351620810\";}s:21:\"snippet:global_header\";a:2:{i:0;s:32:\"5c870e8989b3c1fe76c3843fe5b44c28\";i:1;s:10:\"1351610310\";}s:25:\"snippet:global_navigation\";a:2:{i:0;s:32:\"b1b68f8763f1a5afdae0d16cb4bc24f7\";i:1;s:10:\"1351375061\";}s:21:\"snippet:blog_comments\";a:2:{i:0;s:32:\"2b556a8e74c1331dc50744afaba9ad44\";i:1;s:10:\"1351375103\";}s:19:\"snippet:global_head\";a:2:{i:0;s:32:\"d22d4f7fc2cc6d74ed9408ffbcc4d934\";i:1;s:10:\"1351540679\";}s:24:\"snippet:modules_carousel\";a:2:{i:0;s:32:\"9c22f115695ee8063ddfd25ee8ca7d40\";i:1;s:10:\"1351533320\";}s:27:\"snippet:modules_getinvolved\";a:2:{i:0;s:32:\"b00f8df21c51e021a0c9e72694ab77b6\";i:1;s:10:\"1351495912\";}s:26:\"snippet:modules_latestnews\";a:2:{i:0;s:32:\"011f4d45bf1578522c04df58962eb5e4\";i:1;s:10:\"1351493718\";}s:25:\"snippet:modules_spotlight\";a:2:{i:0;s:32:\"e2b597def149ba3a4268f0eeaeb7fdb5\";i:1;s:10:\"1351546759\";}s:21:\"snippet:news_comments\";a:2:{i:0;s:32:\"127e5ddfbe14eda31e39c2dc2cc44d1d\";i:1;s:10:\"1351586927\";}s:20:\"snippet:news_entries\";a:2:{i:0;s:32:\"8327315ec7a31bcb66bc609e8b4eb252\";i:1;s:10:\"1351546171\";}s:20:\"snippet:stash_footer\";a:2:{i:0;s:32:\"5a443a84ebae4216f0ea794f3c1deb35\";i:1;s:10:\"1351484431\";}s:18:\"snippet:stash_head\";a:2:{i:0;s:32:\"d22d4f7fc2cc6d74ed9408ffbcc4d934\";i:1;s:10:\"1351540679\";}s:20:\"snippet:stash_header\";a:2:{i:0;s:32:\"ae4ee18de5e30ef5de98e9bee5d8e1f0\";i:1;s:10:\"1351542329\";}s:22:\"snippet:layouts_footer\";a:2:{i:0;s:32:\"5a443a84ebae4216f0ea794f3c1deb35\";i:1;s:10:\"1351484431\";}s:20:\"snippet:layouts_head\";a:2:{i:0;s:32:\"d22d4f7fc2cc6d74ed9408ffbcc4d934\";i:1;s:10:\"1351540679\";}s:22:\"snippet:layouts_header\";a:2:{i:0;s:32:\"c417159a6cb1ca0d60105954009f30b3\";i:1;s:10:\"1351596843\";}s:22:\"snippet:global_sidebar\";a:2:{i:0;s:32:\"f22b5d2800a625243d3acd2f8b36fc4d\";i:1;s:10:\"1351582044\";}s:18:\"snippet:if_sidebar\";a:2:{i:0;s:32:\"e0fe5f731c9a375df92733da99ed693d\";i:1;s:10:\"1351580394\";}s:25:\"snippet:global_pagebanner\";a:2:{i:0;s:32:\"a6f22e05e2a1dd73bff452f0ae9a7e36\";i:1;s:10:\"1351580458\";}s:24:\"snippet:if_titleoverride\";a:2:{i:0;s:32:\"d8b859a76f62223f6ea3e42546083067\";i:1;s:10:\"1351580618\";}s:20:\"snippet:modules_form\";a:2:{i:0;s:32:\"04209fb0bc327f4f106f363555a14c8f\";i:1;s:10:\"1351594042\";}}}s:6:\"prefix\";s:8:\"snippet:\";s:16:\"subfolder_suffix\";s:1:\"_\";s:17:\"msm_shared_folder\";s:6:\"shared\";s:9:\"hide_menu\";s:1:\"n\";s:14:\"enable_cleanup\";s:1:\"n\";}',8,'1.3','y');

/*!40000 ALTER TABLE `ee2_extensions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_field_formatting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_field_formatting`;

CREATE TABLE `ee2_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_field_formatting` WRITE;
/*!40000 ALTER TABLE `ee2_field_formatting` DISABLE KEYS */;

INSERT INTO `ee2_field_formatting` (`formatting_id`, `field_id`, `field_fmt`)
VALUES
	(1,1,'none'),
	(2,1,'br'),
	(3,1,'xhtml'),
	(31,11,'none'),
	(32,11,'br'),
	(33,11,'xhtml'),
	(34,12,'none'),
	(35,12,'br'),
	(36,12,'xhtml'),
	(249,83,'xhtml'),
	(248,83,'br'),
	(247,83,'none'),
	(55,19,'none'),
	(56,19,'br'),
	(57,19,'xhtml'),
	(70,24,'none'),
	(71,24,'br'),
	(72,24,'xhtml'),
	(76,26,'none'),
	(77,26,'br'),
	(78,26,'xhtml'),
	(82,28,'none'),
	(83,28,'br'),
	(84,28,'xhtml'),
	(85,29,'none'),
	(86,29,'br'),
	(87,29,'xhtml'),
	(91,31,'none'),
	(92,31,'br'),
	(93,31,'xhtml'),
	(94,32,'none'),
	(95,32,'br'),
	(96,32,'xhtml'),
	(97,33,'none'),
	(98,33,'br'),
	(99,33,'xhtml'),
	(100,34,'none'),
	(101,34,'br'),
	(102,34,'xhtml'),
	(103,35,'none'),
	(104,35,'br'),
	(105,35,'xhtml'),
	(106,36,'none'),
	(107,36,'br'),
	(108,36,'xhtml'),
	(109,37,'none'),
	(110,37,'br'),
	(111,37,'xhtml'),
	(112,38,'none'),
	(113,38,'br'),
	(114,38,'xhtml'),
	(115,39,'none'),
	(116,39,'br'),
	(117,39,'xhtml'),
	(118,40,'none'),
	(119,40,'br'),
	(120,40,'xhtml'),
	(121,41,'none'),
	(122,41,'br'),
	(123,41,'xhtml'),
	(124,42,'none'),
	(125,42,'br'),
	(126,42,'xhtml'),
	(127,43,'none'),
	(128,43,'br'),
	(129,43,'xhtml'),
	(130,44,'none'),
	(131,44,'br'),
	(132,44,'xhtml'),
	(133,45,'none'),
	(134,45,'br'),
	(135,45,'xhtml'),
	(136,46,'none'),
	(137,46,'br'),
	(138,46,'xhtml'),
	(139,47,'none'),
	(140,47,'br'),
	(141,47,'xhtml'),
	(142,48,'none'),
	(143,48,'br'),
	(144,48,'xhtml'),
	(145,49,'none'),
	(146,49,'br'),
	(147,49,'xhtml'),
	(148,50,'none'),
	(149,50,'br'),
	(150,50,'xhtml'),
	(151,51,'none'),
	(152,51,'br'),
	(153,51,'xhtml'),
	(154,52,'none'),
	(155,52,'br'),
	(156,52,'xhtml'),
	(157,53,'none'),
	(158,53,'br'),
	(159,53,'xhtml'),
	(160,54,'none'),
	(161,54,'br'),
	(162,54,'xhtml'),
	(163,55,'none'),
	(164,55,'br'),
	(165,55,'xhtml'),
	(166,56,'none'),
	(167,56,'br'),
	(168,56,'xhtml'),
	(169,57,'none'),
	(170,57,'br'),
	(171,57,'xhtml'),
	(172,58,'none'),
	(173,58,'br'),
	(174,58,'xhtml'),
	(175,59,'none'),
	(176,59,'br'),
	(177,59,'xhtml'),
	(178,60,'none'),
	(179,60,'br'),
	(180,60,'xhtml'),
	(181,61,'none'),
	(182,61,'br'),
	(183,61,'xhtml'),
	(276,92,'xhtml'),
	(275,92,'br'),
	(274,92,'none'),
	(273,91,'xhtml'),
	(272,91,'br'),
	(271,91,'none'),
	(270,90,'xhtml'),
	(269,90,'br'),
	(268,90,'none'),
	(267,89,'xhtml'),
	(266,89,'br'),
	(265,89,'none'),
	(264,88,'xhtml'),
	(263,88,'br'),
	(262,88,'none'),
	(214,72,'none'),
	(215,72,'br'),
	(216,72,'xhtml'),
	(217,73,'none'),
	(218,73,'br'),
	(219,73,'xhtml'),
	(220,74,'none'),
	(221,74,'br'),
	(222,74,'xhtml'),
	(223,75,'none'),
	(224,75,'br'),
	(225,75,'xhtml'),
	(226,76,'none'),
	(227,76,'br'),
	(228,76,'xhtml'),
	(229,77,'none'),
	(230,77,'br'),
	(231,77,'xhtml'),
	(232,78,'none'),
	(233,78,'br'),
	(234,78,'xhtml'),
	(235,79,'none'),
	(236,79,'br'),
	(237,79,'xhtml'),
	(238,80,'none'),
	(239,80,'br'),
	(240,80,'xhtml'),
	(241,81,'none'),
	(242,81,'br'),
	(243,81,'xhtml'),
	(244,82,'none'),
	(245,82,'br'),
	(246,82,'xhtml'),
	(250,84,'none'),
	(251,84,'br'),
	(252,84,'xhtml'),
	(253,85,'none'),
	(254,85,'br'),
	(255,85,'xhtml'),
	(256,86,'none'),
	(257,86,'br'),
	(258,86,'xhtml'),
	(259,87,'none'),
	(260,87,'br'),
	(261,87,'xhtml');

/*!40000 ALTER TABLE `ee2_field_formatting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_field_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_field_groups`;

CREATE TABLE `ee2_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_field_groups` WRITE;
/*!40000 ALTER TABLE `ee2_field_groups` DISABLE KEYS */;

INSERT INTO `ee2_field_groups` (`group_id`, `site_id`, `group_name`)
VALUES
	(1,1,'Pages'),
	(2,1,'Products'),
	(4,1,'Orders'),
	(8,1,'News');

/*!40000 ALTER TABLE `ee2_field_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_fieldtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_fieldtypes`;

CREATE TABLE `ee2_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_fieldtypes` WRITE;
/*!40000 ALTER TABLE `ee2_fieldtypes` DISABLE KEYS */;

INSERT INTO `ee2_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`)
VALUES
	(1,'select','1.0','YTowOnt9','n'),
	(2,'text','1.0','YTowOnt9','n'),
	(3,'textarea','1.0','YTowOnt9','n'),
	(4,'date','1.0','YTowOnt9','n'),
	(5,'file','1.0','YTowOnt9','n'),
	(6,'multi_select','1.0','YTowOnt9','n'),
	(7,'checkboxes','1.0','YTowOnt9','n'),
	(8,'radio','1.0','YTowOnt9','n'),
	(9,'rel','1.0','YTowOnt9','n'),
	(24,'structure','3.2.3','YTowOnt9','n'),
	(16,'matrix','2.3','YTowOnt9','y'),
	(17,'wygwam','2.3.5','YTowOnt9','y'),
	(18,'playa','4.1.0.3','YTowOnt9','y'),
	(25,'pt_checkboxes','1.0.3','YTowOnt9','n'),
	(26,'pt_dropdown','1.0.3','YTowOnt9','n'),
	(27,'pt_list','1.0.3','YTowOnt9','n'),
	(28,'pt_multiselect','1.0.3','YTowOnt9','n'),
	(29,'pt_pill','1.0.3','YTowOnt9','n'),
	(30,'pt_radio_buttons','1.0.3','YTowOnt9','n'),
	(31,'pt_switch','1.0.4','YTowOnt9','n'),
	(32,'plates','1.0.2','YTowOnt9','n'),
	(33,'safecracker_file','2.1','YTowOnt9','n'),
	(34,'store','1.5.3.1','YTowOnt9','n'),
	(35,'low_variables','2.2.1','YTowOnt9','n'),
	(36,'low_freeform_field','1.0.1','YTowOnt9','n');

/*!40000 ALTER TABLE `ee2_fieldtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_file_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_file_categories`;

CREATE TABLE `ee2_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_file_dimensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_file_dimensions`;

CREATE TABLE `ee2_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_file_dimensions` WRITE;
/*!40000 ALTER TABLE `ee2_file_dimensions` DISABLE KEYS */;

INSERT INTO `ee2_file_dimensions` (`id`, `upload_location_id`, `title`, `short_name`, `resize_type`, `width`, `height`, `watermark_id`, `site_id`)
VALUES
	(1,5,'thumbnail','thumbnail','constrain',50,50,0,1),
	(2,5,'standard','standard','constrain',350,350,0,1),
	(3,5,'large','large','constrain',600,600,0,1),
	(4,6,'resized','resized','none',220,0,0,1),
	(5,7,'resized','resized','crop',940,265,0,1),
	(6,3,'thumbnail','thumbnail','crop',75,75,0,1),
	(7,2,'twocolumnbanner','twocolumnbanner','constrain',706,0,0,1),
	(8,8,'resized','resized','constrain',265,0,0,1),
	(9,9,'resized','resized','constrain',200,0,0,1),
	(10,10,'resized','resized','constrain',200,0,0,1);

/*!40000 ALTER TABLE `ee2_file_dimensions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_file_watermarks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_file_watermarks`;

CREATE TABLE `ee2_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(11) DEFAULT NULL,
  `wm_vrt_offset` int(11) DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_files`;

CREATE TABLE `ee2_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_files` WRITE;
/*!40000 ALTER TABLE `ee2_files` DISABLE KEYS */;

INSERT INTO `ee2_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`, `credit`, `location`)
VALUES
	(1,1,'slider-volunteer.jpeg',7,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/carousel/slider-volunteer.jpeg','image/jpeg','slider-volunteer.jpeg',93695,NULL,2,1351493148,2,1351493358,'265 940',NULL,NULL),
	(2,1,'fpo-library.jpeg',7,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/carousel/fpo-library.jpeg','image/jpeg','fpo-library.jpeg',108258,NULL,2,1351493187,2,1351493358,'265 940',NULL,NULL),
	(4,1,'fpo-donation-1.jpeg',7,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/carousel/fpo-donation-1.jpeg','image/jpeg','fpo-donation-1.jpeg',74408,NULL,2,1351493322,2,1351493322,'265 940',NULL,NULL),
	(5,1,'get-involved-couple.jpg',6,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/getinvolved/get-involved-couple.jpg','image/jpeg','get-involved-couple.jpg',22385,NULL,2,1351493417,2,1351493417,'103 163',NULL,NULL),
	(6,1,'fpo-library-1.jpeg',3,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/news/fpo-library-1.jpeg','image/jpeg','fpo-library-1.jpeg',108258,NULL,2,1351494391,2,1351544269,'265 940',NULL,NULL),
	(7,1,'home.jpeg',3,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/news/home.jpeg','image/jpeg','home.jpeg',6373,NULL,2,1351495530,2,1351544269,'66 75',NULL,NULL),
	(8,1,'home-1.jpeg',3,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/news/home-1.jpeg','image/jpeg','home-1.jpeg',10128,NULL,2,1351495774,2,1351544269,'73 74',NULL,NULL),
	(9,1,'img-interior-banner-fpo.jpeg',2,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/pages/img-interior-banner-fpo.jpeg','image/jpeg','img-interior-banner-fpo.jpeg',114192,NULL,2,1351497145,2,1351498740,'281 706',NULL,NULL),
	(10,1,'bg-banner-overlay-home.png',2,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/pages/bg-banner-overlay-home.png','image/png','bg-banner-overlay-home.png',366899,NULL,2,1351498878,2,1351498878,'267 706',NULL,NULL),
	(11,1,'fpo-library.jpg',2,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/pages/fpo-library.jpg','image/jpeg','fpo-library.jpg',108257,NULL,2,1351498917,2,1351498917,'265 940',NULL,NULL),
	(12,1,'img_event-fpo.jpeg',8,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/news/feature/img_event-fpo.jpeg','image/jpeg','img_event-fpo.jpeg',55933,NULL,2,1351544113,2,1351582389,'248 263',NULL,NULL),
	(13,1,'how-to-add-related-content.png',2,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/pages/how-to-add-related-content.png','image/png','how-to-add-related-content.png',17050,NULL,2,1351575711,2,1351575711,'198 408',NULL,NULL),
	(14,1,'news.jpeg',9,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/related/callout/news.jpeg','image/jpeg','news.jpeg',27047,NULL,2,1351578263,2,1351579273,'209 197',NULL,NULL),
	(15,1,'news-1.jpeg',10,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/related/preview/news-1.jpeg','image/jpeg','news-1.jpeg',21577,NULL,2,1351578337,2,1351579660,'95 210',NULL,NULL),
	(16,1,'news-detail.jpeg',10,'/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/related/preview/news-detail.jpeg','image/jpeg','news-detail.jpeg',10803,NULL,2,1351581302,2,1351581302,'96 211',NULL,NULL);

/*!40000 ALTER TABLE `ee2_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_freeform_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_attachments`;

CREATE TABLE `ee2_freeform_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pref_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `server_path` varchar(150) NOT NULL DEFAULT '',
  `filename` varchar(150) NOT NULL DEFAULT '',
  `extension` varchar(7) NOT NULL DEFAULT '',
  `filesize` int(10) NOT NULL DEFAULT '1',
  `emailed` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`attachment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `pref_id` (`pref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ee2_freeform_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_entries`;

CREATE TABLE `ee2_freeform_entries` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `weblog_id` int(4) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `form_name` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(150) NOT NULL DEFAULT '',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `status` char(10) NOT NULL DEFAULT 'open',
  `name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `website` varchar(50) NOT NULL DEFAULT '',
  `street1` varchar(50) NOT NULL DEFAULT '',
  `street2` varchar(50) NOT NULL DEFAULT '',
  `street3` varchar(50) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `state` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `postalcode` varchar(50) NOT NULL DEFAULT '',
  `phone1` varchar(50) NOT NULL DEFAULT '',
  `phone2` varchar(50) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `interests` varchar(150) NOT NULL DEFAULT '',
  `comments` text NOT NULL,
  PRIMARY KEY (`entry_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_freeform_entries` WRITE;
/*!40000 ALTER TABLE `ee2_freeform_entries` DISABLE KEYS */;

INSERT INTO `ee2_freeform_entries` (`entry_id`, `group_id`, `weblog_id`, `author_id`, `ip_address`, `form_name`, `template`, `entry_date`, `edit_date`, `status`, `name`, `email`, `website`, `street1`, `street2`, `street3`, `city`, `state`, `country`, `postalcode`, `phone1`, `phone2`, `fax`, `interests`, `comments`)
VALUES
	(1,1,0,2,'127.0.0.1','Request Information','',1351593510,1351593510,'open','Jarrett Barnett','me@jarrettbarnett.com','','','','','','','','','','','','Backend Development','test'),
	(2,1,0,2,'127.0.0.1','Request Information','',1351593542,1351593542,'open','Jarrett Barnett','me@jarrettbarnett.com','','','','','','','','','','','','Backend Development','test test'),
	(3,1,0,2,'127.0.0.1','Request Information','',1351593615,1351593615,'open','Jarrett Barnett','me@jarrettbarnett.com','','','','','','','','','','','','Backend Development','tes testest se'),
	(4,1,0,2,'127.0.0.1','Request Information','admin_template',1351594098,1351594098,'open','Jarrett Barnett','me@jarrettbarnett.com','','','','','','','','','','','','MVC Frameworks','Test test test');

/*!40000 ALTER TABLE `ee2_freeform_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_freeform_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_fields`;

CREATE TABLE `ee2_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_order` int(10) NOT NULL DEFAULT '0',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_length` int(3) NOT NULL DEFAULT '150',
  `form_name` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `name_old` varchar(50) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `weblog_id` int(4) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `editable` char(1) NOT NULL DEFAULT 'y',
  `status` char(10) NOT NULL DEFAULT 'open',
  PRIMARY KEY (`field_id`),
  KEY `name` (`name`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_freeform_fields` WRITE;
/*!40000 ALTER TABLE `ee2_freeform_fields` DISABLE KEYS */;

INSERT INTO `ee2_freeform_fields` (`field_id`, `field_order`, `field_type`, `field_length`, `form_name`, `name`, `name_old`, `label`, `weblog_id`, `author_id`, `entry_date`, `edit_date`, `editable`, `status`)
VALUES
	(1,1,'text',150,'','name','','Name',0,0,0,0,'n','open'),
	(2,2,'text',150,'','email','','Email',0,0,0,0,'n','open'),
	(3,3,'text',150,'','website','','Website',0,0,0,0,'n','open'),
	(4,4,'text',150,'','street1','','Street 1',0,0,0,0,'n','open'),
	(5,5,'text',150,'','street2','','Street 2',0,0,0,0,'n','open'),
	(6,6,'text',150,'','street3','','Street 3',0,0,0,0,'n','open'),
	(7,7,'text',150,'','city','','City',0,0,0,0,'n','open'),
	(8,8,'text',150,'','state','','State',0,0,0,0,'n','open'),
	(9,9,'text',150,'','country','','Country',0,0,0,0,'n','open'),
	(10,10,'text',150,'','postalcode','','Postal Code',0,0,0,0,'n','open'),
	(11,11,'text',150,'','phone1','','Phone 1',0,0,0,0,'n','open'),
	(12,12,'text',150,'','phone2','','Phone 2',0,0,0,0,'n','open'),
	(13,13,'text',150,'','fax','','Fax',0,0,0,0,'n','open'),
	(14,14,'text',150,'','interests','','Interests',0,0,0,0,'y','open'),
	(15,15,'textarea',150,'','comments','','Comments',0,0,0,0,'y','open');

/*!40000 ALTER TABLE `ee2_freeform_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_freeform_params
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_params`;

CREATE TABLE `ee2_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_freeform_params` WRITE;
/*!40000 ALTER TABLE `ee2_freeform_params` DISABLE KEYS */;

INSERT INTO `ee2_freeform_params` (`params_id`, `entry_date`, `data`)
VALUES
	(139,1351801888,'a:25:{s:15:\"require_captcha\";s:2:\"no\";s:9:\"form_name\";s:8:\"About Us\";s:10:\"require_ip\";s:0:\"\";s:11:\"ee_required\";s:0:\"\";s:9:\"ee_notify\";s:21:\"me@jarrettbarnett.com\";s:18:\"allowed_file_types\";s:0:\"\";s:8:\"reply_to\";b:0;s:20:\"reply_to_email_field\";s:0:\"\";s:19:\"reply_to_name_field\";s:0:\"\";s:11:\"output_json\";s:1:\"n\";s:12:\"ajax_request\";s:1:\"y\";s:10:\"recipients\";s:1:\"n\";s:15:\"recipient_limit\";s:2:\"10\";s:17:\"static_recipients\";b:0;s:22:\"static_recipients_list\";a:0:{}s:18:\"recipient_template\";s:16:\"default_template\";s:13:\"discard_field\";s:0:\"\";s:15:\"send_attachment\";s:0:\"\";s:15:\"send_user_email\";s:3:\"yes\";s:20:\"send_user_attachment\";s:0:\"\";s:18:\"attachment_profile\";s:0:\"\";s:19:\"user_email_template\";s:19:\"user_email_template\";s:8:\"template\";s:14:\"admin_template\";s:20:\"prevent_duplicate_on\";s:0:\"\";s:11:\"file_upload\";s:0:\"\";}'),
	(140,1351802557,'a:25:{s:15:\"require_captcha\";s:2:\"no\";s:9:\"form_name\";s:8:\"About Us\";s:10:\"require_ip\";s:0:\"\";s:11:\"ee_required\";s:0:\"\";s:9:\"ee_notify\";s:21:\"me@jarrettbarnett.com\";s:18:\"allowed_file_types\";s:0:\"\";s:8:\"reply_to\";b:0;s:20:\"reply_to_email_field\";s:0:\"\";s:19:\"reply_to_name_field\";s:0:\"\";s:11:\"output_json\";s:1:\"n\";s:12:\"ajax_request\";s:1:\"y\";s:10:\"recipients\";s:1:\"n\";s:15:\"recipient_limit\";s:2:\"10\";s:17:\"static_recipients\";b:0;s:22:\"static_recipients_list\";a:0:{}s:18:\"recipient_template\";s:16:\"default_template\";s:13:\"discard_field\";s:0:\"\";s:15:\"send_attachment\";s:0:\"\";s:15:\"send_user_email\";s:3:\"yes\";s:20:\"send_user_attachment\";s:0:\"\";s:18:\"attachment_profile\";s:0:\"\";s:19:\"user_email_template\";s:19:\"user_email_template\";s:8:\"template\";s:14:\"admin_template\";s:20:\"prevent_duplicate_on\";s:0:\"\";s:11:\"file_upload\";s:0:\"\";}'),
	(141,1351802612,'a:25:{s:15:\"require_captcha\";s:2:\"no\";s:9:\"form_name\";s:8:\"About Us\";s:10:\"require_ip\";s:0:\"\";s:11:\"ee_required\";s:0:\"\";s:9:\"ee_notify\";s:21:\"me@jarrettbarnett.com\";s:18:\"allowed_file_types\";s:0:\"\";s:8:\"reply_to\";b:0;s:20:\"reply_to_email_field\";s:0:\"\";s:19:\"reply_to_name_field\";s:0:\"\";s:11:\"output_json\";s:1:\"n\";s:12:\"ajax_request\";s:1:\"y\";s:10:\"recipients\";s:1:\"n\";s:15:\"recipient_limit\";s:2:\"10\";s:17:\"static_recipients\";b:0;s:22:\"static_recipients_list\";a:0:{}s:18:\"recipient_template\";s:16:\"default_template\";s:13:\"discard_field\";s:0:\"\";s:15:\"send_attachment\";s:0:\"\";s:15:\"send_user_email\";s:3:\"yes\";s:20:\"send_user_attachment\";s:0:\"\";s:18:\"attachment_profile\";s:0:\"\";s:19:\"user_email_template\";s:19:\"user_email_template\";s:8:\"template\";s:14:\"admin_template\";s:20:\"prevent_duplicate_on\";s:0:\"\";s:11:\"file_upload\";s:0:\"\";}'),
	(142,1351802616,'a:25:{s:15:\"require_captcha\";s:2:\"no\";s:9:\"form_name\";s:8:\"About Us\";s:10:\"require_ip\";s:0:\"\";s:11:\"ee_required\";s:0:\"\";s:9:\"ee_notify\";s:21:\"me@jarrettbarnett.com\";s:18:\"allowed_file_types\";s:0:\"\";s:8:\"reply_to\";b:0;s:20:\"reply_to_email_field\";s:0:\"\";s:19:\"reply_to_name_field\";s:0:\"\";s:11:\"output_json\";s:1:\"n\";s:12:\"ajax_request\";s:1:\"y\";s:10:\"recipients\";s:1:\"n\";s:15:\"recipient_limit\";s:2:\"10\";s:17:\"static_recipients\";b:0;s:22:\"static_recipients_list\";a:0:{}s:18:\"recipient_template\";s:16:\"default_template\";s:13:\"discard_field\";s:0:\"\";s:15:\"send_attachment\";s:0:\"\";s:15:\"send_user_email\";s:3:\"yes\";s:20:\"send_user_attachment\";s:0:\"\";s:18:\"attachment_profile\";s:0:\"\";s:19:\"user_email_template\";s:19:\"user_email_template\";s:8:\"template\";s:14:\"admin_template\";s:20:\"prevent_duplicate_on\";s:0:\"\";s:11:\"file_upload\";s:0:\"\";}');

/*!40000 ALTER TABLE `ee2_freeform_params` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_freeform_preferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_preferences`;

CREATE TABLE `ee2_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) NOT NULL DEFAULT '',
  `preference_value` text NOT NULL,
  PRIMARY KEY (`preference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ee2_freeform_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_templates`;

CREATE TABLE `ee2_freeform_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `html` char(1) NOT NULL DEFAULT 'n',
  `template_name` varchar(150) NOT NULL DEFAULT '',
  `template_label` varchar(150) NOT NULL DEFAULT '',
  `data_from_name` varchar(150) NOT NULL DEFAULT '',
  `data_from_email` varchar(200) NOT NULL DEFAULT '',
  `data_title` varchar(80) NOT NULL DEFAULT '',
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_freeform_templates` WRITE;
/*!40000 ALTER TABLE `ee2_freeform_templates` DISABLE KEYS */;

INSERT INTO `ee2_freeform_templates` (`template_id`, `enable_template`, `wordwrap`, `html`, `template_name`, `template_label`, `data_from_name`, `data_from_email`, `data_title`, `template_data`)
VALUES
	(1,'y','y','n','default_template','Default Template','','','Someone has posted to Freeform','Someone has posted to Freeform. Here are the details:  \n			 		\nEntry Date: {entry_date}\n{all_custom_fields}'),
	(2,'y','n','n','user_email_template','User Template','Custom Form @ Jarrett\'s NAT','me@jarrettbarnett.com','{name}, thank you for your request','Thank you for your reaching out. Your request for more information will be handled shortly.\n\nHere was all the information you provided (in case you wanted to know):\n{all_custom_fields}'),
	(3,'y','n','n','admin_template','Admin Template','Custom Form @ Jarrett\'s NAT','me@jarrettbarnett.com','Information Request from {name}','{name} has submitted a request for more information on {entry_date}.\n\nThe details are below:\n{all_custom_fields}');

/*!40000 ALTER TABLE `ee2_freeform_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_freeform_user_email
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_freeform_user_email`;

CREATE TABLE `ee2_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ee2_global_variables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_global_variables`;

CREATE TABLE `ee2_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_global_variables` WRITE;
/*!40000 ALTER TABLE `ee2_global_variables` DISABLE KEYS */;

INSERT INTO `ee2_global_variables` (`variable_id`, `site_id`, `variable_name`, `variable_data`)
VALUES
	(1,1,'scriptspath','/scripts/'),
	(2,1,'stylespath','/styles/'),
	(3,1,'graphicspath','/images/'),
	(4,1,'analytics_id','UA-35910878-1'),
	(5,1,'showadmintoolbar','y'),
	(6,1,'contactinfo','1'),
	(7,1,'addressinfo','1'),
	(8,1,'cachedriver','dummy');

/*!40000 ALTER TABLE `ee2_global_variables` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_html_buttons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_html_buttons`;

CREATE TABLE `ee2_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_html_buttons` WRITE;
/*!40000 ALTER TABLE `ee2_html_buttons` DISABLE KEYS */;

INSERT INTO `ee2_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`)
VALUES
	(1,1,0,'b','<strong>','</strong>','b',1,'1','btn_b'),
	(2,1,0,'i','<em>','</em>','i',2,'1','btn_i'),
	(3,1,0,'blockquote','<blockquote>','</blockquote>','q',3,'1','btn_blockquote'),
	(4,1,0,'a','<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>','</a>','a',4,'1','btn_a'),
	(5,1,0,'img','<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />','','',5,'1','btn_img');

/*!40000 ALTER TABLE `ee2_html_buttons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_layout_publish
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_layout_publish`;

CREATE TABLE `ee2_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_layout_publish` WRITE;
/*!40000 ALTER TABLE `ee2_layout_publish` DISABLE KEYS */;

INSERT INTO `ee2_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`)
VALUES
	(47,1,2004,2,'a:4:{s:7:\"publish\";a:13:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:12;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:83;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}}s:4:\"meta\";a:2:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"settings\";a:4:{s:10:\"_tab_label\";s:8:\"Settings\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(48,1,1,2,'a:4:{s:7:\"publish\";a:13:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:12;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:83;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}}s:4:\"meta\";a:2:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"settings\";a:4:{s:10:\"_tab_label\";s:8:\"Settings\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(49,1,6,2,'a:4:{s:7:\"publish\";a:13:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:12;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:83;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}}s:4:\"meta\";a:2:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"settings\";a:4:{s:10:\"_tab_label\";s:8:\"Settings\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(50,1,7,2,'a:4:{s:7:\"publish\";a:13:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:12;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:83;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}}s:4:\"meta\";a:2:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"settings\";a:4:{s:10:\"_tab_label\";s:8:\"Settings\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(51,1,8,2,'a:4:{s:7:\"publish\";a:13:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:12;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:83;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}}s:4:\"meta\";a:2:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"settings\";a:4:{s:10:\"_tab_label\";s:8:\"Settings\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(52,1,2000,9,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:89;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:1:{s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}}'),
	(53,1,1,9,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:89;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:1:{s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}}'),
	(54,1,6,9,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:89;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:1:{s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}}'),
	(55,1,7,9,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:89;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:1:{s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}}'),
	(56,1,8,9,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:89;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:1:{s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}}'),
	(57,1,2006,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:89;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(77,1,2000,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(108,1,1,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:86;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:85;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:72;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(109,1,6,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:86;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:85;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:72;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(110,1,7,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:86;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:85;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:72;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(111,1,8,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:86;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:85;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:72;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(107,1,2007,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:86;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}i:85;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:72;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(87,1,2005,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"50%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(92,1,2002,1,'a:5:{s:7:\"publish\";a:20:{s:10:\"_tab_label\";s:7:\"Publish\";s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"60%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:72;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:87;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}i:89;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"70%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}i:79;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:80;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:82;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:84;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:85;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:86;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:90;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:92;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"30%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";i:81;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"meta\";a:6:{s:10:\"_tab_label\";s:4:\"Meta\";s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:76;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:77;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:78;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:75;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(97,1,2002,8,'a:5:{s:7:\"publish\";a:9:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}i:74;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"80%\";}i:73;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:88;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:91;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"Revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:1:{s:10:\"_tab_label\";s:9:\"Structure\";}}'),
	(98,1,1,8,'a:5:{s:7:\"publish\";a:9:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}i:74;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"80%\";}i:73;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:88;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:91;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"Revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:1:{s:10:\"_tab_label\";s:9:\"Structure\";}}'),
	(99,1,6,8,'a:5:{s:7:\"publish\";a:9:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}i:74;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"80%\";}i:73;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:88;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:91;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"Revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:1:{s:10:\"_tab_label\";s:9:\"Structure\";}}'),
	(100,1,7,8,'a:5:{s:7:\"publish\";a:9:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}i:74;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"80%\";}i:73;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:88;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:91;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"Revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:1:{s:10:\"_tab_label\";s:9:\"Structure\";}}'),
	(101,1,8,8,'a:5:{s:7:\"publish\";a:9:{s:10:\"_tab_label\";s:7:\"Publish\";s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"20%\";}i:74;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"80%\";}i:73;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:88;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:91;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:3:\"40%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"revisions\";a:2:{s:10:\"_tab_label\";s:9:\"Revisions\";s:9:\"revisions\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:1:{s:10:\"_tab_label\";s:9:\"Structure\";}}');

/*!40000 ALTER TABLE `ee2_layout_publish` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_low_reorder_orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_low_reorder_orders`;

CREATE TABLE `ee2_low_reorder_orders` (
  `set_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  `sort_order` text,
  PRIMARY KEY (`set_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_low_reorder_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_low_reorder_sets`;

CREATE TABLE `ee2_low_reorder_sets` (
  `set_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `set_label` varchar(100) NOT NULL,
  `set_notes` text NOT NULL,
  `new_entries` enum('append','prepend') NOT NULL DEFAULT 'append',
  `clear_cache` enum('y','n') NOT NULL DEFAULT 'y',
  `channels` varchar(255) NOT NULL,
  `cat_option` enum('all','some','one') NOT NULL DEFAULT 'all',
  `cat_groups` varchar(255) NOT NULL,
  `parameters` text NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_low_variable_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_low_variable_groups`;

CREATE TABLE `ee2_low_variable_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(6) unsigned NOT NULL,
  `group_label` varchar(100) NOT NULL,
  `group_notes` text NOT NULL,
  `group_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_low_variable_groups` WRITE;
/*!40000 ALTER TABLE `ee2_low_variable_groups` DISABLE KEYS */;

INSERT INTO `ee2_low_variable_groups` (`group_id`, `site_id`, `group_label`, `group_notes`, `group_order`)
VALUES
	(1,1,'global','',0);

/*!40000 ALTER TABLE `ee2_low_variable_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_low_variables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_low_variables`;

CREATE TABLE `ee2_low_variables` (
  `variable_id` int(6) unsigned NOT NULL,
  `group_id` int(6) unsigned NOT NULL,
  `variable_label` varchar(100) NOT NULL,
  `variable_notes` text NOT NULL,
  `variable_type` varchar(50) NOT NULL,
  `variable_settings` text NOT NULL,
  `variable_order` int(4) unsigned NOT NULL,
  `early_parsing` char(1) NOT NULL DEFAULT 'n',
  `is_hidden` char(1) NOT NULL DEFAULT 'n',
  `save_as_file` char(1) NOT NULL DEFAULT 'n',
  `edit_date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_low_variables` WRITE;
/*!40000 ALTER TABLE `ee2_low_variables` DISABLE KEYS */;

INSERT INTO `ee2_low_variables` (`variable_id`, `group_id`, `variable_label`, `variable_notes`, `variable_type`, `variable_settings`, `variable_order`, `early_parsing`, `is_hidden`, `save_as_file`, `edit_date`)
VALUES
	(1,1,'','','low_textarea','',0,'n','n','n',1351620813),
	(2,1,'','','low_textarea','',0,'n','n','n',1351620813),
	(3,1,'','','low_textarea','',0,'n','n','n',1351620813),
	(4,1,'','','low_textarea','',0,'n','n','n',1351620813),
	(5,1,'Admin Toolbar','','pt_switch','YTo1OntzOjk6Im9mZl9sYWJlbCI7czozOiJPRkYiO3M6Nzoib2ZmX3ZhbCI7czowOiIiO3M6ODoib25fbGFiZWwiO3M6MjoiT04iO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czoyOiJvbiI7fQ',5,'y','y','n',1351620813),
	(6,1,'Footer Contact','','matrix','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjI6IjM1IjtpOjE7czoyOiIzNiI7aToyO3M6MjoiMzciO319',6,'y','n','n',1351620813),
	(7,1,'Footer Address','','matrix','YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjY6e2k6MDtzOjI6IjM4IjtpOjE7czoyOiIzOSI7aToyO3M6MjoiNDAiO2k6MztzOjI6IjQxIjtpOjQ7czoyOiI0MiI7aTo1O3M6MjoiNDMiO319',7,'y','n','n',1351620813),
	(8,1,'Cache Driver','','low_select','YTo0OntzOjc6Im9wdGlvbnMiO3M6MTA3OiJmaWxlIDogRmlsZQpkYiA6IERhdGFiYXNlCmFwYyA6IEFQQwpyZWRpcyA6IFJlZGlzCm1lbWNhY2hlIDogbWVtY2FjaGUKbWVtY2FjaGVkIDogbWVtY2FjaGVkCmR1bW15IDogRGlzYWJsZSI7czo5OiJzZXBhcmF0b3IiO3M6NzoibmV3bGluZSI7czoxNToibXVsdGlfaW50ZXJmYWNlIjtzOjY6InNlbGVjdCI7czo4OiJtdWx0aXBsZSI7czowOiIiO30',8,'y','n','n',0);

/*!40000 ALTER TABLE `ee2_low_variables` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_matrix_cols
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_matrix_cols`;

CREATE TABLE `ee2_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_matrix_cols` WRITE;
/*!40000 ALTER TABLE `ee2_matrix_cols` DISABLE KEYS */;

INSERT INTO `ee2_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`)
VALUES
	(1,1,75,NULL,'og_title','Page Title','Will default to entry title','text','n','n',0,'120p','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(2,1,75,NULL,'og_type','Page Type','Will default to website','pt_dropdown','n','n',1,'','YToxOntzOjc6Im9wdGlvbnMiO2E6ODp7czoxMDoiQWN0aXZpdGllcyI7YToyOntzOjg6ImFjdGl2aXR5IjtzOjg6ImFjdGl2aXR5IjtzOjU6InNwb3J0IjtzOjU6InNwb3J0Ijt9czoxMDoiQnVzaW5lc3NlcyI7YTo0OntzOjM6ImJhciI7czozOiJiYXIiO3M6NzoiY29tcGFueSI7czo3OiJjb21wYW55IjtzOjU6ImhvdGVsIjtzOjU6ImhvdGVsIjtzOjEwOiJyZXN0YXVyYW50IjtzOjEwOiJyZXN0YXVyYW50Ijt9czo2OiJHcm91cHMiO2E6Mzp7czo1OiJjYXVzZSI7czo1OiJjYXVzZSI7czoxMzoic3BvcnRzX2xlYWd1ZSI7czoxMzoic3BvcnRzX2xlYWd1ZSI7czoxMToic3BvcnRzX3RlYW0iO3M6MTE6InNwb3J0c190ZWFtIjt9czoxMzoiT3JnYW5pemF0aW9ucyI7YTo1OntzOjQ6ImJhbmQiO3M6NDoiYmFuZCI7czoxMDoiZ292ZXJubWVudCI7czoxMDoiZ292ZXJubWVudCI7czoxMDoibm9uX3Byb2ZpdCI7czoxMDoibm9uX3Byb2ZpdCI7czo2OiJzY2hvb2wiO3M6Njoic2Nob29sIjtzOjEwOiJ1bml2ZXJzaXR5IjtzOjEwOiJ1bml2ZXJzaXR5Ijt9czo2OiJQZW9wbGUiO2E6Nzp7czo1OiJhY3RvciI7czo1OiJhY3RvciI7czo3OiJhdGhsZXRlIjtzOjc6ImF0aGxldGUiO3M6NjoiYXV0aG9yIjtzOjY6ImF1dGhvciI7czo4OiJkaXJlY3RvciI7czo4OiJkaXJlY3RvciI7czo4OiJtdXNpY2lhbiI7czo4OiJtdXNpY2lhbiI7czoxMDoicG9saXRpY2lhbiI7czoxMDoicG9saXRpY2lhbiI7czoxMzoicHVibGljX2ZpZ3VyZSI7czoxMzoicHVibGljX2ZpZ3VyZSI7fXM6NjoiUGxhY2VzIjthOjQ6e3M6NDoiY2l0eSI7czo0OiJjaXR5IjtzOjc6ImNvdW50cnkiO3M6NzoiY291bnRyeSI7czo4OiJsYW5kbWFyayI7czo4OiJsYW5kbWFyayI7czoxNDoic3RhdGVfcHJvdmluY2UiO3M6MTQ6InN0YXRlX3Byb3ZpbmNlIjt9czoyNjoiUHJvZHVjdHMgYW5kIEVudGVydGFpbm1lbnQiO2E6OTp7czo1OiJhbGJ1bSI7czo1OiJhbGJ1bSI7czo0OiJib29rIjtzOjQ6ImJvb2siO3M6NToiZHJpbmsiO3M6NToiZHJpbmsiO3M6NDoiZm9vZCI7czo0OiJmb29kIjtzOjQ6ImdhbWUiO3M6NDoiZ2FtZSI7czo3OiJwcm9kdWN0IjtzOjc6InByb2R1Y3QiO3M6NDoic29uZyI7czo0OiJzb25nIjtzOjU6Im1vdmllIjtzOjU6Im1vdmllIjtzOjc6InR2X3Nob3ciO3M6NzoidHZfc2hvdyI7fXM6NzoiV2Vic2l0ZSI7YTozOntzOjQ6ImJsb2ciO3M6NDoiYmxvZyI7czo3OiJ3ZWJzaXRlIjtzOjc6IndlYnNpdGUiO3M6NzoiYXJ0aWNsZSI7czo3OiJhcnRpY2xlIjt9fX0='),
	(3,1,75,NULL,'og_url','Page URL','Will default to current page url','text','n','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(4,1,75,NULL,'og_image','Page Image','Will default to website logo','file','n','n',3,'','YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czo1OiJpbWFnZSI7fQ=='),
	(5,1,75,NULL,'og_description','Page Description','Short blurb about this page','text','n','n',4,'','YTozOntzOjQ6Im1heGwiO3M6MzoiMjAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(6,1,78,NULL,'metakeywords_keyword','Meta Keywords','','text','n','n',0,'33%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(7,1,84,NULL,'getinvolved_active','Active','','pt_switch','n','n',0,'120p','YTo1OntzOjk6Im9mZl9sYWJlbCI7czozOiJPRkYiO3M6Nzoib2ZmX3ZhbCI7czowOiIiO3M6ODoib25fbGFiZWwiO3M6MjoiT04iO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czoyOiJvbiI7fQ=='),
	(8,1,84,NULL,'getinvolved_title','Title','','text','n','n',1,'160p','YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(9,1,84,NULL,'getinvolved_image','Image','','file','n','n',2,'','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI2IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30='),
	(10,1,84,NULL,'getinvolved_summary','Preview Text','','wygwam','n','n',3,'','YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6InkiO30='),
	(11,1,84,NULL,'getinvolved_link','Page Link','','structure','n','n',4,'','YTowOnt9'),
	(12,1,85,NULL,'carousel_active','Active','Toggle On/Off','pt_switch','n','y',0,'120p','YTo1OntzOjk6Im9mZl9sYWJlbCI7czozOiJPRkYiO3M6Nzoib2ZmX3ZhbCI7czowOiIiO3M6ODoib25fbGFiZWwiO3M6MjoiT04iO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czoyOiJvbiI7fQ=='),
	(13,1,85,NULL,'carousel_image','Image','automatically resized to fit','file','y','n',1,'','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI3IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9'),
	(14,1,85,NULL,'carousel_alt','Alternate Text','for usability and SEO purposes','text','y','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(15,1,74,NULL,'news_images_preview','Preview Image','shown on non-article pages','file','n','y',0,'200p','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIzIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30='),
	(16,1,74,NULL,'news_images_feature','Feature Image','shown on article page','file','n','y',1,'200p','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI4IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30='),
	(23,1,89,NULL,'relatedcontent_active','Active','Toggle On/Off','pt_switch','n','n',0,'120p','YTo1OntzOjk6Im9mZl9sYWJlbCI7czozOiJPRkYiO3M6Nzoib2ZmX3ZhbCI7czowOiIiO3M6ODoib25fbGFiZWwiO3M6MjoiT04iO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czoyOiJvbiI7fQ=='),
	(24,1,89,NULL,'relatedcontent_callout','Callout Selection','<a href=\"/manage.php?D=cp&C=content_publish&M=entry_form&channel_id=9\" target=\"_blank\" style=\"text-decoration:underline;\">Click here to create a new callout</a> (in a new window)','playa','n','n',1,'','YTo3OntzOjU6Im11bHRpIjtzOjE6Im4iO3M6NzoiZXhwaXJlZCI7czoxOiJ5IjtzOjY6ImZ1dHVyZSI7czoxOiJ5IjtzOjg6ImNoYW5uZWxzIjthOjE6e2k6MDtzOjE6IjkiO31zOjg6InN0YXR1c2VzIjthOjE6e2k6MDtzOjQ6Im9wZW4iO31zOjc6Im9yZGVyYnkiO3M6NToidGl0bGUiO3M6NDoic29ydCI7czozOiJBU0MiO30='),
	(19,1,90,NULL,'callout_image','Callout Image','Overrides all other fields to display a single image','file','n','n',0,'200p','YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI5IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30='),
	(20,1,90,NULL,'callout_previewimage','Preview Image','','file','n','n',1,'','YToyOntzOjk6ImRpcmVjdG9yeSI7czoyOiIxMCI7czoxMjoiY29udGVudF90eXBlIjtzOjU6ImltYWdlIjt9'),
	(21,1,90,NULL,'callout_linktext','Link Text','Displayed as a link to this page','text','n','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(22,1,90,NULL,'callout_linkoverride','Link to different page','Overrides the link to this page','structure','n','n',3,'','YTowOnt9'),
	(25,1,91,NULL,'newsrelatedcontent_active','Active','Toggle On/Off','pt_switch','n','n',0,'120p','YTo1OntzOjk6Im9mZl9sYWJlbCI7czoyOiJOTyI7czo3OiJvZmZfdmFsIjtzOjA6IiI7czo4OiJvbl9sYWJlbCI7czozOiJZRVMiO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czoyOiJvbiI7fQ=='),
	(26,1,91,NULL,'newsrelatedcontent_callout','Callout Selection','<a href=\"/manage.php?D=cp&C=content_publish&M=entry_form&channel_id=9\" target=\"_blank\" style=\"text-decoration:underline;\">Click here to create a new callout</a> (in a new window)','playa','n','n',1,'','YTo3OntzOjU6Im11bHRpIjtzOjE6Im4iO3M6NzoiZXhwaXJlZCI7czoxOiJ5IjtzOjY6ImZ1dHVyZSI7czoxOiJ5IjtzOjg6ImNoYW5uZWxzIjthOjE6e2k6MDtzOjE6IjkiO31zOjg6InN0YXR1c2VzIjthOjE6e2k6MDtzOjQ6Im9wZW4iO31zOjc6Im9yZGVyYnkiO3M6NToidGl0bGUiO3M6NDoic29ydCI7czozOiJBU0MiO30='),
	(27,1,74,NULL,'news_images_previewalt','Preview Alt Text','for accessibility and SEO purposes','text','n','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(28,1,74,NULL,'news_images_featurealt','Feature Alt Text','for accessibility and SEO purposes','text','n','n',3,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(29,1,92,NULL,'form_active','Active','Toggle On/Off','pt_switch','n','n',0,'120p','YTo1OntzOjk6Im9mZl9sYWJlbCI7czozOiJPRkYiO3M6Nzoib2ZmX3ZhbCI7czowOiIiO3M6ODoib25fbGFiZWwiO3M6MjoiT04iO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czozOiJvZmYiO30='),
	(30,1,92,NULL,'form_required','Required?','','pt_switch','n','n',1,'120p','YTo1OntzOjk6Im9mZl9sYWJlbCI7czoyOiJOTyI7czo3OiJvZmZfdmFsIjtzOjA6IiI7czo4OiJvbl9sYWJlbCI7czozOiJZRVMiO3M6Njoib25fdmFsIjtzOjE6InkiO3M6NzoiZGVmYXVsdCI7czozOiJvZmYiO30='),
	(31,1,92,NULL,'form_field','Field Name','','low_freeform_field','n','n',3,'','YTowOnt9'),
	(32,1,92,NULL,'form_options','Dropdown Options','Only applies to dropdowns and multiselects','pt_list','n','n',5,'','YTowOnt9'),
	(33,1,92,NULL,'form_type','Field Type','','pt_dropdown','n','n',4,'','YToxOntzOjc6Im9wdGlvbnMiO2E6NDp7czo0OiJ0ZXh0IjtzOjQ6InRleHQiO3M6ODoidGV4dGFyZWEiO3M6ODoidGV4dGFyZWEiO3M6ODoiZHJvcGRvd24iO3M6ODoiZHJvcGRvd24iO3M6MTE6Im11bHRpc2VsZWN0IjtzOjExOiJtdWx0aXNlbGVjdCI7fX0='),
	(34,1,92,NULL,'form_label','Field Label','','text','n','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(35,1,NULL,6,'contactinfo_url','Website URL','','text','n','n',0,'','YTozOntzOjQ6Im1heGwiO3M6MzoiMjUwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(36,1,NULL,6,'contactinfo_email','Contact Email','','text','n','n',1,'','YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(37,1,NULL,6,'contactinfo_phone','Contact Phone','','text','n','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MjoiNTAiO3M6MzoiZm10IjtzOjQ6Im5vbmUiO3M6NzoiY29udGVudCI7czozOiJhbGwiO30='),
	(38,1,NULL,7,'addressinfo_name','Organization Name','','text','n','n',0,'33%','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(39,1,NULL,7,'addressinfo_street','Street Address','','text','n','n',1,'','YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(40,1,NULL,7,'addressinfo_city','City','','text','n','n',2,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(41,1,NULL,7,'addressinfo_state','State','','text','n','n',3,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(42,1,NULL,7,'addressinfo_postal',' Postal Code','','text','n','n',4,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
	(43,1,NULL,7,'addressinfo_country','Country','','text','n','n',5,'','YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9');

/*!40000 ALTER TABLE `ee2_matrix_cols` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_matrix_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_matrix_data`;

CREATE TABLE `ee2_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  `col_id_14` text,
  `col_id_15` text,
  `col_id_16` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  `col_id_24` text,
  `col_id_25` text,
  `col_id_26` text,
  `col_id_27` text,
  `col_id_28` text,
  `col_id_29` text,
  `col_id_30` text,
  `col_id_31` text,
  `col_id_32` text,
  `col_id_33` text,
  `col_id_34` text,
  `col_id_35` text,
  `col_id_36` text,
  `col_id_37` text,
  `col_id_38` text,
  `col_id_39` text,
  `col_id_40` text,
  `col_id_41` text,
  `col_id_42` text,
  `col_id_43` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_matrix_data` WRITE;
/*!40000 ALTER TABLE `ee2_matrix_data` DISABLE KEYS */;

INSERT INTO `ee2_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `row_order`, `col_id_1`, `col_id_2`, `col_id_3`, `col_id_4`, `col_id_5`, `col_id_6`, `col_id_7`, `col_id_8`, `col_id_9`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`, `col_id_14`, `col_id_15`, `col_id_16`, `col_id_19`, `col_id_20`, `col_id_21`, `col_id_22`, `col_id_23`, `col_id_24`, `col_id_25`, `col_id_26`, `col_id_27`, `col_id_28`, `col_id_29`, `col_id_30`, `col_id_31`, `col_id_32`, `col_id_33`, `col_id_34`, `col_id_35`, `col_id_36`, `col_id_37`, `col_id_38`, `col_id_39`, `col_id_40`, `col_id_41`, `col_id_42`, `col_id_43`)
VALUES
	(1,1,4,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,1,3,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,1,5,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,1,6,75,NULL,1,'','activity','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,1,7,75,NULL,1,'','activity','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,1,1,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,1,2,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,1,1,84,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'y','Get Involved!','{filedir_6}get-involved-couple.jpg','<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam justo nunc, sagittis et placerat nec, feugiat in lectus. Aenean venenatis erat ut sapien iaculis pulvinar. Nullam mauris libero, vulputate quis varius auctor, eleifend in odio.</p>\n','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,1,1,85,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','{filedir_7}slider-volunteer.jpeg','Volunteer Slide',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,1,1,85,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','{filedir_7}fpo-library.jpeg','Library slide',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,1,1,85,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','{filedir_7}fpo-donation-1.jpeg','Donation slide.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,1,11,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,1,12,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,1,13,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,1,14,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,1,15,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(17,1,16,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(18,1,16,85,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','{filedir_7}fpo-library.jpeg','Library Group',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(19,1,17,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(20,1,18,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(21,1,19,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(22,1,20,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(23,1,21,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,1,22,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(25,1,23,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(26,1,9,74,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_3}home-1.jpeg','{filedir_8}img_event-fpo.jpeg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,1,8,74,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_3}home.jpeg','{filedir_8}img_event-fpo.jpeg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(28,1,10,74,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_3}fpo-library-1.jpeg','{filedir_8}img_event-fpo.jpeg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(29,1,24,74,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_3}home-1.jpeg','{filedir_8}img_event-fpo.jpeg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(30,1,25,74,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_3}home.jpeg','{filedir_8}img_event-fpo.jpeg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(31,1,26,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(32,1,27,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(33,1,27,90,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_9}news.jpeg',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(34,1,2,89,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[27] [imagine-no-malaria] Imagine No Malaria',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(35,1,28,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(36,1,28,90,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_10}news-1.jpeg','View Event','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(37,1,2,89,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[28] [donate-to-japan] Donate to Japan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(38,1,5,89,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[28] [donate-to-japan] Donate to Japan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(39,1,29,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(40,1,29,90,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{filedir_10}news-detail.jpeg','Sign Up','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(41,1,9,91,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[29] [prefer-print] Prefer Print?',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(42,1,9,91,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[28] [donate-to-japan] Donate to Japan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(43,1,9,91,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[27] [imagine-no-malaria] Imagine No Malaria',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(44,1,8,91,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[27] [imagine-no-malaria] Imagine No Malaria',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(45,1,8,91,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','[28] [donate-to-japan] Donate to Japan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(47,1,4,92,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','y','interests','Backend Development\nFrontend Development\nMVC Frameworks\nLESS CSS\nBackbone.js\nCodeIgniter Framework\nKohana\nZend Framework 2','dropdown','Program of Interest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(48,1,4,92,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','y','name','','text','Your Name',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(49,1,4,92,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','y','email','','text','Your Email',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(50,1,4,92,NULL,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'y','y','comments','','textarea','Questions/Comments',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(51,1,30,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(52,1,31,75,NULL,1,'','activity','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(53,1,NULL,NULL,6,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'www.minnesotoutreach.org','info@minnesotaoutreach.org','(612) 870-0458',NULL,NULL,NULL,NULL,NULL,NULL),
	(54,1,NULL,NULL,7,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Minnesota Outreach','122 West Grand Avenue, Suite 200','St. Paul','MN','55104','United States');

/*!40000 ALTER TABLE `ee2_matrix_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_member_bulletin_board
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_member_bulletin_board`;

CREATE TABLE `ee2_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_member_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_member_data`;

CREATE TABLE `ee2_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  `m_field_id_1` varchar(100) DEFAULT NULL,
  `m_field_id_3` varchar(100) DEFAULT NULL,
  `m_field_id_4` varchar(100) DEFAULT NULL,
  `m_field_id_5` varchar(100) DEFAULT NULL,
  `m_field_id_6` varchar(100) DEFAULT NULL,
  `m_field_id_7` varchar(100) DEFAULT NULL,
  `m_field_id_8` varchar(100) DEFAULT NULL,
  `m_field_id_9` varchar(100) DEFAULT NULL,
  `m_field_id_10` varchar(100) DEFAULT NULL,
  `m_field_id_11` varchar(100) DEFAULT NULL,
  `m_field_id_12` varchar(100) DEFAULT NULL,
  `m_field_id_13` varchar(100) DEFAULT NULL,
  `m_field_id_15` varchar(100) DEFAULT NULL,
  `m_field_id_16` varchar(100) DEFAULT NULL,
  `m_field_id_17` varchar(100) DEFAULT NULL,
  `m_field_id_18` varchar(100) DEFAULT NULL,
  `m_field_id_19` varchar(100) DEFAULT NULL,
  `m_field_id_20` varchar(100) DEFAULT NULL,
  `m_field_id_21` varchar(100) DEFAULT NULL,
  `m_field_id_22` varchar(100) DEFAULT NULL,
  `m_field_id_23` varchar(100) DEFAULT NULL,
  `m_field_id_24` varchar(100) DEFAULT NULL,
  `m_field_id_25` varchar(100) DEFAULT NULL,
  `m_field_id_26` varchar(100) DEFAULT NULL,
  `m_field_id_27` varchar(100) DEFAULT NULL,
  `m_field_id_28` varchar(100) DEFAULT NULL,
  `m_field_id_29` varchar(100) DEFAULT NULL,
  `m_field_id_30` varchar(100) DEFAULT NULL,
  `m_field_id_31` varchar(100) DEFAULT NULL,
  `m_field_id_32` varchar(100) DEFAULT NULL,
  `m_field_id_33` varchar(100) DEFAULT NULL,
  `m_field_id_34` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_member_data` WRITE;
/*!40000 ALTER TABLE `ee2_member_data` DISABLE KEYS */;

INSERT INTO `ee2_member_data` (`member_id`, `m_field_id_1`, `m_field_id_3`, `m_field_id_4`, `m_field_id_5`, `m_field_id_6`, `m_field_id_7`, `m_field_id_8`, `m_field_id_9`, `m_field_id_10`, `m_field_id_11`, `m_field_id_12`, `m_field_id_13`, `m_field_id_15`, `m_field_id_16`, `m_field_id_17`, `m_field_id_18`, `m_field_id_19`, `m_field_id_20`, `m_field_id_21`, `m_field_id_22`, `m_field_id_23`, `m_field_id_24`, `m_field_id_25`, `m_field_id_26`, `m_field_id_27`, `m_field_id_28`, `m_field_id_29`, `m_field_id_30`, `m_field_id_31`, `m_field_id_32`, `m_field_id_33`, `m_field_id_34`)
VALUES
	(1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'Jarrett Barnett','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),
	(8,'The Nerdery','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `ee2_member_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_member_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_member_fields`;

CREATE TABLE `ee2_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_member_fields` WRITE;
/*!40000 ALTER TABLE `ee2_member_fields` DISABLE KEYS */;

INSERT INTO `ee2_member_fields` (`m_field_id`, `m_field_name`, `m_field_label`, `m_field_description`, `m_field_type`, `m_field_list_items`, `m_field_ta_rows`, `m_field_maxl`, `m_field_width`, `m_field_search`, `m_field_required`, `m_field_public`, `m_field_reg`, `m_field_cp_reg`, `m_field_fmt`, `m_field_order`)
VALUES
	(1,'name','Name','','text','',10,100,'100%','y','n','n','y','y','none',1),
	(3,'address','Address','','text','',10,100,'100%','y','n','n','y','y','none',3),
	(4,'address2','Address 2','','text','',10,100,'100%','y','n','n','y','y','none',4),
	(5,'city','City','','text','',10,100,'100%','y','n','n','y','y','none',5),
	(6,'state','State','','text','',10,100,'100%','y','n','n','y','y','none',6),
	(7,'zip','Zipcode','','text','',10,100,'100%','y','n','n','y','y','none',7),
	(8,'country','Country','','text','',10,100,'100%','y','n','n','y','y','none',8),
	(9,'country_code','Country Code','','text','',10,100,'100%','y','n','n','y','y','none',9),
	(10,'company','Company','','text','',10,100,'100%','y','n','n','y','y','none',10),
	(11,'phone','Phone','','text','',10,100,'100%','y','n','n','y','y','none',11),
	(12,'use_billing_info','Billing Is Shipping','','text','',10,100,'100%','y','n','n','y','y','none',12),
	(13,'shipping_name','Shipping | Name','','text','',10,100,'100%','y','n','n','y','y','none',13),
	(26,'shipping_phone','Shipping | Phone','','text','',10,100,'100%','y','n','n','y','y','none',23),
	(15,'shipping_address','Shipping | Address','','text','',10,100,'100%','y','n','n','y','y','none',15),
	(16,'shipping_address2','Shipping | Address 2','','text','',10,100,'100%','y','n','n','y','y','none',16),
	(17,'shipping_city','Shipping | City','','text','',10,100,'100%','y','n','n','y','y','none',17),
	(18,'shipping_state','Shipping | State','','text','',10,100,'100%','y','n','n','y','y','none',18),
	(19,'shipping_zip','Shipping | Zipcode','','text','',10,100,'100%','y','n','n','y','y','none',19),
	(20,'shipping_country','Shipping | Country','','text','',10,100,'100%','y','n','n','y','y','none',20),
	(21,'shipping_country_code','Shipping | Country Code','','text','',10,100,'100%','y','n','n','y','y','none',21),
	(22,'shipping_company','Shipping | Company','','text','',10,100,'100%','y','n','n','y','y','none',22),
	(23,'language','Language','','text','',10,100,'100%','y','n','n','y','y','none',99),
	(24,'shipping_option','Shipping | Option','','text','',10,100,'100%','y','n','n','y','y','none',98),
	(25,'region','Billing | Region','','text','',10,100,'100%','y','n','n','y','y','none',10),
	(27,'address3','Billing | Address 3','','text','',10,100,'100%','y','n','n','y','y','none',5),
	(28,'shipping_address3','Shipping | Address 3','','text','',10,100,'100%','y','n','n','y','y','none',17),
	(29,'shipping_region','Shipping | Region','','text','',10,100,'100%','y','n','n','y','y','none',23),
	(30,'order_custom1','Custom 1','','text','',10,100,'100%','y','n','n','n','n','none',28),
	(31,'order_custom2','Custom 2','','text','',10,100,'100%','y','n','n','n','n','none',29),
	(32,'order_custom3','Custom 3','','text','',10,100,'100%','y','n','n','n','n','none',30),
	(33,'order_custom4','Custom 4','','text','',10,100,'100%','y','n','n','n','n','none',31),
	(34,'order_custom5','Custom 5','','text','',10,100,'100%','y','n','n','n','n','none',32);

/*!40000 ALTER TABLE `ee2_member_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_member_groups`;

CREATE TABLE `ee2_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_member_groups` WRITE;
/*!40000 ALTER TABLE `ee2_member_groups` DISABLE KEYS */;

INSERT INTO `ee2_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`)
VALUES
	(1,1,'Super Admins','','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','','y','y','y',0,'y',20,60,'y','y','y','y','y'),
	(2,1,'Banned','','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','','n','n','n',60,'n',20,60,'n','n','n','n','n'),
	(3,1,'Guests','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),
	(4,1,'Pending','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),
	(5,1,'Members','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','n','','y','n','y',10,'y',20,60,'y','n','n','y','y'),
	(6,1,'Administrators','','n','y','y','y','y','y','y','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','y','y','n','n','n','y','y','y','y','y','y','y','y','y','y','y','y','y','n','n','n','n','n','y','n','y','','y','y','y',1,'n',20,60,'n','n','y','n','n'),
	(7,1,'Publishers','','n','y','y','y','y','y','y','y','n','n','n','n','n','n','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','y','y','y','y','y','y','y','y','y','y','y','y','y','n','n','n','n','n','y','n','y','','y','y','y',1,'n',20,60,'n','n','y','y','y'),
	(8,1,'Editors','','n','y','y','y','y','y','y','y','n','n','n','n','n','n','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','y','y','y','y','y','y','y','y','y','y','n','n','n','n','n','y','n','y','','y','y','y',1,'n',20,60,'n','n','y','y','y');

/*!40000 ALTER TABLE `ee2_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_member_homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_member_homepage`;

CREATE TABLE `ee2_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_member_homepage` WRITE;
/*!40000 ALTER TABLE `ee2_member_homepage` DISABLE KEYS */;

INSERT INTO `ee2_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`)
VALUES
	(1,'l',1,'l',2,'n',0,'r',1,'n',0,'r',2,'r',0,'l',0),
	(2,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),
	(8,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0);

/*!40000 ALTER TABLE `ee2_member_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_member_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_member_search`;

CREATE TABLE `ee2_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_members
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_members`;

CREATE TABLE `ee2_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_members` WRITE;
/*!40000 ALTER TABLE `ee2_members` DISABLE KEYS */;

INSERT INTO `ee2_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`)
VALUES
	(1,1,'admin','Administrator','f22c9db466a5123bb2e8512d13192aa8462c2d68ebb16f50b35aebd3095914499965e6e2f7ffbc1010ecc12bba6a6e6c5d1a5b92b29825d78b902349a6ea17c2','Rb?<[fwv!wSK=4t9zCl1z0h1ErRoQeZr/e|5-n6hkGz&yRKVO*\"M\']q(#xZ6iBf2#`:Nvg%^:)\">7,(t&)mk?QRY?\"@d$>VqdE[reCH\"u;FF*5R@|2c_5Eun\\eX}=5)A','263287ec932d4e406db5a94dcf5a73f3679c043c','29ff73c7317274f85b33e9f552121017a4f0becb',NULL,'developer@jarrettbarnett.com','http://www.jarrettbarnett.com','Chico, CA','Admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1324428627,1324490527,1324512095,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UM8','y','n','us','jarrettbarnett',NULL,NULL,NULL,'20',NULL,'18','Support Request|http://nerdery.com/contact|1\nThe Nerdery|http://nerdery.com/|2\nGoogle Analytics|http://analytics.google.com|3\nGoogle Mail|http://www.gmail.com/|4\nMailChimp|http://www.mailchimp.com/|5\nConstantContact|http://www.constantcontact.com/|6\nSalesForce|http://www.salesforce.com/|7','Settings|manage.php?S=95afb2c416d7ec0c887664d2e2d854b2b77bbf35&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=low_variables|1','y',0),
	(2,1,'jarrettbarnett','Jarrett Barnett','09c90e4e8fa49893ed4747924101ea11c678490d305fb6e511db0f89c83265ce56fa56ee6aeb6923dc8d84b98cef703fe5d716077a711ff42674d0d2fa826240','HKG@.4XF;Ih{b1twF,{Yo:\\,0M-K`96(w<|L1tY@4S*xx}#uZ!FD23Y/6P|(eqDg{%T-g<a;bi_dl=u`ylvMm:]XNMcQApE=kPkD^Fad\'MZxN8g\\<J!\\D<AQs9/DYzsy','0fcc13eb2d86c6f75620333c202bd18be64c738f','58464b9f11aedcde8e345976cb1b6aacbb260bde',NULL,'me@jarrettbarnett.com','http://www.jarrettbarnett.com','Chico, CA','Lead Developer','',16,1,1986,'jmbelite','','','','',NULL,'uploads/avatar_2.jpg',500,500,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1324490669,1351793037,1351802933,31,4,0,0,1351594165,1351585193,0,0,'n','y','y','y','y','y','y','y','y','english','UM8','y','n','us','jarrettbarnett',NULL,NULL,NULL,'20',NULL,'18','Support Request|http://nerdery.com/contact|1\nThe Nerdery|http://nerdery.com/|2\nGoogle Analytics|http://analytics.google.com|3\nGoogle Mail|http://www.gmail.com/|4\nMailChimp|http://www.mailchimp.com/|5\nConstantContact|http://www.constantcontact.com/|6\nSalesForce|http://www.salesforce.com/|7','Settings|manage.php?S=95afb2c416d7ec0c887664d2e2d854b2b77bbf35&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=low_variables|1','n',0),
	(8,1,'nerdery','The Nerdery','444f50e55cc2c7935e0d8322942b17dea88ee01f8afe2fb1328f9f37d759fc6fac7084932a6ed8816e3c362c61e62fe89bbd1e5be6d646aa76d8fabdc7d2c688','FBZ|\'NJq\\\\2?FZe)%*eqMD]U*rLI*Cm{^._eTdU)@9\\WfD-qvZ(cN0w7Wj~i!MC^VTE_#.>?[9[|?-EjkY75kxch/)aGp7.n@:^Ke)!8[TZO\'\'$=pM5KTJwy]FJ@#%R^','a9abedab5bea5ab8025d7bb96ae96b8d9df5a301','a1f2608fddbdf3d015ae3aeed98f4106bacba1f0',NULL,'nerdery@jarrettbarnett.com','http://nerdery.com','','','',NULL,NULL,NULL,'','','','','',NULL,'uploads/avatar_8.png',128,128,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'127.0.0.1',1351609608,1351624651,1351627762,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UM8','y','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18','Support Request|http://nerdery.com/contact|1\nThe Nerdery|http://nerdery.com/|2\nGoogle Analytics|http://analytics.google.com|3\nGoogle Mail|http://www.gmail.com/|4\nMailChimp|http://www.mailchimp.com/|5\nConstantContact|http://www.constantcontact.com/|6\nSalesForce|http://www.salesforce.com/|7','Settings|manage.php?S=95afb2c416d7ec0c887664d2e2d854b2b77bbf35&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=low_variables|1','y',0);

/*!40000 ALTER TABLE `ee2_members` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_message_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_message_attachments`;

CREATE TABLE `ee2_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_message_copies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_message_copies`;

CREATE TABLE `ee2_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_message_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_message_data`;

CREATE TABLE `ee2_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_message_folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_message_folders`;

CREATE TABLE `ee2_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_message_folders` WRITE;
/*!40000 ALTER TABLE `ee2_message_folders` DISABLE KEYS */;

INSERT INTO `ee2_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`)
VALUES
	(2,'InBox','Sent','','','','','','','',''),
	(8,'InBox','Sent','','','','','','','','');

/*!40000 ALTER TABLE `ee2_message_folders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_message_listed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_message_listed`;

CREATE TABLE `ee2_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_minicp_preferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_minicp_preferences`;

CREATE TABLE `ee2_minicp_preferences` (
  `pref_key` varchar(250) NOT NULL DEFAULT '',
  `pref_value` text,
  PRIMARY KEY (`pref_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_minicp_toolbars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_minicp_toolbars`;

CREATE TABLE `ee2_minicp_toolbars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `enabled` int(1) DEFAULT NULL,
  `left_links` text,
  `right_links` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_minicp_toolbars` WRITE;
/*!40000 ALTER TABLE `ee2_minicp_toolbars` DISABLE KEYS */;

INSERT INTO `ee2_minicp_toolbars` (`id`, `user_id`, `enabled`, `left_links`, `right_links`)
VALUES
	(1,2,1,'0','1'),
	(2,3,1,'0','1');

/*!40000 ALTER TABLE `ee2_minicp_toolbars` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_module_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_module_member_groups`;

CREATE TABLE `ee2_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_module_member_groups` WRITE;
/*!40000 ALTER TABLE `ee2_module_member_groups` DISABLE KEYS */;

INSERT INTO `ee2_module_member_groups` (`group_id`, `module_id`)
VALUES
	(6,19),
	(7,19),
	(8,19);

/*!40000 ALTER TABLE `ee2_module_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_modules`;

CREATE TABLE `ee2_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  `settings` text,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_modules` WRITE;
/*!40000 ALTER TABLE `ee2_modules` DISABLE KEYS */;

INSERT INTO `ee2_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`, `settings`)
VALUES
	(1,'Comment','2.3','y','n',NULL),
	(2,'Email','2.0','n','n',NULL),
	(3,'Emoticon','2.0','n','n',NULL),
	(4,'Jquery','1.0','n','n',NULL),
	(5,'Rss','2.0','n','n',NULL),
	(6,'Safecracker','2.1','y','n',NULL),
	(7,'Search','2.2','n','n',NULL),
	(8,'Channel','2.0.1','n','n',NULL),
	(9,'Member','2.1','n','n',NULL),
	(10,'Stats','2.0','n','n',NULL),
	(11,'File','1.0.0','n','n',NULL),
	(25,'Low_variables','2.3.0','y','n',NULL),
	(19,'Structure','3.3.3','y','y',NULL),
	(14,'Wygwam','2.5','y','n',NULL),
	(16,'Automin','2.0','y','n',NULL),
	(17,'Playa','4.1.0.3','n','n',NULL),
	(23,'Query','2.0','n','n',NULL),
	(20,'Mountee','2.2','n','n',NULL),
	(21,'Minicp','1.5','y','n',NULL),
	(22,'Republic_analytics','2.2.2','y','n',NULL),
	(24,'Store','1.5.3.1','y','n',NULL),
	(26,'Authenticate','1.1.1','n','n',NULL),
	(27,'Zoo_flexible_admin','1.4','y','n',NULL),
	(28,'Low_reorder','2.0.2','y','n',NULL),
	(29,'Stash','2.3.2','n','n',NULL),
	(30,'Freeform','3.1.4','y','n',NULL);

/*!40000 ALTER TABLE `ee2_modules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_online_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_online_users`;

CREATE TABLE `ee2_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_password_lockout
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_password_lockout`;

CREATE TABLE `ee2_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_password_lockout` WRITE;
/*!40000 ALTER TABLE `ee2_password_lockout` DISABLE KEYS */;

INSERT INTO `ee2_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`)
VALUES
	(20,1351369962,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4','jarrett.barnett'),
	(17,1351209186,'127.0.0.1','Mountee v36','jarrett.barnett'),
	(18,1351209187,'127.0.0.1','Mountee v3','jarrett.barnett'),
	(19,1351209196,'127.0.0.1','Mountee v36','jarrettbarnett'),
	(21,1351482733,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4','jarrett.barnett'),
	(22,1351789895,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4','jarrettbarnett');

/*!40000 ALTER TABLE `ee2_password_lockout` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_ping_servers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_ping_servers`;

CREATE TABLE `ee2_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_playa_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_playa_relationships`;

CREATE TABLE `ee2_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_playa_relationships` WRITE;
/*!40000 ALTER TABLE `ee2_playa_relationships` DISABLE KEYS */;

INSERT INTO `ee2_playa_relationships` (`rel_id`, `parent_entry_id`, `parent_field_id`, `parent_col_id`, `parent_row_id`, `child_entry_id`, `rel_order`)
VALUES
	(5,1,86,NULL,NULL,10,0),
	(7,2,89,24,34,27,0),
	(8,2,89,24,37,28,0),
	(26,5,89,24,38,28,0),
	(16,9,91,26,41,29,0),
	(17,9,91,26,42,28,0),
	(18,9,91,26,43,27,0),
	(22,8,91,26,44,27,0),
	(23,8,91,26,45,28,0);

/*!40000 ALTER TABLE `ee2_playa_relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_relationships`;

CREATE TABLE `ee2_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_remember_me
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_remember_me`;

CREATE TABLE `ee2_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_republic_analytics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_republic_analytics`;

CREATE TABLE `ee2_republic_analytics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned DEFAULT NULL,
  `settings` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_republic_analytics` WRITE;
/*!40000 ALTER TABLE `ee2_republic_analytics` DISABLE KEYS */;

INSERT INTO `ee2_republic_analytics` (`id`, `site_id`, `settings`)
VALUES
	(1,1,'YTozNDp7czoxNzoicmVkaXJlY3Rfb25fbG9naW4iO3M6MToieSI7czozMDoibWVtYmVyX2dyb3VwX3JlZGlyZWN0X29uX2xvZ2luIjthOjE6e2k6MDtzOjE6IjEiO31zOjIyOiJvdmVycmlkZV9ob21lcGFnZV9pY29uIjtzOjE6InkiO3M6MjI6Im92ZXJyaWRlX2hvbWVwYWdlX3BhZ2UiO3M6MToieSI7czoxMjoiYWRkb25fYWNjZXNzIjthOjA6e31zOjEwOiJncmFwaF90eXBlIjtzOjM6ImJhciI7czoxMjoidmlzaXRzX2NvbG9yIjtzOjc6IiNhYmJhYjIiO3M6MTQ6InZpc2l0b3JzX2NvbG9yIjtzOjc6IiM4OTlhOTIiO3M6MTY6InBhZ2VzX3ZpZXdfY29sb3IiO3M6NzoiI2M5ZDNjZSI7czoxMjoiZ29vZ2xlX3Rva2VuIjtzOjQ1OiIxL3dueDlwVXZfaV9ycjZfN0pNalQ2UnNrMVlpNVIzNFJCYzN1eGtzLUxKeGsiO3M6MTI6ImFjY2Vzc190b2tlbiI7czo1MzoieWEyOS5BSEVTNlpUZGlsNVRSYkN2dHZCYV9McGV2NHl3LXo5b1ItbU9adVFCbXRrOGZ1ZE0iO3M6MjM6ImFjY2Vzc190b2tlbl9leHBpcmVzX2luIjtpOjM2MDA7czoyMDoiYWNjZXNzX3Rva2VuX2NyZWF0ZWQiO2k6MTM1MTgwMDM0NztzOjk6ImNsaWVudF9pZCI7czozOToiMjA4MTE5Nzc1Mzc5LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIjtzOjEzOiJjbGllbnRfc2VjcmV0IjtzOjI0OiJvTnNzNGpscE9UWmZJZHgzUWdDOU1sbmUiO3M6MTI6InJlZGlyZWN0X3VybCI7czoxNDA6Imh0dHAlM0ElMkYlMkZzY2htby5wcm8lMkZtYW5hZ2UucGhwJTNGRCUzRGNwJTI2QyUzRGFkZG9uc19tb2R1bGVzJTI2TSUzRHNob3dfbW9kdWxlX2NwJTI2bW9kdWxlJTNEcmVwdWJsaWNfYW5hbHl0aWNzJTI2bWV0aG9kJTNEYXV0aGVudGljYXRlIjtzOjE0OiJnb29nbGVfYWNjb3VudCI7YToyOntzOjEwOiJwcm9maWxlX2lkIjtzOjg6IjY1MDI2NzIzIjtzOjEyOiJwcm9maWxlX25hbWUiO3M6MTg6ImphcnJldHRiYXJuZXR0LmNvbSI7fXM6NToiY2FjaGUiO2E6Mzp7aTo2NTAyNjcyMzthOjQ6e3M6MTg6InNvdXJjZXNfc3RhdGlzdGljcyI7YToxMDA6e3M6NjoiZ29vZ2xlIjthOjM6e3M6NjoidmlzaXRzIjtzOjU6IjE5ODQ0IjtzOjc6ImJvdW5jZXMiO3M6NToiMTA3MTUiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjEyNi43ODE3NDc2MzE1MjU5Ijt9czo4OiIoZGlyZWN0KSI7YTozOntzOjY6InZpc2l0cyI7czo0OiI3MzgzIjtzOjc6ImJvdW5jZXMiO3M6NDoiMzk3MyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTk5LjE4MjAzOTgyMTIxMDg4Ijt9czoxMjoiZmFjZWJvb2suY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjM6Ijc5MCI7czo3OiJib3VuY2VzIjtzOjM6IjM5NCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTUyLjY1NTY5NjIwMjUzMTY2Ijt9czo0OiJiaW5nIjthOjM6e3M6NjoidmlzaXRzIjtzOjM6Ijc0NiI7czo3OiJib3VuY2VzIjtzOjM6IjI5NCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMjE2LjMwNjk3MDUwOTM4MzM4Ijt9czo1OiJ5YWhvbyI7YTozOntzOjY6InZpc2l0cyI7czozOiI3MTgiO3M6NzoiYm91bmNlcyI7czozOiIyODMiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjE2NS41NTE1MzIwMzM0MjYxOCI7fXM6MTA6Imdvb2dsZS5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MzoiNDg0IjtzOjc6ImJvdW5jZXMiO3M6MzoiMjg1IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE3OiI2MC40NDgzNDcxMDc0MzgwMiI7fXM6MjA6InRoaXNhbWVyaWNhbmxpZmUub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjM6IjMwNCI7czo3OiJib3VuY2VzIjtzOjM6IjI1NCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiMzIuMjUzMjg5NDczNjg0MjEiO31zOjE1OiJtb3RoZXJqb25lcy5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MzoiMjc5IjtzOjc6ImJvdW5jZXMiO3M6MzoiMTM4IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxNDAuMTc1NjI3MjQwMTQzMzciO31zOjE2OiJlbi53aWtpcGVkaWEub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjM6IjIyNSI7czo3OiJib3VuY2VzIjtzOjM6IjE3NiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNDcuMTc3Nzc3Nzc3Nzc3NzgiO31zOjEzOiJwaW50ZXJlc3QuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjM6IjE0NiI7czo3OiJib3VuY2VzIjtzOjI6Ijk4IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE2OiI3OC4wOTU4OTA0MTA5NTg5Ijt9czozOiJhb2wiO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MzoiMTQzIjtzOjc6ImJvdW5jZXMiO3M6MjoiNzUiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjE3Ni4zMDA2OTkzMDA2OTkzIjt9czoxNDoibS5mYWNlYm9vay5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MzoiMTQxIjtzOjc6ImJvdW5jZXMiO3M6MjoiNjQiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTY6IjE3OS42NDUzOTAwNzA5MjIiO31zOjIxOiJkYXZpZHNlZGFyaXNib29rcy5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MzoiMTM0IjtzOjc6ImJvdW5jZXMiO3M6MzoiMTEwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE3OiIzMy4xNzE2NDE3OTEwNDQ3OCI7fXM6OToiaG91enouY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjM6IjEzNCI7czo3OiJib3VuY2VzIjtzOjI6IjYwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIyNzIuNjc5MTA0NDc3NjExOTMiO31zOjI5OiJDb3B5IG9mIEZhbGwgSG9tZSBTYWxlIC0gMjAxMiI7YTozOntzOjY6InZpc2l0cyI7czoyOiI5MyI7czo3OiJib3VuY2VzIjtzOjI6IjMwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxNTQuMDY0NTE2MTI5MDMyMjYiO31zOjIyOiJtb2JpLmJhcmNsYXlhZ2VuY3kuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjkyIjtzOjc6ImJvdW5jZXMiO3M6MjoiMzAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjQ1LjgzNjk1NjUyMTczOTEzIjt9czozOiJhc2siO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiODAiO3M6NzoiYm91bmNlcyI7czoyOiIzOSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czo2OiIxODMuOTUiO31zOjIwOiJjaS52aWN0b3J2aWxsZS5jYS51cyI7YTozOntzOjY6InZpc2l0cyI7czoyOiI3OSI7czo3OiJib3VuY2VzIjtzOjI6IjE1IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIyOTcuNjcwODg2MDc1OTQ5MzQiO31zOjIwOiJsYWRlc2lnbmNvbmNlcHRzLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiI3NCI7czo3OiJib3VuY2VzIjtzOjI6IjYwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIyMTMuMDEzNTEzNTEzNTEzNTIiO31zOjEyOiJsaW5rZWRpbi5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiNzMiO3M6NzoiYm91bmNlcyI7czoyOiI0NyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNjoiODMuNDkzMTUwNjg0OTMxNSI7fXM6MjI6IlN1bW1lciAxMiBOZXcgUHJvZHVjdHMiO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiNjkiO3M6NzoiYm91bmNlcyI7czoxOiI4IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxMTcyLjk3MTAxNDQ5Mjc1MzUiO31zOjIwOiJuYW50dWNrZXRob21laW5jLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiI2OSI7czo3OiJib3VuY2VzIjtzOjI6IjMyIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE2OiIxMzYuNzY4MTE1OTQyMDI5Ijt9czo3OiJjb21jYXN0IjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjYwIjtzOjc6ImJvdW5jZXMiO3M6MjoiMjIiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjE5NS44ODMzMzMzMzMzMzMzMyI7fXM6MTM6ImFiZHV6ZWVkby5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiNTQiO3M6NzoiYm91bmNlcyI7czoyOiIzMCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMzQ5Ljg1MTg1MTg1MTg1MTg1Ijt9czozNjoiMzZvaGs2ZGdtY2Qxbi1jLmMueW9tLm1haWwueWFob28ubmV0IjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjUzIjtzOjc6ImJvdW5jZXMiO3M6MjoiMjAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjI3Mi40OTA1NjYwMzc3MzU4NSI7fXM6NzoiYmFieWxvbiI7YTozOntzOjY6InZpc2l0cyI7czoyOiI1MyI7czo3OiJib3VuY2VzIjtzOjI6IjM1IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxMjMuMjA3NTQ3MTY5ODExMzIiO31zOjIwOiJwb2V0cnlmb3VuZGF0aW9uLm9yZyI7YTozOntzOjY6InZpc2l0cyI7czoyOiI1MyI7czo3OiJib3VuY2VzIjtzOjI6IjQ0IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiI0OC44Njc5MjQ1MjgzMDE4ODQiO31zOjE2OiJ0aGV0dXNjYW5zdW4uY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjUzIjtzOjc6ImJvdW5jZXMiO3M6MjoiNDQiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjE5LjQ1MjgzMDE4ODY3OTI0NyI7fXM6MTM6ImFuY2hlZW1pbi5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiNTIiO3M6NzoiYm91bmNlcyI7czoyOiIzMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMjI5LjI2OTIzMDc2OTIzMDc3Ijt9czoyMjoibGFkb2xmaW5hLmJsb2dzcG90LmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiI1MiI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjE4My44NjUzODQ2MTUzODQ2Ijt9czoyNDoicHJvbW9zaHEud2lsZGZpcmVhcHAuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjUyIjtzOjc6ImJvdW5jZXMiO3M6MjoiNDEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjEzLjIxMTUzODQ2MTUzODQ2MiI7fXM6MzoiYXZnIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjQ4IjtzOjc6ImJvdW5jZXMiO3M6MjoiMTQiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjE1Ny43MDgzMzMzMzMzMzMzNCI7fXM6Mzc6InNmZGVzaWduY2VudGVyLmRlc2lnbmNlbnRlcnNlYXJjaC5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiNDciO3M6NzoiYm91bmNlcyI7czoyOiIzNiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNjYuNTc0NDY4MDg1MTA2MzkiO31zOjIyOiJzZWFyY2gubXl3ZWJzZWFyY2guY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjQzIjtzOjc6ImJvdW5jZXMiO3M6MjoiMTgiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjIzMS40ODgzNzIwOTMwMjMyNiI7fXM6MjE6ImluZGlnZW5vdXNkZXNpZ25zLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiI0MiI7czo3OiJib3VuY2VzIjtzOjI6IjE0IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE3OiIzMDQuNjY2NjY2NjY2NjY2NyI7fXM6MTQ6InNlYXJjaC1yZXN1bHRzIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjM5IjtzOjc6ImJvdW5jZXMiO3M6MjoiMjIiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjIxMC44NDYxNTM4NDYxNTM4NCI7fXM6MjM6ImN1cmF0b3Jzb2ZsaWZlc3R5bGUuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjM1IjtzOjc6ImJvdW5jZXMiO3M6MToiOSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNTI5LjAyODU3MTQyODU3MTUiO31zOjE2OiJ3aGl0ZWFwcmljb3QuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjM0IjtzOjc6ImJvdW5jZXMiO3M6MToiNyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiMjU5LjgyMzUyOTQxMTc2NDciO31zOjEzOiJjdWx0aXZhdGUuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjMyIjtzOjc6ImJvdW5jZXMiO3M6MjoiMjQiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NzoiNzMuODEyNSI7fXM6MTQ6ImxpZWRjZW50ZXIub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjMxIjtzOjc6ImJvdW5jZXMiO3M6MjoiMjciO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjMxLjkzNTQ4Mzg3MDk2Nzc0Ijt9czoxMjoiZ29vZ2xlLmNvLnVrIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjMwIjtzOjc6ImJvdW5jZXMiO3M6MjoiMjIiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjIwLjgzMzMzMzMzMzMzMzMzMiI7fXM6MjA6ImludGVyaW9yc2RpZ2l0YWwuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjI5IjtzOjc6ImJvdW5jZXMiO3M6MjoiMjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjM5MS40ODI3NTg2MjA2ODk2NSI7fXM6MTg6InNmZGVzaWduY2VudGVyLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIyOSI7czo3OiJib3VuY2VzIjtzOjI6IjI2IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxNTMuMjQxMzc5MzEwMzQ0ODMiO31zOjk6Imdvb2dsZS5jYSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIyOCI7czo3OiJib3VuY2VzIjtzOjI6IjE3IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjQ6IjQyLjUiO31zOjE1OiJzdHVtYmxldXBvbi5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjciO3M6NzoiYm91bmNlcyI7czoyOiIyMiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTkuMzMzMzMzMzMzMzMzMzMyIjt9czoxNjoiVmVydGljYWxSZXNwb25zZSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIyNiI7czo3OiJib3VuY2VzIjtzOjI6IjE4IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxMy4zNDYxNTM4NDYxNTM4NDciO31zOjc6Imh1ZC5nb3YiO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjQiO3M6NzoiYm91bmNlcyI7czoyOiIxNCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czo3OiIxMDkuMTI1Ijt9czoyMToibS50dXNjYW5yaWRnZWNsdWIuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjI0IjtzOjc6ImJvdW5jZXMiO3M6MToiNCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMjM0LjY2NjY2NjY2NjY2NjY2Ijt9czoxODoidGhlbGFsb25kZWdyb3VwLmNhIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjI0IjtzOjc6ImJvdW5jZXMiO3M6MToiNyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNTE2LjgzMzMzMzMzMzMzMzQiO31zOjQ6InQuY28iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjMiO3M6NzoiYm91bmNlcyI7czoyOiIxNSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMjguNjk1NjUyMTczOTEzMDQzIjt9czoyMDoiYXBhcnRtZW50dGhlcmFweS5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjIiO3M6NzoiYm91bmNlcyI7czoxOiI2IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE3OiI4Ny42ODE4MTgxODE4MTgxOSI7fXM6MjU6InlvdWNhbnRraWxsdGhlcm9vc3Rlci5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjIiO3M6NzoiYm91bmNlcyI7czoyOiIxOCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTEuMDQ1NDU0NTQ1NDU0NTQ1Ijt9czoyMDoiRmFsbCAxMiBOZXcgUHJvZHVjdHMiO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjEiO3M6NzoiYm91bmNlcyI7czoxOiI2IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjU6IjQwOS4wIjt9czoxNjoiZ3JlZW5hbWVyaWNhLm9yZyI7YTozOntzOjY6InZpc2l0cyI7czoyOiIyMSI7czo3OiJib3VuY2VzIjtzOjE6IjIiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6Ijk5LjcxNDI4NTcxNDI4NTcxIjt9czoxNjoiY29udGVtcG9yaXN0LmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIyMCI7czo3OiJib3VuY2VzIjtzOjI6IjEyIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjU6IjE0My40Ijt9czoyMDoibGlhaWdyZWZ1cm5pdHVyZS5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMjAiO3M6NzoiYm91bmNlcyI7czoyOiIxOCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czo0OiI3LjA1Ijt9czo3OiJzdm4ub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjIwIjtzOjc6ImJvdW5jZXMiO3M6MToiOCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czo1OiIxMTcuOSI7fXM6MTI6Imdvb2dsZS5jby5pbiI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxOSI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjE4MC42ODQyMTA1MjYzMTU3OCI7fXM6MjE6ImluZGlnZW5vdXNkZXNpZ25zLm9yZyI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxOSI7czo3OiJib3VuY2VzIjtzOjE6IjMiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6Ijg5LjEwNTI2MzE1Nzg5NDc0Ijt9czo3OiJsb2MuZ292IjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjE5IjtzOjc6ImJvdW5jZXMiO3M6MjoiMTUiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTY6IjU1Ljg0MjEwNTI2MzE1NzkiO31zOjIxOiJmcmVlZG9tYW5kZmFzaGlvbi5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTgiO3M6NzoiYm91bmNlcyI7czoxOiI2IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxNTguOTQ0NDQ0NDQ0NDQ0NDYiO31zOjE0OiJtYWdpY3NwYWNlLm5ldCI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxOCI7czo3OiJib3VuY2VzIjtzOjI6IjEyIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiI1MC4xNjY2NjY2NjY2NjY2NjQiO31zOjE4OiJtZWRpYXNoYXJlc3BvdC5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTgiO3M6NzoiYm91bmNlcyI7czoyOiIxNSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTQyLjcyMjIyMjIyMjIyMjIzIjt9czoxMDoiYW1hem9uLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxNyI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjYxLjc2NDcwNTg4MjM1Mjk0Ijt9czoxNToibWlyYW5kYWp1bHkuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjE3IjtzOjc6ImJvdW5jZXMiO3M6MjoiMTAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjEwLjQxMTc2NDcwNTg4MjM1MyI7fXM6MjA6ImdhbGJyYWl0aGFuZHBhdWwuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjE2IjtzOjc6ImJvdW5jZXMiO3M6MjoiMTMiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NDoiMjEuMCI7fXM6MTQ6Imx1bWluYXJpdW0ub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjE2IjtzOjc6ImJvdW5jZXMiO3M6MjoiMTUiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NjoiNDEuODc1Ijt9czo4OiJya3ByLm5ldCI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxNiI7czo3OiJib3VuY2VzIjtzOjE6IjQiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NzoiNTIzLjEyNSI7fXM6MzA6ImFkcmlhbm5hbmRqYXNvbi5vdXJ3ZWRkaW5nLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxNSI7czo3OiJib3VuY2VzIjtzOjE6IjQiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NDoiOTEuMCI7fXM6MTE6ImRzaGJsb2cuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjE1IjtzOjc6ImJvdW5jZXMiO3M6MToiOCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNTI5LjQ2NjY2NjY2NjY2NjciO31zOjExOiJlY29tYWxsLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxNSI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NToiMTM5LjIiO31zOjE4OiJmYi53aWxkZmlyZWFwcC5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTUiO3M6NzoiYm91bmNlcyI7czoxOiI4IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiI1Ny42NjY2NjY2NjY2NjY2NjQiO31zOjE3OiJncm9jZXJ5c21hcnRzLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxNSI7czo3OiJib3VuY2VzIjtzOjE6IjkiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6Ijc1LjkzMzMzMzMzMzMzMzM0Ijt9czoxNjoibGVjdHVyZWFnZW50LmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxNSI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjE1My4zMzMzMzMzMzMzMzMzNCI7fXM6ODoicm9tby5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTUiO3M6NzoiYm91bmNlcyI7czoyOiIxMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNTQuOTMzMzMzMzMzMzMzMzMiO31zOjIxOiJ1cHRvd250aGVhdHJlbmFwYS5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTUiO3M6NzoiYm91bmNlcyI7czoxOiI5IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxNi42NjY2NjY2NjY2NjY2NjgiO31zOjIxOiJzYW5kc3RvbmVjb25jZXJ0cy5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTQiO3M6NzoiYm91bmNlcyI7czoyOiIxNCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czozOiIwLjAiO31zOjIyOiJzdGF0aWMuYWsuZmFjZWJvb2suY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjE0IjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiODY5LjcxNDI4NTcxNDI4NTciO31zOjE4OiJ2aWN0b3J2YWxsZXljYS5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTQiO3M6NzoiYm91bmNlcyI7czoxOiI3IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxMjMuMzU3MTQyODU3MTQyODYiO31zOjE0OiJpbmRpZ2Vub3VzLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxMyI7czo3OiJib3VuY2VzIjtzOjE6IjkiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjEyMC4zODQ2MTUzODQ2MTUzOSI7fXM6MTc6Im1pY2hhZWxjaGFib24uY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjEzIjtzOjc6ImJvdW5jZXMiO3M6MToiNiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czo0OiIyOS4wIjt9czoxMDoicmVkZGl0LmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxMyI7czo3OiJib3VuY2VzIjtzOjI6IjEwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIzLjQ2MTUzODQ2MTUzODQ2MTciO31zOjg6InllbHAuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjEzIjtzOjc6ImJvdW5jZXMiO3M6MToiMiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiMTM4LjM4NDYxNTM4NDYxNTQiO31zOjI3OiJjbGlwbXVuay5zb3V0aGVybnNhdmVycy5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTIiO3M6NzoiYm91bmNlcyI7czoxOiI3IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiI0OS44MzMzMzMzMzMzMzMzMzYiO31zOjg6ImNvb2MuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjEyIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTA3LjU4MzMzMzMzMzMzMzMzIjt9czoyMToibWFyeW9saXZlci5iZWFjb24ub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjEyIjtzOjc6ImJvdW5jZXMiO3M6MToiMiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiMTQ2LjE2NjY2NjY2NjY2NjY2Ijt9czoxNzoib3J2aWxsZXNjaGVsbC5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTIiO3M6NzoiYm91bmNlcyI7czoxOiI1IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjQ6IjczLjUiO31zOjE1OiJ0eXBlYXBhcmVudC5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTIiO3M6NzoiYm91bmNlcyI7czoxOiI1IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE3OiIzNTguMTY2NjY2NjY2NjY2NyI7fXM6Mzg6IkNvcHkgb2YgRmFsbCBIb21lIFNhbGUvVXBncmFkZWQgTGl2aW5nIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjExIjtzOjc6ImJvdW5jZXMiO3M6MToiNCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxODoiNDI2LjcyNzI3MjcyNzI3Mjc1Ijt9czoxODoiYWlhbW9udGVyZXliYXkub3JnIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjExIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiMjg3LjE4MTgxODE4MTgxODIiO31zOjE5OiJhcm1pc3RlYWRtYXVwaW4uY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjExIjtzOjc6ImJvdW5jZXMiO3M6MToiNiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNDAuMTgxODE4MTgxODE4MTgiO31zOjE2OiJibGluZGZvbGRtYWcuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjExIjtzOjc6ImJvdW5jZXMiO3M6MToiMyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiNzYyLjM2MzYzNjM2MzYzNjQiO31zOjE0OiJibG9nLnh1aXRlLm5ldCI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxMSI7czo3OiJib3VuY2VzIjtzOjE6IjUiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NToiMTg0LjAiO31zOjIzOiJib2FyZHMuc3RyYWlnaHRkb3BlLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxMSI7czo3OiJib3VuY2VzIjtzOjI6IjExIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjM6IjAuMCI7fXM6MTA6ImRwYWNuYy5jb20iO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTEiO3M6NzoiYm91bmNlcyI7czoxOiI4IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIyMC44MTgxODE4MTgxODE4MTciO31zOjk6ImVDb21tZXJjZSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTc6IjYyMS45MDkwOTA5MDkwOTA5Ijt9czoxOToiZWNvZmFzaGlvbndvcmxkLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIxMSI7czo3OiJib3VuY2VzIjtzOjE6IjIiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjI0NC41NDU0NTQ1NDU0NTQ1MyI7fXM6MTA6ImZlZWRidXJuZXIiO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTEiO3M6NzoiYm91bmNlcyI7czoxOiI1IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE3OiI1Ny42MzYzNjM2MzYzNjM2MyI7fXM6MTM6Imdvb2dsZS5jb20uYXUiO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MjoiMTEiO3M6NzoiYm91bmNlcyI7czoxOiI0IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIxNy41NDU0NTQ1NDU0NTQ1NDciO31zOjE2OiJydW5uZXJzd29ybGQuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjExIjtzOjc6ImJvdW5jZXMiO3M6MToiMyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czoxNzoiMzIuNzI3MjcyNzI3MjcyNzMiO319czoxMjoibGFzdF91cGRhdGVkIjtpOjEzNTE4MDI2MzU7czoxNToicGFnZV9zdGF0aXN0aWNzIjthOjgyOntpOjE7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMzoiL3NlZGFyaXMuaHRtbCI7czo5OiJwYWdlVGl0bGUiO3M6NDI6IkRhdmlkIFNlZGFyaXMgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6NDoiMzIyNyI7czo3OiJib3VuY2VzIjtzOjQ6IjIxMDkiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjY3Ljk0NzcwMjA2MDIyMTg3Ijt9aToyO2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoid3d3LmluZGlnZW5vdXMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE6Ii8iO3M6OToicGFnZVRpdGxlIjtzOjczOiJJbmRpZ2Vub3VzIERlc2lnbnMgfCBGYWlyIFRyYWRlIENsb3RoaW5nIHwgRWNvIEZhc2hpb24gfCBPcmdhbmljIENsb3RoaW5nIjtzOjY6InZpc2l0cyI7aToxNTAzOTtzOjc6ImJvdW5jZXMiO2k6NTM2MTtzOjEzOiJhdmdUaW1lT25QYWdlIjtkOjEwMS40MDk2NTUyNjQzOTk5MTYwNjM2NjY3NTExNDI1OTEyMzgwMjE4NTA1ODU5Mzc1O31pOjM7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjA6Ii9zaG9wL2NhdGVnb3J5L3dvbWVuIjtzOjk6InBhZ2VUaXRsZSI7czo4MzoiV29tZW4ncyA+IEluZGlnZW5vdXMgRGVzaWducyB8IEZhaXIgVHJhZGUgQ2xvdGhpbmcgfCBFY28gRmFzaGlvbiB8IE9yZ2FuaWMgQ2xvdGhpbmciO3M6NjoidmlzaXRzIjtpOjE3NTQ7czo3OiJib3VuY2VzIjtpOjM2MDtzOjEzOiJhdmdUaW1lT25QYWdlIjtkOjUyLjM3NzIxODY4MjIyNzc0MTkwODI2Mzg1MTY1NTY0NzE1ODYyMjc0MTY5OTIxODc1O31pOjQ7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czozNDoiL3NwZWFrZXJzL2FwcGVhcmFuY2VzL3NlZGFyaXMuaHRtbCI7czo5OiJwYWdlVGl0bGUiO3M6MjE6IlN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6NDoiMTIxNCI7czo3OiJib3VuY2VzIjtzOjM6IjM4NSI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxODoiMTE4LjQxNDk2NTk4NjM5NDU2Ijt9aTo1O2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoid3d3LmluZGlnZW5vdXMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEyOiIvc2hvcC93b21lbnMiO3M6OToicGFnZVRpdGxlIjtzOjEyNzoiV29tZW4ncyBvcmdhbmljIGNsb3RoaW5nIGZvciBhIGNvbnNjaW91cyBlY28gbGlmZXN0eWxlLiBTdXBwb3J0IGZhaXIgdHJhZGUgY2xvdGhpbmcsIG9yZ2FuaWMgY290dG9uIGFuZCBuYXR1cmFsIGFscGFjYSBmYXNoaW9uLiI7czo2OiJ2aXNpdHMiO2k6MTc4MTtzOjc6ImJvdW5jZXMiO2k6MTEwO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO2Q6NTYuMjcxMTk2NTMwNTMxNjg5NDYxNDQwNjkxOTU3MjUwMjM3NDY0OTA0Nzg1MTU2MjU7fWk6NjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjI1OiIvY2F0ZWdvcnkvY3ljbGluZy1qZXdlbHJ5IjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiOTMwIjtzOjc6ImJvdW5jZXMiO3M6MzoiMzE4IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIzMi4xMDc0MzgwMTY1Mjg5MjQiO31pOjc7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI1OiJ3d3cub3ZlcmxhbmRlcXVpcG1lbnQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjU6Ii9zaG9wIjtzOjk6InBhZ2VUaXRsZSI7czozOToiT3ZlcmxhbmQgRXF1aXBtZW50IC0gU2ltcGx5IEhvbmVzdCBCYWdzIjtzOjY6InZpc2l0cyI7aToxMTQ5O3M6NzoiYm91bmNlcyI7aTo0MDtzOjEzOiJhdmdUaW1lT25QYWdlIjtkOjQ4LjIxNDkxNDUwODIzOTY2NTczNTgwMDM3MjIwMzgxMjAwMzEzNTY4MTE1MjM0Mzc1O31pOjg7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMjoiL2xhbW90dC5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czo0MDoiQW5uZSBMYW1vdHQgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6MzoiNzI1IjtzOjc6ImJvdW5jZXMiO3M6MzoiNDczIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxMDMuNjgwODUxMDYzODI5NzkiO31pOjk7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTA6Ii9zaG9wL3NhbGUiO3M6OToicGFnZVRpdGxlIjtzOjgwOiJTYWxlID4gSW5kaWdlbm91cyBEZXNpZ25zIHwgRmFpciBUcmFkZSBDbG90aGluZyB8IEVjbyBGYXNoaW9uIHwgT3JnYW5pYyBDbG90aGluZyI7czo2OiJ2aXNpdHMiO2k6ODEwO3M6NzoiYm91bmNlcyI7aToxNDtzOjEzOiJhdmdUaW1lT25QYWdlIjtkOjM4LjgzOTQ3OTEyMDgzODQ0MDQ1MTI3OTg5Mzc1MDMyNDg0NTMxNDAyNTg3ODkwNjI1O31pOjEwO2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjQ6Ii9jYXRlZ29yeS9oaWtpbmctamV3ZWxyeSI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjU3NiI7czo3OiJib3VuY2VzIjtzOjI6IjEyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIyOS40MTA1NzkzNDUwODgxNjMiO31pOjExO2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTk6Ii9wYWdlcy9jb250YWN0Lmh0bWwiO3M6OToicGFnZVRpdGxlIjtzOjIxOiJTdGV2ZW4gQmFyY2xheSBBZ2VuY3kiO3M6NjoidmlzaXRzIjtzOjM6IjU2NyI7czo3OiJib3VuY2VzIjtzOjI6IjQzIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI4OC40Mzc4MjM4MzQxOTY4OSI7fWk6MTI7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoyNToiL2NhdGVnb3J5L3J1bm5pbmctamV3ZWxyeSI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjU2NCI7czo3OiJib3VuY2VzIjtzOjI6IjM4IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIyNS4wMTAzMjQ0ODM3NzU4MSI7fWk6MTM7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxODoiL2NhdGVnb3J5L21vdW50YWluIjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiNTUxIjtzOjc6ImJvdW5jZXMiO3M6MToiMiI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiMjMuMjE5NTEyMTk1MTIxOTUiO31pOjE0O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTY6Ii9jYXRlZ29yeS9uYXR1cmUiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiI1NDYiO3M6NzoiYm91bmNlcyI7czoxOiI4IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIyNi44MzQxNzcyMTUxODk4NyI7fWk6MTU7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTU6Ii9jb21wYW55L2ZpYmVycyI7czo5OiJwYWdlVGl0bGUiO3M6ODI6IkZpYmVycyA+IEluZGlnZW5vdXMgRGVzaWducyB8IEZhaXIgVHJhZGUgQ2xvdGhpbmcgfCBFY28gRmFzaGlvbiB8IE9yZ2FuaWMgQ2xvdGhpbmciO3M6NjoidmlzaXRzIjtzOjM6IjUwNSI7czo3OiJib3VuY2VzIjtzOjM6IjE4MyI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiNzYuMTkzMTQ2NDE3NDQ1NDgiO31pOjE2O2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6MzI6Ii9zcGVha2Vycy9hcHBlYXJhbmNlcy9nbGFzcy5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czoyMToiU3RldmVuIEJhcmNsYXkgQWdlbmN5IjtzOjY6InZpc2l0cyI7czozOiI0OTYiO3M6NzoiYm91bmNlcyI7czozOiIzNDEiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjE1My4zNTgyMDg5NTUyMjM5Ijt9aToxNzthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE2OiIvY2F0ZWdvcnkvY2hhcm1zIjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiNDkxIjtzOjc6ImJvdW5jZXMiO3M6MjoiMTkiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjM1LjEzNzc3MDg5NzgzMjgxNiI7fWk6MTg7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxNjoiL2NhdGVnb3J5L3NwaXJpdCI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjQ4OSI7czo3OiJib3VuY2VzIjtzOjE6IjQiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjI1Ljk4Nzc0ODg1MTQ1NDgyNSI7fWk6MTk7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTc6Ii9jb21wYW55L2FydGlzYW5zIjtzOjk6InBhZ2VUaXRsZSI7czo4NDoiQXJ0aXNhbnMgPiBJbmRpZ2Vub3VzIERlc2lnbnMgfCBGYWlyIFRyYWRlIENsb3RoaW5nIHwgRWNvIEZhc2hpb24gfCBPcmdhbmljIENsb3RoaW5nIjtzOjY6InZpc2l0cyI7czozOiI0ODciO3M6NzoiYm91bmNlcyI7czoyOiI2OSI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiNjEuMjY3MzI2NzMyNjczMjciO31pOjIwO2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTY6Ii9jYXRlZ29yeS9hY3RpdmUiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiI0ODAiO3M6NzoiYm91bmNlcyI7czoxOiI2IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIyMS45Njg0NzYzNTcyNjc5NSI7fWk6MjE7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE5OiJ3d3cubW9vbmV5ZmFybXMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjk6Ii9wcm9kdWN0cyI7czo5OiJwYWdlVGl0bGUiO3M6MjU6IkJlbGxhIFN1biBMdWNpIHwgUHJvZHVjdHMiO3M6NjoidmlzaXRzIjtzOjM6IjQ3OCI7czo3OiJib3VuY2VzIjtzOjI6IjUyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI5OS4wMDI3MzIyNDA0MzcxNSI7fWk6MjI7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI1OiJ3d3cub3ZlcmxhbmRlcXVpcG1lbnQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE0OiIvc2hvcC9zaG91bGRlciI7czo5OiJwYWdlVGl0bGUiO3M6Mzk6Ik92ZXJsYW5kIEVxdWlwbWVudCAtIFNpbXBseSBIb25lc3QgQmFncyI7czo2OiJ2aXNpdHMiO3M6MzoiNDcyIjtzOjc6ImJvdW5jZXMiO3M6MjoiMTQiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjM4LjM3MTgyMjAzMzg5ODMwNCI7fWk6MjM7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxNjoiL2NhdGVnb3J5L3N0b25lcyI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjQxNSI7czo3OiJib3VuY2VzIjtzOjE6IjQiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjMzLjY4MzAyNjU4NDg2NzA4Ijt9aToyNDthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTk6Ind3dy5tb29uZXlmYXJtcy5jb20iO3M6ODoicGFnZVBhdGgiO3M6ODoiL3JlY2lwZXMiO3M6OToicGFnZVRpdGxlIjtzOjI0OiJCZWxsYSBTdW4gTHVjaSB8IFJlY2lwZXMiO3M6NjoidmlzaXRzIjtzOjM6IjQwOCI7czo3OiJib3VuY2VzIjtzOjI6IjIwIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjg6IjM5LjE1MTI1Ijt9aToyNTthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjI3OiIvY2F0ZWdvcnkvQXBwYWxhY2hpYW4tVHJhaWwiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiI0MDEiO3M6NzoiYm91bmNlcyI7czoyOiI1NSI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxODoiMjcuNzk1MzM2Nzg3NTY0NzY4Ijt9aToyNjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE1OiIvY2F0ZWdvcnkvY2Fpcm4iO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiI0MDEiO3M6NzoiYm91bmNlcyI7czoyOiIyNCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxODoiMzAuMDgyNDk0OTY5ODE4OTE0Ijt9aToyNzthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6Ind3dy5pbmRpZ2Vub3VzLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxOToiL3Nob3AvY2F0ZWdvcnkvc2FsZSI7czo5OiJwYWdlVGl0bGUiO3M6ODA6IlNhbGUgPiBJbmRpZ2Vub3VzIERlc2lnbnMgfCBGYWlyIFRyYWRlIENsb3RoaW5nIHwgRWNvIEZhc2hpb24gfCBPcmdhbmljIENsb3RoaW5nIjtzOjY6InZpc2l0cyI7czozOiIzNzAiO3M6NzoiYm91bmNlcyI7czoxOiI5IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIzOS43Mzc4MDQ4NzgwNDg3OCI7fWk6Mjg7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoyMToiL2NhdGVnb3J5L3dlZWtseS1kZWFsIjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiMzY3IjtzOjc6ImJvdW5jZXMiO3M6MjoiMTAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjI1Ljc2NjEyOTAzMjI1ODA2NCI7fWk6Mjk7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTg6Ii9zaG9wL2NhdGVnb3J5L21lbiI7czo5OiJwYWdlVGl0bGUiO3M6ODE6Ik1lbidzID4gSW5kaWdlbm91cyBEZXNpZ25zIHwgRmFpciBUcmFkZSBDbG90aGluZyB8IEVjbyBGYXNoaW9uIHwgT3JnYW5pYyBDbG90aGluZyI7czo2OiJ2aXNpdHMiO3M6MzoiMzYxIjtzOjc6ImJvdW5jZXMiO3M6MjoiNjUiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTY6IjM1LjY1MzA2MTIyNDQ4OTgiO31pOjMwO2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoid3d3LmluZGlnZW5vdXMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjk6Ii9zaG9wL21lbiI7czo5OiJwYWdlVGl0bGUiO3M6ODM6IkVjbyBGYXNoaW9uIGNsb3RoaW5nIGZvciBtZW4gaXMgb3JnYW5pYywgRmFpciBUcmFkZSwgc3VzdGFpbmFibGUsIGFuZCBlY28tZnJpZW5kbHkuIjtzOjY6InZpc2l0cyI7czozOiIzNTkiO3M6NzoiYm91bmNlcyI7czoyOiIyOCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiMzIuODQ1OTAxNjM5MzQ0MjYiO31pOjMxO2E6Njp7czo4OiJob3N0bmFtZSI7czoyNToid3d3Lm92ZXJsYW5kZXF1aXBtZW50LmNvbSI7czo4OiJwYWdlUGF0aCI7czo5OiIvc2hvcC9uZXciO3M6OToicGFnZVRpdGxlIjtzOjM5OiJPdmVybGFuZCBFcXVpcG1lbnQgLSBTaW1wbHkgSG9uZXN0IEJhZ3MiO3M6NjoidmlzaXRzIjtzOjM6IjM1OSI7czo3OiJib3VuY2VzIjtzOjI6IjI2IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIzOC4yOTc2OTM5MjAzMzU0MjYiO31pOjMyO2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoid3d3LmluZGlnZW5vdXMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEwOiIvd2hvbGVzYWxlIjtzOjk6InBhZ2VUaXRsZSI7czo4NToiV2hvbGVzYWxlID4gSW5kaWdlbm91cyBEZXNpZ25zIHwgRmFpciBUcmFkZSBDbG90aGluZyB8IEVjbyBGYXNoaW9uIHwgT3JnYW5pYyBDbG90aGluZyI7czo2OiJ2aXNpdHMiO3M6MzoiMzQ5IjtzOjc6ImJvdW5jZXMiO3M6MjoiMjciO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjU1LjAxNTA2MDI0MDk2Mzg1NSI7fWk6MzM7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI0OiJ3d3cubmFudHVja2V0aG9tZWluYy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjA6Ii9pbnRlcmlvcnMvcG9ydGZvbGlvIjtzOjk6InBhZ2VUaXRsZSI7czozMjoiTmFudHVja2V0IEhvbWUsIEluYy4gfCBQb3J0Zm9saW8iO3M6NjoidmlzaXRzIjtzOjM6IjM0OCI7czo3OiJib3VuY2VzIjtzOjI6IjgyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiI0MC4zMzkyODU3MTQyODU3MTUiO31pOjM0O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTc6Ii9jYXRlZ29yeS9vbi1zYWxlIjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiMzQwIjtzOjc6ImJvdW5jZXMiO3M6MToiMyI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiMjIuNTU3NTQ0NzU3MDMzMjUiO31pOjM1O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTY6Ii9jYXRlZ29yeS9jb3BwZXIiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIzMzAiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIxOC4zNjI2MzczNjI2MzczNiI7fWk6MzY7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czoyMDoiL3BhZ2VzL2Fib3V0LXVzLmh0bWwiO3M6OToicGFnZVRpdGxlIjtzOjIxOiJTdGV2ZW4gQmFyY2xheSBBZ2VuY3kiO3M6NjoidmlzaXRzIjtzOjM6IjMxNSI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjYzLjU0NDM1NDgzODcwOTY4Ijt9aTozNzthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEyOiIvY2F0ZWdvcnkvb20iO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIzMDciO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIyNS40MjA3NDkyNzk1Mzg5MDciO31pOjM4O2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTY6Ii9zcGllZ2VsbWFuLmh0bWwiO3M6OToicGFnZVRpdGxlIjtzOjQzOiJBcnQgU3BpZWdlbG1hbiA6OiBUaGUgU3RldmVuIEJhcmNsYXkgQWdlbmN5IjtzOjY6InZpc2l0cyI7czozOiIzMDEiO3M6NzoiYm91bmNlcyI7czozOiIyMzUiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTY6IjI5MC4wMzg5NjEwMzg5NjEiO31pOjM5O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTY6Ii9jYXRlZ29yeS93aW50ZXIiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyOTUiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxOC4wNDM0NzgyNjA4Njk1NjYiO31pOjQwO2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjE6Ii9jYXRlZ29yeS9zZWVkcy1mcnVpdCI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjI5MCI7czo3OiJib3VuY2VzIjtzOjE6IjkiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjE1LjM0Mjc2NzI5NTU5NzQ4NSI7fWk6NDE7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxNjoiL2NhdGVnb3J5L3NwaXJhbCI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjI5MCI7czo3OiJib3VuY2VzIjtzOjE6IjciO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6OToiMjEuOTQ2ODc1Ijt9aTo0MjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTc6ImJhcmNsYXlhZ2VuY3kuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEzOiIvY29sbGlucy5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czo0MjoiQmlsbHkgQ29sbGlucyA6OiBUaGUgU3RldmVuIEJhcmNsYXkgQWdlbmN5IjtzOjY6InZpc2l0cyI7czozOiIyODYiO3M6NzoiYm91bmNlcyI7czozOiIyMDYiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjEwNS41MzMzMzMzMzMzMzMzMyI7fWk6NDM7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxNzoiL21tNS9tZXJjaGFudC5tdmMiO3M6OToicGFnZVRpdGxlIjtzOjIyOiJTaG9wcGluZyBDYXJ0IENvbnRlbnRzIjtzOjY6InZpc2l0cyI7czozOiIyNzciO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIyMi45MzM1NjA0NzcwMDE3MDIiO31pOjQ0O2E6Njp7czo4OiJob3N0bmFtZSI7czoyNDoid3d3Lm5hbnR1Y2tldGhvbWVpbmMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjk6Ii9zaG93cm9vbSI7czo5OiJwYWdlVGl0bGUiO3M6MzE6Ik5hbnR1Y2tldCBIb21lLCBJbmMuIHwgU2hvd3Jvb20iO3M6NjoidmlzaXRzIjtzOjM6IjI3NCI7czo3OiJib3VuY2VzIjtzOjI6IjE0IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI0OC4wMjc0NTA5ODAzOTIxNiI7fWk6NDU7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxNToiL2NhdGVnb3J5L2dsYXNzIjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiMjY0IjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxODoiMTguMzQ5MjA2MzQ5MjA2MzQ4Ijt9aTo0NjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE1OiIvY2F0ZWdvcnkvcmVzaW4iO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyNjMiO3M6NzoiYm91bmNlcyI7czoxOiIzIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxOC43NDAyMTM1MjMxMzE2NzIiO31pOjQ3O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTM6Ii9jYXRlZ29yeS9wY3QiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyNTkiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjU6IjE1LjU3Ijt9aTo0ODthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTc6ImJhcmNsYXlhZ2VuY3kuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjMzOiIvc3BlYWtlcnMvYXBwZWFyYW5jZXMvbGFtb3R0Lmh0bWwiO3M6OToicGFnZVRpdGxlIjtzOjIxOiJTdGV2ZW4gQmFyY2xheSBBZ2VuY3kiO3M6NjoidmlzaXRzIjtzOjM6IjI1MyI7czo3OiJib3VuY2VzIjtzOjI6Ijk3IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIxMzMuNjE0NDU3ODMxMzI1MyI7fWk6NDk7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMjoiL29saXZlci5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czo0MDoiTWFyeSBPbGl2ZXIgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6MzoiMjQ5IjtzOjc6ImJvdW5jZXMiO3M6MzoiMTUxIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIxMDMuMzkzMTYyMzkzMTYyNCI7fWk6NTA7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMDoiL3dhcmUuaHRtbCI7czo5OiJwYWdlVGl0bGUiO3M6Mzk6IkNocmlzIFdhcmUgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6MzoiMjQ3IjtzOjc6ImJvdW5jZXMiO3M6MzoiMTU3IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI0MS44MTE4ODExODgxMTg4MSI7fWk6NTE7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6NDA6Ii9zaG9wL3Byb2R1Y3QvNjcxMTEtd2ludGVyLWNvYXQtY2hhcmNvYWwiO3M6OToicGFnZVRpdGxlIjtzOjg3OiJXaW50ZXIgQ29hdCA+IEluZGlnZW5vdXMgRGVzaWducyB8IEZhaXIgVHJhZGUgQ2xvdGhpbmcgfCBFY28gRmFzaGlvbiB8IE9yZ2FuaWMgQ2xvdGhpbmciO3M6NjoidmlzaXRzIjtzOjM6IjI0MyI7czo3OiJib3VuY2VzIjtzOjI6IjQ0IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI0NC43Njc1Mjc2NzUyNzY3NSI7fWk6NTI7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxNDoiL2NhdGVnb3J5L3lvZ2EiO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyMzYiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIyMi41OTU2Njc4NzAwMzYxMDIiO31pOjUzO2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoid3d3LmluZGlnZW5vdXMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjI5OiIvc2hvcC9wcm9kdWN0L21peGVkLWtuaXQtY29hdCI7czo5OiJwYWdlVGl0bGUiO3M6OTE6Ik1peGVkIEtuaXQgQ29hdCA+IEluZGlnZW5vdXMgRGVzaWducyB8IEZhaXIgVHJhZGUgQ2xvdGhpbmcgfCBFY28gRmFzaGlvbiB8IE9yZ2FuaWMgQ2xvdGhpbmciO3M6NjoidmlzaXRzIjtzOjM6IjIzNiI7czo3OiJib3VuY2VzIjtzOjE6IjkiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjM4Ljc3MTkyOTgyNDU2MTQwNCI7fWk6NTQ7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czo0MzoiL2NhdGVnb3J5L2N5Y2xpbmctamV3ZWxyeT9vZmZzZXQ9MTImU29ydGJ5PSI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjIyNiI7czo3OiJib3VuY2VzIjtzOjE6IjIiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjE3LjA0NDYwOTY2NTQyNzUwOCI7fWk6NTU7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTg6Ii9jb21wYW55L291ci1zdG9yeSI7czo5OiJwYWdlVGl0bGUiO3M6ODU6Ik91ciBTdG9yeSA+IEluZGlnZW5vdXMgRGVzaWducyB8IEZhaXIgVHJhZGUgQ2xvdGhpbmcgfCBFY28gRmFzaGlvbiB8IE9yZ2FuaWMgQ2xvdGhpbmciO3M6NjoidmlzaXRzIjtzOjM6IjIyNCI7czo3OiJib3VuY2VzIjtzOjI6IjE4IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI4Ny44OTk1MjE1MzExMDA0OCI7fWk6NTY7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE3OiJiYXJjbGF5YWdlbmN5LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMjoiL3Zvd2VsbC5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czo0MToiU2FyYWggVm93ZWxsIDo6IFRoZSBTdGV2ZW4gQmFyY2xheSBBZ2VuY3kiO3M6NjoidmlzaXRzIjtzOjM6IjIyMiI7czo3OiJib3VuY2VzIjtzOjM6IjE1NiI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiODMuOTY4MjUzOTY4MjUzOTYiO31pOjU3O2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTM6Ii9zYXRyYXBpLmh0bWwiO3M6OToicGFnZVRpdGxlIjtzOjQ0OiJNYXJqYW5lIFNhdHJhcGkgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6MzoiMjIxIjtzOjc6ImJvdW5jZXMiO3M6MzoiMTc4IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxODAuNDgwNzY5MjMwNzY5MjMiO31pOjU4O2E6Njp7czo4OiJob3N0bmFtZSI7czoyNToid3d3LmNhcnZlcnNjaGlja2V0YW56LmNvbSI7czo4OiJwYWdlUGF0aCI7czozMDoiL2FyY2hpdGVjdHVyZS9kYW5pLXJpZGdlLWhvdXNlIjtzOjk6InBhZ2VUaXRsZSI7czozOToiQ2FydmVyICsgU2NoaWNrZXRhbnogfCBEYW5pIFJpZGdlIEhvdXNlIjtzOjY6InZpc2l0cyI7czozOiIyMTkiO3M6NzoiYm91bmNlcyI7czoyOiI0MiI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNjoiODUuMjgxNzY3OTU1ODAxMSI7fWk6NTk7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI1OiJ3d3cuY2FydmVyc2NoaWNrZXRhbnouY29tIjtzOjg6InBhZ2VQYXRoIjtzOjMyOiIvYXJjaGl0ZWN0dXJlL2Nhcm1lbC1taWQtY2VudHVyeSI7czo5OiJwYWdlVGl0bGUiO3M6NDg6IkNhcnZlciArIFNjaGlja2V0YW56IHwgQ2FybWVsIE1pZC1DZW50dXJ5IC0gTEVFRCI7czo2OiJ2aXNpdHMiO3M6MzoiMjE3IjtzOjc6ImJvdW5jZXMiO3M6MjoiMzUiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6Ijc2LjI4Nzk1ODExNTE4MzI0Ijt9aTo2MDthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjQyOiIvY2F0ZWdvcnkvaGlraW5nLWpld2Vscnk/b2Zmc2V0PTEyJlNvcnRieT0iO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyMTQiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxOS44MTk3ODc5ODU4NjU3MjMiO31pOjYxO2E6Njp7czo4OiJob3N0bmFtZSI7czoyNDoid3d3Lm5hbnR1Y2tldGhvbWVpbmMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjk6Ii9hYm91dC11cyI7czo5OiJwYWdlVGl0bGUiO3M6Mjc6Ik5hbnR1Y2tldCBIb21lLCBJbmMuIHwgSG9tZSI7czo2OiJ2aXNpdHMiO3M6MzoiMjA4IjtzOjc6ImJvdW5jZXMiO3M6MjoiMzEiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjQ3LjQ0ODQ1MzYwODI0NzQyIjt9aTo2MjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTc6ImJhcmNsYXlhZ2VuY3kuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEzOiIvaGFuZGxlci5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czo0MzoiRGFuaWVsIEhhbmRsZXIgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6MzoiMjA3IjtzOjc6ImJvdW5jZXMiO3M6MzoiMTQ5IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxOTAuMjU3NTc1NzU3NTc1NzUiO31pOjYzO2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6NDM6Ii9jYXRlZ29yeS9jeWNsaW5nLWpld2Vscnk/b2Zmc2V0PTI0JlNvcnRieT0iO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyMDUiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIzMy4zNDMwMjMyNTU4MTM5NTQiO31pOjY0O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTM6Ii9jYXRlZ29yeS9uZXciO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIyMDUiO3M6NzoiYm91bmNlcyI7czoxOiIzIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIyNC43Nzg4MDE4NDMzMTc5NyI7fWk6NjU7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjIwOiJ3d3cudGFybWFkZXNpZ25zLmNvbSI7czo4OiJwYWdlUGF0aCI7czozNDoiL2NhdGVnb3J5L25hdHVyZT9vZmZzZXQ9MTImU29ydGJ5PSI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjIwMiI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjIwLjg5MTg5MTg5MTg5MTg5Ijt9aTo2NjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE5OiIvY2F0ZWdvcnkvYWNjZXNzb3J5IjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiMTkzIjtzOjc6ImJvdW5jZXMiO3M6MToiNiI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiMjkuMzU0MDg1NjAzMTEyODQiO31pOjY3O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTg6Ii9jYXRlZ29yeS9wYWludGluZyI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjE5MyI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjE3LjkxMjg3ODc4Nzg3ODc5Ijt9aTo2ODthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6Ind3dy5pbmRpZ2Vub3VzLmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMDoiL3Nob3AvY2FydCI7czo5OiJwYWdlVGl0bGUiO3M6OTI6IllvdXIgQ2FydCA+IFNob3AgPiBJbmRpZ2Vub3VzIERlc2lnbnMgfCBGYWlyIFRyYWRlIENsb3RoaW5nIHwgRWNvIEZhc2hpb24gfCBPcmdhbmljIENsb3RoaW5nIjtzOjY6InZpc2l0cyI7czozOiIxOTIiO3M6NzoiYm91bmNlcyI7czoxOiI0IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjEwOiIzOC4xOTUzMTI1Ijt9aTo2OTthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjk6Ii9jaGVja291dCI7czo5OiJwYWdlVGl0bGUiO3M6MjU6IkNoZWNrb3V0IC0gQ3VzdG9tZXIgTG9naW4iO3M6NjoidmlzaXRzIjtzOjM6IjE5MCI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjE3LjA1MzQzNTExNDUwMzgxNyI7fWk6NzA7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI1OiJ3d3cub3ZlcmxhbmRlcXVpcG1lbnQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEzOiIvc2hvcC93YWxsZXRzIjtzOjk6InBhZ2VUaXRsZSI7czozOToiT3ZlcmxhbmQgRXF1aXBtZW50IC0gU2ltcGx5IEhvbmVzdCBCYWdzIjtzOjY6InZpc2l0cyI7czozOiIxODgiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiI3OS4yNzE5MDMzMjMyNjI4MyI7fWk6NzE7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI1OiJ3d3cuY2FydmVyc2NoaWNrZXRhbnouY29tIjtzOjg6InBhZ2VQYXRoIjtzOjMwOiIvYXJjaGl0ZWN0dXJlL2NvYXN0bGFuZHMtaG91c2UiO3M6OToicGFnZVRpdGxlIjtzOjM5OiJDYXJ2ZXIgKyBTY2hpY2tldGFueiB8IENvYXN0bGFuZHMgSG91c2UiO3M6NjoidmlzaXRzIjtzOjM6IjE4NyI7czo3OiJib3VuY2VzIjtzOjI6IjExIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxMDguMDU3MTQyODU3MTQyODYiO31pOjcyO2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTE6Ii9nbGFzcy5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czozODoiSXJhIEdsYXNzIDo6IFRoZSBTdGV2ZW4gQmFyY2xheSBBZ2VuY3kiO3M6NjoidmlzaXRzIjtzOjM6IjE4NiI7czo3OiJib3VuY2VzIjtzOjI6IjU2IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiI1NC4xNDkyNTM3MzEzNDMyODYiO31pOjczO2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjE6Ii9wcm9kdWN0LzEwMDUzNS9jYWlybiI7czo5OiJwYWdlVGl0bGUiO3M6MTc6IkNhaXJuIFBlbmRhbnQgMm1tIjtzOjY6InZpc2l0cyI7czozOiIxODUiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE3OiIzOS44ODQyMTA1MjYzMTU3OSI7fWk6NzQ7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJ3d3cuaW5kaWdlbm91cy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjY6Ii9zaG9wL2NhdGVnb3J5L2FjY2Vzc29yaWVzIjtzOjk6InBhZ2VUaXRsZSI7czo4NzoiQWNjZXNzb3JpZXMgPiBJbmRpZ2Vub3VzIERlc2lnbnMgfCBGYWlyIFRyYWRlIENsb3RoaW5nIHwgRWNvIEZhc2hpb24gfCBPcmdhbmljIENsb3RoaW5nIjtzOjY6InZpc2l0cyI7czozOiIxODUiO3M6NzoiYm91bmNlcyI7czoxOiI0IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIzNC42OTMzOTYyMjY0MTUwOTYiO31pOjc1O2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6Mjk6Ii9wYWdlcy9leHRlbmRlZC1zcGVha2Vycy5odG1sIjtzOjk6InBhZ2VUaXRsZSI7czoyMToiU3RldmVuIEJhcmNsYXkgQWdlbmN5IjtzOjY6InZpc2l0cyI7czozOiIxODQiO3M6NzoiYm91bmNlcyI7czoyOiIxMiI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiNjEuMjA5NzkwMjA5NzkwMjEiO31pOjc2O2E6Njp7czo4OiJob3N0bmFtZSI7czoyNDoid3d3Lm5hbnR1Y2tldGhvbWVpbmMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjM1OiIvc2hvcC9jYXRlZ29yeS9vYmpldC1ldC1hY2Nlc3NvcmllcyI7czo5OiJwYWdlVGl0bGUiO3M6MzY6Ik5hbnR1Y2tldCBIb21lLCBJbmMuIHwgRGVzaWduIEFkdmljZSI7czo2OiJ2aXNpdHMiO3M6MzoiMTgzIjtzOjc6ImJvdW5jZXMiO3M6MToiNSI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxODoiMjIuNzQxNTI1NDIzNzI4ODEzIjt9aTo3NzthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjM0OiIvY2F0ZWdvcnkvbmF0dXJlP29mZnNldD0yNCZTb3J0Ynk9IjtzOjk6InBhZ2VUaXRsZSI7czozNzoiR28gb24gYW4gYWR2ZW50dXJlIHdpdGggVGFybWEgRGVzaWducyI7czo2OiJ2aXNpdHMiO3M6MzoiMTgyIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uUGFnZSI7czoxNzoiMjEuMjUyNjMxNTc4OTQ3MzciO31pOjc4O2E6Njp7czo4OiJob3N0bmFtZSI7czoxNzoiYmFyY2xheWFnZW5jeS5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTI6Ii9sYWhpcmkuaHRtbCI7czo5OiJwYWdlVGl0bGUiO3M6NDI6IkpodW1wYSBMYWhpcmkgOjogVGhlIFN0ZXZlbiBCYXJjbGF5IEFnZW5jeSI7czo2OiJ2aXNpdHMiO3M6MzoiMTgxIjtzOjc6ImJvdW5jZXMiO3M6MzoiMTQ1IjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxMzIuNTk0NTk0NTk0NTk0NTgiO31pOjc5O2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6NDI6Ii9jYXRlZ29yeS9oaWtpbmctamV3ZWxyeT9vZmZzZXQ9MjQmU29ydGJ5PSI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjE4MCI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjIxLjgxODU4NDA3MDc5NjQ2Ijt9aTo4MDthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjA6Ind3dy50YXJtYWRlc2lnbnMuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjM2OiIvY2F0ZWdvcnkvbW91bnRhaW4/b2Zmc2V0PTEyJlNvcnRieT0iO3M6OToicGFnZVRpdGxlIjtzOjM3OiJHbyBvbiBhbiBhZHZlbnR1cmUgd2l0aCBUYXJtYSBEZXNpZ25zIjtzOjY6InZpc2l0cyI7czozOiIxNzciO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxNy4zNzU2NjEzNzU2NjEzNzciO31pOjgxO2E6Njp7czo4OiJob3N0bmFtZSI7czoyMDoid3d3LnRhcm1hZGVzaWducy5jb20iO3M6ODoicGFnZVBhdGgiO3M6MTU6Ii9jYXRlZ29yeS9CbGFuayI7czo5OiJwYWdlVGl0bGUiO3M6Mzc6IkdvIG9uIGFuIGFkdmVudHVyZSB3aXRoIFRhcm1hIERlc2lnbnMiO3M6NjoidmlzaXRzIjtzOjM6IjE3NiI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTc6IjMxLjYwMjIwOTk0NDc1MTM4Ijt9aTo4MjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjQ6Ind3dy5uYW50dWNrZXRob21laW5jLmNvbSI7czo4OiJwYWdlUGF0aCI7czoyNzoiL3Nob3AvY2F0ZWdvcnkvdW5pcXVlLWZpbmRzIjtzOjk6InBhZ2VUaXRsZSI7czozNjoiTmFudHVja2V0IEhvbWUsIEluYy4gfCBEZXNpZ24gQWR2aWNlIjtzOjY6InZpc2l0cyI7czozOiIxNzUiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjE4OiIxNi40NDg5Nzk1OTE4MzY3MzYiO319czoxNjoidGFibGVfc3RhdGlzdGljcyI7YTo5Mzp7czoxMDoiMjAxMi0wOC0wMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTAzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0wNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDUiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTA2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0wNyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDgiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTA5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xMCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTEyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTE1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTE4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTIxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjMiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTI0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yNSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjYiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTI3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTMwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0zMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTAyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTA1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTA4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTExIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTMiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTE0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xNSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTYiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTE3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTIwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTIzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjUiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTI2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yNyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjgiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTI5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0zMCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTAyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTA1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTA4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTExIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0xMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjEzODtzOjk6InBhZ2VWaWV3cyI7aTo1NzQ7czo2OiJ2aXNpdHMiO2k6MTM4O3M6ODoidmlzaXRvcnMiO2k6MTMwO3M6NzoiYm91bmNlcyI7aTo2NDtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjExOS43NjA4Njk1NjUyMTczOTA2ODY0ODQ1Nzc1OTk5MTI4ODE4NTExOTYyODkwNjI1O3M6OToibmV3VmlzaXRzIjtpOjExNDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6Njp7czo5OiIobm90IHNldCkiO2k6MztzOjc6IkFuZHJvaWQiO2k6MztzOjU6IkxpbnV4IjtzOjE6IjEiO3M6OToiTWFjaW50b3NoIjtpOjQyO3M6NzoiV2luZG93cyI7aTo2MjtzOjM6ImlPUyI7aToyNzt9czo3OiJicm93c2VyIjthOjg6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjM7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtpOjM7fX1zOjEwOiJPcGVyYSBNaW5pIjthOjI6e3M6NToidG90YWwiO2k6MjtzOjc6InZlcnNpb24iO2E6Mjp7czo5OiI1LjEuMjEwNTEiO3M6MToiMSI7czo5OiI2LjUuMjc0NTIiO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIyIjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO3M6MToiMiI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aToyMjtzOjc6InZlcnNpb24iO2E6Nzp7aToxNTtpOjEzO2k6MTQ7aToyO2k6MTA7czoxOiIxIjtpOjE2O3M6MToiMiI7aToxNztzOjE6IjEiO2k6MztzOjE6IjIiO2k6NztzOjE6IjEiO319czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTozMjtzOjc6InZlcnNpb24iO2E6Mzp7aToyMTtzOjE6IjEiO2k6MjI7aTozMDtpOjE3O3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtpOjU1O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo1NTt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6MTk7czo3OiJ2ZXJzaW9uIjthOjM6e2k6NjtzOjE6IjEiO2k6ODtzOjE6IjUiO2k6OTtzOjI6IjEzIjt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjMiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoxOiIzIjt9fX19czoxMDoiMjAxMi0xMC0xMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjU4NjtzOjk6InBhZ2VWaWV3cyI7aToyNjU4O3M6NjoidmlzaXRzIjtpOjU4NjtzOjg6InZpc2l0b3JzIjtpOjQ5MjtzOjc6ImJvdW5jZXMiO2k6MjU2O3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTUzLjY2MjExNjA0MDk1NTYyMzkzMDEzMDk2ODgwMTY3NzIyNzAyMDI2MzY3MTg3NTtzOjk6Im5ld1Zpc2l0cyI7aTo0MjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjY6e3M6OToiKG5vdCBzZXQpIjtpOjk7czo3OiJBbmRyb2lkIjtpOjEzO3M6MTA6IkJsYWNrQmVycnkiO2k6MztzOjk6Ik1hY2ludG9zaCI7aToxNTk7czo3OiJXaW5kb3dzIjtpOjMxMztzOjM6ImlPUyI7aTo4OTt9czo3OiJicm93c2VyIjthOjEwOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aTo3O3M6NzoidmVyc2lvbiI7YToxOntpOjU7aTo3O319czoxMDoiT3BlcmEgTWluaSI7YToyOntzOjU6InRvdGFsIjtpOjM7czo3OiJ2ZXJzaW9uIjthOjM6e3M6OToiNC4yLjE4OTc1IjtzOjE6IjEiO3M6OToiNC4yLjIzNDQ5IjtzOjE6IjEiO3M6OToiNy4wLjMwNTY3IjtzOjE6IjEiO319czoxNToiQW5kcm9pZCBCcm93c2VyIjthOjI6e3M6NToidG90YWwiO2k6MTI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjEyO319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtpOjU7czo3OiJ2ZXJzaW9uIjthOjM6e3M6NDoiTW9iaSI7czoxOiIxIjtpOjExO3M6MToiMSI7aToxMjtzOjE6IjMiO319czo2OiJTYWZhcmkiO2E6Mjp7czo1OiJ0b3RhbCI7aToxODg7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjE4ODt9fXM6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO2k6MTQ2O3M6NzoidmVyc2lvbiI7YTo3OntpOjE1O3M6MToiMSI7aToyMTtpOjQ7aToyMjtpOjEzNztpOjEwO3M6MToiMSI7aToxNjtzOjE6IjEiO2k6MTg7czoxOiIxIjtpOjM7czoxOiIxIjt9fXM6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtpOjEwOTtzOjc6InZlcnNpb24iO2E6ODp7aToxNDtpOjQ7aToxNTtpOjc1O2k6MTY7aToxNztpOjM7aTo2O2k6NjtzOjE6IjIiO2k6NztpOjI7aToxMjtzOjE6IjIiO2k6ODtzOjE6IjEiO319czoyMDoiSUUgd2l0aCBDaHJvbWUgRnJhbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aTo4O3M6MToiMSI7fX1zOjE3OiJJbnRlcm5ldCBFeHBsb3JlciI7YToyOntzOjU6InRvdGFsIjtpOjEwNztzOjc6InZlcnNpb24iO2E6Mzp7aTo3O3M6MToiNiI7aTo4O3M6MjoiMzIiO2k6OTtzOjI6IjY5Ijt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjgiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoxOiI4Ijt9fX19czoxMDoiMjAxMi0xMC0xNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjE4NzM7czo5OiJwYWdlVmlld3MiO2k6NzUzMDtzOjY6InZpc2l0cyI7aToxODczO3M6ODoidmlzaXRvcnMiO2k6MTcxMTtzOjc6ImJvdW5jZXMiO2k6OTcyO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTM1LjYwNzU4MTQyMDE4MTUzMTkzOTMwNjI5NjQwODE3NjQyMjExOTE0MDYyNTtzOjk6Im5ld1Zpc2l0cyI7aToxNDc0O3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo3OntzOjk6Iihub3Qgc2V0KSI7aToyNztzOjc6IkFuZHJvaWQiO2k6NTE7czoxMDoiQmxhY2tCZXJyeSI7aToyO3M6NToiTGludXgiO2k6MTU7czo5OiJNYWNpbnRvc2giO2k6NTI4O3M6NzoiV2luZG93cyI7aTo4NjI7czozOiJpT1MiO2k6Mzg4O31zOjc6ImJyb3dzZXIiO2E6MTE6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtzOjI6IjIyIjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O3M6MjoiMjIiO319czoxMDoiT3BlcmEgTWluaSI7YToyOntzOjU6InRvdGFsIjtpOjY7czo3OiJ2ZXJzaW9uIjthOjQ6e3M6OToiNC40LjI5NDc2IjtzOjE6IjEiO3M6OToiNS4xLjIxMDUxIjtzOjE6IjEiO3M6NToiNi4wLjMiO3M6MToiMyI7czo5OiI3LjUuMzE2NTciO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0NztzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NDc7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjM5MDtzOjc6InZlcnNpb24iO2E6MTI6e2k6MTg7aTo1O2k6MjE7aTozMDtpOjE0O3M6MToiMSI7aToxNTtzOjE6IjEiO2k6MTc7czoxOiIxIjtpOjIyO2k6MzQzO2k6MjQ7aToyO2k6MTE7czoxOiIxIjtpOjE2O2k6MjtpOjE5O3M6MToiMSI7aToyMDtzOjE6IjEiO2k6MjM7czoxOiIyIjt9fXM6NToiT3BlcmEiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2O3M6NzoidmVyc2lvbiI7YToyOntzOjY6IlRhYmxldCI7czoxOiIxIjtpOjEyO2k6NTt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NzExO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo3MTE7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aTozMDA7czo3OiJ2ZXJzaW9uIjthOjE1OntpOjExO2k6NDtpOjE1O2k6MTUzO2k6MTY7aTo4NDtpOjM7aToxMjtpOjc7aTo2O2k6MTI7aTo4O2k6MTM7aTo1O2k6MTQ7aToxNDtpOjQ7czoxOiIyIjtpOjY7aToyO2k6MTA7aTo0O2k6MTc7czoxOiIyIjtpOjU7aToyO2k6ODtzOjE6IjEiO2k6OTtzOjE6IjEiO319czo2OiJDYW1pbm8iO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo1OiIyLjEuMiI7czoxOiIxIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiMyI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjMiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTozNTg7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjEiO2k6NztzOjI6IjEyIjtpOjg7czozOiIxMDQiO2k6OTtzOjM6IjI0MSI7fX1zOjE1OiJTYWZhcmkgKGluLWFwcCkiO2E6Mjp7czo1OiJ0b3RhbCI7czoyOiIyOSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjI6IjI5Ijt9fX19czoxMDoiMjAxMi0xMC0xNSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjIzNzU7czo5OiJwYWdlVmlld3MiO2k6ODc4OTtzOjY6InZpc2l0cyI7aToyMzc1O3M6ODoidmlzaXRvcnMiO2k6MjE0MjtzOjc6ImJvdW5jZXMiO2k6MTI2MztzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjEzNS4zOTY2MzE1Nzg5NDczNjQwMDUyMTY1MTM3NjAzODc4OTc0OTE0NTUwNzgxMjU7czo5OiJuZXdWaXNpdHMiO2k6MTgwNjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6OTp7czo5OiIobm90IHNldCkiO2k6MzA7czo3OiJBbmRyb2lkIjtpOjczO3M6MTA6IkJsYWNrQmVycnkiO3M6MToiMSI7czo5OiJDaHJvbWUgT1MiO3M6MToiMSI7czo1OiJMaW51eCI7aToxNTtzOjk6Ik1hY2ludG9zaCI7aTo2NTI7czo3OiJXaW5kb3dzIjtpOjEzMzg7czoxMzoiV2luZG93cyBQaG9uZSI7czoxOiIyIjtzOjM6ImlPUyI7aToyNjM7fXM6NzoiYnJvd3NlciI7YToxMjp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO2k6MzA7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtpOjMwO319czo4OiJOZXRGcm9udCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjU6IjMuNS4xIjtzOjE6IjEiO319czoxMDoiT3BlcmEgTWluaSI7YToyOntzOjU6InRvdGFsIjtpOjI7czo3OiJ2ZXJzaW9uIjthOjI6e3M6OToiNC4xLjE1MDgyIjtzOjE6IjEiO3M6OToiNi41LjI2OTkxIjtzOjE6IjEiO319czoxNToiQW5kcm9pZCBCcm93c2VyIjthOjI6e3M6NToidG90YWwiO2k6NjY7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjY2O319czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1NDg7czo3OiJ2ZXJzaW9uIjthOjE1OntpOjE4O2k6ODtpOjIxO2k6MjQ7aToxMTtpOjI7aToxOTtpOjQ7aToyMDtpOjc7aToyMjtpOjQ3OTtpOjIzO2k6NztpOjEyO2k6NztpOjE0O2k6MjtpOjEzO2k6MjtpOjE3O3M6MToiMSI7aToyO3M6MToiMSI7aToyNDtpOjI7aTo3O3M6MToiMSI7aTo4O3M6MToiMSI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aTo0NTk7czo3OiJ2ZXJzaW9uIjthOjE2OntpOjE2O2k6MTE4O2k6MTE7aTo5O2k6MztpOjE4O2k6MTA7aToxMDtpOjEyO2k6MTg7aToxMztpOjk7aToxNDtpOjE5O2k6MTU7aToyMzI7aTo0O2k6NDtpOjU7aToyO2k6NjtpOjI7aTo4O2k6MztpOjk7aTo4O2k6MTc7czoxOiI0IjtpOjE4O3M6MToiMSI7aTo3O3M6MToiMiI7fX1zOjU6Ik9wZXJhIjthOjI6e3M6NToidG90YWwiO2k6MztzOjc6InZlcnNpb24iO2E6Mjp7czo0OiJNb2JpIjtzOjE6IjEiO2k6MTI7czoxOiIyIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NjM2O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo2MzY7fX1zOjg6IlJvY2tNZWx0IjthOjI6e3M6NToidG90YWwiO2k6MjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6Mjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiNCI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjQiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2MDQ7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjUiO2k6NztzOjI6IjM1IjtpOjg7czozOiIyMjEiO2k6OTtpOjM0Mzt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjIwIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMjAiO319fX1zOjEwOiIyMDEyLTEwLTE2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MjMxMDtzOjk6InBhZ2VWaWV3cyI7aTo5NjQzO3M6NjoidmlzaXRzIjtpOjIzMTA7czo4OiJ2aXNpdG9ycyI7aToyMDc2O3M6NzoiYm91bmNlcyI7aToxMTY1O3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTU1LjQ0MzcyMjk0MzcyMjk0NjE4MzY5Nzc4NjE4MjE2NTE0NTg3NDAyMzQzNzU7czo5OiJuZXdWaXNpdHMiO2k6MTc0MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6ODp7czo5OiIobm90IHNldCkiO2k6Mjg7czo3OiJBbmRyb2lkIjtpOjY1O3M6MTA6IkJsYWNrQmVycnkiO3M6MToiMiI7czo5OiJDaHJvbWUgT1MiO3M6MToiMSI7czo1OiJMaW51eCI7aToxMDtzOjk6Ik1hY2ludG9zaCI7aTo2NjI7czo3OiJXaW5kb3dzIjtpOjEyNDY7czozOiJpT1MiO2k6Mjk2O31zOjc6ImJyb3dzZXIiO2E6MTI6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjM2O3M6NzoidmVyc2lvbiI7YToxOntpOjU7aTozNjt9fXM6MTA6Ik9wZXJhIE1pbmkiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiI0LjIuMTgxNDkiO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2MDtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NjA7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjQ4OTtzOjc6InZlcnNpb24iO2E6MTI6e2k6MTg7aTo2O2k6MjM7aToyO2k6MjA7aTozO2k6MjE7aTozMztpOjE5O2k6NjtpOjIyO2k6NDMxO2k6NTtzOjE6IjEiO2k6MTI7czoxOiIyIjtpOjE0O3M6MToiMSI7aToxNjtzOjE6IjEiO2k6MTc7aToyO2k6MjQ7czoxOiIxIjt9fXM6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtpOjQzMztzOjc6InZlcnNpb24iO2E6MTQ6e2k6MTA7aTo3O2k6MTQ7aToxMjtpOjE1O2k6MjEwO2k6MTY7aToxNTE7aToxMjtpOjEyO2k6MTM7aTo4O2k6MztpOjE0O2k6NTtzOjE6IjEiO2k6NjtzOjE6IjEiO2k6OTtpOjY7aToxMTtzOjE6IjUiO2k6NDtzOjE6IjMiO2k6NztzOjE6IjIiO2k6ODtzOjE6IjEiO319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtpOjY7czo3OiJ2ZXJzaW9uIjthOjI6e3M6NDoiTW9iaSI7czoxOiIxIjtpOjEyO2k6NTt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NjgxO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo2ODE7fX1zOjk6IktvbnF1ZXJvciI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IjMuNSI7czoxOiIxIjt9fXM6NjoiQ2FtaW5vIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NToiMi4xLjIiO3M6MToiMSI7fX1zOjIwOiJJRSB3aXRoIENocm9tZSBGcmFtZSI7YToyOntzOjU6InRvdGFsIjtpOjc7czo3OiJ2ZXJzaW9uIjthOjI6e2k6ODtzOjE6IjYiO2k6OTtzOjE6IjEiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1Njg7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjMiO2k6NztzOjI6IjM2IjtpOjg7czozOiIyMTAiO2k6OTtzOjM6IjMxOSI7fX1zOjE1OiJTYWZhcmkgKGluLWFwcCkiO2E6Mjp7czo1OiJ0b3RhbCI7czoyOiIyNyI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjI6IjI3Ijt9fX19czoxMDoiMjAxMi0xMC0xNyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjIzMjU7czo5OiJwYWdlVmlld3MiO2k6OTM2NDtzOjY6InZpc2l0cyI7aToyMzI1O3M6ODoidmlzaXRvcnMiO2k6MjA4MjtzOjc6ImJvdW5jZXMiO2k6MTIxMjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjE3MS41NzI5MDMyMjU4MDY0NDIxNTEyMjQ0NzM0OTEzMTEwNzMzMDMyMjI2NTYyNTtzOjk6Im5ld1Zpc2l0cyI7aToxNzAzO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo5OntzOjk6Iihub3Qgc2V0KSI7aTo0MDtzOjc6IkFuZHJvaWQiO2k6NjI7czoxMDoiQmxhY2tCZXJyeSI7aToyO3M6NToiTGludXgiO2k6OTtzOjk6Ik1hY2ludG9zaCI7aTo2NDY7czo0OiJTb255IjtzOjE6IjEiO3M6NzoiV2luZG93cyI7aToxMjU3O3M6MTM6IldpbmRvd3MgUGhvbmUiO3M6MToiMSI7czozOiJpT1MiO2k6MzA3O31zOjc6ImJyb3dzZXIiO2E6MTU6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjQ3O3M6NzoidmVyc2lvbiI7YToxOntpOjU7aTo0Nzt9fXM6MTA6Ik9wZXJhIE1pbmkiO2E6Mjp7czo1OiJ0b3RhbCI7aToyO3M6NzoidmVyc2lvbiI7YToyOntzOjk6IjQuNC4yOTYyMCI7czoxOiIxIjtzOjU6IjcuMC40IjtzOjE6IjEiO319czoxNDoiUGhhbnRvbS5qcyBib3QiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2MDtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NjA7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjQ0NjtzOjc6InZlcnNpb24iO2E6MTM6e2k6MTg7aToxMDtpOjE3O2k6MjtpOjIxO2k6MjQ7aToyMjtpOjM5MztpOjExO2k6MjtpOjE1O2k6MjtpOjE2O3M6MToiMSI7aToxOTtpOjQ7aToyMDtzOjE6IjEiO2k6MjM7aTo0O2k6MTA7czoxOiIxIjtpOjE0O3M6MToiMSI7aToyNDtzOjE6IjEiO319czoxNDoiQmxhY2tCZXJyeTk4MDAiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiI2LjAuMC42MDAiO3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtpOjcxMjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NzEyO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6NDE0O3M6NzoidmVyc2lvbiI7YToxNTp7aToxMDtpOjEyO2k6MTE7aTozO2k6NDtpOjY7aToxMztpOjk7aToxNDtpOjE3O2k6MTU7aToxNzU7aToxNjtpOjE1NztpOjE4O2k6MztpOjM7aToxNTtpOjg7aTo1O2k6MTtzOjE6IjEiO2k6MTI7czoxOiI2IjtpOjU7czoxOiIxIjtpOjc7czoxOiIyIjtpOjk7czoxOiIyIjt9fXM6NjoiQ2FtaW5vIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NToiMi4xLjEiO3M6MToiMSI7fX1zOjE2OiJTb255RXJpY3Nzb25KMjBpIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NDoiUjdDQSI7czoxOiIxIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiNiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjYiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2MDY7czo3OiJ2ZXJzaW9uIjthOjU6e2k6MTA7czoxOiIyIjtpOjY7czoxOiI3IjtpOjc7czoyOiI0NCI7aTo4O3M6MzoiMjM2IjtpOjk7aTozMTc7fX1zOjU6Ik9wZXJhIjthOjI6e3M6NToidG90YWwiO2k6NDtzOjc6InZlcnNpb24iO2E6MTp7aToxMjtpOjQ7fX1zOjg6IlJvY2tNZWx0IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtzOjE6IjEiO319czoxNToiU2FmYXJpIChpbi1hcHApIjthOjI6e3M6NToidG90YWwiO3M6MjoiMjMiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoyOiIyMyI7fX19fXM6MTA6IjIwMTItMTAtMTgiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyMTg1O3M6OToicGFnZVZpZXdzIjtpOjgwNzM7czo2OiJ2aXNpdHMiO2k6MjE4NTtzOjg6InZpc2l0b3JzIjtpOjE5NDk7czo3OiJib3VuY2VzIjtpOjExOTU7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNTMuOTc0MzcwNzA5MzgyMTU2OTc0MjQzMTE2NTY1MDQ4Njk0NjEwNTk1NzAzMTI1O3M6OToibmV3VmlzaXRzIjtpOjE1OTI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjk6e3M6OToiKG5vdCBzZXQpIjtpOjM0O3M6NzoiQW5kcm9pZCI7aTo0ODtzOjEwOiJCbGFja0JlcnJ5IjtpOjU7czo5OiJDaHJvbWUgT1MiO3M6MToiMSI7czo1OiJMaW51eCI7aToxNjtzOjk6Ik1hY2ludG9zaCI7aTo2MTE7czo5OiJTeW1iaWFuT1MiO3M6MToiMSI7czo3OiJXaW5kb3dzIjtpOjExOTg7czozOiJpT1MiO2k6MjcxO31zOjc6ImJyb3dzZXIiO2E6MTI6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjM0O3M6NzoidmVyc2lvbiI7YToxOntpOjU7aTozNDt9fXM6MTA6Ik9wZXJhIE1pbmkiO2E6Mjp7czo1OiJ0b3RhbCI7aToyO3M6NzoidmVyc2lvbiI7YToyOntzOjk6IjQuMi4xODE0OSI7czoxOiIxIjtzOjk6IjcuMC4yOTkxNSI7czoxOiIxIjt9fXM6MTU6IkFuZHJvaWQgQnJvd3NlciI7YToyOntzOjU6InRvdGFsIjtpOjQzO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo0Mzt9fXM6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO2k6NDMxO3M6NzoidmVyc2lvbiI7YToxNDp7aToxODtpOjc7aToyMTtpOjE2O2k6MjA7aTozO2k6MTY7aTozO2k6MTc7aTozO2k6MTk7aToyO2k6MjI7aTozODg7aToyMztpOjI7aToyNDtzOjE6IjEiO2k6MTA7czoxOiIxIjtpOjEyO3M6MToiMSI7aToxNDtzOjE6IjEiO2k6MTU7czoxOiIxIjtpOjU7czoxOiIyIjt9fXM6MTQ6IkJsYWNrQmVycnk5NTUwIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MTA6IjUuMC4wLjEwMTUiO3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtpOjY1MztzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NjUzO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6NDE4O3M6NzoidmVyc2lvbiI7YToxNTp7aToxMTtpOjEyO2k6MTQ7aToxNTtpOjE1O2k6MTc2O2k6MTY7aToxNDU7aTozO2k6MjA7aTo0O2k6MjtpOjEwO2k6NztpOjEyO2k6MTQ7aToxMztpOjc7aTo1O3M6MToiMiI7aTo3O3M6MToiMSI7aTo5O2k6NztpOjE3O3M6MToiOCI7aToyO3M6MToiMSI7aTo4O3M6MToiMSI7fX1zOjg6IlJvY2tNZWx0IjthOjI6e3M6NToidG90YWwiO2k6MjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6Mjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiNyI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjciO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1NjY7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjIiO2k6NztzOjI6IjQxIjtpOjg7czozOiIyMjUiO2k6OTtzOjM6IjI5OCI7fX1zOjU6Ik9wZXJhIjthOjI6e3M6NToidG90YWwiO3M6MToiNSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTI7czoxOiI1Ijt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjIzIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMjMiO319fX1zOjEwOiIyMDEyLTEwLTE5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTY2MTtzOjk6InBhZ2VWaWV3cyI7aTo1OTM0O3M6NjoidmlzaXRzIjtpOjE2NjE7czo4OiJ2aXNpdG9ycyI7aToxNDkyO3M6NzoiYm91bmNlcyI7aTo4ODY7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxMzEuMjUxMDUzNTgyMTc5NDIzNTI4OTQxNjk3NDQxMDQxNDY5NTczOTc0NjA5Mzc1O3M6OToibmV3VmlzaXRzIjtpOjEyMTE7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjg6e3M6OToiKG5vdCBzZXQpIjtpOjI3O3M6NzoiQW5kcm9pZCI7aTo0MDtzOjEwOiJCbGFja0JlcnJ5IjtpOjQ7czo1OiJMaW51eCI7aToxMDtzOjk6Ik1hY2ludG9zaCI7aTo0NjI7czo1OiJOb2tpYSI7czoxOiIxIjtzOjc6IldpbmRvd3MiO2k6ODkyO3M6MzoiaU9TIjtpOjIyNTt9czo3OiJicm93c2VyIjthOjEyOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aToyNTtzOjc6InZlcnNpb24iO2E6MTp7aTo1O2k6MjU7fX1zOjEwOiJPcGVyYSBNaW5pIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiNC4yLjEzMzM3IjtzOjE6IjEiO319czo2OiJTYWZhcmkiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1MTE7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjUxMTt9fXM6MTU6IkFuZHJvaWQgQnJvd3NlciI7YToyOntzOjU6InRvdGFsIjtpOjM2O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTozNjt9fXM6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO2k6MzIyO3M6NzoidmVyc2lvbiI7YTo5OntpOjE4O2k6NDtpOjIwO2k6NDtpOjIxO2k6MTE7aToxOTtpOjM7aToyMjtpOjI4OTtpOjIzO2k6NDtpOjEyO3M6MToiMiI7aToxNDtzOjE6IjEiO2k6MTc7aTo0O319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtpOjY7czo3OiJ2ZXJzaW9uIjthOjI6e3M6NDoiTW9iaSI7czoxOiIxIjtpOjEyO2k6NTt9fXM6MTQ6IkJsYWNrQmVycnk4NTIwIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiNS4wLjAuOTAwIjtzOjE6IjEiO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6Mjg1O3M6NzoidmVyc2lvbiI7YToxMjp7aToxMztpOjc7aToxNjtpOjExMjtpOjM7aToxMztpOjEyO2k6NDtpOjE0O2k6MTA7aToxNTtpOjExMztpOjE3O2k6MztpOjQ7aTo0O2k6OTtpOjI7aToxMDtpOjg7aToxMTtzOjE6IjYiO2k6NztzOjE6IjMiO319czo2OiJDYW1pbm8iO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo1OiIyLjEuMiI7czoxOiIxIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiMyI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NztzOjE6IjMiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0NTI7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjIiO2k6NztzOjI6IjI3IjtpOjg7czozOiIxNjkiO2k6OTtzOjM6IjI1NCI7fX1zOjE1OiJTYWZhcmkgKGluLWFwcCkiO2E6Mjp7czo1OiJ0b3RhbCI7czoyOiIxOCI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjI6IjE4Ijt9fX19czoxMDoiMjAxMi0xMC0yMCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjEwNjE7czo5OiJwYWdlVmlld3MiO2k6MzkzODtzOjY6InZpc2l0cyI7aToxMDYxO3M6ODoidmlzaXRvcnMiO2k6OTcwO3M6NzoiYm91bmNlcyI7aTo1Nzk7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxMzcuMDg5NTM4MTcxNTM2Mjc1OTY3ODEwMDcwMTQyMTQ5OTI1MjMxOTMzNTkzNzU7czo5OiJuZXdWaXNpdHMiO2k6ODA0O3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo2OntzOjk6Iihub3Qgc2V0KSI7aToyMTtzOjc6IkFuZHJvaWQiO2k6NDU7czo1OiJMaW51eCI7aTo0O3M6OToiTWFjaW50b3NoIjtpOjMyNztzOjc6IldpbmRvd3MiO2k6NDQ0O3M6MzoiaU9TIjtpOjIyMDt9czo3OiJicm93c2VyIjthOjEwOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aToyMTtzOjc6InZlcnNpb24iO2E6MTp7aTo1O2k6MjE7fX1zOjU6Ik9wZXJhIjthOjI6e3M6NToidG90YWwiO2k6MztzOjc6InZlcnNpb24iO2E6Mjp7aTo5O3M6MToiMSI7aToxMjtpOjI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0MTtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NDE7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjE4ODtzOjc6InZlcnNpb24iO2E6OTp7aToxODtpOjQ7aToyMDtpOjI7aToyMTtpOjExO2k6MjI7aToxNjU7aToyMztzOjE6IjEiO2k6MTE7czoxOiIxIjtpOjEyO2k6MjtpOjE3O3M6MToiMSI7aTozO3M6MToiMSI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aToxNzI7czo3OiJ2ZXJzaW9uIjthOjExOntpOjE1O2k6NjA7aToxMztpOjY7aTozO2k6NDtpOjEwO2k6MjtpOjEyO2k6NztpOjE0O2k6NTtpOjE2O2k6ODE7aTo1O3M6MToiMyI7aToxMTtzOjE6IjEiO2k6NDtpOjI7aTo4O3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtpOjQyMDtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NDIwO319czoyMDoiSUUgd2l0aCBDaHJvbWUgRnJhbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1O3M6NzoidmVyc2lvbiI7YToyOntpOjc7czoxOiIzIjtpOjg7czoxOiIyIjt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6MTkyO3M6NzoidmVyc2lvbiI7YTozOntpOjc7czoxOiI4IjtpOjg7czoyOiI1NyI7aTo5O3M6MzoiMTI3Ijt9fXM6OToiU2VhTW9ua2V5IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NjoiMi4xMy4xIjtzOjE6IjEiO319czoxNToiU2FmYXJpIChpbi1hcHApIjthOjI6e3M6NToidG90YWwiO3M6MjoiMTgiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoyOiIxOCI7fX19fXM6MTA6IjIwMTItMTAtMjEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToxMzQ2O3M6OToicGFnZVZpZXdzIjtpOjQ2ODU7czo2OiJ2aXNpdHMiO2k6MTM0NjtzOjg6InZpc2l0b3JzIjtpOjEyMzk7czo3OiJib3VuY2VzIjtpOjc3NTtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjEyNS4yMTAyNTI2MDAyOTcxODE0MDIzMTEyNTI4OTU3NDI2NTQ4MDA0MTUwMzkwNjI1O3M6OToibmV3VmlzaXRzIjtpOjEwNjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjk6e3M6OToiKG5vdCBzZXQpIjtpOjIwO3M6NzoiQW5kcm9pZCI7aTo0MztzOjEwOiJCbGFja0JlcnJ5IjtpOjI7czo5OiJHb29nbGUgVFYiO3M6MToiMSI7czo1OiJMaW51eCI7aTo5O3M6OToiTWFjaW50b3NoIjtpOjQxNDtzOjc6IldpbmRvd3MiO2k6NTc5O3M6MTM6IldpbmRvd3MgUGhvbmUiO3M6MToiMSI7czozOiJpT1MiO2k6Mjc3O31zOjc6ImJyb3dzZXIiO2E6MTA6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjIwO3M6NzoidmVyc2lvbiI7YToxOntpOjU7aToyMDt9fXM6MTA6Ik9wZXJhIE1pbmkiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiI0LjIuMTgxNTQiO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0MTtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NDE7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjIzOTtzOjc6InZlcnNpb24iO2E6MTA6e2k6MTg7aTo1O2k6MTE7aToyO2k6MjI7aToyMDQ7aToyMDtpOjM7aToyMTtpOjE4O2k6MjM7aToyO2k6MTU7czoxOiIxIjtpOjE3O2k6MjtpOjI0O3M6MToiMSI7aTo5O3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtpOjU0MjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NTQyO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6MjM3O3M6NzoidmVyc2lvbiI7YToxMzp7aToxNjtpOjEzMDtpOjM7aTo4O2k6MTI7aTo2O2k6MTU7aTo3MDtpOjU7czoxOiIxIjtpOjk7czoxOiIyIjtpOjEwO3M6MToiMiI7aToxMTtzOjE6IjIiO2k6MTM7aTo0O2k6MTQ7czoxOiI4IjtpOjE3O3M6MToiMSI7aTo3O3M6MToiMSI7aTo4O2k6Mjt9fXM6NToiT3BlcmEiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2O3M6NzoidmVyc2lvbiI7YToxOntpOjEyO2k6Njt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6MjQ0O3M6NzoidmVyc2lvbiI7YTo0OntpOjY7czoxOiIxIjtpOjc7czoyOiIxMCI7aTo4O3M6MjoiNjkiO2k6OTtpOjE2NDt9fXM6OToiU2VhTW9ua2V5IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NjoiMi4xMy4xIjtzOjE6IjEiO319czoxNToiU2FmYXJpIChpbi1hcHApIjthOjI6e3M6NToidG90YWwiO3M6MjoiMTUiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoyOiIxNSI7fX19fXM6MTA6IjIwMTItMTAtMjIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToxNjYzO3M6OToicGFnZVZpZXdzIjtpOjU2MTM7czo2OiJ2aXNpdHMiO2k6MTY2MztzOjg6InZpc2l0b3JzIjtpOjE0ODE7czo3OiJib3VuY2VzIjtpOjk5NjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjE2Mi4wODU5ODkxNzYxODc2MDk5MjgwOTI1NzAwNDQxMDAyODQ1NzY0MTYwMTU2MjU7czo5OiJuZXdWaXNpdHMiO2k6MTE4NjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6Nzp7czo5OiIobm90IHNldCkiO2k6MjI7czo3OiJBbmRyb2lkIjtpOjM5O3M6MTA6IkJsYWNrQmVycnkiO2k6NDtzOjU6IkxpbnV4IjtpOjExO3M6OToiTWFjaW50b3NoIjtpOjQ2MztzOjc6IldpbmRvd3MiO2k6ODc3O3M6MzoiaU9TIjtpOjI0Nzt9czo3OiJicm93c2VyIjthOjExOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aToyNDtzOjc6InZlcnNpb24iO2E6MTp7aTo1O2k6MjQ7fX1zOjEyOiJub29rIGJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czozOiIxLjAiO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTozODtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6Mzg7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjI5MTtzOjc6InZlcnNpb24iO2E6Nzp7aToxODtpOjQ7aToyMztpOjU7aToxNjtpOjI7aToyMTtpOjE5O2k6MjI7aToyNTk7aToxMDtzOjE6IjEiO2k6MTk7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NTI4O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo1Mjg7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aToyNjU7czo3OiJ2ZXJzaW9uIjthOjE2OntpOjEwO2k6MTA7aToxNjtpOjE0ODtpOjM7aToxMztpOjEyO2k6MTA7aToxNDtpOjk7aToxNTtpOjU2O2k6MjtzOjE6IjEiO2k6NTtzOjE6IjEiO2k6NjtpOjI7aTo5O2k6MztpOjExO3M6MToiMyI7aToxMztpOjM7aToxNztzOjE6IjIiO2k6MTg7czoxOiIyIjtpOjQ7czoxOiIxIjtpOjc7czoxOiIxIjt9fXM6NjoiQ2FtaW5vIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NToiMi4xLjIiO3M6MToiMSI7fX1zOjIwOiJJRSB3aXRoIENocm9tZSBGcmFtZSI7YToyOntzOjU6InRvdGFsIjtpOjg7czo3OiJ2ZXJzaW9uIjthOjM6e2k6NjtzOjE6IjEiO2k6NztzOjE6IjIiO2k6ODtzOjE6IjUiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0ODc7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjEiO2k6NztzOjI6IjM1IjtpOjg7czozOiIyMTQiO2k6OTtzOjM6IjIzNyI7fX1zOjk6IlNlYU1vbmtleSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjY6IjIuMTMuMSI7czoxOiIxIjt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjE5IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMTkiO319fX1zOjEwOiIyMDEyLTEwLTIzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTYzNjtzOjk6InBhZ2VWaWV3cyI7aTo1MTQ2O3M6NjoidmlzaXRzIjtpOjE2MzY7czo4OiJ2aXNpdG9ycyI7aToxNDYxO3M6NzoiYm91bmNlcyI7aTo5NDM7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNTMuMzU1MTM0NDc0MzI3NjI3NDU4NDc4NjczMzYxMjQxODE3NDc0MzY1MjM0Mzc1O3M6OToibmV3VmlzaXRzIjtpOjExNzE7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjg6e3M6OToiKG5vdCBzZXQpIjtpOjE1O3M6NzoiQW5kcm9pZCI7aToyNTtzOjEwOiJCbGFja0JlcnJ5IjtzOjE6IjEiO3M6NToiTGludXgiO2k6NDtzOjk6Ik1hY2ludG9zaCI7aTo0Njk7czo1OiJOb2tpYSI7czoxOiIxIjtzOjc6IldpbmRvd3MiO2k6OTQzO3M6MzoiaU9TIjtpOjE3ODt9czo3OiJicm93c2VyIjthOjEzOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aToxODtzOjc6InZlcnNpb24iO2E6MTp7aTo1O2k6MTg7fX1zOjEwOiJPcGVyYSBNaW5pIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiNS4xIjtzOjE6IjEiO319czoxNToiQW5kcm9pZCBCcm93c2VyIjthOjI6e3M6NToidG90YWwiO2k6MjM7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjIzO319czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTozMDY7czo3OiJ2ZXJzaW9uIjthOjk6e2k6MTg7aTozO2k6MTU7aToyO2k6MTY7czoxOiIxIjtpOjIwO3M6MToiMSI7aToyMTtpOjE3O2k6MjI7aToyNzg7aToyMztpOjI7aToxMjtzOjE6IjEiO2k6MTc7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NDU1O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo0NTU7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aTozMDk7czo3OiJ2ZXJzaW9uIjthOjExOntpOjE2O2k6MTg1O2k6MztpOjEzO2k6MTA7aToxMTtpOjEyO2k6NztpOjEzO2k6NDtpOjE0O2k6MTA7aToxNTtpOjcwO2k6NjtpOjM7aToxMTtzOjE6IjMiO2k6NztzOjE6IjEiO2k6OTtzOjE6IjIiO319czo5OiJLb25xdWVyb3IiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czozOiIzLjUiO3M6MToiMSI7fX1zOjY6IkNhbWlubyI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjU6IjIuMS4yIjtzOjE6IjEiO319czoyMDoiSUUgd2l0aCBDaHJvbWUgRnJhbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1O3M6NzoidmVyc2lvbiI7YToyOntpOjc7czoxOiIyIjtpOjg7czoxOiIzIjt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6NTAwO3M6NzoidmVyc2lvbiI7YTo0OntpOjY7czoxOiIyIjtpOjc7czoyOiIzNyI7aTo4O3M6MzoiMTg0IjtpOjk7czozOiIyNzciO319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjEyO3M6MToiMSI7fX1zOjk6IlNlYU1vbmtleSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjY6IjIuMTMuMSI7czoxOiIxIjt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjE1IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMTUiO319fX1zOjEwOiIyMDEyLTEwLTI0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTU5MztzOjk6InBhZ2VWaWV3cyI7aTo1NjA3O3M6NjoidmlzaXRzIjtpOjE1OTM7czo4OiJ2aXNpdG9ycyI7aToxNDE0O3M6NzoiYm91bmNlcyI7aTo5MzQ7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNDkuODE5ODM2Nzg1OTM4NDkxNjMwMDc2ODk2Mzk5MjU5NTY3MjYwNzQyMTg3NTtzOjk6Im5ld1Zpc2l0cyI7aToxMTQzO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo3OntzOjk6Iihub3Qgc2V0KSI7czoyOiIxNCI7czo3OiJBbmRyb2lkIjtpOjQwO3M6MTA6IkJsYWNrQmVycnkiO3M6MToiMSI7czo1OiJMaW51eCI7aToxMztzOjk6Ik1hY2ludG9zaCI7aTo0Mzk7czo3OiJXaW5kb3dzIjtpOjkyNTtzOjM6ImlPUyI7aToxNjE7fXM6NzoiYnJvd3NlciI7YToxMjp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO2k6MTY7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtpOjE2O319czoxNToiQW5kcm9pZCBCcm93c2VyIjthOjI6e3M6NToidG90YWwiO2k6Mzc7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjM3O319czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTozMjg7czo3OiJ2ZXJzaW9uIjthOjEzOntpOjE4O2k6NTtpOjE2O3M6MToiMyI7aToyMDtpOjQ7aToyMTtpOjE4O2k6MjI7aToyODQ7aToxOTtzOjE6IjEiO2k6MjM7aTo0O2k6MjQ7czoxOiIyIjtpOjEwO3M6MToiMSI7aToxMjtzOjE6IjMiO2k6MTM7czoxOiIxIjtpOjE1O3M6MToiMSI7aTo4O3M6MToiMSI7fX1zOjEwOiJPcGVyYSBNaW5pIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiNy4wLjMwMDI0IjtzOjE6IjEiO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6MjkxO3M6NzoidmVyc2lvbiI7YToxNjp7aToxMjtpOjg7aToxNjtpOjE3NDtpOjM7aToxODtpOjY7aToyO2k6MTA7aTo3O2k6MTE7aTo3O2k6MTM7aTozO2k6MTQ7aTo4O2k6MTU7aTo0NDtpOjE4O3M6MToiMSI7aTo0O2k6MztpOjg7aTozO2k6MTc7czoxOiI1IjtpOjI7czoxOiIxIjtpOjc7aTozO2k6OTtzOjE6IjQiO319czo2OiJTYWZhcmkiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0MDg7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjQwODt9fXM6NjoiQ2FtaW5vIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6NToiMi4xLjIiO3M6MToiMSI7fX1zOjIwOiJJRSB3aXRoIENocm9tZSBGcmFtZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjQiO3M6NzoidmVyc2lvbiI7YToxOntpOjg7czoxOiI0Ijt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6NDg2O3M6NzoidmVyc2lvbiI7YTo0OntpOjY7czoxOiI0IjtpOjc7czoyOiI0NiI7aTo4O3M6MzoiMTcyIjtpOjk7czozOiIyNjQiO319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjEyO3M6MToiMSI7fX1zOjk6IlNlYU1vbmtleSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjY6IjIuMTMuMSI7czoxOiIxIjt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjE5IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMTkiO319fX1zOjEwOiIyMDEyLTEwLTI1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTk3OTtzOjk6InBhZ2VWaWV3cyI7aTo3NzE3O3M6NjoidmlzaXRzIjtpOjE5Nzk7czo4OiJ2aXNpdG9ycyI7aToxNzY1O3M6NzoiYm91bmNlcyI7aTo5OTY7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNTUuNTQ2NzQwNzc4MTcwODAwNDY3NzA1NzAzMzQwNDcwNzkwODYzMDM3MTA5Mzc1O3M6OToibmV3VmlzaXRzIjtpOjE0NTU7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjg6e3M6OToiKG5vdCBzZXQpIjtpOjIyO3M6NzoiQW5kcm9pZCI7aTo2ODtzOjEwOiJCbGFja0JlcnJ5IjtpOjM7czo5OiJDaHJvbWUgT1MiO3M6MToiMiI7czo1OiJMaW51eCI7aToxMztzOjk6Ik1hY2ludG9zaCI7aTo0ODU7czo3OiJXaW5kb3dzIjtpOjExMjY7czozOiJpT1MiO2k6MjYwO31zOjc6ImJyb3dzZXIiO2E6MTA6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjI4O3M6NzoidmVyc2lvbiI7YToxOntpOjU7aToyODt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NTM0O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo1MzQ7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2NjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NjY7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjQzMjtzOjc6InZlcnNpb24iO2E6MTE6e2k6MTg7aTozO2k6MjE7aToxOTtpOjE2O2k6NDtpOjIwO2k6MztpOjIyO2k6MzkwO2k6MTU7czoxOiIxIjtpOjIzO2k6NztpOjEwO3M6MToiMSI7aToxMjtzOjE6IjIiO2k6MTk7czoxOiIxIjtpOjI0O3M6MToiMSI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aTozMjI7czo3OiJ2ZXJzaW9uIjthOjE1OntpOjE1O2k6NDM7aToxMDtpOjk7aToxMjtpOjEwO2k6MztpOjEyO2k6NDtpOjM7aToxMztpOjg7aToxNDtpOjExO2k6MTY7aToxOTk7aTo2O2k6NDtpOjk7aTozO2k6MTE7czoxOiI2IjtpOjE3O3M6MToiMiI7aTo1O2k6MztpOjc7czoxOiIyIjtpOjg7aTo3O319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtpOjY7czo3OiJ2ZXJzaW9uIjthOjI6e2k6MTE7czoxOiIyIjtpOjEyO3M6MToiNCI7fX1zOjIwOiJJRSB3aXRoIENocm9tZSBGcmFtZSI7YToyOntzOjU6InRvdGFsIjtpOjU7czo3OiJ2ZXJzaW9uIjthOjI6e2k6NztzOjE6IjEiO2k6ODtzOjE6IjQiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1NTU7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjMiO2k6NztzOjI6IjQ4IjtpOjg7czozOiIyMjEiO2k6OTtzOjM6IjI4MyI7fX1zOjk6IlNlYU1vbmtleSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjY6IjIuMTMuMSI7czoxOiIxIjt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjMwIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMzAiO319fX1zOjEwOiIyMDEyLTEwLTI2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MjA3NDtzOjk6InBhZ2VWaWV3cyI7aTo4MTc5O3M6NjoidmlzaXRzIjtpOjIwNzQ7czo4OiJ2aXNpdG9ycyI7aToxODQ4O3M6NzoiYm91bmNlcyI7aToxMDU4O3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTU3LjI3MTkzODI4MzUxMDEzODQ4OTg3MzQ5Nzc0Njg4NDgyMjg0NTQ1ODk4NDM3NTtzOjk6Im5ld1Zpc2l0cyI7aToxNTIyO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo5OntzOjk6Iihub3Qgc2V0KSI7aTozMDtzOjc6IkFuZHJvaWQiO2k6NDg7czoxMDoiQmxhY2tCZXJyeSI7aTo1O3M6NToiTGludXgiO2k6NDtzOjk6Ik1hY2ludG9zaCI7aTo1Mjc7czoxMzoiUGxheXN0YXRpb24gMyI7czoxOiIxIjtzOjc6IldpbmRvd3MiO2k6MTE4MztzOjEzOiJXaW5kb3dzIFBob25lIjtzOjE6IjIiO3M6MzoiaU9TIjtpOjI3NDt9czo3OiJicm93c2VyIjthOjEzOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aTozMztzOjc6InZlcnNpb24iO2E6MTp7aTo1O2k6MzM7fX1zOjEwOiJPcGVyYSBNaW5pIjthOjI6e3M6NToidG90YWwiO2k6MztzOjc6InZlcnNpb24iO2E6Mzp7czo5OiI0LjEuMTM1NzIiO3M6MToiMSI7czozOiI1LjEiO3M6MToiMSI7czo5OiI3LjAuMzA1NjciO3M6MToiMSI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTozOTtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6Mzk7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjQyMjtzOjc6InZlcnNpb24iO2E6MTQ6e2k6MTg7aToxMDtpOjEwO2k6MjtpOjE0O3M6MToiMSI7aToxOTtpOjI7aToyMTtpOjE5O2k6MjI7aTozNzM7aToyMztpOjM7aToxMjtzOjE6IjMiO2k6MTU7czoxOiIxIjtpOjE3O3M6MToiMSI7aToyMDtzOjE6IjIiO2k6MjQ7czoxOiIzIjtpOjQ7czoxOiIxIjtpOjU7czoxOiIxIjt9fXM6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtpOjQwMztzOjc6InZlcnNpb24iO2E6MTQ6e2k6MTY7aToyNzQ7aTozO2k6MjE7aToxMDtpOjc7aToxMjtpOjEyO2k6MTM7aTo4O2k6MTQ7aToxNTtpOjE1O2k6NDg7aTo0O3M6MToiMSI7aTo1O3M6MToiMSI7aTo2O2k6NDtpOjExO3M6MToiNSI7aToxNztzOjE6IjMiO2k6ODtpOjI7aTo5O3M6MToiMiI7fX1zOjE0OiJCbGFja0JlcnJ5OTMwMCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6IjUuMC4wLjk3NyI7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NTQwO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo1NDA7fX1zOjU6Ik9wZXJhIjthOjI6e3M6NToidG90YWwiO2k6MztzOjc6InZlcnNpb24iO2E6Mjp7aToxMTtzOjE6IjEiO2k6MTI7czoxOiIyIjt9fXM6MTM6IlBsYXlzdGF0aW9uIDMiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MToiMSI7fX1zOjIwOiJJRSB3aXRoIENocm9tZSBGcmFtZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjYiO3M6NzoidmVyc2lvbiI7YToxOntpOjg7czoxOiI2Ijt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6NTU1O3M6NzoidmVyc2lvbiI7YTo0OntpOjY7czoxOiI1IjtpOjc7czoyOiIyOSI7aTo4O3M6MzoiMjUwIjtpOjk7aToyNzE7fX1zOjk6IlNlYU1vbmtleSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjY6IjIuMTMuMSI7czoxOiIxIjt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjY3IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiNjciO319fX1zOjEwOiIyMDEyLTEwLTI3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTYzNDtzOjk6InBhZ2VWaWV3cyI7aTo2NTA4O3M6NjoidmlzaXRzIjtpOjE2MzQ7czo4OiJ2aXNpdG9ycyI7aToxNDg3O3M6NzoiYm91bmNlcyI7aTo4NDQ7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxMzQuNjU5NzMwNzIyMTU0MjIyMzQ4NzYzMTQxNzgxMDkxNjkwMDYzNDc2NTYyNTtzOjk6Im5ld1Zpc2l0cyI7aToxMjk3O3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxMDp7czo5OiIobm90IHNldCkiO2k6MzI7czo3OiJBbmRyb2lkIjtpOjc4O3M6MTA6IkJsYWNrQmVycnkiO3M6MToiMSI7czo1OiJMaW51eCI7aToxMjtzOjk6Ik1hY2ludG9zaCI7aTo0NTU7czo1OiJOb2tpYSI7czoxOiIyIjtzOjc6IlNhbXN1bmciO3M6MToiMSI7czo3OiJXaW5kb3dzIjtpOjc0MjtzOjEzOiJXaW5kb3dzIFBob25lIjtzOjE6IjEiO3M6MzoiaU9TIjtpOjMxMDt9czo3OiJicm93c2VyIjthOjEyOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aTozMjtzOjc6InZlcnNpb24iO2E6Mjp7aTo0O3M6MToiMSI7aTo1O2k6MzE7fX1zOjEwOiJPcGVyYSBNaW5pIjthOjI6e3M6NToidG90YWwiO2k6NDtzOjc6InZlcnNpb24iO2E6Mzp7czo5OiI2LjEuMjYyNjYiO3M6MToiMSI7czo5OiI3LjAuMzA2OTciO3M6MToiMSI7czo1OiI3LjAuMyI7czoxOiIyIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NTg0O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo1ODQ7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo3NDtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NzQ7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjI2NztzOjc6InZlcnNpb24iO2E6OTp7aToxODtzOjE6IjMiO2k6MjA7czoxOiIxIjtpOjIxO2k6MTQ7aToxNztzOjE6IjEiO2k6MjI7aToyNDA7aToyMztpOjM7aToxMTtzOjE6IjEiO2k6MTM7czoxOiIxIjtpOjE1O2k6Mzt9fXM6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtpOjI3OTtzOjc6InZlcnNpb24iO2E6MTU6e2k6MTY7aToxOTE7aTozO2k6MTE7aToxMDtpOjEzO2k6MTE7aTo0O2k6MTI7aToxMTtpOjEzO2k6MztpOjE0O2k6NjtpOjE1O2k6Mjc7aToxODtzOjE6IjEiO2k6MjtzOjE6IjEiO2k6NTtpOjQ7aTo4O3M6MToiMSI7aTo5O2k6MjtpOjE3O3M6MToiMyI7aTo0O3M6MToiMSI7fX1zOjU6Ik9wZXJhIjthOjI6e3M6NToidG90YWwiO2k6NDtzOjc6InZlcnNpb24iO2E6Mjp7aToxMjtpOjM7aTo5O3M6MToiMSI7fX1zOjg6IlJvY2tNZWx0IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtzOjE6IjEiO319czo4OiJOb2tpYTMwMiI7YToyOntzOjU6InRvdGFsIjtzOjE6IjIiO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IjUuMCI7czoxOiIyIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiMyI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjMiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTozMzk7czo3OiJ2ZXJzaW9uIjthOjU6e2k6MTA7czoxOiIxIjtpOjY7czoxOiIzIjtpOjc7czoyOiIxMyI7aTo4O3M6MjoiOTkiO2k6OTtpOjIyMzt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjQ1IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiNDUiO319fX1zOjEwOiIyMDEyLTEwLTI4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTgyNTtzOjk6InBhZ2VWaWV3cyI7aTo3MjM0O3M6NjoidmlzaXRzIjtpOjE4MjU7czo4OiJ2aXNpdG9ycyI7aToxNjExO3M6NzoiYm91bmNlcyI7aTo5NTE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNDAuNjEzMTUwNjg0OTMxNDk3MzgwNjAzMTA0ODI5Nzg4MjA4MDA3ODEyNTtzOjk6Im5ld1Zpc2l0cyI7aToxMzcxO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo4OntzOjk6Iihub3Qgc2V0KSI7aToyMTtzOjc6IkFuZHJvaWQiO2k6ODk7czoxMDoiQmxhY2tCZXJyeSI7czoxOiIyIjtzOjU6IkxpbnV4IjtpOjE0O3M6OToiTWFjaW50b3NoIjtpOjUyMjtzOjc6IldpbmRvd3MiO2k6ODMyO3M6MTM6IldpbmRvd3MgUGhvbmUiO3M6MToiMSI7czozOiJpT1MiO2k6MzQ0O31zOjc6ImJyb3dzZXIiO2E6MTE6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtpOjIzO3M6NzoidmVyc2lvbiI7YToxOntpOjU7aToyMzt9fXM6MTA6Ik9wZXJhIE1pbmkiO2E6Mjp7czo1OiJ0b3RhbCI7aToyO3M6NzoidmVyc2lvbiI7YToyOntzOjk6IjQuMS4xMzkwNyI7czoxOiIxIjtzOjk6IjcuMC4yOTk1MiI7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NjcyO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo2NzI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo3NjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6NzY7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjM0MTtzOjc6InZlcnNpb24iO2E6Nzp7aToxODtzOjE6IjkiO2k6MTk7aTozO2k6MjA7aTo4O2k6MjE7aToxNTtpOjIyO2k6Mjk3O2k6MTc7aToyO2k6MjM7aTo3O319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6MzAwO3M6NzoidmVyc2lvbiI7YToxNjp7aToxNTtpOjM1O2k6MTY7aToxOTc7aToxNDtpOjE4O2k6OTtpOjM7aToxMDtpOjQ7aToxMjtpOjEzO2k6MTM7aTozO2k6MjtzOjE6IjEiO2k6MztpOjc7aTo0O2k6MztpOjU7aTozO2k6NztpOjI7aToxMTtzOjE6IjIiO2k6MTc7czoxOiI0IjtpOjY7czoxOiIyIjtpOjg7czoxOiIzIjt9fXM6NToiT3BlcmEiO2E6Mjp7czo1OiJ0b3RhbCI7aTo5O3M6NzoidmVyc2lvbiI7YTozOntzOjQ6Ik1vYmkiO3M6MToiMSI7aToxMjtzOjE6IjYiO2k6OTtzOjE6IjIiO319czo2OiJDYW1pbm8iO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czo1OiIyLjEuMiI7czoxOiIxIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO2k6MztzOjc6InZlcnNpb24iO2E6Mjp7aTo3O3M6MToiMSI7aTo4O3M6MToiMiI7fX1zOjE3OiJJbnRlcm5ldCBFeHBsb3JlciI7YToyOntzOjU6InRvdGFsIjtpOjM0OTtzOjc6InZlcnNpb24iO2E6NDp7aTo2O3M6MToiMSI7aTo3O3M6MToiNiI7aTo4O3M6MjoiOTYiO2k6OTtpOjI0Njt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjQ5IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiNDkiO319fX1zOjEwOiIyMDEyLTEwLTI5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MjEzNDtzOjk6InBhZ2VWaWV3cyI7aTo4MTc1O3M6NjoidmlzaXRzIjtpOjIxMzQ7czo4OiJ2aXNpdG9ycyI7aToxOTEyO3M6NzoiYm91bmNlcyI7aToxMTE5O3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTUwLjA1NDgyNjYxNjY4MjI3NzA2Mjg2NDE1OTIzMzg2ODEyMjEwMDgzMDA3ODEyNTtzOjk6Im5ld1Zpc2l0cyI7aToxNTY4O3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxMDp7czo5OiIobm90IHNldCkiO3M6MToiOCI7czo3OiJBbmRyb2lkIjtpOjY3O3M6MTA6IkJsYWNrQmVycnkiO2k6NDtzOjU6IkxpbnV4IjtpOjE0O3M6OToiTWFjaW50b3NoIjtpOjYyMDtzOjU6Ik5va2lhIjtzOjE6IjEiO3M6MTM6IlBsYXlzdGF0aW9uIDMiO3M6MToiMSI7czo3OiJXaW5kb3dzIjtpOjExNjA7czoxMzoiV2luZG93cyBQaG9uZSI7czoxOiIxIjtzOjM6ImlPUyI7aToyNTg7fXM6NzoiYnJvd3NlciI7YToxMjp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO2k6MTg7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtpOjE4O319czoxNToiQW5kcm9pZCBCcm93c2VyIjthOjI6e3M6NToidG90YWwiO2k6NjM7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjYzO319czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0NDE7czo3OiJ2ZXJzaW9uIjthOjE1OntpOjE4O2k6NztpOjExO3M6MToiMiI7aToyMDtpOjY7aToyMjtpOjQwMTtpOjEwO3M6MToiMSI7aToxMztzOjE6IjEiO2k6MTY7aToyO2k6MjE7aToxMDtpOjIzO3M6MToiMyI7aToxMjtzOjE6IjEiO2k6MTQ7aToyO2k6MTc7czoxOiIxIjtpOjE5O3M6MToiMSI7aToyNDtzOjE6IjIiO2k6NjtzOjE6IjEiO319czoxMDoiT3BlcmEgTWluaSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6IjcuNS4zMTY1NyI7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NjI3O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo2Mjc7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aTo0MTE7czo3OiJ2ZXJzaW9uIjthOjE1OntpOjE0O2k6MjA7aToxNjtpOjI4MztpOjM7aToxNTtpOjY7aTozO2k6MTA7aToxNjtpOjEyO2k6OTtpOjEzO2k6OTtpOjE1O2k6MzQ7aToxNztpOjQ7aTo0O2k6MjtpOjU7czoxOiIyIjtpOjc7czoxOiIyIjtpOjg7aTo1O2k6MTE7czoxOiI1IjtpOjk7czoxOiIyIjt9fXM6NzoiTW96aWxsYSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjU6IjkuMC4xIjtzOjE6IjEiO319czoxMzoiUGxheXN0YXRpb24gMyI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoxOiIxIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiOCI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjgiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1Mzk7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjIiO2k6NztzOjI6IjMzIjtpOjg7czozOiIyMDIiO2k6OTtpOjMwMjt9fXM6NToiT3BlcmEiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToxMjtzOjE6IjEiO319czoxNToiU2FmYXJpIChpbi1hcHApIjthOjI6e3M6NToidG90YWwiO3M6MjoiMjMiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoyOiIyMyI7fX19fXM6MTA6IjIwMTItMTAtMzAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyMDc1O3M6OToicGFnZVZpZXdzIjtpOjc2MTY7czo2OiJ2aXNpdHMiO2k6MjA3NTtzOjg6InZpc2l0b3JzIjtpOjE4NDM7czo3OiJib3VuY2VzIjtpOjExMjM7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNTcuMTAxNjg2NzQ2OTg3OTUxNDM3NDA0MjYyODMzMjk3MjUyNjU1MDI5Mjk2ODc1O3M6OToibmV3VmlzaXRzIjtpOjE1Mjc7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjg6e3M6OToiKG5vdCBzZXQpIjtpOjE0O3M6NzoiQW5kcm9pZCI7aTo2NTtzOjEwOiJCbGFja0JlcnJ5IjtpOjQ7czo1OiJMaW51eCI7aToxOTtzOjk6Ik1hY2ludG9zaCI7aTo1NTY7czoxMzoiUGxheXN0YXRpb24gMyI7czoxOiIyIjtzOjc6IldpbmRvd3MiO2k6MTE1OTtzOjM6ImlPUyI7aToyNTY7fXM6NzoiYnJvd3NlciI7YToxMTp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO2k6MTI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtpOjEyO319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtpOjk7czo3OiJ2ZXJzaW9uIjthOjI6e2k6OTtzOjE6IjEiO2k6MTI7aTo4O319czoxMDoiT3BlcmEgTWluaSI7YToyOntzOjU6InRvdGFsIjtpOjM7czo3OiJ2ZXJzaW9uIjthOjM6e3M6OToiNC4xLjEzOTA3IjtzOjE6IjEiO3M6OToiNy4wLjMwNTY3IjtzOjE6IjEiO3M6OToiNy41LjMxNjU3IjtzOjE6IjEiO319czo2OiJTYWZhcmkiO2E6Mjp7czo1OiJ0b3RhbCI7aTo2MDY7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjYwNjt9fXM6MTU6IkFuZHJvaWQgQnJvd3NlciI7YToyOntzOjU6InRvdGFsIjtpOjYxO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo2MTt9fXM6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO2k6Mzk2O3M6NzoidmVyc2lvbiI7YTo3OntpOjE4O2k6NDtpOjIwO2k6NjtpOjIxO2k6MTQ7aTo1O3M6MToiMSI7aToxOTtzOjE6IjEiO2k6MjI7aTozNjk7aToyNDtzOjE6IjEiO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO2k6MzgwO3M6NzoidmVyc2lvbiI7YToxNjp7aToxMTtpOjU7aToxMjtpOjEwO2k6MTM7aTo3O2k6MTQ7aToxNjtpOjE2O2k6MjYxO2k6MTk7czoxOiIxIjtpOjM7aToxNTtpOjc7aTo1O2k6ODtpOjU7aToxMDtpOjg7aToxNTtpOjM2O2k6NDtzOjE6IjIiO2k6NjtpOjI7aToxNztzOjE6IjEiO2k6NTtzOjE6IjEiO2k6OTtzOjE6IjUiO319czoxMzoiUGxheXN0YXRpb24gMyI7YToyOntzOjU6InRvdGFsIjtzOjE6IjIiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6Iihub3Qgc2V0KSI7czoxOiIyIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO3M6MToiNCI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjQiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTo1Nzk7czo3OiJ2ZXJzaW9uIjthOjQ6e2k6NjtzOjE6IjYiO2k6NztzOjI6IjQwIjtpOjg7czozOiIxOTciO2k6OTtzOjM6IjMzNiI7fX1zOjE1OiJTYWZhcmkgKGluLWFwcCkiO2E6Mjp7czo1OiJ0b3RhbCI7czoyOiIyMyI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjI6IjIzIjt9fX19czoxMDoiMjAxMi0xMC0zMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjIwMzI7czo5OiJwYWdlVmlld3MiO2k6Njg3ODtzOjY6InZpc2l0cyI7aToyMDMyO3M6ODoidmlzaXRvcnMiO2k6MTc5MztzOjc6ImJvdW5jZXMiO2k6MTExMjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjEzMy45MjAyNzU1OTA1NTExNzA4MDc4ODQ3NzMyMzk0OTMzNzAwNTYxNTIzNDM3NTtzOjk6Im5ld1Zpc2l0cyI7aToxNDYyO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTo5OntzOjk6Iihub3Qgc2V0KSI7aToxMDtzOjc6IkFuZHJvaWQiO2k6NjI7czoxMDoiQmxhY2tCZXJyeSI7aTo1O3M6NToiTGludXgiO2k6MTU7czo5OiJNYWNpbnRvc2giO2k6NjEzO3M6NToiTm9raWEiO3M6MToiMSI7czo3OiJXaW5kb3dzIjtpOjEwODI7czoxMzoiV2luZG93cyBQaG9uZSI7czoxOiIxIjtzOjM6ImlPUyI7aToyNDM7fXM6NzoiYnJvd3NlciI7YToxNTp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO2k6MTA7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtpOjEwO319czoxMDoiT3BlcmEgTWluaSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjk6IjQuMi4yMzQ0OSI7czoxOiIxIjt9fXM6MTQ6IlBoYW50b20uanMgYm90IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjE6IjEiO319czozNjoiY2U0OTc5ZTYtNjFjMC00NDYyLWI4MWUtM2FjZjNiMWNlNDY0IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjE6IjEiO319czoxNToiQW5kcm9pZCBCcm93c2VyIjthOjI6e3M6NToidG90YWwiO2k6NTg7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjU4O319czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0MjQ7czo3OiJ2ZXJzaW9uIjthOjg6e2k6MTg7aTo2O2k6MTc7czoxOiIyIjtpOjIzO2k6NDtpOjIwO2k6NjtpOjIxO2k6MTc7aToyMjtpOjM4NjtpOjExO3M6MToiMSI7aToxMjtzOjE6IjIiO319czoxNDoiQmxhY2tCZXJyeTk3MDAiO2E6Mjp7czo1OiJ0b3RhbCI7aToyO3M6NzoidmVyc2lvbiI7YToyOntzOjk6IjUuMC4wLjU5MyI7czoxOiIxIjtzOjk6IjUuMC4wLjk3OSI7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6NjA4O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTo2MDg7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aTozODU7czo3OiJ2ZXJzaW9uIjthOjE0OntpOjE0O2k6MTA7aToxNTtpOjM4O2k6MTY7aToyODA7aTozO2k6MTM7aToxMDtpOjc7aToxMTtpOjc7aToxMjtpOjEwO2k6MTM7aToyO2k6MTc7aTo2O2k6NDtpOjQ7aTo1O2k6MztpOjk7czoxOiIyIjtpOjY7czoxOiIxIjtpOjc7czoxOiIyIjt9fXM6OToiS29ucXVlcm9yIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiMy41IjtzOjE6IjEiO319czo1OiJPcGVyYSI7YToyOntzOjU6InRvdGFsIjtpOjc7czo3OiJ2ZXJzaW9uIjthOjI6e2k6MTI7aTo2O2k6OTtzOjE6IjEiO319czoxMDoiTm9raWFDMi0wMSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IjUuMCI7czoxOiIxIjt9fXM6MjA6IklFIHdpdGggQ2hyb21lIEZyYW1lIjthOjI6e3M6NToidG90YWwiO2k6NTtzOjc6InZlcnNpb24iO2E6Mzp7aTo3O3M6MToiMSI7aTo4O3M6MToiMyI7aTo5O3M6MToiMSI7fX1zOjE3OiJJbnRlcm5ldCBFeHBsb3JlciI7YToyOntzOjU6InRvdGFsIjtpOjQ5OTtzOjc6InZlcnNpb24iO2E6NTp7aToxMDtzOjE6IjEiO2k6NjtzOjE6IjMiO2k6NztzOjI6IjMyIjtpOjg7czozOiIxNjIiO2k6OTtpOjMwMTt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjI5IjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMjkiO319fX1zOjEwOiIyMDEyLTExLTAxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MTIwNjtzOjk6InBhZ2VWaWV3cyI7aTozODkxO3M6NjoidmlzaXRzIjtpOjEyMDY7czo4OiJ2aXNpdG9ycyI7aToxMTA3O3M6NzoiYm91bmNlcyI7aTo2NjM7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxMjguNjE1MjU3MDQ4MDkyODcxNjI3ODg2Nzc5NjA2MzQyMzE1NjczODI4MTI1O3M6OToibmV3VmlzaXRzIjtpOjkwMTtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6OTp7czo5OiIobm90IHNldCkiO3M6MToiNiI7czo3OiJBbmRyb2lkIjtpOjM5O3M6MTA6IkJsYWNrQmVycnkiO2k6NjtzOjk6Ikdvb2dsZSBUViI7czoxOiIxIjtzOjU6IkxpbnV4IjtpOjY7czo5OiJNYWNpbnRvc2giO2k6MjkwO3M6NToiTm9raWEiO3M6MToiMSI7czo3OiJXaW5kb3dzIjtpOjczODtzOjM6ImlPUyI7aToxMTk7fXM6NzoiYnJvd3NlciI7YTo5OntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7aToxMjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O2k6MTI7fX1zOjE1OiJBbmRyb2lkIEJyb3dzZXIiO2E6Mjp7czo1OiJ0b3RhbCI7aTozNztzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO2k6Mzc7fX1zOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtpOjI0MDtzOjc6InZlcnNpb24iO2E6MTA6e2k6MTg7czoxOiIyIjtpOjExO2k6MztpOjIyO2k6MjE5O2k6MTQ7aToyO2k6MjE7aTo3O2k6MjM7aTozO2k6MjQ7czoxOiIxIjtpOjEwO3M6MToiMSI7aToxNjtzOjE6IjEiO2k6MTc7czoxOiIxIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO2k6MzA1O3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7aTozMDU7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7aToyMDk7czo3OiJ2ZXJzaW9uIjthOjEzOntpOjEzO2k6NDtpOjE2O2k6MTQ4O2k6MztpOjg7aToxMDtpOjU7aToxNDtpOjk7aToxNTtpOjE0O2k6NTtpOjI7aToxMTtzOjE6IjciO2k6MTI7czoxOiI1IjtpOjE3O3M6MToiMyI7aTo0O3M6MToiMiI7aTo4O3M6MToiMSI7aTo5O3M6MToiMSI7fX1zOjg6Ik5va2lhMjAwIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiMi4wIjtzOjE6IjEiO319czoyMDoiSUUgd2l0aCBDaHJvbWUgRnJhbWUiO2E6Mjp7czo1OiJ0b3RhbCI7aTo0O3M6NzoidmVyc2lvbiI7YToyOntpOjc7czoxOiIxIjtpOjg7czoxOiIzIjt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO2k6Mzg4O3M6NzoidmVyc2lvbiI7YTo0OntpOjEwO3M6MToiMyI7aTo3O3M6MjoiMzIiO2k6ODtzOjM6IjE1NyI7aTo5O3M6MzoiMTk2Ijt9fXM6MTU6IlNhZmFyaSAoaW4tYXBwKSI7YToyOntzOjU6InRvdGFsIjtzOjI6IjEwIjtzOjc6InZlcnNpb24iO2E6MTp7czo5OiIobm90IHNldCkiO3M6MjoiMTAiO319fX19fWk6NjUwMjk0MjE7YTo0OntzOjE4OiJzb3VyY2VzX3N0YXRpc3RpY3MiO2E6MTp7czo4OiIoZGlyZWN0KSI7YTozOntzOjY6InZpc2l0cyI7czoyOiIyNCI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6NzoiNzA2LjYyNSI7fX1zOjEyOiJsYXN0X3VwZGF0ZWQiO2k6MTM1MTc5MzMxMztzOjE1OiJwYWdlX3N0YXRpc3RpY3MiO2E6NTp7aToxO2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoiamFycmV0dGJhcm5ldHQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjE6Ii8iO3M6OToicGFnZVRpdGxlIjtzOjIyOiJIb21lID4gSmFycmV0dCBCYXJuZXR0IjtzOjY6InZpc2l0cyI7czoyOiIyNCI7czo3OiJib3VuY2VzIjtzOjE6IjgiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MTg6IjEwOS4yMjIyMjIyMjIyMjIyMyI7fWk6MjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6ImphcnJldHRiYXJuZXR0LmNvbSI7czo4OiJwYWdlUGF0aCI7czo3OiIvZGVzaWduIjtzOjk6InBhZ2VUaXRsZSI7czoyMjoiSG9tZSA+IEphcnJldHQgQmFybmV0dCI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6NDoiMTAuMCI7fWk6MzthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6ImphcnJldHRiYXJuZXR0LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMjoiL2RldmVsb3BtZW50IjtzOjk6InBhZ2VUaXRsZSI7czoyMjoiSG9tZSA+IEphcnJldHQgQmFybmV0dCI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6MzoiOC4wIjt9aTo0O2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoiamFycmV0dGJhcm5ldHQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjIyOiIvaW5kZXgucGhwL2RldmVsb3BtZW50IjtzOjk6InBhZ2VUaXRsZSI7czoyMjoiSG9tZSA+IEphcnJldHQgQmFybmV0dCI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6NjoiMTIwNS4wIjt9aTo1O2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoiamFycmV0dGJhcm5ldHQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEyOiIvcGhvdG9ncmFwaHkiO3M6OToicGFnZVRpdGxlIjtzOjIyOiJIb21lID4gSmFycmV0dCBCYXJuZXR0IjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czozOiIwLjAiO319czoxNjoidGFibGVfc3RhdGlzdGljcyI7YTo5Mzp7czoxMDoiMjAxMi0wOC0wMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTAzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0wNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDUiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTA2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0wNyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDgiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTA5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xMCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTEyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTE1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTE4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTIxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjMiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTI0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yNSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjYiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTI3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTMwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0zMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTAyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTA1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTA4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTExIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTMiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTE0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xNSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTYiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTE3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTIwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTIzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjUiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTI2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yNyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjgiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTI5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0zMCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTAyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTA1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTA4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTExIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0xMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjEiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MDtzOjk6Im5ld1Zpc2l0cyI7czoxOiIxIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMTMiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToxMTtzOjk6InBhZ2VWaWV3cyI7aTo4OTtzOjY6InZpc2l0cyI7aToxMTtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjM7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDo2MDIuNDU0NTQ1NDU0NTQ1NDk1ODg2MTIyODA3ODYwMzc0NDUwNjgzNTkzNzU7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO2k6MTE7fXM6NzoiYnJvd3NlciI7YToyOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjciO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiNyI7fX1zOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiI0IjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O3M6MToiNCI7fX19fXM6MTA6IjIwMTItMTAtMTQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTE1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MjtzOjk6InBhZ2VWaWV3cyI7aTozO3M6NjoidmlzaXRzIjtpOjI7czo4OiJ2aXNpdG9ycyI7aToyO3M6NzoiYm91bmNlcyI7aToxO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTA7czo5OiJuZXdWaXNpdHMiO2k6MTtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6Mjp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7czo3OiJXaW5kb3dzIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMjtzOjE6IjEiO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIxIjt9fX19czoxMDoiMjAxMi0xMC0xNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjQ7czo5OiJwYWdlVmlld3MiO2k6NjM7czo2OiJ2aXNpdHMiO2k6NDtzOjg6InZpc2l0b3JzIjtpOjM7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoyNTIyLjI1O3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjQ7fXM6NzoiYnJvd3NlciI7YTozOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToxNjtzOjE6IjEiO319czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjIiO319fX1zOjEwOiIyMDEyLTEwLTE3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiNyI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNDg7czo5OiJuZXdWaXNpdHMiO3M6MToiMSI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTE4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0xOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjQiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6NTc7czo5OiJuZXdWaXNpdHMiO3M6MToiMCI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTIwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0yMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMjIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyO3M6OToicGFnZVZpZXdzIjtpOjM7czo2OiJ2aXNpdHMiO2k6MjtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDo5O3M6OToibmV3VmlzaXRzIjtpOjI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjI7fXM6NzoiYnJvd3NlciI7YToyOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7czoxOiIxIjt9fX19czoxMDoiMjAxMi0xMC0yMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjEiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MDtzOjk6Im5ld1Zpc2l0cyI7czoxOiIwIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMjQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTI1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0yNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMjciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIxIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjA7czo5OiJuZXdWaXNpdHMiO3M6MToiMCI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTI4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0yOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMzAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTMxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMS0wMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fX19aTo1MDYxNTI0OTthOjQ6e3M6MTg6InNvdXJjZXNfc3RhdGlzdGljcyI7YTo3OntzOjg6IihkaXJlY3QpIjthOjM6e3M6NjoidmlzaXRzIjtzOjI6IjcwIjtzOjc6ImJvdW5jZXMiO3M6MjoiMzIiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MTg6IjQ5OS45ODU3MTQyODU3MTQyNyI7fXM6NjoiZ29vZ2xlIjthOjM6e3M6NjoidmlzaXRzIjtzOjE6IjciO3M6NzoiYm91bmNlcyI7czoxOiI0IjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIyNC4yODU3MTQyODU3MTQyODUiO31zOjEwOiJnb29nbGUuY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjE6IjMiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjE4OiIzNS4zMzMzMzMzMzMzMzMzMzYiO31zOjI2OiJqYXJyZXR0YmFybmV0dC5zbXVnbXVnLmNvbSI7YTozOntzOjY6InZpc2l0cyI7czoxOiIyIjtzOjc6ImJvdW5jZXMiO3M6MToiMiI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czozOiIwLjAiO31zOjQ6ImJpbmciO2E6Mzp7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO3M6MzoiMC4wIjt9czoxMjoiZmFjZWJvb2suY29tIjthOjM6e3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtzOjM6IjAuMCI7fXM6MTI6ImxhYi5oYWtpbS5zZSI7YTozOntzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7czozOiIwLjAiO319czoxMjoibGFzdF91cGRhdGVkIjtpOjEzNTE3OTMzMjk7czoxNToicGFnZV9zdGF0aXN0aWNzIjthOjEwOntpOjE7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJqYXJyZXR0YmFybmV0dC5jb20iO3M6ODoicGFnZVBhdGgiO3M6MToiLyI7czo5OiJwYWdlVGl0bGUiO3M6MjI6IkhvbWUgPiBKYXJyZXR0IEJhcm5ldHQiO3M6NjoidmlzaXRzIjtpOjgyO3M6NzoiYm91bmNlcyI7aTo0MTtzOjEzOiJhdmdUaW1lT25QYWdlIjtkOjEwMC41NDEzMDg1NTU5NDI3MjAyMDQyNTcwNTcwNDgzODAzNzQ5MDg0NDcyNjU2MjU7fWk6MjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MjY6ImphcnJldHRiYXJuZXR0LnNtdWdtdWcuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjg1OiIvaW5kZXgubWc/b2ZmZXJlZD15ZXMmbG9naW49MSZ1c2VyPXNtdWdtdWdAamFycmV0dGJhcm5ldHQuY29tJnRlbXBwYXNzPTc0ZTAwM2Y2YTIxYmVmIjtzOjk6InBhZ2VUaXRsZSI7czo1MzoiU211Z011Zy4gUGhvdG8gU2hhcmluZy4gWW91ciBQaG90b3MgTG9vayBCZXR0ZXIgSGVyZS4iO3M6NjoidmlzaXRzIjtzOjE6IjIiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjM6IjMuMCI7fWk6MzthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6InNlY3VyZS5zbXVnbXVnLmNvbSI7czo4OiJwYWdlUGF0aCI7czozMDoiL3NldHRpbmdzLz9uaWNrPWphcnJldHRiYXJuZXR0IjtzOjk6InBhZ2VUaXRsZSI7czoyNjoiU211Z011ZyAtIEFjY291bnQgU2V0dGluZ3MiO3M6NjoidmlzaXRzIjtzOjE6IjIiO3M6NzoiYm91bmNlcyI7czoxOiIyIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjM6IjAuMCI7fWk6NDthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6ImphcnJldHRiYXJuZXR0LmNvbSI7czo4OiJwYWdlUGF0aCI7czo2OiIvYWJvdXQiO3M6OToicGFnZVRpdGxlIjtzOjIyOiJIb21lID4gSmFycmV0dCBCYXJuZXR0IjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czo1OiIxMzIuNSI7fWk6NTthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6ImphcnJldHRiYXJuZXR0LmNvbSI7czo4OiJwYWdlUGF0aCI7czo3OiIvZGVzaWduIjtzOjk6InBhZ2VUaXRsZSI7czoyMjoiSG9tZSA+IEphcnJldHQgQmFybmV0dCI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblBhZ2UiO3M6NDoiMTAuMCI7fWk6NjthOjY6e3M6ODoiaG9zdG5hbWUiO3M6MTg6ImphcnJldHRiYXJuZXR0LmNvbSI7czo4OiJwYWdlUGF0aCI7czoxMDoiL2RldmVsb3BlciI7czo5OiJwYWdlVGl0bGUiO3M6MjI6IkhvbWUgPiBKYXJyZXR0IEJhcm5ldHQiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25QYWdlIjtzOjU6IjE3Ljc1Ijt9aTo3O2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoiamFycmV0dGJhcm5ldHQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEyOiIvZGV2ZWxvcG1lbnQiO3M6OToicGFnZVRpdGxlIjtzOjIyOiJIb21lID4gSmFycmV0dCBCYXJuZXR0IjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czozOiI4LjAiO31pOjg7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjI2OiJqYXJyZXR0YmFybmV0dC5zbXVnbXVnLmNvbSI7czo4OiJwYWdlUGF0aCI7czo3MzoiL2luZGV4Lm1nP2xvZ2luPTEmdXNlcj1zbXVnbXVnQGphcnJldHRiYXJuZXR0LmNvbSZ0ZW1wcGFzcz03NGUwMDNmNmEyMWJlZiI7czo5OiJwYWdlVGl0bGUiO3M6NTM6IlNtdWdNdWcuIFBob3RvIFNoYXJpbmcuIFlvdXIgUGhvdG9zIExvb2sgQmV0dGVyIEhlcmUuIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czozOiIyLjAiO31pOjk7YTo2OntzOjg6Imhvc3RuYW1lIjtzOjE4OiJqYXJyZXR0YmFybmV0dC5jb20iO3M6ODoicGFnZVBhdGgiO3M6MjI6Ii9pbmRleC5waHAvZGV2ZWxvcG1lbnQiO3M6OToicGFnZVRpdGxlIjtzOjIyOiJIb21lID4gSmFycmV0dCBCYXJuZXR0IjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czo2OiIxMjA1LjAiO31pOjEwO2E6Njp7czo4OiJob3N0bmFtZSI7czoxODoiamFycmV0dGJhcm5ldHQuY29tIjtzOjg6InBhZ2VQYXRoIjtzOjEyOiIvcGhvdG9ncmFwaHkiO3M6OToicGFnZVRpdGxlIjtzOjIyOiJIb21lID4gSmFycmV0dCBCYXJuZXR0IjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uUGFnZSI7czozOiIwLjAiO319czoxNjoidGFibGVfc3RhdGlzdGljcyI7YTo5Mzp7czoxMDoiMjAxMi0wOC0wMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTAzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiMSI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtzOjE6IjAiO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjE6e3M6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjE0O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMDgtMDQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIxIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjA7czo5OiJuZXdWaXNpdHMiO3M6MToiMSI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA4LTA1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0wNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIzIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjYyO3M6OToibmV3VmlzaXRzIjtzOjE6IjEiO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjc6IkFuZHJvaWQiO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjE4O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMDgtMDgiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyO3M6OToicGFnZVZpZXdzIjtpOjI7czo2OiJ2aXNpdHMiO2k6MjtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtpOjI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjI6e3M6NToiTGludXgiO3M6MToiMSI7czozOiJpT1MiO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtpOjI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtpOjI7fX19fXM6MTA6IjIwMTItMDgtMDkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyO3M6OToicGFnZVZpZXdzIjtpOjI7czo2OiJ2aXNpdHMiO2k6MjtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtpOjI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjI6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO3M6NzoiV2luZG93cyI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjI6e3M6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtzOjE6IjEiO319czoxNzoiSW50ZXJuZXQgRXhwbG9yZXIiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aTo5O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMDgtMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIxIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjA7czo5OiJuZXdWaXNpdHMiO3M6MToiMSI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA4LTExIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiMSI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtzOjE6IjEiO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjE6e3M6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA4LTEyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMTQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTE1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjM7czo5OiJwYWdlVmlld3MiO2k6NTtzOjY6InZpc2l0cyI7aTozO3M6ODoidmlzaXRvcnMiO2k6MztzOjc6ImJvdW5jZXMiO2k6MTtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjUwLjMzMzMzMzMzMzMzMzMzNTcwMTgwOTExOTIwMDMzMzk1MjkwMzc0NzU1ODU5Mzc1O3M6OToibmV3VmlzaXRzIjtpOjM7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjI6e3M6OToiTWFjaW50b3NoIjtzOjE6IjIiO3M6NzoiV2luZG93cyI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjI6e3M6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO3M6MToiMiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MjE7czoxOiIyIjt9fXM6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjE2O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMDgtMTciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTE4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0xOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTIxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMyI7czo5OiJwYWdlVmlld3MiO3M6MToiMyI7czo2OiJ2aXNpdHMiO3M6MToiMyI7czo4OiJ2aXNpdG9ycyI7czoxOiIyIjtzOjc6ImJvdW5jZXMiO3M6MToiMyI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtzOjE6IjIiO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7czoxOiIzIjt9czo3OiJicm93c2VyIjthOjE6e3M6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjMiO3M6NzoidmVyc2lvbiI7YToxOntpOjE0O3M6MToiMyI7fX19fXM6MTA6IjIwMTItMDgtMjIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTo0O3M6OToicGFnZVZpZXdzIjtpOjU7czo2OiJ2aXNpdHMiO2k6NDtzOjg6InZpc2l0b3JzIjtpOjM7czo3OiJib3VuY2VzIjtpOjM7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxOS43NTtzOjk6Im5ld1Zpc2l0cyI7aTozO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToyOntzOjk6Ik1hY2ludG9zaCI7aTozO3M6NzoiV2luZG93cyI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjM6e3M6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MjE7czoxOiIxIjt9fXM6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjIiO3M6NzoidmVyc2lvbiI7YToxOntpOjU7czoxOiIyIjt9fXM6MTc6IkludGVybmV0IEV4cGxvcmVyIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6ODtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA4LTIzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MjtzOjk6InBhZ2VWaWV3cyI7aToyO3M6NjoidmlzaXRzIjtpOjI7czo4OiJ2aXNpdG9ycyI7aToyO3M6NzoiYm91bmNlcyI7aToyO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MDtzOjk6Im5ld1Zpc2l0cyI7aToxO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7aToyO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMTtzOjE6IjEiO319czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA4LTI0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0yNSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjYiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyO3M6OToicGFnZVZpZXdzIjtpOjQ7czo2OiJ2aXNpdHMiO2k6MjtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoyLjU7czo5OiJuZXdWaXNpdHMiO2k6MTtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czozOiJpT1MiO2k6Mjt9czo3OiJicm93c2VyIjthOjI6e3M6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtzOjE6IjEiO319czoxNToiU2FmYXJpIChpbi1hcHApIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6OToiKG5vdCBzZXQpIjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA4LTI3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiMSI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtzOjE6IjEiO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjE6e3M6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIxIjt9fX19czoxMDoiMjAxMi0wOC0yOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDgtMjkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA4LTMwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOC0zMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTAyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTA1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTA4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0wOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyO3M6OToicGFnZVZpZXdzIjtpOjI7czo2OiJ2aXNpdHMiO2k6MjtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtpOjE7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjI6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO3M6NzoiV2luZG93cyI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjI6e3M6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjU7czoxOiIxIjt9fXM6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjE1O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMDktMTEiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTEyIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjM7czo5OiJwYWdlVmlld3MiO2k6NzY7czo2OiJ2aXNpdHMiO2k6MztzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoyMzQwLjY2NjY2NjY2NjY2NjUxNTA4NDIxNjM3MTE3ODYyNzAxNDE2MDE1NjI1O3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjM7fXM6NzoiYnJvd3NlciI7YToyOntzOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToxNTtzOjE6IjEiO319czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjIiO319fX1zOjEwOiIyMDEyLTA5LTE0IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MztzOjk6InBhZ2VWaWV3cyI7aToxNztzOjY6InZpc2l0cyI7aTozO3M6ODoidmlzaXRvcnMiO2k6MjtzOjc6ImJvdW5jZXMiO2k6MTtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjI3Mi42NjY2NjY2NjY2NjY2ODU2MTQ0NzI5NTM2MDI2NzE2MjMyMjk5ODA0Njg3NTtzOjk6Im5ld1Zpc2l0cyI7aToxO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7aTozO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO3M6MToiMiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIyIjt9fXM6NjoiU2FmYXJpIjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e3M6MzoiQWxsIjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA5LTE1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMTciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTE4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0xOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjM7czo5OiJwYWdlVmlld3MiO2k6MjY7czo2OiJ2aXNpdHMiO2k6MztzOjg6InZpc2l0b3JzIjtpOjM7czo3OiJib3VuY2VzIjtpOjE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDo5NTY7czo5OiJuZXdWaXNpdHMiO2k6MTtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO2k6Mzt9czo3OiJicm93c2VyIjthOjI6e3M6NzoiRmlyZWZveCI7YToyOntzOjU6InRvdGFsIjtpOjI7czo3OiJ2ZXJzaW9uIjthOjI6e2k6MTU7czoxOiIxIjtpOjE3O3M6MToiMSI7fX1zOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMDktMjAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiI3IjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjc1MTtzOjk6Im5ld1Zpc2l0cyI7czoxOiIwIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToxNTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA5LTIxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0wOS0yMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjM7czo5OiJwYWdlVmlld3MiO2k6MzI7czo2OiJ2aXNpdHMiO2k6MztzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxMTAxLjY2NjY2NjY2NjY2Njc0MjQ1Nzg5MTgxNDQxMDY4NjQ5MjkxOTkyMTg3NTtzOjk6Im5ld1Zpc2l0cyI7aToxO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7aTozO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIxIjt9fXM6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjIiO3M6NzoidmVyc2lvbiI7YToxOntpOjU7czoxOiIyIjt9fX19czoxMDoiMjAxMi0wOS0yMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjM7czo5OiJwYWdlVmlld3MiO2k6MTU7czo2OiJ2aXNpdHMiO2k6MztzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDo1NDcuNjY2NjY2NjY2NjY2NjI4NzcxMDU0MDkyNzk0NjU2NzUzNTQwMDM5MDYyNTtzOjk6Im5ld1Zpc2l0cyI7aToxO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7aTozO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMTtzOjE6IjEiO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO3M6MToiMiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIyIjt9fX19czoxMDoiMjAxMi0wOS0yNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMDktMjUiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTA5LTI2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6NTtzOjk6InBhZ2VWaWV3cyI7aToxMDtzOjY6InZpc2l0cyI7aTo1O3M6ODoidmlzaXRvcnMiO2k6NDtzOjc6ImJvdW5jZXMiO2k6MztzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjQ2LjYwMDAwMDAwMDAwMDAwMTQyMTA4NTQ3MTUyMDIwMDM3MTc0MjI0ODUzNTE1NjI1O3M6OToibmV3VmlzaXRzIjtpOjM7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjU7fXM6NzoiYnJvd3NlciI7YTozOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjIiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMiI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIyIjtzOjc6InZlcnNpb24iO2E6MTp7aToxNTtzOjE6IjIiO319czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA5LTI3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MztzOjk6InBhZ2VWaWV3cyI7aTo0O3M6NjoidmlzaXRzIjtpOjM7czo4OiJ2aXNpdG9ycyI7aToyO3M6NzoiYm91bmNlcyI7aToyO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MjM3LjMzMzMzMzMzMzMzMzM0MjgwNzIzNjQ3NjgwMTMzNTgxMTYxNDk5MDIzNDM3NTtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjk6Ik1hY2ludG9zaCI7aTozO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIxIjt9fXM6MjQ6Ik1vemlsbGEgQ29tcGF0aWJsZSBBZ2VudCI7YToyOntzOjU6InRvdGFsIjtzOjE6IjIiO3M6NzoidmVyc2lvbiI7YToxOntpOjU7czoxOiIyIjt9fX19czoxMDoiMjAxMi0wOS0yOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjI7czo5OiJwYWdlVmlld3MiO2k6MztzOjY6InZpc2l0cyI7aToyO3M6ODoidmlzaXRvcnMiO2k6MjtzOjc6ImJvdW5jZXMiO2k6MTtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjQ0O3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjI7fXM6NzoiYnJvd3NlciI7YToyOntzOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToxNTtzOjE6IjEiO319czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTA5LTI5IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MztzOjk6InBhZ2VWaWV3cyI7aTo4O3M6NjoidmlzaXRzIjtpOjM7czo4OiJ2aXNpdG9ycyI7aTozO3M6NzoiYm91bmNlcyI7aToxO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTA2O3M6OToibmV3VmlzaXRzIjtpOjM7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjI6e3M6OToiTWFjaW50b3NoIjtzOjE6IjIiO3M6NzoiV2luZG93cyI7czoxOiIxIjt9czo3OiJicm93c2VyIjthOjE6e3M6NjoiQ2hyb21lIjthOjI6e3M6NToidG90YWwiO2k6MztzOjc6InZlcnNpb24iO2E6MTp7aToyMjtpOjM7fX19fXM6MTA6IjIwMTItMDktMzAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIxIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjA7czo5OiJuZXdWaXNpdHMiO3M6MToiMCI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTAxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiMSI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMSI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDowO3M6OToibmV3VmlzaXRzIjtzOjE6IjEiO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YToxOntzOjc6IldpbmRvd3MiO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMDIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTAzIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0wNCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMDUiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTA2IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiMiI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNDtzOjk6Im5ld1Zpc2l0cyI7czoxOiIxIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMDciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIxIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjA7czo5OiJuZXdWaXNpdHMiO3M6MToiMSI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6NToiTGludXgiO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7czoxOiIxIjt9fX19czoxMDoiMjAxMi0xMC0wOCI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjQiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6ODg7czo5OiJuZXdWaXNpdHMiO3M6MToiMSI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJTYWZhcmkiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7czozOiJBbGwiO3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMDkiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIyIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIwIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjE1OTtzOjk6Im5ld1Zpc2l0cyI7czoxOiIwIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMTAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTExIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0xMiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjEiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MDtzOjk6Im5ld1Zpc2l0cyI7czoxOiIxIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMTMiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToxMTtzOjk6InBhZ2VWaWV3cyI7aTo4OTtzOjY6InZpc2l0cyI7aToxMTtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjM7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDo2MDIuNDU0NTQ1NDU0NTQ1NDk1ODg2MTIyODA3ODYwMzc0NDUwNjgzNTkzNzU7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO2k6MTE7fXM6NzoiYnJvd3NlciI7YToyOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjciO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiNyI7fX1zOjI0OiJNb3ppbGxhIENvbXBhdGlibGUgQWdlbnQiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiI0IjtzOjc6InZlcnNpb24iO2E6MTp7aTo1O3M6MToiNCI7fX19fXM6MTA6IjIwMTItMTAtMTQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTE1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MjtzOjk6InBhZ2VWaWV3cyI7aTozO3M6NjoidmlzaXRzIjtpOjI7czo4OiJ2aXNpdG9ycyI7aToyO3M6NzoiYm91bmNlcyI7aToxO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MTA7czo5OiJuZXdWaXNpdHMiO2k6MTtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6Mjp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7czo3OiJXaW5kb3dzIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6Mjp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMjtzOjE6IjEiO319czo3OiJGaXJlZm94IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6MTU7czoxOiIxIjt9fX19czoxMDoiMjAxMi0xMC0xNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjQ7czo5OiJwYWdlVmlld3MiO2k6NjM7czo2OiJ2aXNpdHMiO2k6NDtzOjg6InZpc2l0b3JzIjtpOjM7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoyNTIyLjI1O3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjQ7fXM6NzoiYnJvd3NlciI7YTozOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX1zOjc6IkZpcmVmb3giO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToxNjtzOjE6IjEiO319czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMiI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjIiO319fX1zOjEwOiIyMDEyLTEwLTE3IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO3M6MToiMSI7czo5OiJwYWdlVmlld3MiO3M6MToiNyI7czo2OiJ2aXNpdHMiO3M6MToiMSI7czo4OiJ2aXNpdG9ycyI7czoxOiIxIjtzOjc6ImJvdW5jZXMiO3M6MToiMCI7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDoxNDg7czo5OiJuZXdWaXNpdHMiO3M6MToiMSI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTE4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0xOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjQiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjAiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6NTc7czo5OiJuZXdWaXNpdHMiO3M6MToiMCI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czo2OiJDaHJvbWUiO2E6Mjp7czo1OiJ0b3RhbCI7czoxOiIxIjtzOjc6InZlcnNpb24iO2E6MTp7aToyMjtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTIwIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0yMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMjIiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aToyO3M6OToicGFnZVZpZXdzIjtpOjM7czo2OiJ2aXNpdHMiO2k6MjtzOjg6InZpc2l0b3JzIjtpOjI7czo3OiJib3VuY2VzIjtpOjE7czoxMzoiYXZnVGltZU9uU2l0ZSI7ZDo5O3M6OToibmV3VmlzaXRzIjtpOjI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtpOjI7fXM6NzoiYnJvd3NlciI7YToyOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX1zOjY6IlNhZmFyaSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntzOjM6IkFsbCI7czoxOiIxIjt9fX19czoxMDoiMjAxMi0xMC0yMyI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtzOjE6IjEiO3M6OToicGFnZVZpZXdzIjtzOjE6IjEiO3M6NjoidmlzaXRzIjtzOjE6IjEiO3M6ODoidmlzaXRvcnMiO3M6MToiMSI7czo3OiJib3VuY2VzIjtzOjE6IjEiO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2Q6MDtzOjk6Im5ld1Zpc2l0cyI7czoxOiIwIjtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MTp7czo5OiJNYWNpbnRvc2giO3M6MToiMSI7fXM6NzoiYnJvd3NlciI7YToxOntzOjY6IkNocm9tZSI7YToyOntzOjU6InRvdGFsIjtzOjE6IjEiO3M6NzoidmVyc2lvbiI7YToxOntpOjIyO3M6MToiMSI7fX19fXM6MTA6IjIwMTItMTAtMjQiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTI1IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0yNiI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMjciO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7czoxOiIxIjtzOjk6InBhZ2VWaWV3cyI7czoxOiIxIjtzOjY6InZpc2l0cyI7czoxOiIxIjtzOjg6InZpc2l0b3JzIjtzOjE6IjEiO3M6NzoiYm91bmNlcyI7czoxOiIxIjtzOjEzOiJhdmdUaW1lT25TaXRlIjtkOjA7czo5OiJuZXdWaXNpdHMiO3M6MToiMCI7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjE6e3M6OToiTWFjaW50b3NoIjtzOjE6IjEiO31zOjc6ImJyb3dzZXIiO2E6MTp7czoyNDoiTW96aWxsYSBDb21wYXRpYmxlIEFnZW50IjthOjI6e3M6NToidG90YWwiO3M6MToiMSI7czo3OiJ2ZXJzaW9uIjthOjE6e2k6NTtzOjE6IjEiO319fX1zOjEwOiIyMDEyLTEwLTI4IjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMC0yOSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fXM6MTA6IjIwMTItMTAtMzAiO2E6OTp7czoyMjoiZ2V0YXZnVGltZU9uU2l0ZVZpc2l0cyI7aTowO3M6NjoidmlzaXRzIjtpOjA7czo5OiJwYWdlVmlld3MiO2k6MDtzOjg6InZpc2l0b3JzIjtpOjA7czo3OiJib3VuY2VzIjtpOjA7czoxMzoiYXZnVGltZU9uU2l0ZSI7aTowO3M6OToibmV3VmlzaXRzIjtpOjA7czoxNToib3BlcmF0aXZzeXN0ZW1zIjthOjA6e31zOjc6ImJyb3dzZXIiO2E6MDp7fX1zOjEwOiIyMDEyLTEwLTMxIjthOjk6e3M6MjI6ImdldGF2Z1RpbWVPblNpdGVWaXNpdHMiO2k6MDtzOjY6InZpc2l0cyI7aTowO3M6OToicGFnZVZpZXdzIjtpOjA7czo4OiJ2aXNpdG9ycyI7aTowO3M6NzoiYm91bmNlcyI7aTowO3M6MTM6ImF2Z1RpbWVPblNpdGUiO2k6MDtzOjk6Im5ld1Zpc2l0cyI7aTowO3M6MTU6Im9wZXJhdGl2c3lzdGVtcyI7YTowOnt9czo3OiJicm93c2VyIjthOjA6e319czoxMDoiMjAxMi0xMS0wMSI7YTo5OntzOjIyOiJnZXRhdmdUaW1lT25TaXRlVmlzaXRzIjtpOjA7czo2OiJ2aXNpdHMiO2k6MDtzOjk6InBhZ2VWaWV3cyI7aTowO3M6ODoidmlzaXRvcnMiO2k6MDtzOjc6ImJvdW5jZXMiO2k6MDtzOjEzOiJhdmdUaW1lT25TaXRlIjtpOjA7czo5OiJuZXdWaXNpdHMiO2k6MDtzOjE1OiJvcGVyYXRpdnN5c3RlbXMiO2E6MDp7fXM6NzoiYnJvd3NlciI7YTowOnt9fX19fXM6Mjc6Imdvb2dsZV9hbGxvd19wcm9maWxlX3N3aXRjaCI7YjowO3M6MjY6Imdvb2dsZV9hbGxvd19tZW1iZXJfZ3JvdXBzIjtzOjE6Im4iO3M6MjA6Imdyb3VwX2dvb2dsZV9hY2NvdW50IjthOjA6e31zOjE2OiJ1cGRhdGVfZnJlcXVlbmN5IjtzOjExOiItMzAgbWludXRlcyI7czoxNzoic2hvd19tb250aGx5X3ZpZXciO3M6MToieSI7czoxNToic2hvd190b2RheV92aWV3IjtzOjE6InkiO3M6MTk6InNob3dfeWVzdGVyZGF5X3ZpZXciO3M6MToieSI7czoxNDoic2hvd193ZWVrX3ZpZXciO3M6MToieSI7czoxNToic2hvd19tb250aF92aWV3IjtzOjE6InkiO3M6MTY6InNob3dfc291cmNlX3ZpZXciO3M6MToieSI7czoxNToic2hvd19wYWdlc192aWV3IjtzOjE6InkiO3M6MTc6InNob3dfYnJvd3Nlcl92aWV3IjtzOjE6InkiO3M6MjQ6InNob3dfb3BlcmF0aXZzeXN0ZW1fdmlldyI7czoxOiJ5IjtzOjI0OiJzaG93X3BhZ2VzX3ZpZXdfaW5fdGFibGUiO3M6MToieSI7czoxMzoiZXhjbHVkZV9ob3N0cyI7czowOiIiO3M6MTM6ImluY2x1ZGVfaG9zdHMiO3M6MDoiIjt9');

/*!40000 ALTER TABLE `ee2_republic_analytics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_reset_password
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_reset_password`;

CREATE TABLE `ee2_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_revision_tracker
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_revision_tracker`;

CREATE TABLE `ee2_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_revision_tracker` WRITE;
/*!40000 ALTER TABLE `ee2_revision_tracker` DISABLE KEYS */;

INSERT INTO `ee2_revision_tracker` (`tracker_id`, `item_id`, `item_table`, `item_field`, `item_date`, `item_author_id`, `item_data`)
VALUES
	(3,1,'ee2_templates','template_data',1351209639,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content!\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries} '),
	(4,1,'ee2_templates','template_data',1351209680,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries} '),
	(5,47,'ee2_templates','template_data',1351209754,2,'{header}\n{exp:stash:get name=\"content\"}\n{footer}'),
	(6,1,'ee2_templates','template_data',1351209835,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set name=\"title\"}{title}{/exp:stash:}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(7,1,'ee2_templates','template_data',1351210110,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set name=\"title\"}{title}{/exp:stash:}\n    {exp:stash:set_value name=\"title\" value=\"{title}\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(8,1,'ee2_templates','template_data',1351210187,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set name=\"title\"}{title}{/exp:stash:}\n    {exp:stash:set_value name=\"meta_title\" value=\"{title}\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(9,1,'ee2_templates','template_data',1351210197,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set name=\"title\"}{title}{/exp:stash:}\n    {exp:stash:set_value name=\"title\" value=\"{title}\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(10,1,'ee2_templates','template_data',1351210247,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {!--{exp:stash:set name=\"title\"}{title}{/exp:stash:set}--}\n    {exp:stash:set_value name=\"etitle\" value=\"{title}\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(11,1,'ee2_templates','template_data',1351210251,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {!--{exp:stash:set name=\"title\"}{title}{/exp:stash:set}--}\n    {exp:stash:set_value name=\"meta_title\" value=\"{title}\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(12,1,'ee2_templates','template_data',1351210266,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {!--{exp:stash:set name=\"title\"}{title}{/exp:stash:set}--}\n    {exp:stash:set_value name=\"meta_title\" value=\"test\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(13,1,'ee2_templates','template_data',1351210289,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {!--{exp:stash:set name=\"title\"}{title}{/exp:stash:set}--}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(14,1,'ee2_templates','template_data',1351210302,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(15,1,'ee2_templates','template_data',1351210321,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(16,1,'ee2_templates','template_data',1351210462,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(17,1,'ee2_templates','template_data',1351210589,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"jarrett\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(18,1,'ee2_templates','template_data',1351210598,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"jarrett\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(19,1,'ee2_templates','template_data',1351210599,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"jarrett\" type=\"snippet\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(20,1,'ee2_templates','template_data',1351210622,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\" type=\"snippet\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"jarrett\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(21,1,'ee2_templates','template_data',1351210640,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\"  parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"jarrett\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(22,1,'ee2_templates','template_data',1351210652,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"test2\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"test3\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"test4\"  parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"jarrett\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(23,1,'ee2_templates','template_data',1351210674,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"{title}\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\"  parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}'),
	(24,1,'ee2_templates','template_data',1351210684,2,'{embed=\"layouts/pages\"}\n\n{exp:channel:entries limit=\"1\" entry_id=\"1\" dynamic=\"off\"}\n    {exp:stash:set name=\"bodyclass\"}home{/exp:stash:set}\n    {exp:stash:set_value name=\"meta_title\" value=\"{title}\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_keywords\" value=\"\" parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_description\" value=\"\"  parse=\"inward\"}\n    {exp:stash:set_value name=\"meta_author\" value=\"\" parse=\"inward\"}\n\n    {exp:stash:set name=\"content\"}\n        <section class=\"body\">\n            <div class=\"grid\">\n               Content here\n            </div>\n        </section>\n    {/exp:stash:set}\n{/exp:channel:entries}');

/*!40000 ALTER TABLE `ee2_revision_tracker` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_search`;

CREATE TABLE `ee2_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_search_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_search_log`;

CREATE TABLE `ee2_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_security_hashes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_security_hashes`;

CREATE TABLE `ee2_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_security_hashes` WRITE;
/*!40000 ALTER TABLE `ee2_security_hashes` DISABLE KEYS */;

INSERT INTO `ee2_security_hashes` (`hash_id`, `date`, `ip_address`, `hash`)
VALUES
	(3297,1351803166,'127.0.0.1','8ecd164d6350a09b4d90993375311ae9fd1febf6'),
	(3296,1351803165,'127.0.0.1','9f948331e30be1aee90628ace2a1ce7fb8cd4a01'),
	(3234,1351793322,'127.0.0.1','1876c1612f828094f3c54fec2d759f0f29039cf0'),
	(3233,1351793321,'127.0.0.1','a297685bfb952b48392b5516fe893546a59919e8'),
	(3232,1351793311,'127.0.0.1','698961d1bd1a48d6f73d480a455abc30c629aa00'),
	(3231,1351793310,'127.0.0.1','610ab23a0ac1b5d2e943d9dd553b9ae9f7db731a'),
	(3230,1351793293,'127.0.0.1','10774aa025cd72a20a15d59dd376e2348f34f2a7'),
	(3229,1351793291,'127.0.0.1','aa4b2c82f576c0ffef6004730d187d8203b8727e'),
	(3228,1351793038,'127.0.0.1','014cc04d1588a794e8d5b19e8202b5479d3dd7eb'),
	(3295,1351803164,'127.0.0.1','84f8d933393fb284333e603e4dd023afdc08891c'),
	(3294,1351803032,'127.0.0.1','3428a57ff69523859aa19b7d8563b8a2b55a6dea'),
	(3293,1351803028,'127.0.0.1','2119615239fd62f7fb8818f11460c35df704a059'),
	(3292,1351803022,'127.0.0.1','ee60d1e11a4ec4af7617f1861728865a6820aa25'),
	(3291,1351802835,'127.0.0.1','a4edf7ba3f18910a6f217c38e22f9982bc5c0c5d'),
	(3290,1351802828,'127.0.0.1','061519eb2726ad3a131850623edf19aa24f342c8'),
	(3289,1351802820,'127.0.0.1','3568feddd1d3ec50c2f8017578cce453d27d55dd'),
	(3288,1351802819,'127.0.0.1','5a0602028a512abf0718b0bad7813981c02cee32'),
	(3287,1351802816,'127.0.0.1','b443947b7a2313698c1cbdcbe5f13b7d421980d6'),
	(3286,1351802781,'127.0.0.1','7cf09aedbf999b712c14a1495b6587cd870929ee'),
	(3285,1351802731,'127.0.0.1','58b3d5bde35ba41e2a9e70e6ddefde7e4b7bc3d6'),
	(3284,1351802721,'127.0.0.1','5bd9ade854bec7c245652826474517391cec109c'),
	(3283,1351802693,'127.0.0.1','bb44e11b5adef5953fec573be28873ce324e928a'),
	(3282,1351802685,'127.0.0.1','e6317328eb06a9034d87be69ca75e5f236a2b4ee'),
	(3281,1351802659,'127.0.0.1','7b78ee1492d23e41a72dae0b81596d0ad5b6dc2c'),
	(3280,1351802646,'127.0.0.1','82f64499f78c5bc6530508258c15754811e35423'),
	(3279,1351802630,'127.0.0.1','b6d60507c2ab528ee6773addb165b69e8bcc1972'),
	(3278,1351802628,'127.0.0.1','8442863a3b0325571cdccd4568bd6980962d6286'),
	(3277,1351801886,'127.0.0.1','2a0b3018da5ce28e9c20a556e5bb509a60e57e8d'),
	(3276,1351801301,'127.0.0.1','7ebfad2cbca9b2a25000a51d56451c6c7094f2cd'),
	(3275,1351801295,'127.0.0.1','1730425a39c07c6a4c33a32a4cd3405743f86f53'),
	(3274,1351801293,'127.0.0.1','d7e23a5a67eef5f219f29694f45e7d695d4d03cb'),
	(3273,1351801275,'127.0.0.1','8c13e1d44fb4118dd6b55d632d3d37adbe644f00'),
	(3272,1351801275,'127.0.0.1','19be929d46fb7a450a97f882f7bc01cf8951bded'),
	(3271,1351801257,'127.0.0.1','0091ab8a180d0c6e8d685ecb0b761e76b1eeda81'),
	(3270,1351801255,'127.0.0.1','4953a408922a02339fd34075b9e6ed9156f2e7f9'),
	(3269,1351801225,'127.0.0.1','247f555b3c2ccbc51ae580b6f14519886f09bc04'),
	(3268,1351801203,'127.0.0.1','d7dc0038fd97b32ce798871c67b4a129b09b7694'),
	(3267,1351801202,'127.0.0.1','17e32c2f3a2d38ffd1a550268d5f784bec34fda9'),
	(3266,1351801185,'127.0.0.1','c94341f78eeb22d051041fbd86859dfcbb0f2588'),
	(3265,1351801115,'127.0.0.1','90ca493f509290192f1c0416b60c8c6522bb220d'),
	(3264,1351800674,'127.0.0.1','567eee4c9537270c5c3d5dd5fba14790ca3c2e40'),
	(3263,1351800666,'127.0.0.1','ada118f4d0a1307eb1ecf7da0f3c5b42d9e8504a'),
	(3262,1351800474,'127.0.0.1','c1436fcc28f2eac2f98c3575728907db1d752947'),
	(3261,1351800472,'127.0.0.1','e75dc165fe70ac1c377dbccd5ba5e2646957c7dd'),
	(3260,1351800472,'127.0.0.1','ddf834ad2d7502f72e7edc08340990388209e339'),
	(3259,1351800378,'127.0.0.1','ce7224c7f0872187d1caae88ec32582d963c207d'),
	(3258,1351800377,'127.0.0.1','103e814977eceef97eb2e0f9a78019a97141e008'),
	(3257,1351800377,'127.0.0.1','867296cea31f1273b21fb8e570c5c065529c01cd'),
	(3256,1351800377,'127.0.0.1','adc82fa930efd9878b25498744e863a23ca40fb3'),
	(3255,1351800377,'127.0.0.1','a48e44e05fad9e54d793c76c82d40cbafebc51e8'),
	(3254,1351800376,'127.0.0.1','0f6ce79267a85e275dcbd791051486e84369d36c'),
	(3253,1351800374,'127.0.0.1','4eab924bd84b22999211d71c374ec43eb88eed61'),
	(3252,1351800372,'127.0.0.1','80dd86a3b72535d50a7d33d26aa080f69f39e384'),
	(3251,1351800372,'127.0.0.1','0a8cdc8c8dc8423001edcc89af1a1717251c55da'),
	(3250,1351800359,'127.0.0.1','71f66d623fac39db8a44c41bfcaf38c1310d83a3'),
	(3249,1351800359,'127.0.0.1','de7d503175fa86fe43577a72e91c67599f042732'),
	(3248,1351800359,'127.0.0.1','9e2dd29dcea9b30b070e7aaf89cd3cec4ab6fba2'),
	(3247,1351800358,'127.0.0.1','6d4028dcfdf32e4c97aaf0f7b7613dcd08905b46'),
	(3246,1351800358,'127.0.0.1','93cdd9b5959dda69316fb0e7de097f354a95d85d'),
	(3245,1351800357,'127.0.0.1','e523ac410afcb6112934e072e45dd5e8a378673d'),
	(3244,1351800346,'127.0.0.1','53b1e91ace11da7fd247ce407a6d65cf1a2afe58'),
	(3243,1351800343,'127.0.0.1','2580791897eae814760ee62a06941fa0c9b1eb40'),
	(3242,1351800343,'127.0.0.1','acbf9dcb4d78caa31a9ae4c2613d08726c6dc8a2'),
	(3241,1351800289,'127.0.0.1','081118ce92ab8c10dc7830b02a5d23504ac9734b'),
	(3240,1351800281,'127.0.0.1','e4b60c1c70bd94695b610b4a36d0964f9c766fcc'),
	(3239,1351800135,'127.0.0.1','d3d509c759511a7bb5f280b71021f928bac30b97'),
	(3238,1351800135,'127.0.0.1','e3e018bd184d1399353493b6160ec63ac8323c38'),
	(3237,1351800128,'127.0.0.1','08229cbe649095ecb3aaaecbb19f41fea71b6c5e'),
	(3236,1351800072,'127.0.0.1','1b3d91b79ead96cb76e4328cc234193c6249a430');

/*!40000 ALTER TABLE `ee2_security_hashes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_sessions`;

CREATE TABLE `ee2_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_sessions` WRITE;
/*!40000 ALTER TABLE `ee2_sessions` DISABLE KEYS */;

INSERT INTO `ee2_sessions` (`session_id`, `site_id`, `member_id`, `admin_sess`, `ip_address`, `user_agent`, `last_activity`)
VALUES
	('52a5f6bc626bd9674546eb5e69990bc125f8172b',1,2,1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4',1351803196);

/*!40000 ALTER TABLE `ee2_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_sites`;

CREATE TABLE `ee2_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_sites` WRITE;
/*!40000 ALTER TABLE `ee2_sites` DISABLE KEYS */;

INSERT INTO `ee2_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`)
VALUES
	(1,'Minnesota Outreach','default_site',NULL,'YTo5MDp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MTk6Imh0dHA6Ly9uZXJkZXJ5LmxvYy8iO3M6MTY6InRoZW1lX2ZvbGRlcl91cmwiO3M6ODoiL3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czoyMzoid2VibWFzdGVyQG1jMmRlc2lnbi5jb20iO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjQzOiJodHRwOi8vZWUyLnBsZWFzZXByb29mLmNvbS9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6MToiLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJuIjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjM6IlVNOCI7czoxMzoic2VydmVyX29mZnNldCI7czowOiIiO3M6MTY6ImRheWxpZ2h0X3NhdmluZ3MiO3M6MToieSI7czoyMToiZGVmYXVsdF9zaXRlX3RpbWV6b25lIjtzOjM6IlVNOCI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6MTQ6ImphcnJldHRiYXJuZXR0IjtzOjIxOiJlbWFpbF9tb2R1bGVfY2FwdGNoYXMiO3M6MToibiI7czoxNjoibG9nX3NlYXJjaF90ZXJtcyI7czoxOiJ5IjtzOjEyOiJzZWN1cmVfZm9ybXMiO3M6MToieSI7czoxOToiZGVueV9kdXBsaWNhdGVfZGF0YSI7czoxOiJ5IjtzOjI0OiJyZWRpcmVjdF9zdWJtaXR0ZWRfbGlua3MiO3M6MToibiI7czoxNjoiZW5hYmxlX2NlbnNvcmluZyI7czoxOiJuIjtzOjE0OiJjZW5zb3JlZF93b3JkcyI7czowOiIiO3M6MTg6ImNlbnNvcl9yZXBsYWNlbWVudCI7czowOiIiO3M6MTA6ImJhbm5lZF9pcHMiO3M6MDoiIjtzOjEzOiJiYW5uZWRfZW1haWxzIjtzOjA6IiI7czoxNjoiYmFubmVkX3VzZXJuYW1lcyI7czowOiIiO3M6MTk6ImJhbm5lZF9zY3JlZW5fbmFtZXMiO3M6MDoiIjtzOjEwOiJiYW5fYWN0aW9uIjtzOjg6InJlc3RyaWN0IjtzOjExOiJiYW5fbWVzc2FnZSI7czozNDoiVGhpcyBzaXRlIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSI7czoxNToiYmFuX2Rlc3RpbmF0aW9uIjtzOjIxOiJodHRwOi8vd3d3LnlhaG9vLmNvbS8iO3M6MTY6ImVuYWJsZV9lbW90aWNvbnMiO3M6MToieSI7czoxMjoiZW1vdGljb25fdXJsIjtzOjQyOiJodHRwOi8vZWUyLnBsZWFzZXByb29mLmNvbS9pbWFnZXMvc21pbGV5cy8iO3M6MTk6InJlY291bnRfYmF0Y2hfdG90YWwiO3M6NDoiMTAwMCI7czoxNzoibmV3X3ZlcnNpb25fY2hlY2siO3M6MToibiI7czoxNzoiZW5hYmxlX3Rocm90dGxpbmciO3M6MToibiI7czoxNzoiYmFuaXNoX21hc2tlZF9pcHMiO3M6MToieSI7czoxNDoibWF4X3BhZ2VfbG9hZHMiO3M6MjoiMTAiO3M6MTM6InRpbWVfaW50ZXJ2YWwiO3M6MToiOCI7czoxMjoibG9ja291dF90aW1lIjtzOjI6IjMwIjtzOjE1OiJiYW5pc2htZW50X3R5cGUiO3M6NzoibWVzc2FnZSI7czoxNDoiYmFuaXNobWVudF91cmwiO3M6MDoiIjtzOjE4OiJiYW5pc2htZW50X21lc3NhZ2UiO3M6NTA6IllvdSBoYXZlIGV4Y2VlZGVkIHRoZSBhbGxvd2VkIHBhZ2UgbG9hZCBmcmVxdWVuY3kuIjtzOjE3OiJlbmFibGVfc2VhcmNoX2xvZyI7czoxOiJ5IjtzOjE5OiJtYXhfbG9nZ2VkX3NlYXJjaGVzIjtzOjM6IjUwMCI7czoxNzoidGhlbWVfZm9sZGVyX3BhdGgiO3M6Njk6Ii9Vc2Vycy9qYXJyZXR0L1NpdGVzL2phcnJldHRiYXJuZXR0L25lcmRlcnkubG9jL3Jvb3QvaHR0cGRvY3MvdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5Ijt9','YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==','YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToieSI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NDoibm9uZSI7czoyMzoibmV3X21lbWJlcl9ub3RpZmljYXRpb24iO3M6MToieSI7czoyMzoibWJyX25vdGlmaWNhdGlvbl9lbWFpbHMiO3M6Mjg6ImRldmVsb3BlckBqYXJyZXR0YmFybmV0dC5jb20iO3M6MjQ6InJlcXVpcmVfdGVybXNfb2Zfc2VydmljZSI7czoxOiJuIjtzOjIyOiJ1c2VfbWVtYmVyc2hpcF9jYXB0Y2hhIjtzOjE6Im4iO3M6MjA6ImRlZmF1bHRfbWVtYmVyX2dyb3VwIjtzOjE6IjUiO3M6MTU6InByb2ZpbGVfdHJpZ2dlciI7czo2OiJtZW1iZXIiO3M6MTI6Im1lbWJlcl90aGVtZSI7czoxMToiZGVmYXVsdF9vbGQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6InkiO3M6MTA6ImF2YXRhcl91cmwiO3M6MTY6Ii9wdWJsaWMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjc3OiIvVXNlcnMvamFycmV0dC9TaXRlcy9qYXJyZXR0YmFybmV0dC9uZXJkZXJ5LmxvYy9yb290L2h0dHBkb2NzL3B1YmxpYy9hdmF0YXJzLyI7czoxNjoiYXZhdGFyX21heF93aWR0aCI7czozOiI1MDAiO3M6MTc6ImF2YXRhcl9tYXhfaGVpZ2h0IjtzOjM6IjUwMCI7czoxMzoiYXZhdGFyX21heF9rYiI7czozOiIzNTAiO3M6MTM6ImVuYWJsZV9waG90b3MiO3M6MToibiI7czo5OiJwaG90b191cmwiO3M6NDA6Imh0dHA6Ly9uZXJkZXJ5LmxvYy9wdWJsaWMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6ODM6Ii9Vc2Vycy9qYXJyZXR0L1NpdGVzL2phcnJldHRiYXJuZXR0L25lcmRlcnkubG9jL3Jvb3QvaHR0cGRvY3MvcHVibGljL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NDg6Imh0dHA6Ly9uZXJkZXJ5LmxvYy9wdWJsaWMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjkxOiIvVXNlcnMvamFycmV0dC9TaXRlcy9qYXJyZXR0YmFybmV0dC9uZXJkZXJ5LmxvYy9yb290L2h0dHBkb2NzL3B1YmxpYy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjE3OiJzaWdfaW1nX21heF93aWR0aCI7czozOiI0ODAiO3M6MTg6InNpZ19pbWdfbWF4X2hlaWdodCI7czoyOiI4MCI7czoxNDoic2lnX2ltZ19tYXhfa2IiO3M6MjoiMzAiO3M6MTk6InBydl9tc2dfdXBsb2FkX3BhdGgiO3M6ODQ6Ii9Vc2Vycy9qYXJyZXR0L1NpdGVzL2phcnJldHRiYXJuZXR0L25lcmRlcnkubG9jL3Jvb3QvaHR0cGRvY3MvcHVibGljL3BtX2F0dGFjaG1lbnRzLyI7czoyMzoicHJ2X21zZ19tYXhfYXR0YWNobWVudHMiO3M6MToiMyI7czoyMjoicHJ2X21zZ19hdHRhY2hfbWF4c2l6ZSI7czozOiIyNTAiO3M6MjA6InBydl9tc2dfYXR0YWNoX3RvdGFsIjtzOjM6IjEwMCI7czoxOToicHJ2X21zZ19odG1sX2Zvcm1hdCI7czo0OiJzYWZlIjtzOjE4OiJwcnZfbXNnX2F1dG9fbGlua3MiO3M6MToibiI7czoxNzoicHJ2X21zZ19tYXhfY2hhcnMiO3M6NDoiNjAwMCI7czoxOToibWVtYmVybGlzdF9vcmRlcl9ieSI7czoxNzoidG90YWxfZm9ydW1fcG9zdHMiO3M6MjE6Im1lbWJlcmxpc3Rfc29ydF9vcmRlciI7czo0OiJkZXNjIjtzOjIwOiJtZW1iZXJsaXN0X3Jvd19saW1pdCI7czoyOiIyMCI7fQ==','YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJ5IjtzOjg6InNpdGVfNDA0IjtzOjg6InNpdGUvNDA0IjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoyOiIxMCI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo3ODoiL1VzZXJzL2phcnJldHQvU2l0ZXMvamFycmV0dGJhcm5ldHQvbmVyZGVyeS5sb2Mvcm9vdC9odHRwZG9jcy92aWV3cy90ZW1wbGF0ZXMvIjt9','YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJ5IjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=','YToxOntzOjcxOiIvVXNlcnMvamFycmV0dC9TaXRlcy9qYXJyZXR0YmFybmV0dC9uZXJkZXJ5LmxvYy9yb290L2h0dHBkb2NzL2luZGV4LnBocCI7czozMjoiYWY0MGFmNDU5NzViMzQ2YTBkYzM5MTJmYWI1ZDNlZmMiO30=','YToxOntpOjE7YTozOntzOjM6InVybCI7czoxOToiaHR0cDovL25lcmRlcnkubG9jLyI7czo0OiJ1cmlzIjthOjMwOntpOjE7czo5OiIvaG9tZXBhZ2UiO2k6NTtzOjY6Ii9hYm91dCI7aToxMTtzOjIyOiIvYWJvdXQvc3RhZmYtZGlyZWN0b3J5IjtpOjEyO3M6MTQ6Ii9hYm91dC9oaXN0b3J5IjtpOjI7czo1OiIvbmV3cyI7aToxMztzOjE1OiIvbmV3cy9kaXN0cmljdHMiO2k6MTQ7czoxMjoiL25ld3MvZXZlbnRzIjtpOjE1O3M6MTc6Ii9uZXdzL2NsYXNzaWZpZWRzIjtpOjE2O3M6MjQ6Ii9uZXdzL2NvbmZlcmVuY2Utc2Vzc2lvbiI7aToxNztzOjEzOiIvZ2V0LWludm9sdmVkIjtpOjQ7czozMzoiL2dldC1pbnZvbHZlZC9yZXF1ZXN0LWluZm9ybWF0aW9uIjtpOjMxO3M6NDE6Ii9nZXQtaW52b2x2ZWQvcmVxdWVzdC1pbmZvcm1hdGlvbi9zdWNjZXNzIjtpOjIyO3M6MjQ6Ii9nZXQtaW52b2x2ZWQvbGVhcm4tbW9yZSI7aToxODtzOjEzOiIvb3VyLXByb2dyYW1zIjtpOjIwO3M6MjM6Ii9vdXItcHJvZ3JhbXMvcHJvZ3JhbS0xIjtpOjE5O3M6MTA6Ii9yZXNvdXJjZXMiO2k6MjE7czoyNDoiL3Jlc291cmNlcy93b21lbnMtY2xpbmljIjtpOjI2O3M6MTY6Ii9yZWxhdGVkLWNvbnRlbnQiO2k6MzA7czoxMDoiL2ZvdXJvZm91ciI7aTozO3M6NjoiL3N0b3JlIjtpOjY7czoxMzoiL3N0b3JlL29yZGVycyI7aTo3O3M6MTU6Ii9zdG9yZS9wcm9kdWN0cyI7aTo4O3M6MzE6Ii9uZXdzL3BhY2lmaWMtZWFydGhxdWFrZS1yZWxpZWYiO2k6OTtzOjYwOiIvbmV3cy9ub3J0aGVybi1taW5uZXNvdGEtY2FtcHVzZXMtYXJlLWZlcnRpbGUtbWlzc2lvbi1maWVsZHMiO2k6MTA7czo0MzoiL25ld3MvbWlubmVzb3RhLW91dHJlYWNoLXdvbWVuLWhvdy1pdC13b3JrcyI7aToyNDtzOjMzOiIvbmV3cy9jb25zZWN0ZXR1ci1hZGlwaXNjaW5nLWVsaXQiO2k6MjU7czoyNzoiL25ld3MvZG9uZWMtdmVsLXNhcGllbi1xdWFtIjtpOjI3O3M6MzU6Ii9yZWxhdGVkLWNvbnRlbnQvaW1hZ2luZS1uby1tYWxhcmlhIjtpOjI4O3M6MzI6Ii9yZWxhdGVkLWNvbnRlbnQvZG9uYXRlLXRvLWphcGFuIjtpOjI5O3M6Mjk6Ii9yZWxhdGVkLWNvbnRlbnQvcHJlZmVyLXByaW50Ijt9czo5OiJ0ZW1wbGF0ZXMiO2E6MzA6e2k6MTtzOjE6IjEiO2k6MjtzOjI6IjUzIjtpOjM7czoyOiI1MyI7aTo1O3M6MjoiMTAiO2k6NjtzOjI6IjEwIjtpOjc7czoyOiIxMCI7aTo4O3M6MjoiNTEiO2k6OTtzOjI6IjUxIjtpOjEwO3M6MjoiNTEiO2k6MTE7czoyOiIxMCI7aToxMjtzOjI6IjEwIjtpOjEzO3M6MjoiMTAiO2k6MTQ7czoyOiIxMCI7aToxNTtzOjI6IjEwIjtpOjE2O3M6MjoiMTAiO2k6MTc7czoyOiIxMCI7aToxODtzOjI6IjEwIjtpOjE5O3M6MjoiMTAiO2k6MjA7czoyOiIxMCI7aToyMTtzOjI6IjEwIjtpOjIyO3M6MjoiMTAiO2k6MjQ7czoyOiI1MSI7aToyNTtzOjI6IjUxIjtpOjI2O3M6MjoiMTAiO2k6Mjc7czoyOiI2MCI7aToyODtzOjI6IjYwIjtpOjI5O3M6MjoiNjAiO2k6NDtzOjI6IjEwIjtpOjMwO3M6MjoiMTAiO2k6MzE7czoyOiIxMCI7fX19');

/*!40000 ALTER TABLE `ee2_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_snippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_snippets`;

CREATE TABLE `ee2_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_snippets` WRITE;
/*!40000 ALTER TABLE `ee2_snippets` DISABLE KEYS */;

INSERT INTO `ee2_snippets` (`snippet_id`, `site_id`, `snippet_name`, `snippet_contents`)
VALUES
	(5,1,'snippet:global_footer','        </div><!--wrapper-->\n    </section><!-- #content -->\n    <footer class=\"site-info\" role=\"contentinfo\">\n        <div class=\"wrapper\">\n            <h1 class=\"hidden\">Site Information</h1>\n            <section class=\"banner\">\n                <div class=\"vcard\">\n                    <address>\n                        {addressinfo limit=\"1\"}\n                        <span class=\"org fn\">{addressinfo_name}</span>\n                        <span class=\"address-info\">\n                            <span class=\"street-address\">{addressinfo_street}</span>\n                            <span class=\"locality\">{addressinfo_city}</span>,\n                            <span class=\"region\">{addressinfo_state}</span>\n                            <span class=\"postal-code\">{addressinfo_postal}</span>\n                            <span class=\"country-name hidden\">{addressinfo_country}</span>\n                        </span>\n                        {/addressinfo}\n                    </address>\n                    <div class=\"contact-info\">\n                        {contactinfo limit=\"1\"}\n                        <a class=\"url hidden\" href=\"http://{contactinfo_url}\">{contactinfo_url}</a>\n                        <a class=\"email\" href=\"mailto:{contactinfo_email}\">{contactinfo_email}</a>\n                        <span class=\"tel\">{contactinfo_phone}</span>\n                        {/contactinfo}\n                    </div>\n                </div>\n            </section>\n            <div class=\"copyright\">&copy; Copyright {current_time format=\"%Y\"}</div>\n        </div>\n    </footer>\n</body>\n</html>'),
	(6,1,'snippet:global_header','<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n    {!-- PRELOAD DEFAULTS --}\n    {preload_replace:page_bodyclass=\"no-js\"}\n    {preload_replace:page_bodyid=\"\"}\n    {preload_replace:og=\"og_pages\"}\n    {preload_replace:disable_breadcrumb=\"n\"}\n    {preload_replace:disable_subnav=\"n\"}\n\n    {!-- META DATA --}\n    <meta charset=\"utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <!--[if IE]><meta http-equiv=\"cleartype\" content=\"on\" /><![endif]-->\n    <!--[if lt IE 9]><meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\" /><![endif]-->\n    \n    {!-- SEO and FACEBOOK --}\n    <title>{title}</title>\n    <meta name=\"description\" content=\"{meta_description}\" />\n    <meta name=\"keywords\" content=\"{meta_keywords}{meta_keyword}{/meta_keywords}\" />\n    <meta name=\"robots\" content=\"index,follow,archive\" />\n    <meta property=\"og:title\" content=\"{if \'{{og}_title}\'}{if:else}{title} > {site_name}{/if}\"/>\n    <meta property=\"og:type\" content=\"{if \'{{og}_type}\'}{if:else}website{/if}\"/>\n    <meta property=\"og:url\" content=\"og_url\" />\n    <meta property=\"og:image\" content=\"{if {{og}_image}{if:else}{site_url}images/logo.gif{/if}\"/>\n    <meta property=\"og:site_name\" content=\"{site_name}\"/>\n    <meta property=\"og:description\" content=\"{{og}_description}\"/>\n    \n    {!-- ICONS --}\n    <link rel=\"shortcut icon\" type=\"image/ico\" href=\"/favicon.ico\" />\n    <link rel=\"apple-touch-icon\" href=\"/images/apple-touch-icon.png\" />\n    \n    {!-- STYLESHEETS --}\n    <link rel=\"stylesheet\" media=\"screen, projection\" href=\"/styles/reset.css\" />\n    <link rel=\"stylesheet\" media=\"screen, projection\" href=\"/styles/jcarousel.css\" />\n    <link rel=\"stylesheet\" media=\"screen, projection\" href=\"/styles/screen.css\" />\n    <!--[if lte IE 9]><link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"/styles/ie9.css\" /><![endif]-->\n    <!--[if lte IE 8]><link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"/styles/ie8.css\" /><![endif]-->\n    <!--[if lte IE 7]><link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"/styles/ie7.css\" /><![endif]-->\n    <link rel=\"alternate\" href=\"/xml/news/\" title=\"{site_name} News Feed\" type=\"application/rss+xml\" />\n    \n    {!-- JAVASCRIPT --}\n    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js\"></script>\n    <!--[if lt IE 9]><script type=\"text/javascript\" src=\"/scripts/html5shiv.js\"></script><![endif]-->\n    <script src=\"/scripts/jcarousel.js\"></script>\n    <script src=\"/scripts/global.js\"></script>\n    <script type=\"text/javascript\">var _gaq = _gaq || []; _gaq.push([\'_setAccount\', \'{analytics_id}\'],[\'_trackPageview\']); (function() { var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true; ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\'; var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s); })();</script>\n\n    {!-- MINI-CP --}\n    {if showadmintoolbar == \"y\"}{exp:minicp jquery=\"no\" jqueryui=\"yes\"}{/if}\n    \n</head>\n<body id=\"{page_bodyid}\" class=\"{page_bodyclass}\">\n    {if showadmintoolbar == \"y\"}{exp:minicp:widget entry_id=\"{entry_id}\"}{/if}\n    \n    {!-- HEADER --}\n    <header class=\"masthead\" role=\"banner\">\n        <div class=\"wrapper\">\n            <a href=\"/\" title=\"{site_name}\" class=\"logo pngfix\">\n                <h1>{site_name}</h1>\n            </a>\n            <nav class=\"nav-member\">\n                <a href=\"/member/profile\">View Profile</a> | {if logged_in}<a href=\"/member/logout\">Logout</a>{if:else}<a href=\"/member/login\">Login</a>{/if}\n            </nav>\n             <nav class=\"nav-main\" role=\"navigation\">\n                <h2 class=\"hidden\">Main Navigation (<a href=\"#content\">Skip to Main Content</a>)</h2>\n                {exp:structure:nav max_depth=\"1\" css_id=\"leftnav\" current_class=\"active\" add_unique_ids=\"yes\"}\n            </nav>\n        </div>\n    </header>\n    \n    {!-- WRAPPER --}\n    <section id=\"content\" class=\"content-main has-sub-nav\" role=\"main\">\n        <div class=\"wrapper\">\n            \n            \n        {if \"{disable_breadcrumb}\" != \"y\"}\n            <ol class=\"breadcrumbs\">\n                {exp:structure:breadcrumb here_as_title=\"yes\" wrap_each=\"li\"}\n            </ol>\n        {/if}\n        {if \"{disable_subnav}\" != \"y\"}\n            <aside class=\"sidebar sub-nav\" role=\"complementary\">\n                {exp:structure:nav start_from=\"/{segment_1}/\" max_depth=\"1\" css_id=\"leftnav\" current_class=\"active\"}\n            </aside><!-- #sidebar -->\n        {/if}'),
	(10,1,'snippet:modules_carousel','{if \"{carousel:total_rows search:carousel_active=\'=y\'}\" > 0}\n    <ul class=\"media-image slider carousel jcarousel-skin-default\">\n        {carousel search:carousel_active=\"=y\"}\n            <li><img src=\"{carousel_image:resized}\" alt=\"{carousel_alt}\" /></li>\n        {/carousel}\n    </ul>\n{/if}'),
	(11,1,'snippet:modules_getinvolved','{getinvolved limit=\"1\"}\n    <aside class=\"sidebar ancillary\" role=\"complementary\">\n        <h1 class=\"hidden\">Related Content</h1>\n        <div class=\"call-out highlight\">\n            <h2>{getinvolved_title}</h2>\n            <ul class=\"call-out-content\"> \n                <li>\n                <div class=\"media-image\">\n                    <a href=\"{getinvolved_link}\"><img src=\"{getinvolved_image}\" alt=\"{getinvolved_title}\" /></a>\n                </div>\n                <p>\n                <p>{getinvolved_summary text_only=\"yes\"} <a class=\"read-more\" href=\"{getinvolved_link}\">Read&nbsp;More</a></p>\n                </li>\n            </ul>\n            <a class=\"call-out-link\" href=\"{getinvolved_link}\">Find our how to help</a>\n        </div><!--callout-->\n    </aside><!--sidebar.ancillary-->\n{/getinvolved}'),
	(12,1,'snippet:modules_latestnews','<div class=\"articles recent\">\n    <h2>Latest News</h2>\n    <a class=\"all-articles-link\" href=\"/news/\">See All</a>\n    <ul class=\"articles-list\">\n        {exp:channel:entries channel=\"news\" limit=\"2\"}\n        <li class=\"list-item\">\n            <div class=\"media-image\">                    \n                <a href=\"/news/article/\"><img src=\"{blog_summary}\" alt=\"{title}\" /></a>\n            </div>\n            <h3><a href=\"/news/article/{url_title}\">{title}</a></h3>\n            <p>{content text_only=\"yes\"} &#8230; <a class=\"read-more\" href=\"#\">Read&nbsp;More</a></p>\n        </li>\n        {/exp:channel:entries}\n    </ul>\n</div><!--articles-->'),
	(23,1,'snippet:if_sidebar','{if \"{relatedcontent:total_rows search:relatedcontent_active=\"=y\"}\" > 0}has-sidebar{/if}'),
	(24,1,'snippet:global_pagebanner','{if pagebanner}\n<div class=\"media-image\">\n    <span class=\"overlay-interior\"></span>\n    <img src=\"{pagebanner:twocolumnbanner}\" alt=\"{title}\" />\n</div>\n{/if}'),
	(25,1,'snippet:if_titleoverride','{if pagetitle_override}{pagetitle_override}{if:else}{title}{/if}'),
	(26,1,'snippet:modules_form','{if form}\n{exp:freeform:form \n    collection=\"{title}\" \n    notify=\"me@jarrettbarnett.com\"\n    template=\"admin_template\" \n    send_user_email=\"yes\" \n    user_email_template=\"user_email_template\" \n    form:id=\"comment-form\" \n    form:class=\"form\" \n    required=\"{form search:field_required=\"=y\" backspace=\"1\"}{form_field}|{/form}\"\n    return=\"{page_uri}/success\"\n}\n    <fieldset>\n        <legend class=\"legend-heading\">Please fill in the fields below...</legend>\n        <ul>\n        {form search:form_active=\"=y\"}\n            {exp:switchee variable = \"{form_type}\" parse=\"inward\"}\n                \n                {case value=\"text\"}\n                    <li class=\"{form_field}\">\n                        <label for=\"{form_field}\">{form_label}:</label>\n                        <input class=\"input-text\" type=\"text\" name=\"{form_field}\" id=\"field-{form_field}\" placeholder=\"Enter your {form_field}\" {if form_required}required{/if} />\n                    </li>\n                {/case}\n                \n                {case value=\"textarea\"}\n                    <li class=\"{form_field}\">\n                        <label for=\"{form_field}\">{form_label}:</label>\n                        <textarea name=\"{form_field}\" id=\"field-{form_field}\" class=\"input-{form_type}\" rows=\"10\" cols=\"40\"{if form_required == \'y\'} required{/if}></textarea>\n                    </li>\n                {/case}\n                \n                {case value=\"dropdown\"}\n                    <li class=\"{form_field}\">\n                        <label for=\"{form_field}\">{form_label}:</label>\n                        <select name=\"{form_field}\" class=\"input-select\"{if form_required == \'y\'} required{/if}>\n                            {form_options}<option value=\"{item}\">{item}</option>{/form_options}\n                        </select>\n                    </li>\n                {/case}\n                \n                {case value=\"multiselect\"}\n                    <li class=\"{form_field}\">\n                        <label for=\"{form_field}\">{form_label}:</label>\n                        <select name=\"{form_field}\" multiple=\"multiple\" class=\"input-{form_type}\"{if form_required == \'y\'} required{/if}>\n                            {form_options}<option value=\"{item}\">{item}</option>{/form_options}\n                        </select>\n                    </li>\n                {/case}\n                \n            {/exp:switchee}\n        {/form}\n        </ul>\n    </fieldset>\n    <div class=\"action\">\n        <div class=\"button-wrapper\">\n            <button class=\"button highlight\" type=\"submit\">Submit Form</button>\n        </div>\n    </div>\n{/exp:freeform:form}\n{/if}'),
	(13,1,'snippet:modules_spotlight','{!-- since we\'re only iterating once --}\n{exp:playa:children field=\"inthespotlight\" var_prefix=\"spotlight\"}\n<div class=\"articles spotlight\">\n    <h2>In the Spotlight</h2>\n    <a class=\"all-articles-link\" href=\"/news/article/{spotlight:url_title}\">Full Story</a>\n    <ul class=\"articles-list\">\n        <li class=\"list-item\">\n            <h3><a href=\"/news/article/{spotlight:url_title}\">{spotlight:title}</a></h3>\n            <p>{spotlight:news_preview text_only=\"yes\"} <a class=\"read-more\" href=\"/news/article/{spotlight:url_title}\">Read&nbsp;More</a></p>\n        </li>        \n    </ul>\n</div><!--articles-->\n{/exp:playa:children}'),
	(19,1,'snippet:layouts_footer','    <footer class=\"site-info\" role=\"contentinfo\">\n        <div class=\"wrapper\">\n            <h1 class=\"hidden\">Site Information</h1>\n            <section class=\"banner\">\n                <div class=\"vcard\">\n                    <address>\n                        <span class=\"org fn\">Minnesota Outreach</span>\n                        <span class=\"address-info\">\n                            <span class=\"street-address\">122 West Grand Avenue, Suite 200</span>\n                            <span class=\"locality\">St. Paul</span>,\n                            <span class=\"region\">MN</span>\n                            <span class=\"postal-code\">55104</span>\n                            <span class=\"country-name hidden\">United States</span>\n                        </span>\n                    </address>\n                    <div class=\"contact-info\">\n                        <a class=\"url hidden\" href=\"http://www.minnesotaumc.org/\">www.minnesotoutreach.org</a>\n                        <a class=\"email\" href=\"mailto:info@minnesotaumc.org\">info@minnesotaoutreach.org</a>\n                        <span class=\"tel\">(612) 870-0458</span>\n                    </div>\n                </div>\n            </section>\n            <div class=\"copyright\">&copy; Copyright 2012</div>\n        </div>\n    </footer>'),
	(20,1,'snippet:layouts_head','    {!-- META DATA --}\n    <meta charset=\"utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <!--[if IE]><meta http-equiv=\"cleartype\" content=\"on\" /><![endif]-->\n    <!--[if lt IE 9]><meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\" /><![endif]-->\n    \n    {!-- SEO and FACEBOOK --}\n    <title>{exp:stash:get name=\'meta_title\'}</title>\n    <meta name=\"description\" content=\"{exp:stash:get name=\'meta_desc\'}\" />\n    <meta name=\"author\" content=\"{exp:stash:get name=\'meta_author\'}\" />\n    <meta name=\"keywords\" content=\"{exp:stash:get name=\'meta_keywords\'}\" />\n    <meta name=\"robots\" content=\"index,follow,archive\" />\n    <meta property=\"og:title\" content=\"{if \"{exp:stash:get name=\'og_title\'}\"}{exp:stash:get name=\'og_title\'}{if:else}{exp:stash:get name=\'page_title\'} > {site_name}{/if}\"/>\n    <meta property=\"og:type\" content=\"{if \"{exp:stash:get name=\'og_type\'}\"}{exp:stash:get name=\'og_type\'}{if:else}website{/if}\"/>\n    <meta property=\"og:url\" content=\"{exp:stash:get name=\'og_url\'}\" />\n    <meta property=\"og:image\" content=\"{if \"{exp:stash:get name=\'og_image\'}\"}{exp:stash:get name=\'og_image\'}{if:else}{site_url}images/logo.gif{/if}\"/>\n    <meta property=\"og:site_name\" content=\"{site_name}\"/>\n    <meta property=\"og:description\" content=\"{exp:stash:get name=\'og_description\'}\"/>\n    \n    {!-- ICONS --}\n    <link rel=\"shortcut icon\" type=\"image/ico\" href=\"/favicon.ico\" />\n    <link rel=\"apple-touch-icon\" href=\"/images/apple-touch-icon.png\" />\n    \n    {!-- STYLESHEETS --}\n    <link rel=\"stylesheet\" media=\"screen, projection\" href=\"/styles/reset.css\" />\n    <link rel=\"stylesheet\" media=\"screen, projection\" href=\"/styles/jcarousel.css\" />\n    <link rel=\"stylesheet\" media=\"screen, projection\" href=\"/styles/screen.css\" />\n    <!--[if lte IE 9]><link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"/styles/ie9.css\" /><![endif]-->\n    <!--[if lte IE 8]><link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"/styles/ie8.css\" /><![endif]-->\n    <!--[if lte IE 7]><link rel=\"stylesheet\" type=\"text/css\" media=\"screen, projection\" href=\"/styles/ie7.css\" /><![endif]-->\n    <link rel=\"alternate\" href=\"/rss/rss_2.0/\" title=\"{site_name} News Feed\" type=\"application/rss+xml\" />\n    \n    {!-- JAVASCRIPT --}\n    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js\"></script>\n    <!--[if lt IE 9]><script type=\"text/javascript\" src=\"/scripts/html5shiv.js\"></script><![endif]-->\n    <script src=\"/scripts/jcarousel.js\"></script>\n    <script src=\"/scripts/global.js\"></script>\n    <script type=\"text/javascript\">var _gaq = _gaq || []; _gaq.push([\'_setAccount\', \'{analytics_id}\'],[\'_trackPageview\']); (function() { var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true; ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\'; var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s); })();</script>\n\n    {!-- MINI-CP --}\n    {if showadmintoolbar == \"y\"}{exp:minicp jquery=\"no\" jqueryui=\"yes\"}{/if}'),
	(21,1,'snippet:layouts_header','    {if showadmintoolbar == \"y\"}{exp:minicp:widget entry_id=\"{exp:stash:get name=\"entry_id\"}\"}{/if}\n    <header class=\"masthead\" role=\"banner\">\n        <div class=\"wrapper\">\n            <a href=\"/\" title=\"{site_name}\" class=\"logo pngfix\">\n                <h1>{site_name}</h1>\n            </a>\n            <nav class=\"nav-member\">\n                <a href=\"/member/profile\">View Profile</a> | {if logged_in}<a href=\"/member/logout\">Logout</a>{if:else}<a href=\"/member/login\">Login</a>{/if}\n            </nav>\n             <nav class=\"nav-main\" role=\"navigation\">\n                <h2 class=\"hidden\">Main Navigation (<a href=\"#content\">Skip to Main Content</a>)</h2>\n                {exp:structure:nav max_depth=\"1\" css_id=\"leftnav\" current_class=\"active\" add_unique_ids=\"yes\"}\n            </nav>\n        </div>\n    </header>'),
	(22,1,'snippet:global_sidebar','{preload_replace:sidebar_field=\"relatedcontent\"}{!-- fallback --}\n\n{exp:playa:children field=\"{preload_sidebar}\" var_prefix=\"sidebar\" search:relatedcontent_active=\"=y\" limit=\"3\"}\n\n    {if \"{sidebar:count}\" == 1}\n    <aside class=\"sidebar ancillary\" role=\"complementary\">\n        <h1 class=\"hidden\">Related Content</h1>\n        <div class=\"featured\">\n            <ul class=\"featured-list\">\n    {/if}\n    \n    \n        {sidebar:callout}    \n            {exp:ifelse parse=\"inward\"}\n                {if callout_image}\n                    <li class=\"list-item alt last\">\n                        <h3 class=\"hidden\">{sidebar:title}</h3>\n                        <div class=\"media-image\">\n                            <a href=\"{if \"{callout_linkoverride}\"}{callout_linkoverride}{if:else}{page_uri}{/if}\"><img src=\"{callout_image:resized}\" alt=\"{sidebar:title}\" /></a>\n                        </div>\n                    </li>\n                {if:else}\n                    <li class=\"list-item last\">\n                        {if callout_previewimage}\n                        <div class=\"media-image vignette\">\n                            <a href=\"{if callout_linkoverride}{callout_linkoverride}{if:else}{page_uri}{/if}\"><img src=\"{callout_previewimage:resized}\" alt=\"{sidebar:title}\" /></a>\n                        </div>\n                        {/if}\n                        <div class=\"content\">\n                            <h3><a href=\"{if callout_linkoverride}{callout_linkoverride}{if:else}{page_uri}{/if}\">{sidebar:title}</a></h3>\n                            {sidebar:content}\n                            {if callout_linktext}<a href=\"{if callout_linkoverride}{callout_linkoverride}{if:else}{page_uri}{/if}\" class=\"button highlight\">{callout_linktext}</a>{/if}\n                        </div>\n                    </li>\n                {/if}\n            {/exp:ifelse}\n        {/sidebar:callout}\n        \n        \n    {if \"{sidebar:count}\" == \"{sidebar:total_results}\"}\n            </ul>\n        </div><!--articles-->\n    </aside><!--sidebar.ancillary-->\n    {/if}\n    \n{/exp:playa:children}');

/*!40000 ALTER TABLE `ee2_snippets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_specialty_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_specialty_templates`;

CREATE TABLE `ee2_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_specialty_templates` WRITE;
/*!40000 ALTER TABLE `ee2_specialty_templates` DISABLE KEYS */;

INSERT INTO `ee2_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`)
VALUES
	(1,1,'y','offline_template','','<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
	(2,1,'y','message_template','','<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
	(3,1,'y','admin_notify_reg','Notification of new member registration','New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
	(4,1,'y','admin_notify_entry','A new channel entry has been posted','A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
	(5,1,'y','admin_notify_mailinglist','Someone has subscribed to your mailing list','A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
	(6,1,'y','admin_notify_comment','You have just received a comment','You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
	(7,1,'y','mbr_activation_instructions','Enclosed is your activation code','Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
	(8,1,'y','forgot_password_instructions','Login information','{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
	(9,1,'y','reset_password_notification','New Login Information','{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
	(10,1,'y','validated_member_notify','Your membership account has been activated','{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
	(11,1,'y','decline_member_validation','Your membership account has been declined','{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
	(12,1,'y','mailinglist_activation_instructions','Email Confirmation','Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
	(13,1,'y','comment_notification','Someone just responded to your comment','{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
	(14,1,'y','comments_opened_notification','New comments have been added','Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
	(15,1,'y','private_message_notification','Someone has sent you a Private Message','\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
	(16,1,'y','pm_inbox_full','Your private message mailbox is full','{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/*!40000 ALTER TABLE `ee2_specialty_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_stash
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_stash`;

CREATE TABLE `ee2_stash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `session_id` varchar(40) DEFAULT NULL,
  `bundle_id` int(11) unsigned NOT NULL DEFAULT '1',
  `key_name` varchar(64) NOT NULL,
  `key_label` varchar(64) DEFAULT NULL,
  `created` int(10) unsigned NOT NULL,
  `expire` int(10) unsigned NOT NULL DEFAULT '0',
  `parameters` text,
  PRIMARY KEY (`id`),
  KEY `bundle_id` (`bundle_id`),
  KEY `key_session` (`key_name`,`session_id`),
  KEY `key_name` (`key_name`),
  KEY `site_id` (`site_id`),
  CONSTRAINT `ee2_stash_fk` FOREIGN KEY (`bundle_id`) REFERENCES `ee2_stash_bundles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_stash` WRITE;
/*!40000 ALTER TABLE `ee2_stash` DISABLE KEYS */;

INSERT INTO `ee2_stash` (`id`, `site_id`, `session_id`, `bundle_id`, `key_name`, `key_label`, `created`, `expire`, `parameters`)
VALUES
	(3,1,'_global',1,'modules_latestnews','modules_latestnews',1351792699,1351879099,'<div class=\"articles recent\">\n    <h2>Latest News</h2>\n    <a class=\"all-articles-link\" href=\"/news/\">See All</a>\n    <ul class=\"articles-list\">\n        {exp:channel:entries channel=\"news\" limit=\"2\" dynamic=\"off\"}\n        <li class=\"list-item\">\n            {if news_featured}\n            <div class=\"media-image\">\n                <a href=\"/news/article/\"><img src=\"{news_featured:thumbnail}\" alt=\"{title}\" /></a>\n            </div>\n            {/if}\n            <h3><a href=\"/news/article/{url_title}\">{title}</a></h3>\n            {if news_preview}\n                {news_preview text_only=\"yes\"} &#8230; <a class=\"read-more\" href=\"/news/article/{url_title}\">Read&nbsp;More</a>\n            {if:else}\n                <p>{exp:eehive_hacksaw chars=\"74\" allow=\"\" append=\' &#8230; <a class=\"read-more\" href=\"/news/article/{url_title}\">Read&nbsp;More</a>\'}{news_summary}{/exp:eehive_hacksaw}</p>\n            {/if}\n        </li>\n        {/exp:channel:entries}\n    </ul>\n</div><!--articles-->\n');

/*!40000 ALTER TABLE `ee2_stash` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_stash_bundles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_stash_bundles`;

CREATE TABLE `ee2_stash_bundles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `bundle_name` varchar(64) NOT NULL,
  `bundle_label` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bundle` (`bundle_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_stash_bundles` WRITE;
/*!40000 ALTER TABLE `ee2_stash_bundles` DISABLE KEYS */;

INSERT INTO `ee2_stash_bundles` (`id`, `site_id`, `bundle_name`, `bundle_label`)
VALUES
	(1,1,'default','Default');

/*!40000 ALTER TABLE `ee2_stash_bundles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_stats`;

CREATE TABLE `ee2_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_stats` WRITE;
/*!40000 ALTER TABLE `ee2_stats` DISABLE KEYS */;

INSERT INTO `ee2_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`)
VALUES
	(1,1,3,8,'The Nerdery',29,0,0,7,1351594132,0,1351800071,0,0,0,1352149254);

/*!40000 ALTER TABLE `ee2_stats` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_status_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_status_groups`;

CREATE TABLE `ee2_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_status_groups` WRITE;
/*!40000 ALTER TABLE `ee2_status_groups` DISABLE KEYS */;

INSERT INTO `ee2_status_groups` (`group_id`, `site_id`, `group_name`)
VALUES
	(1,1,'Statuses'),
	(2,1,'Orders Channel');

/*!40000 ALTER TABLE `ee2_status_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_status_no_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_status_no_access`;

CREATE TABLE `ee2_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_status_no_access` WRITE;
/*!40000 ALTER TABLE `ee2_status_no_access` DISABLE KEYS */;

INSERT INTO `ee2_status_no_access` (`status_id`, `member_group`)
VALUES
	(5,5),
	(6,5),
	(7,5),
	(10,5),
	(11,5),
	(12,5),
	(13,5),
	(15,5);

/*!40000 ALTER TABLE `ee2_status_no_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_statuses`;

CREATE TABLE `ee2_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_statuses` WRITE;
/*!40000 ALTER TABLE `ee2_statuses` DISABLE KEYS */;

INSERT INTO `ee2_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`)
VALUES
	(1,1,1,'open',1,'009933'),
	(2,1,1,'closed',2,'990000'),
	(3,1,2,'open',1,'009933'),
	(4,1,2,'closed',2,'990000'),
	(5,1,2,'Payment Failed',3,'A60F0F'),
	(6,1,2,'Payment Declined',4,'A60F0F'),
	(7,1,2,'Payment Pending',5,'A60F0F'),
	(8,1,2,'Payment Expired',6,'A60F0F'),
	(9,1,2,'Payment Cancelled',7,'A60F0F'),
	(10,1,2,'Payment Voided',8,'A60F0F'),
	(11,1,2,'Payment Refunded',9,'A60F0F'),
	(12,1,2,'Payment Reversed',10,'A60F0F'),
	(13,1,2,'On Hold',11,'D2DE28'),
	(14,1,2,'Processing',12,'0C991F'),
	(15,1,2,'Payment Processing',13,'D2DE28');

/*!40000 ALTER TABLE `ee2_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_carts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_carts`;

CREATE TABLE `ee2_store_carts` (
  `cart_id` varchar(32) NOT NULL,
  `site_id` int(5) NOT NULL,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `contents` text NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `site_id` (`site_id`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_config`;

CREATE TABLE `ee2_store_config` (
  `site_id` int(5) NOT NULL,
  `store_preferences` text,
  PRIMARY KEY (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_config` WRITE;
/*!40000 ALTER TABLE `ee2_store_config` DISABLE KEYS */;

INSERT INTO `ee2_store_config` (`site_id`, `store_preferences`)
VALUES
	(1,'YToyODp7czoxNToiY3VycmVuY3lfc3ltYm9sIjtzOjE6IiQiO3M6MTU6ImN1cnJlbmN5X3N1ZmZpeCI7czowOiIiO3M6MTc6ImN1cnJlbmN5X2RlY2ltYWxzIjtzOjE6IjIiO3M6MTg6ImN1cnJlbmN5X2RlY19wb2ludCI7czoxOiIuIjtzOjIyOiJjdXJyZW5jeV90aG91c2FuZHNfc2VwIjtzOjE6IiwiO3M6MTM6ImN1cnJlbmN5X2NvZGUiO3M6MzoiVVNEIjtzOjEyOiJ3ZWlnaHRfdW5pdHMiO3M6Mjoia2ciO3M6MTU6ImRpbWVuc2lvbl91bml0cyI7czoxOiJtIjtzOjEwOiJmcm9tX2VtYWlsIjtzOjIzOiJ3ZWJtYXN0ZXJAbWMyZGVzaWduLmNvbSI7czo5OiJmcm9tX25hbWUiO3M6OToiV2VibWFzdGVyIjtzOjIyOiJleHBvcnRfcGRmX29yaWVudGF0aW9uIjtzOjE6IlAiO3M6MjI6ImV4cG9ydF9wZGZfcGFnZV9mb3JtYXQiO3M6MjoiQTQiO3M6MjA6Im9yZGVyX2RldGFpbHNfaGVhZGVyIjtzOjA6IiI7czoyNjoib3JkZXJfZGV0YWlsc19oZWFkZXJfcmlnaHQiO3M6MDoiIjtzOjIwOiJvcmRlcl9kZXRhaWxzX2Zvb3RlciI7czowOiIiO3M6MjE6ImRlZmF1bHRfb3JkZXJfYWRkcmVzcyI7czoyNDoic2hpcHBpbmdfc2FtZV9hc19iaWxsaW5nIjtzOjEyOiJ0YXhfcm91bmRpbmciO3M6MToibiI7czoxODoiZm9yY2VfbWVtYmVyX2xvZ2luIjtzOjE6Im4iO3M6MTI6Im9yZGVyX2ZpZWxkcyI7YToyMjp7czoxMjoiYmlsbGluZ19uYW1lIjthOjE6e3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMjoibV9maWVsZF9pZF8xIjt9czoxNjoiYmlsbGluZ19hZGRyZXNzMSI7YToxOntzOjEyOiJtZW1iZXJfZmllbGQiO3M6MTI6Im1fZmllbGRfaWRfMyI7fXM6MTY6ImJpbGxpbmdfYWRkcmVzczIiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEyOiJtX2ZpZWxkX2lkXzQiO31zOjE2OiJiaWxsaW5nX2FkZHJlc3MzIjthOjE6e3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8yNyI7fXM6MTQ6ImJpbGxpbmdfcmVnaW9uIjthOjE6e3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8yNSI7fXM6MTU6ImJpbGxpbmdfY291bnRyeSI7YToxOntzOjEyOiJtZW1iZXJfZmllbGQiO3M6MTI6Im1fZmllbGRfaWRfOCI7fXM6MTY6ImJpbGxpbmdfcG9zdGNvZGUiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEyOiJtX2ZpZWxkX2lkXzciO31zOjEzOiJiaWxsaW5nX3Bob25lIjthOjE6e3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8xMSI7fXM6MTM6InNoaXBwaW5nX25hbWUiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzEzIjt9czoxNzoic2hpcHBpbmdfYWRkcmVzczEiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzE1Ijt9czoxNzoic2hpcHBpbmdfYWRkcmVzczIiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzE2Ijt9czoxNzoic2hpcHBpbmdfYWRkcmVzczMiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzI4Ijt9czoxNToic2hpcHBpbmdfcmVnaW9uIjthOjE6e3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8yOSI7fXM6MTY6InNoaXBwaW5nX2NvdW50cnkiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzIwIjt9czoxNzoic2hpcHBpbmdfcG9zdGNvZGUiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzE5Ijt9czoxNDoic2hpcHBpbmdfcGhvbmUiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjEzOiJtX2ZpZWxkX2lkXzI2Ijt9czoxMToib3JkZXJfZW1haWwiO2E6MTp7czoxMjoibWVtYmVyX2ZpZWxkIjtzOjA6IiI7fXM6MTM6Im9yZGVyX2N1c3RvbTEiO2E6Mjp7czo1OiJ0aXRsZSI7czowOiIiO3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8zMCI7fXM6MTM6Im9yZGVyX2N1c3RvbTIiO2E6Mjp7czo1OiJ0aXRsZSI7czowOiIiO3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8zMSI7fXM6MTM6Im9yZGVyX2N1c3RvbTMiO2E6Mjp7czo1OiJ0aXRsZSI7czowOiIiO3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8zMiI7fXM6MTM6Im9yZGVyX2N1c3RvbTQiO2E6Mjp7czo1OiJ0aXRsZSI7czowOiIiO3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8zMyI7fXM6MTM6Im9yZGVyX2N1c3RvbTUiO2E6Mjp7czo1OiJ0aXRsZSI7czowOiIiO3M6MTI6Im1lbWJlcl9maWVsZCI7czoxMzoibV9maWVsZF9pZF8zNCI7fX1zOjg6InNlY3VyaXR5IjtzOjA6IiI7czoxNToiZGVmYXVsdF9jb3VudHJ5IjtzOjA6IiI7czoxNDoiZGVmYXVsdF9yZWdpb24iO3M6MDoiIjtzOjI2OiJkZWZhdWx0X3NoaXBwaW5nX21ldGhvZF9pZCI7czowOiIiO3M6MjA6InNlY3VyZV90ZW1wbGF0ZV90YWdzIjtzOjE6Im4iO3M6MTI6InNob3dfY3BfbWVudSI7czoxOiJ5IjtzOjE3OiJvcmRlcl9pbnZvaWNlX3VybCI7czowOiIiO3M6MjA6ImVtcHR5X2NhcnRfb25fbG9nb3V0IjtzOjE6InkiO3M6MTc6ImNjX3BheW1lbnRfbWV0aG9kIjtzOjk6ImF1dGhvcml6ZSI7fQ==');

/*!40000 ALTER TABLE `ee2_store_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_countries`;

CREATE TABLE `ee2_store_countries` (
  `site_id` int(5) NOT NULL,
  `country_code` char(2) NOT NULL,
  PRIMARY KEY (`site_id`,`country_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_countries` WRITE;
/*!40000 ALTER TABLE `ee2_store_countries` DISABLE KEYS */;

INSERT INTO `ee2_store_countries` (`site_id`, `country_code`)
VALUES
	(1,'ad'),
	(1,'ae'),
	(1,'af'),
	(1,'ag'),
	(1,'ai'),
	(1,'al'),
	(1,'am'),
	(1,'an'),
	(1,'ao'),
	(1,'ap'),
	(1,'aq'),
	(1,'ar'),
	(1,'as'),
	(1,'at'),
	(1,'au'),
	(1,'aw'),
	(1,'ax'),
	(1,'az'),
	(1,'ba'),
	(1,'bb'),
	(1,'bd'),
	(1,'be'),
	(1,'bf'),
	(1,'bg'),
	(1,'bh'),
	(1,'bi'),
	(1,'bj'),
	(1,'bm'),
	(1,'bn'),
	(1,'bo'),
	(1,'br'),
	(1,'bs'),
	(1,'bt'),
	(1,'bv'),
	(1,'bw'),
	(1,'by'),
	(1,'bz'),
	(1,'ca'),
	(1,'cc'),
	(1,'cd'),
	(1,'cf'),
	(1,'cg'),
	(1,'ch'),
	(1,'ci'),
	(1,'ck'),
	(1,'cl'),
	(1,'cm'),
	(1,'cn'),
	(1,'co'),
	(1,'cr'),
	(1,'cs'),
	(1,'cu'),
	(1,'cv'),
	(1,'cx'),
	(1,'cy'),
	(1,'cz'),
	(1,'de'),
	(1,'dj'),
	(1,'dk'),
	(1,'dm'),
	(1,'do'),
	(1,'dz'),
	(1,'ec'),
	(1,'ee'),
	(1,'eg'),
	(1,'eh'),
	(1,'er'),
	(1,'es'),
	(1,'et'),
	(1,'eu'),
	(1,'fi'),
	(1,'fj'),
	(1,'fk'),
	(1,'fm'),
	(1,'fo'),
	(1,'fr'),
	(1,'ga'),
	(1,'gb'),
	(1,'gd'),
	(1,'ge'),
	(1,'gf'),
	(1,'gh'),
	(1,'gi'),
	(1,'gl'),
	(1,'gm'),
	(1,'gn'),
	(1,'gp'),
	(1,'gq'),
	(1,'gr'),
	(1,'gs'),
	(1,'gt'),
	(1,'gu'),
	(1,'gw'),
	(1,'gy'),
	(1,'hk'),
	(1,'hm'),
	(1,'hn'),
	(1,'hr'),
	(1,'ht'),
	(1,'hu'),
	(1,'id'),
	(1,'ie'),
	(1,'il'),
	(1,'in'),
	(1,'io'),
	(1,'iq'),
	(1,'ir'),
	(1,'is'),
	(1,'it'),
	(1,'jm'),
	(1,'jo'),
	(1,'jp'),
	(1,'ke'),
	(1,'kg'),
	(1,'kh'),
	(1,'ki'),
	(1,'km'),
	(1,'kn'),
	(1,'kp'),
	(1,'kr'),
	(1,'kw'),
	(1,'ky'),
	(1,'kz'),
	(1,'la'),
	(1,'lb'),
	(1,'lc'),
	(1,'li'),
	(1,'lk'),
	(1,'lr'),
	(1,'ls'),
	(1,'lt'),
	(1,'lu'),
	(1,'lv'),
	(1,'ly'),
	(1,'ma'),
	(1,'mc'),
	(1,'md'),
	(1,'mg'),
	(1,'mh'),
	(1,'mk'),
	(1,'ml'),
	(1,'mm'),
	(1,'mn'),
	(1,'mo'),
	(1,'mp'),
	(1,'mq'),
	(1,'mr'),
	(1,'ms'),
	(1,'mt'),
	(1,'mu'),
	(1,'mv'),
	(1,'mw'),
	(1,'mx'),
	(1,'my'),
	(1,'mz'),
	(1,'na'),
	(1,'nc'),
	(1,'ne'),
	(1,'nf'),
	(1,'ng'),
	(1,'ni'),
	(1,'nl'),
	(1,'no'),
	(1,'np'),
	(1,'nr'),
	(1,'nt'),
	(1,'nu'),
	(1,'nz'),
	(1,'om'),
	(1,'pa'),
	(1,'pe'),
	(1,'pf'),
	(1,'pg'),
	(1,'ph'),
	(1,'pk'),
	(1,'pl'),
	(1,'pm'),
	(1,'pn'),
	(1,'pr'),
	(1,'ps'),
	(1,'pt'),
	(1,'pw'),
	(1,'py'),
	(1,'qa'),
	(1,'re'),
	(1,'ro'),
	(1,'rs'),
	(1,'ru'),
	(1,'rw'),
	(1,'sa'),
	(1,'sb'),
	(1,'sc'),
	(1,'sd'),
	(1,'se'),
	(1,'sg'),
	(1,'sh'),
	(1,'si'),
	(1,'sj'),
	(1,'sk'),
	(1,'sl'),
	(1,'sm'),
	(1,'sn'),
	(1,'so'),
	(1,'sr'),
	(1,'st'),
	(1,'sv'),
	(1,'sy'),
	(1,'sz'),
	(1,'tc'),
	(1,'td'),
	(1,'tf'),
	(1,'tg'),
	(1,'th'),
	(1,'tj'),
	(1,'tk'),
	(1,'tl'),
	(1,'tm'),
	(1,'tn'),
	(1,'to'),
	(1,'tr'),
	(1,'tt'),
	(1,'tv'),
	(1,'tw'),
	(1,'tz'),
	(1,'ua'),
	(1,'ug'),
	(1,'uk'),
	(1,'us'),
	(1,'uy'),
	(1,'uz'),
	(1,'va'),
	(1,'vc'),
	(1,'ve'),
	(1,'vg'),
	(1,'vi'),
	(1,'vn'),
	(1,'vu'),
	(1,'wf'),
	(1,'ws'),
	(1,'ye'),
	(1,'yt'),
	(1,'yu'),
	(1,'za'),
	(1,'zm'),
	(1,'zw');

/*!40000 ALTER TABLE `ee2_store_countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_email_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_email_templates`;

CREATE TABLE `ee2_store_email_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `name` varchar(30) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `contents` text NOT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `mail_format` varchar(5) NOT NULL,
  `word_wrap` char(1) NOT NULL,
  `enabled` char(1) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_email_templates` WRITE;
/*!40000 ALTER TABLE `ee2_store_email_templates` DISABLE KEYS */;

INSERT INTO `ee2_store_email_templates` (`template_id`, `site_id`, `name`, `subject`, `contents`, `bcc`, `mail_format`, `word_wrap`, `enabled`)
VALUES
	(1,1,'order_confirmation','Order Confirmation','Hi {billing_name},\nThank you for placing your order with us.\nYour order details are -\n\nOrder Id    - {order_id}\nOrder Total - {order_total}\n\nOrder Items :\n{items}\nItem - {title}, Qty - {item_qty}, Item Total - {item_total}\n{/items}\n\nShipping Information :\nName     - {shipping_name}\nAddress  - {shipping_address1}\nAddress  - {shipping_address2}\nAddress  - {shipping_address3}\nRegion   - {shipping_region}\nCountry  - {shipping_country}\nPostcode - {shipping_postcode}\n\nPlease contact us if any of the above details appear incorrect.\nThank You!',NULL,'text','y','y'),
	(2,1,'payment_confirmation','Payment Received','Hi {billing_name},\nYour completed payment has been received for the following order -\n\nOrder Id    - {order_id}\nOrder Total - {order_total}\n\nOrder Items :\n{items}\nItem - {title}, Qty - {item_qty}, Item Total - {item_total}\n{/items}\n\nYour order will be shipped as soon as possible.\n\nShipping Information :\nName     - {shipping_name}\nAddress  - {shipping_address1}\nAddress  - {shipping_address2}\nAddress  - {shipping_address3}\nRegion   - {shipping_region}\nCountry  - {shipping_country}\nPostcode - {shipping_postcode}\n\nPlease contact us if any of the above details appear incorrect.\nThank You!',NULL,'text','y','y');

/*!40000 ALTER TABLE `ee2_store_email_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_order_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_order_history`;

CREATE TABLE `ee2_store_order_history` (
  `order_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `order_status` varchar(20) NOT NULL,
  `order_status_updated` int(10) unsigned NOT NULL DEFAULT '0',
  `order_status_member` int(10) unsigned NOT NULL DEFAULT '0',
  `message` text,
  PRIMARY KEY (`order_history_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_order_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_order_items`;

CREATE TABLE `ee2_store_order_items` (
  `order_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `sku` varchar(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `modifiers` text,
  `price` decimal(19,4) NOT NULL,
  `price_inc_tax` decimal(19,4) NOT NULL,
  `regular_price` decimal(19,4) NOT NULL,
  `regular_price_inc_tax` decimal(19,4) NOT NULL,
  `on_sale` char(1) NOT NULL,
  `weight` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `handling` decimal(19,4) NOT NULL,
  `handling_tax` decimal(19,4) NOT NULL,
  `free_shipping` char(1) NOT NULL,
  `tax_exempt` char(1) NOT NULL,
  `item_qty` int(4) unsigned NOT NULL,
  `item_subtotal` decimal(19,4) NOT NULL,
  `item_tax` decimal(19,4) NOT NULL,
  `item_total` decimal(19,4) NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_order_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_order_statuses`;

CREATE TABLE `ee2_store_order_statuses` (
  `order_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `highlight` varchar(6) NOT NULL,
  `email_template` varchar(30) DEFAULT NULL,
  `display_order` int(4) unsigned NOT NULL,
  `is_default` char(1) DEFAULT NULL,
  PRIMARY KEY (`order_status_id`),
  KEY `site_id` (`site_id`),
  KEY `display_order` (`display_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_order_statuses` WRITE;
/*!40000 ALTER TABLE `ee2_store_order_statuses` DISABLE KEYS */;

INSERT INTO `ee2_store_order_statuses` (`order_status_id`, `site_id`, `name`, `highlight`, `email_template`, `display_order`, `is_default`)
VALUES
	(1,1,'new','0C991F','1',0,'y'),
	(2,1,'Picking','0C991F','0',1,'n'),
	(3,1,'On Hold','CFC811','0',2,'n'),
	(4,1,'Shipped','000000','0',3,'n');

/*!40000 ALTER TABLE `ee2_store_order_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_orders`;

CREATE TABLE `ee2_store_orders` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `order_hash` varchar(32) NOT NULL,
  `member_id` int(10) unsigned DEFAULT NULL,
  `order_date` int(10) unsigned NOT NULL,
  `order_completed_date` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(40) NOT NULL,
  `order_status` varchar(20) DEFAULT NULL,
  `order_status_updated` int(10) unsigned DEFAULT NULL,
  `order_status_member` int(10) unsigned NOT NULL DEFAULT '0',
  `order_qty` int(4) unsigned NOT NULL,
  `order_subtotal` decimal(19,4) NOT NULL,
  `order_subtotal_tax` decimal(19,4) NOT NULL,
  `order_discount` decimal(19,4) NOT NULL,
  `order_discount_tax` decimal(19,4) NOT NULL,
  `order_shipping` decimal(19,4) NOT NULL,
  `order_shipping_tax` decimal(19,4) NOT NULL,
  `order_handling` decimal(19,4) NOT NULL,
  `order_handling_tax` decimal(19,4) NOT NULL,
  `order_tax` decimal(19,4) NOT NULL,
  `order_total` decimal(19,4) NOT NULL,
  `order_paid` decimal(19,4) NOT NULL,
  `order_paid_date` int(10) unsigned DEFAULT NULL,
  `order_email` varchar(100) DEFAULT NULL,
  `promo_code_id` int(10) unsigned DEFAULT NULL,
  `promo_code` varchar(20) DEFAULT NULL,
  `payment_method` varchar(50) DEFAULT NULL,
  `shipping_method` varchar(100) DEFAULT NULL,
  `shipping_method_id` int(10) unsigned DEFAULT NULL,
  `shipping_method_plugin` varchar(50) DEFAULT NULL,
  `shipping_method_rule` varchar(50) DEFAULT NULL,
  `tax_id` int(10) unsigned DEFAULT NULL,
  `tax_name` varchar(40) DEFAULT NULL,
  `tax_rate` double NOT NULL,
  `order_length` double NOT NULL,
  `order_width` double NOT NULL,
  `order_height` double NOT NULL,
  `dimension_units` varchar(5) NOT NULL,
  `order_weight` double NOT NULL,
  `weight_units` varchar(5) NOT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  `billing_address1` varchar(255) DEFAULT NULL,
  `billing_address2` varchar(255) DEFAULT NULL,
  `billing_address3` varchar(255) DEFAULT NULL,
  `billing_region` varchar(255) DEFAULT NULL,
  `billing_country` char(2) DEFAULT NULL,
  `billing_postcode` varchar(10) DEFAULT NULL,
  `billing_phone` varchar(15) DEFAULT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_address1` varchar(255) DEFAULT NULL,
  `shipping_address2` varchar(255) DEFAULT NULL,
  `shipping_address3` varchar(255) DEFAULT NULL,
  `shipping_region` varchar(255) DEFAULT NULL,
  `shipping_country` char(2) DEFAULT NULL,
  `shipping_postcode` varchar(10) DEFAULT NULL,
  `shipping_phone` varchar(15) DEFAULT NULL,
  `billing_same_as_shipping` char(1) NOT NULL,
  `shipping_same_as_billing` char(1) NOT NULL,
  `order_custom1` varchar(255) DEFAULT NULL,
  `order_custom2` varchar(255) DEFAULT NULL,
  `order_custom3` varchar(255) DEFAULT NULL,
  `order_custom4` varchar(255) DEFAULT NULL,
  `order_custom5` varchar(255) DEFAULT NULL,
  `order_custom6` varchar(255) DEFAULT NULL,
  `order_custom7` varchar(255) DEFAULT NULL,
  `order_custom8` varchar(255) DEFAULT NULL,
  `order_custom9` varchar(255) DEFAULT NULL,
  `return_url` varchar(255) DEFAULT NULL,
  `cancel_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_hash` (`order_hash`),
  KEY `site_id` (`site_id`),
  KEY `member_id` (`member_id`),
  KEY `order_date` (`order_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_payment_methods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_payment_methods`;

CREATE TABLE `ee2_store_payment_methods` (
  `payment_method_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `class` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`payment_method_id`),
  UNIQUE KEY `site_id_name` (`site_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_payments`;

CREATE TABLE `ee2_store_payments` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `payment_hash` varchar(32) NOT NULL,
  `payment_status` varchar(20) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `payment_method_class` varchar(50) NOT NULL,
  `amount` decimal(19,4) NOT NULL,
  `payment_date` int(10) unsigned NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `payment_hash` (`payment_hash`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_product_modifiers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_product_modifiers`;

CREATE TABLE `ee2_store_product_modifiers` (
  `product_mod_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `mod_type` varchar(20) NOT NULL,
  `mod_name` varchar(100) NOT NULL,
  `mod_instructions` text,
  `mod_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`product_mod_id`),
  KEY `entry_id` (`entry_id`),
  KEY `mod_order` (`mod_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_product_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_product_options`;

CREATE TABLE `ee2_store_product_options` (
  `product_opt_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_mod_id` int(10) unsigned NOT NULL,
  `opt_name` varchar(100) NOT NULL,
  `opt_price_mod` decimal(19,4) DEFAULT NULL,
  `opt_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`product_opt_id`),
  KEY `product_mod_id` (`product_mod_id`),
  KEY `opt_order` (`opt_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_products`;

CREATE TABLE `ee2_store_products` (
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `regular_price` decimal(19,4) NOT NULL,
  `sale_price` decimal(19,4) DEFAULT NULL,
  `sale_price_enabled` char(1) NOT NULL,
  `sale_start_date` int(10) unsigned DEFAULT NULL,
  `sale_end_date` int(10) unsigned DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `dimension_l` double DEFAULT NULL,
  `dimension_w` double DEFAULT NULL,
  `dimension_h` double DEFAULT NULL,
  `handling` decimal(19,4) NOT NULL,
  `free_shipping` char(1) NOT NULL,
  `tax_exempt` char(1) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_promo_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_promo_codes`;

CREATE TABLE `ee2_store_promo_codes` (
  `promo_code_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `description` varchar(100) NOT NULL,
  `promo_code` varchar(20) DEFAULT NULL,
  `type` char(1) NOT NULL,
  `value` decimal(19,4) DEFAULT NULL,
  `free_shipping` char(1) NOT NULL,
  `start_date` int(10) unsigned DEFAULT NULL,
  `end_date` int(10) unsigned DEFAULT NULL,
  `use_limit` int(4) unsigned DEFAULT NULL,
  `use_count` int(4) unsigned DEFAULT NULL,
  `per_user_limit` int(4) unsigned DEFAULT NULL,
  `member_group_id` int(10) unsigned DEFAULT NULL,
  `enabled` char(1) NOT NULL,
  PRIMARY KEY (`promo_code_id`),
  KEY `site_id` (`site_id`),
  KEY `promo_code` (`promo_code`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_regions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_regions`;

CREATE TABLE `ee2_store_regions` (
  `site_id` int(5) NOT NULL,
  `country_code` char(2) NOT NULL DEFAULT '',
  `region_code` varchar(5) NOT NULL DEFAULT '',
  `region_name` varchar(40) NOT NULL,
  PRIMARY KEY (`site_id`,`country_code`,`region_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_regions` WRITE;
/*!40000 ALTER TABLE `ee2_store_regions` DISABLE KEYS */;

INSERT INTO `ee2_store_regions` (`site_id`, `country_code`, `region_code`, `region_name`)
VALUES
	(1,'us','AL','Alabama'),
	(1,'us','AK','Alaska'),
	(1,'us','AZ','Arizona'),
	(1,'us','AR','Arkansas'),
	(1,'us','CA','California'),
	(1,'us','CO','Colorado'),
	(1,'us','CT','Connecticut'),
	(1,'us','DE','Delaware'),
	(1,'us','DC','District of Columbia'),
	(1,'us','FL','Florida'),
	(1,'us','GA','Georgia'),
	(1,'us','HI','Hawaii'),
	(1,'us','ID','Idaho'),
	(1,'us','IL','Illinois'),
	(1,'us','IN','Indiana'),
	(1,'us','IA','Iowa'),
	(1,'us','KS','Kansas'),
	(1,'us','KY','Kentucky'),
	(1,'us','LA','Louisiana'),
	(1,'us','ME','Maine'),
	(1,'us','MD','Maryland'),
	(1,'us','MA','Massachusetts'),
	(1,'us','MI','Michigan'),
	(1,'us','MN','Minnesota'),
	(1,'us','MS','Mississippi'),
	(1,'us','MO','Missouri'),
	(1,'us','MT','Montana'),
	(1,'us','NE','Nebraska'),
	(1,'us','NV','Nevada'),
	(1,'us','NH','New Hampshire'),
	(1,'us','NJ','New Jersey'),
	(1,'us','NM','New Mexico'),
	(1,'us','NY','New York'),
	(1,'us','NC','North Carolina'),
	(1,'us','ND','North Dakota'),
	(1,'us','OH','Ohio'),
	(1,'us','OK','Oklahoma'),
	(1,'us','OR','Oregon'),
	(1,'us','PA','Pennsylvania'),
	(1,'us','RI','Rhode Island'),
	(1,'us','SC','South Carolina'),
	(1,'us','SD','South Dakota'),
	(1,'us','TN','Tennessee'),
	(1,'us','TX','Texas'),
	(1,'us','UT','Utah'),
	(1,'us','VT','Vermont'),
	(1,'us','VA','Virginia'),
	(1,'us','WA','Washington'),
	(1,'us','WV','West Virginia'),
	(1,'us','WI','Wisconsin'),
	(1,'us','WY','Wyoming'),
	(1,'ca','AB','Alberta'),
	(1,'ca','BC','British Columbia'),
	(1,'ca','MB','Manitoba'),
	(1,'ca','NB','New Brunswick'),
	(1,'ca','NL','Newfoundland and Labrador'),
	(1,'ca','NT','Northwest Territories'),
	(1,'ca','NS','Nova Scotia'),
	(1,'ca','NU','Nunavut'),
	(1,'ca','ON','Ontario'),
	(1,'ca','PE','Prince Edward Island'),
	(1,'ca','QC','Quebec'),
	(1,'ca','SK','Saskatchewan'),
	(1,'ca','YT','Yukon'),
	(1,'au','NSW','New South Wales'),
	(1,'au','NT','Northern Territory'),
	(1,'au','QLD','Queensland'),
	(1,'au','SA','South Australia'),
	(1,'au','TAS','Tasmania'),
	(1,'au','VIC','Victoria'),
	(1,'au','WA','Western Australia');

/*!40000 ALTER TABLE `ee2_store_regions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_shipping_methods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_shipping_methods`;

CREATE TABLE `ee2_store_shipping_methods` (
  `shipping_method_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `class` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL,
  `display_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`shipping_method_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_shipping_methods` WRITE;
/*!40000 ALTER TABLE `ee2_store_shipping_methods` DISABLE KEYS */;

INSERT INTO `ee2_store_shipping_methods` (`shipping_method_id`, `site_id`, `class`, `title`, `settings`, `enabled`, `display_order`)
VALUES
	(1,1,'Store_shipping_ups','UPS','YToxMTp7czoxMDoiYWNjZXNzX2tleSI7czowOiIiO3M6NzoidXNlcl9pZCI7czowOiIiO3M6ODoicGFzc3dvcmQiO3M6MDoiIjtzOjExOiJwaWNrdXBfdHlwZSI7czoyOiIwMSI7czo3OiJzZXJ2aWNlIjtzOjA6IiI7czo5OiJwYWNrYWdpbmciO3M6MjoiMDIiO3M6MTE6InNvdXJjZV9jaXR5IjtzOjA6IiI7czoxMDoic291cmNlX3ppcCI7czowOiIiO3M6MTQ6InNvdXJjZV9jb3VudHJ5IjtzOjA6IiI7czoxMjoiaW5zdXJlX29yZGVyIjtiOjA7czo5OiJ0ZXN0X21vZGUiO2I6MDt9',1,0),
	(2,1,'Store_shipping_fedex','FedEx','YToxMTp7czo3OiJhcGlfa2V5IjtzOjE2OiI3YlVTUkNQejdGblpFck5rIjtzOjg6InBhc3N3b3JkIjtzOjk6Im1jMmFjY2VzcyI7czoxMDoiYWNjb3VudF9ubyI7czo5OiI1MTAwODc1MjYiO3M6ODoibWV0ZXJfbm8iO3M6OToiMTE4NTU2MjUzIjtzOjEyOiJkcm9wb2ZmX3R5cGUiO3M6MTQ6IlJFR1VMQVJfUElDS1VQIjtzOjEyOiJzZXJ2aWNlX3R5cGUiO3M6MTI6IkZFREVYX0dST1VORCI7czoxNDoicGFja2FnaW5nX3R5cGUiO3M6MTQ6IllPVVJfUEFDS0FHSU5HIjtzOjExOiJzb3VyY2VfY2l0eSI7czo1OiJDaGljbyI7czoxMDoic291cmNlX3ppcCI7czo1OiI5NTkyOCI7czoxNDoic291cmNlX2NvdW50cnkiO3M6MjoidXMiO3M6OToidGVzdF9tb2RlIjtiOjE7fQ==',1,1);

/*!40000 ALTER TABLE `ee2_store_shipping_methods` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_shipping_rules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_shipping_rules`;

CREATE TABLE `ee2_store_shipping_rules` (
  `shipping_rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_method_id` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `country_code` char(2) NOT NULL DEFAULT '',
  `region_code` varchar(5) NOT NULL DEFAULT '',
  `postcode` varchar(10) NOT NULL DEFAULT '',
  `min_weight` double DEFAULT NULL,
  `max_weight` double DEFAULT NULL,
  `min_order_total` decimal(19,4) DEFAULT NULL,
  `max_order_total` decimal(19,4) DEFAULT NULL,
  `min_order_qty` int(4) unsigned DEFAULT NULL,
  `max_order_qty` int(4) unsigned DEFAULT NULL,
  `base_rate` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `per_item_rate` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `per_weight_rate` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `percent_rate` double NOT NULL DEFAULT '0',
  `min_rate` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `max_rate` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `priority` int(4) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shipping_rule_id`),
  KEY `shipping_method_id` (`shipping_method_id`),
  KEY `country_code` (`country_code`),
  KEY `region_code` (`region_code`),
  KEY `postcode` (`postcode`),
  KEY `priority` (`priority`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_store_shipping_rules` WRITE;
/*!40000 ALTER TABLE `ee2_store_shipping_rules` DISABLE KEYS */;

INSERT INTO `ee2_store_shipping_rules` (`shipping_rule_id`, `shipping_method_id`, `title`, `country_code`, `region_code`, `postcode`, `min_weight`, `max_weight`, `min_order_total`, `max_order_total`, `min_order_qty`, `max_order_qty`, `base_rate`, `per_item_rate`, `per_weight_rate`, `percent_rate`, `min_rate`, `max_rate`, `priority`, `enabled`)
VALUES
	(1,2,'','','','',NULL,NULL,NULL,NULL,NULL,NULL,0.0000,0.0000,0.0000,0,0.0000,0.0000,0,1);

/*!40000 ALTER TABLE `ee2_store_shipping_rules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_store_stock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_stock`;

CREATE TABLE `ee2_store_stock` (
  `sku` varchar(20) NOT NULL,
  `entry_id` int(10) unsigned NOT NULL,
  `stock_level` int(4) DEFAULT NULL,
  `min_order_qty` int(4) DEFAULT NULL,
  `track_stock` char(1) DEFAULT NULL,
  PRIMARY KEY (`sku`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_stock_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_stock_options`;

CREATE TABLE `ee2_store_stock_options` (
  `sku` varchar(20) NOT NULL,
  `product_mod_id` int(10) unsigned NOT NULL,
  `entry_id` int(10) unsigned NOT NULL,
  `product_opt_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sku`,`product_mod_id`),
  KEY `entry_id` (`entry_id`),
  KEY `product_opt_id` (`product_opt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_store_tax_rates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_store_tax_rates`;

CREATE TABLE `ee2_store_tax_rates` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) NOT NULL,
  `tax_name` varchar(40) NOT NULL,
  `country_code` char(2) NOT NULL,
  `region_code` varchar(5) NOT NULL,
  `tax_rate` double NOT NULL,
  `tax_shipping` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_id`),
  KEY `site_id` (`site_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_structure
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_structure`;

CREATE TABLE `ee2_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_structure` WRITE;
/*!40000 ALTER TABLE `ee2_structure` DISABLE KEYS */;

INSERT INTO `ee2_structure` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `listing_cid`, `lft`, `rgt`, `dead`, `hidden`)
VALUES
	(0,0,0,0,0,1,46,'root','n'),
	(1,1,0,1,0,2,3,'','n'),
	(1,2,0,1,8,10,19,'','n'),
	(1,3,0,1,0,40,45,'','y'),
	(1,4,17,1,0,21,24,'','n'),
	(1,5,0,1,0,4,9,'','n'),
	(1,6,3,1,4,41,42,'','y'),
	(1,7,3,1,2,43,44,'','y'),
	(1,11,5,1,0,5,6,'','n'),
	(1,12,5,1,0,7,8,'','n'),
	(1,13,2,1,0,11,12,'','n'),
	(1,14,2,1,0,13,14,'','n'),
	(1,15,2,1,0,15,16,'','n'),
	(1,16,2,1,0,17,18,'','n'),
	(1,17,0,1,0,20,27,'','n'),
	(1,18,0,1,0,28,31,'','n'),
	(1,19,0,1,0,32,35,'','n'),
	(1,20,18,1,0,29,30,'','n'),
	(1,21,19,1,0,33,34,'','n'),
	(1,22,17,1,0,25,26,'','n'),
	(1,26,0,1,9,36,37,'','y'),
	(1,30,0,1,0,38,39,'','y'),
	(1,31,4,1,0,22,23,'','y');

/*!40000 ALTER TABLE `ee2_structure` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_structure_channels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_structure_channels`;

CREATE TABLE `ee2_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_structure_channels` WRITE;
/*!40000 ALTER TABLE `ee2_structure_channels` DISABLE KEYS */;

INSERT INTO `ee2_structure_channels` (`site_id`, `channel_id`, `template_id`, `type`, `split_assets`, `show_in_page_selector`)
VALUES
	(1,8,51,'listing','n','n'),
	(1,1,10,'page','n','y'),
	(1,3,2,'asset','n','n'),
	(1,6,2,'asset','n','n'),
	(1,4,20,'listing','n','n'),
	(1,7,2,'unmanaged','n','n'),
	(1,2,17,'listing','n','n'),
	(1,5,2,'asset','n','n'),
	(1,9,60,'listing','n','y');

/*!40000 ALTER TABLE `ee2_structure_channels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_structure_listings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_structure_listings`;

CREATE TABLE `ee2_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_structure_listings` WRITE;
/*!40000 ALTER TABLE `ee2_structure_listings` DISABLE KEYS */;

INSERT INTO `ee2_structure_listings` (`site_id`, `entry_id`, `parent_id`, `channel_id`, `template_id`, `uri`)
VALUES
	(1,8,2,8,51,'pacific-earthquake-relief'),
	(1,9,2,8,51,'northern-minnesota-campuses-are-fertile-mission-fields'),
	(1,10,2,8,51,'minnesota-outreach-women-how-it-works'),
	(1,24,2,8,51,'consectetur-adipiscing-elit'),
	(1,25,2,8,51,'donec-vel-sapien-quam'),
	(1,27,26,9,60,'imagine-no-malaria'),
	(1,28,26,9,60,'donate-to-japan'),
	(1,29,26,9,60,'prefer-print');

/*!40000 ALTER TABLE `ee2_structure_listings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_structure_members
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_structure_members`;

CREATE TABLE `ee2_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_structure_members` WRITE;
/*!40000 ALTER TABLE `ee2_structure_members` DISABLE KEYS */;

INSERT INTO `ee2_structure_members` (`member_id`, `site_id`, `nav_state`)
VALUES
	(2,1,'[\"5\",\"2\",\"17\",\"4\",\"18\",\"19\",\"3\"]');

/*!40000 ALTER TABLE `ee2_structure_members` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_structure_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_structure_settings`;

CREATE TABLE `ee2_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_structure_settings` WRITE;
/*!40000 ALTER TABLE `ee2_structure_settings` DISABLE KEYS */;

INSERT INTO `ee2_structure_settings` (`id`, `site_id`, `var`, `var_value`)
VALUES
	(1,0,'action_ajax_move','54'),
	(2,0,'module_id','19'),
	(18,1,'hide_hidden_templates','y'),
	(17,1,'redirect_on_publish','y'),
	(16,1,'redirect_on_login','n'),
	(19,1,'add_trailing_slash','y'),
	(20,1,'perm_admin_structure_6','y'),
	(21,1,'perm_admin_structure_7','y'),
	(22,1,'perm_admin_structure_8','y'),
	(23,1,'perm_admin_channels_6','y'),
	(24,1,'perm_admin_channels_7','y'),
	(25,1,'perm_admin_channels_8','y'),
	(26,1,'perm_view_global_add_page_6','y'),
	(27,1,'perm_view_global_add_page_7','y'),
	(28,1,'perm_view_global_add_page_8','y'),
	(29,1,'perm_view_add_page_6','y'),
	(30,1,'perm_view_add_page_7','y'),
	(31,1,'perm_view_add_page_8','y'),
	(32,1,'perm_view_view_page_6','y'),
	(33,1,'perm_view_view_page_7','y'),
	(34,1,'perm_view_view_page_8','y'),
	(35,1,'perm_delete_6','all'),
	(36,1,'perm_delete_7','all'),
	(37,1,'perm_delete_8','not_top_1'),
	(38,1,'perm_reorder_6','all'),
	(39,1,'perm_reorder_7','all'),
	(40,1,'perm_reorder_8','not_top_1');

/*!40000 ALTER TABLE `ee2_structure_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_template_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_template_groups`;

CREATE TABLE `ee2_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_template_groups` WRITE;
/*!40000 ALTER TABLE `ee2_template_groups` DISABLE KEYS */;

INSERT INTO `ee2_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`)
VALUES
	(1,1,'site',0,'y'),
	(2,1,'store',3,'n'),
	(15,1,'helpers',4,'n'),
	(8,1,'json',5,'n'),
	(9,1,'xml',6,'n'),
	(13,1,'layouts',1,'n'),
	(14,1,'news',2,'n');

/*!40000 ALTER TABLE `ee2_template_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_template_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_template_member_groups`;

CREATE TABLE `ee2_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_template_no_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_template_no_access`;

CREATE TABLE `ee2_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_templates`;

CREATE TABLE `ee2_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_templates` WRITE;
/*!40000 ALTER TABLE `ee2_templates` DISABLE KEYS */;

INSERT INTO `ee2_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`)
VALUES
	(1,1,1,'index','y','webpage','{embed=\"layouts/index\"}\n\n{exp:stash:set parse_tags=\"yes\"}\n    {exp:channel:entries limit=\"1\" entry_id=\"1\" dynamic=\"off\"}\n    \n        {!-- META --}\n        {stash:meta_title}{title}{/stash:meta_title}\n        {stash:meta_keywords}{meta_keywords backspace=\"1\"}{metakeywords_keyword},{/meta_keywords}{/stash:meta_keywords}\n        {stash:meta_description}{meta_description}{/stash:meta_description}\n        {stash:meta_author}{meta_author}{/stash:meta_author}\n        \n        {!-- TEMPLATE --}\n        {stash:page_bodyclass}no-js{/stash:page_bodyclass}\n        {stash:page_title}{title}{/stash:page_title}\n        {stash:template}site/index{/stash:template}\n        {stash:entry_id}{entry_id}{/stash:entry_id}\n        \n        {!-- CONTENT --}\n        {stash:content}\n            {snippet:modules_carousel}\n            {stash:embed:modules_latestnews}\n            {snippet:modules_spotlight}\n            {snippet:modules_getinvolved}\n        {/stash:content}\n        \n    {/exp:channel:entries}\n{/exp:stash:set}','',1351539189,2,'n',0,'','n','n','o',0),
	(2,1,2,'index','y','webpage','','',1351412262,2,'n',0,'','n','n','o',0),
	(8,1,8,'index','y','webpage','',NULL,1351412262,2,'n',0,'','n','n','o',0),
	(9,1,9,'index','y','webpage','{redirect=\"xml/blog\"}','',1351412262,2,'n',0,'','n','n','o',0),
	(10,1,1,'pages','y','webpage','{exp:channel:entries channel=\"pages\" limit=\"1\"}\n{snippet:global_header}\n\n    <section id=\"main-content\" class=\"interior\" role=\"contentinfo\">\n    \n        <div class=\"heading-wrapper\">\n            <h1>{snippet:if_titleoverride}</h1>\n        </div>\n        \n        {snippet:global_pagebanner}\n        \n        <div class=\"interior {snippet:if_sidebar}\">\n            {snippet:global_sidebar}\n            <div class=\"user-content\">\n                {content}\n                {snippet:modules_form}\n            </div><!--user-content-->\n        </div><!--interior-->\n        \n    </section> <!-- #main-content -->\n            \n{snippet:global_footer}\n{/exp:channel:entries}','',1351610165,2,'n',0,'','n','n','o',0),
	(15,1,2,'checkout','y','webpage','','',1351412262,2,'n',0,'','n','n','o',0),
	(16,1,2,'invoice','y','webpage','','',1351412262,2,'n',0,'','n','n','o',0),
	(17,1,2,'product','y','webpage','','',1351412262,2,'n',0,'','n','n','o',0),
	(28,1,2,'checkout_customerfields','y','webpage','',NULL,1351412262,2,'n',0,'','n','n','o',0),
	(33,1,9,'news','y','feed','{preload_replace:rss_channel=\"news\"}\n\n{exp:rss:feed channel=\"{rss_channel}\"}\n<?xml version=\"1.0\" encoding=\"{encoding}\"?>\n<?xml-stylesheet type=\"text/css\" media=\"screen\" href=\"/themes/site_themes/custom/rss.css\" ?>\n\n<rss version=\"2.0\"\n     xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n     xmlns:sy=\"http://purl.org/rss/1.0/modules/syndication/\"\n     xmlns:admin=\"http://webns.net/mvcb/\"\n     xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n     xmlns:content=\"http://purl.org/rss/1.0/modules/content/\">\n\n    <channel>\n        <title><![CDATA[{channel_name}]]></title>\n        <link>{channel_url}</link>\n        <description>{channel_description}</description>\n        <dc:language>{channel_language}</dc:language>\n        <dc:creator>{email}</dc:creator>\n        <dc:rights>Copyright {gmt_date format=\"%Y\"}</dc:rights>\n        <dc:date>{gmt_date format=\"%Y-%m-%dT%H:%i:%s%Q\"}</dc:date>\n        <admin:generatorAgent rdf:resource=\"http://jarrettbarnett.com/\" />\n\n        {exp:channel:entries channel=\"{rss_channel}\" limit=\"10\" dynamic_start=\"on\" disable=\"member_data|pagination\"}\n        <item>\n            <title><![CDATA[{title}]]></title>\n            <link>{title_permalink=\'site/index\'}</link>\n            <guid>{title_permalink=\'site/index\'}#When:{gmt_entry_date format=\"%H:%i:%sZ\"}</guid>\n            <description><![CDATA[{blog_content}]]></description>\n            {categories}<dc:subject><![CDATA[{category_name}]]></dc:subject>{/categories}\n            <dc:date>{gmt_entry_date format=\"%Y-%m-%dT%H:%i:%s%Q\"}</dc:date>\n        </item>\n        {/exp:channel:entries}\n\n    </channel>\n</rss>\n\n{/exp:rss:feed}','',1351610254,2,'n',0,'','n','n','o',0),
	(45,1,1,'contact','y','webpage','','',1351412262,2,'n',0,'','n','n','o',0),
	(48,1,1,'404','y','webpage','{exp:channel:entries channel=\"pages\" limit=\"1\" entry_id=\"30\" dynamic=\"off\"}\n{snippet:global_header}\n\n    <section id=\"main-content\" class=\"interior\" role=\"contentinfo\">\n    \n        <div class=\"heading-wrapper\">\n            <h1>{snippet:if_titleoverride}</h1>\n        </div>\n        \n        {snippet:global_pagebanner}\n        \n        <div class=\"interior {snippet:if_sidebar}\">\n            {snippet:global_sidebar}\n            <div class=\"user-content\">\n                <h2>Oops! The page you were looking for could not be found.<br />Please contact us if you need additional assistance.</h2>\n            </div><!--user-content-->\n        </div><!--interior-->\n        \n    </section> <!-- #main-content -->\n            \n{snippet:global_footer}\n{/exp:channel:entries}','',1351610165,2,'n',0,'','n','n','o',0),
	(49,1,13,'index','y','webpage','{preload_replace:og=\"og_pages\"}\n<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n{snippet:layouts_head}\n</head>\n<body id=\"{exp:stash:get name=\'page_bodyid\'}\" class=\"{exp:stash:page_bodyclass}\">\n{snippet:layouts_header}\n    <section id=\"content\" class=\"content-main\" role=\"main\">\n        <div class=\"wrapper\">\n            <section  class=\"full\" role=\"contentinfo\">\n                <h1 class=\"hidden\">{exp:stash:page_title}</h1>\n                {exp:stash:content}\n            </section><!-- #main-content -->\n        </div>\n    </section><!-- #content -->\n{snippet:layouts_footer}\n</body>\n</html>',NULL,1351575212,2,'n',0,'','n','n','o',0),
	(50,1,14,'archive','y','webpage','',NULL,1351469228,2,'n',0,'','n','n','o',0),
	(51,1,14,'article','y','webpage','{preload_replace:og=\"og_news\"}\n{preload_replace:sidebar_field=\"news_relatedcontent\"}\n\n{exp:channel:entries channel=\"news\" limit=\"1\" parse=\"inward\"}\n{snippet:global_header}\n\n<section id=\"main-content\" class=\"interior\" role=\"contentinfo\">\n    \n    <div class=\"interior {snippet:if_sidebar}\">\n\n        {snippet:global_sidebar}\n\n        <div class=\"user-content\">\n            <article class=\"results results-list\">\n                <header class=\"article-head\">\n    	            <h1 class=\"heading-wrapper\">{title}</h1>\n                    <div class=\"timestamp\">Posted {entry_date format=\"%l %F %j, %Y %g:%i%A\"}<span class=\"sub-info source\">Author: {author}</span></div>\n                    <ul class=\"related-list\">\n                        <li>Categorized In: {categories backspace=\"2\"}</li><li><a href=\"/news/category/{category_url_title}\">{category_name}</a>, {/categories}</li>\n                    </ul>\n                </header>\n                {news_images search:news_images_feature=\"not IS_EMPTY\"}\n                <div class=\"media-image\">                    \n                   <img alt=\"{news_images_featurealt}\" src=\"{news_images_feature:resized}\">\n                </div>\n                {/news_images}\n                {news_summary}\n            </article>\n            \n            {embed=\"news/article_comments entry_id=\"{entry_id}\" comment_total=\"{comment_total}\"}\n	\n        </div><!--user-content-->\n    </div><!--interior-->                    \n</section> <!-- #main-content -->\n\n{snippet:global_footer}\n{/exp:channel:entries}',NULL,1351610165,2,'n',0,'','n','n','o',0),
	(52,1,14,'author','y','webpage','',NULL,1351469228,2,'n',0,'','n','n','o',0),
	(53,1,14,'index','y','webpage','{exp:channel:entries channel=\"pages\" limit=\"1\"}\n{snippet:global_header}\n\n            <section id=\"main-content\" class=\"interior\" role=\"contentinfo\">                \n                <div class=\"interior {snippet:if_sidebar}\">\n                \n                    {snippet:global_sidebar}\n\n                    <div class=\"user-content\">\n                        <div class=\"heading-wrapper\">\n        	                <h1>{snippet:if_titleoverride}</h1>\n    	                </div>\n    	                <form id=\"news-filter\" class=\"form focus\" action=\"#\">\n							<fieldset class=\"filter\">\n								<legend class=\"hidden\">Filter Results</legend>\n    	                		<label for=\"filter\">Filter By: <span class=\"filtertitle\">All News</span></label>\n    	                		<select id=\"filter\">\n    	                		     <option value=\"/news/\" data-cid=\"\">All News</option>\n        	                		{exp:channel:categories channel=\"news\"}\n    	                			<option value=\"/news/category/{category_url_title}\" data-name=\"{category_name}\" data-cid=\"{category_id}\">{category_name}</option>\n    	                			{/exp:channel:categories}\n    	                		</select>\n    	                	</fieldset>\n    	                </form>\n                        {content}\n                        {if segment_2 == \"category\"}\n                            {embed=\"news/index_articles\" dynamic=\"on\"}\n                        {if:else}\n                            {embed=\"news/index_articles\" dynamic=\"off\"}\n                        {/if}\n                    </div><!--user-content-->\n                    \n                </div><!--interior-->\n            </section> <!-- #main-content -->\n\n{snippet:global_footer}\n{/exp:channel:entries}',NULL,1351610165,2,'n',0,'','n','n','o',0),
	(54,1,14,'preview','y','webpage','',NULL,1351469228,2,'n',0,'','n','n','o',0),
	(60,1,1,'related','y','webpage','{!-- strictly for preview and structure module purposes --}\n{exp:channel:entries limit=\"1\"}\n{snippet:global_header}\n\n{snippet:global_sidebar}\n\n{snippet:global_footer}\n{/exp:channel:entries}',NULL,1351575212,2,'n',0,'','n','n','o',0),
	(59,1,14,'index_articles','y','webpage','{exp:channel:entries channel=\"news\" limit=\"5\" dynamic=\"{embed:dynamic}\"}\n    {if count == 1}\n    <div id=\"news-results\" class=\"results\">\n    	<ul class=\"results-list\">\n    {/if}\n    		<li class=\"list-item\">\n                {news_images}\n                    {if news_images_preview}\n                    <div class=\"media-image\">                    \n                        <a href=\"/news/article/{url_title}\"><img alt=\"{title}\" src=\"{news_images_preview:thumbnail}\"></a>\n                    </div>\n                    {/if}\n                {/news_images}\n                <div class=\"news-item\">\n                    <h3><a href=\"/news/article/{url_title}\">{title}</a></h3>\n                    <h4 class=\"timestamp\">Posted {entry_date format=\"%l %F %j, %Y %g:%i%A\"}<span class=\"sub-info source\">Author: {author}</span></h4>\n                    <ul class=\"related-list\">\n                        <li>Categorized In: {categories backspace=\"2\"}</li><li><a href=\"/news/category/{category_url_title}\">{category_name}</a>, {/categories}</li>\n                    </ul>\n                    {if news_preview}\n                        <p>{news_preview text_only=\"yes\"} &#8230; <a class=\"read-more\" href=\"/news/article/{url_title}\">Read&nbsp;More</a></p>\n                    {if:else}\n                        <p>{exp:eehive_hacksaw chars=\"120\" allow=\"\" append=\' &#8230; <a class=\"read-more\" href=\"/news/article/{url_title}\">Read&nbsp;More</a>\'}{news_summary}{/exp:eehive_hacksaw}</p>\n                    {/if}\n                </div>\n            </li>              \n    {if count == total_results}\n    	</ul><!--results-list-->\n    </div>\n    {/if}\n{/exp:channel:entries}',NULL,1351610165,2,'n',0,'','n','n','o',0),
	(57,1,15,'entries','y','webpage','{if segment_3 == \"news\"}\n{exp:channel:entries channel=\"news\" limit=\"5\" category=\"{segment_4}\" dynamic=\"off\"}\n    {if count == 1}\n    <div id=\"news-results\" class=\"results\">\n    	<ul class=\"results-list\">\n    {/if}\n    		<li class=\"list-item\">\n                {news_images}\n                    {if news_images_preview}\n                    <div class=\"media-image\">                    \n                        <a href=\"/news/article/{url_title}\"><img alt=\"{title}\" src=\"{news_images_preview:thumbnail}\"></a>\n                    </div>\n                    {/if}\n                {/news_images}\n                <div class=\"news-item\">\n                    <h3><a href=\"/news/article/{url_title}\">{title}</a></h3>\n                    <h4 class=\"timestamp\">Posted {entry_date format=\"%l %F %j, %Y %g:%i%A\"}<span class=\"sub-info source\">Author: {author}</span></h4>\n                    <ul class=\"related-list\">\n                        <li>Categorized In:</li>\n                        <li>{categories backspace=\"2\"}<a href=\"/news/category/{category_url_title}\">{category_name}</a>, {/categories}</li>\n                    </ul>\n                    {if news_preview}\n                        <p>{news_preview text_only=\"yes\"} &#8230; <a class=\"read-more\" href=\"/news/article/{url_title}\">Read&nbsp;More</a></p>\n                    {if:else}\n                        <p>{exp:eehive_hacksaw chars=\"120\" allow=\"\" append=\' &#8230; <a class=\"read-more\" href=\"/news/article/{url_title}\">Read&nbsp;More</a>\'}{news_summary}{/exp:eehive_hacksaw}</p>\n                    {/if}\n                </div>\n            </li>              \n    {if count == total_results}\n    	</ul><!--results-list-->\n    </div>\n    {/if}\n{/exp:channel:entries}\n{/if}',NULL,1351550625,2,'n',0,'','n','n','o',0),
	(58,1,15,'index','y','webpage','',NULL,1351539189,2,'n',0,'','n','n','o',0),
	(61,1,14,'article_comments','y','webpage','<section class=\"comments user-content\">\n    <h1 class=\"heading-wrapper\">Comments ({embed:comment_total})</h1> \n    \n    {if embed:comment_total > 0}\n    <ul class=\"comment-list\">\n        {exp:comment:entries sort=\"asc\" limit=\"10\" entry_id=\"{embed:entry_id}\"}\n        <?php $gravatar = md5(strtolower(trim( \"{email}\" ))); ?>\n        <li>\n            {if email}\n            <div class=\"media-image grid\">                    \n               <img alt=\"{email}\" src=\"http://www.gravatar.com/avatar/<?=$gravatar?>.jpg\">\n            </div>\n            {/if}\n            <div class=\"comment-content\">\n                {if url}<h2 class=\"title\"><a href=\"{url}\" rel=\"nofollow\">{name}</a></h2>{if:else}<h2 class=\"title\">{name}</h2>{/if}\n                <h3 class=\"timestamp\">Posted {comment_date format=\"%l %F %j, %Y %g:%i%A\"}</h3>\n                <p>{comment}</p>\n            </div> <!-- comment-content -->\n        </li>\n        {/exp:comment:entries}\n    </ul>\n    {/if}\n    \n    {exp:comment:form channel=\"news\" form_class=\"form\" form_id=\"comment-form\" entry_id=\"{embed:entry_id}\"}\n    <form id=\"comment-form\" action=\"#\" class=\"form\">\n        <fieldset>\n            <legend class=\"legend-heading\">Leave a Comment</legend>\n            <ul>\n                {if logged_in}\n                <li>\n                    Name: {name}<br />\n                    Email: {email}<br />\n                    Website: {url}\n                </li>\n                {if:else}\n                <li class=\"name\">\n                    <label for=\"name\">Your Name:</label>\n                    <input class=\"input-text\" type=\"text\" name=\"name\" id=\"name\" placeholder=\"Enter your name\" required />\n                </li>\n                <li class=\"email\">\n                    <label for=\"email\">Your Email:</label>\n                    <input class=\"input-text\" type=\"email\" name=\"email\" id=\"email\" placeholder=\"Enter your email\" required />\n                </li>\n                <li class=\"website\">\n                    <label for=\"email\">Your Website:</label>\n                    <input class=\"input-text\" type=\"url\" name=\"website\" id=\"website\" placeholder=\"Enter your website\" />\n                </li>\n                {/if}\n                <li class=\"textarea\">\n                    <label for=\"comment\">Comment:</label>\n                    <textarea class=\"input-textarea\" id=\"comment\" name=\"comment\" rows=\"10\">{comment}</textarea>\n                </li>\n            </ul>\n        </fieldset>\n        <div class=\"action\">\n            <div class=\"button-wrapper\">\n                <button class=\"button highlight\" type=\"submit\">Submit</button>\n            </div>\n        </div>\n    </form>\n    {/exp:comment:form}\n    \n</section> <!-- comments -->',NULL,1351610165,2,'n',0,'','n','y','o',0);

/*!40000 ALTER TABLE `ee2_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_throttle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_throttle`;

CREATE TABLE `ee2_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ee2_upload_no_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_upload_no_access`;

CREATE TABLE `ee2_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_upload_no_access` WRITE;
/*!40000 ALTER TABLE `ee2_upload_no_access` DISABLE KEYS */;

INSERT INTO `ee2_upload_no_access` (`upload_id`, `upload_loc`, `member_group`)
VALUES
	(1,'cp',5),
	(10,'cp',5),
	(4,'cp',5),
	(5,'cp',5),
	(1,'cp',6),
	(9,'cp',5),
	(4,'cp',6),
	(5,'cp',6),
	(1,'cp',7),
	(8,'cp',5),
	(4,'cp',7),
	(5,'cp',7),
	(1,'cp',8),
	(2,'cp',5),
	(3,'cp',5),
	(4,'cp',8),
	(5,'cp',8),
	(7,'cp',5);

/*!40000 ALTER TABLE `ee2_upload_no_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_upload_prefs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_upload_prefs`;

CREATE TABLE `ee2_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_upload_prefs` WRITE;
/*!40000 ALTER TABLE `ee2_upload_prefs` DISABLE KEYS */;

INSERT INTO `ee2_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`)
VALUES
	(1,1,'Default','/Volumes/Development/local/ee2.local/root/httpdocs/public/uploads/default/','/public/uploads/default/','all','','','','','','','','','','',NULL),
	(2,1,'Pages','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/pages/','http://nerdery.loc/public/uploads/pages/','all','','','','','','','','','','1',NULL),
	(3,1,'News > Preview','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/news/previews/','http://nerdery.loc/public/uploads/news/previews/','img','','','','','','','','','','2',NULL),
	(4,1,'Default Store Directory','/Volumes/Development/local/ee2.local/root/httpdocs/public/uploads/store/','/public/uploads/store/','all','','','','','','','','','','3',NULL),
	(5,1,'Product Images','/Volumes/Development/local/ee2.local/root/httpdocs/public/uploads/store/products/','/public/uploads/store/products/','img','','','','','','','','','','3',NULL),
	(6,1,'Get Involved','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/getinvolved/','/public/uploads/getinvolved/','img','','','','','','','','','','1',NULL),
	(7,1,'Carousel','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/carousel/','/public/uploads/carousel/','img','','','','','','','','','','1',NULL),
	(8,1,'News > Feature','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/','/','img','','','','','','','','','','1',NULL),
	(9,1,'Related Content > Callout','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/related/callout/','/public/uploads/related/callout/','img','','','','','','','','','','2|1|3',NULL),
	(10,1,'Related Content > Preview','/Users/jarrett/Sites/jarrettbarnett/nerdery.loc/root/httpdocs/public/uploads/related/preview/','/public/uploads/related/preview/','img','','','','','','','','','','2|1|3',NULL);

/*!40000 ALTER TABLE `ee2_upload_prefs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_wygwam_configs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_wygwam_configs`;

CREATE TABLE `ee2_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_wygwam_configs` WRITE;
/*!40000 ALTER TABLE `ee2_wygwam_configs` DISABLE KEYS */;

INSERT INTO `ee2_wygwam_configs` (`config_id`, `config_name`, `settings`)
VALUES
	(1,'Basic','YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'),
	(2,'Full','YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');

/*!40000 ALTER TABLE `ee2_wygwam_configs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ee2_zoo_flexible_admin_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ee2_zoo_flexible_admin_menus`;

CREATE TABLE `ee2_zoo_flexible_admin_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT NULL,
  `group_id` int(4) DEFAULT NULL,
  `nav` text,
  `autopopulate` tinyint(1) DEFAULT NULL,
  `hide_sidebar` tinyint(1) DEFAULT NULL,
  `startpage` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `ee2_zoo_flexible_admin_menus` WRITE;
/*!40000 ALTER TABLE `ee2_zoo_flexible_admin_menus` DISABLE KEYS */;

INSERT INTO `ee2_zoo_flexible_admin_menus` (`id`, `site_id`, `group_id`, `nav`, `autopopulate`, `hide_sidebar`, `startpage`)
VALUES
	(1,1,6,'<li class=\"\"  id=\"moduleitem\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=structure\" class=\"first_level\">Pages</a></li><li class=\"newlink\"  id=\"\" ><a href=\"manage.php?D=cp&C=admin_content&M=category_editor&group_id=2\" class=\"first_level\">Categories</a></li><li class=\"\"  id=\"moduleitem\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics\" class=\"first_level\">Analytics</a></li><li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Members</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=view_all_members\">View All</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_group_manager\">Member Groups</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=new_member_form\">New Member</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_banning\">Ban Members</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_validation\">Activate Members</a></li></ul></li><li class=\"newlink\"  id=\"\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=low_variables\" class=\"first_level\">Settings</a></li>',1,0,'manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics'),
	(2,1,7,'<li class=\"\"  id=\"moduleitem\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=structure\" class=\"first_level\">Pages</a></li><li class=\"newlink\"  id=\"\" ><a href=\"manage.php?D=cp&C=admin_content&M=category_editor&group_id=2\" class=\"first_level\">Categories</a></li><li class=\"\"  id=\"moduleitem\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics\" class=\"first_level\">Analytics</a></li><li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Members</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=view_all_members\">View All</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_group_manager\">Member Groups</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=new_member_form\">New Member</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_banning\">Ban Members</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_validation\">Activate Members</a></li></ul></li><li class=\"newlink\"  id=\"\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=low_variables\" class=\"first_level\">Settings</a></li>',1,0,'manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics'),
	(3,1,8,'<li class=\"\"  id=\"moduleitem\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=structure\" class=\"first_level\">Pages</a></li><li class=\"newlink\"  id=\"\" ><a href=\"manage.php?D=cp&C=admin_content&M=category_editor&group_id=2\" class=\"first_level\">Categories</a></li><li class=\"\"  id=\"moduleitem\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics\" class=\"first_level\">Analytics</a></li><li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Members</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=view_all_members\">View All</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_group_manager\">Member Groups</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=new_member_form\">New Member</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_banning\">Ban Members</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_validation\">Activate Members</a></li></ul></li>',1,0,'manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics'),
	(4,1,1,'<li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Store</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=store\">Dashboard</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=store&method=orders\">Orders</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=store&method=inventory\">Inventory</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=store&method=reports\">Reports</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=store&method=settings\">Settings</a></li></ul></li><li class=\"newlink\"  id=\"\" ><a href=\"/manage.php?D=cp&C=addons_modules&M=show_module_cp&module=structure\" class=\"first_level\">Pages</a></li><li class=\"parent newfolder\"  id=\"\" ><a href=\"#\" class=\"first_level\">Templates</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=manager\">Template Manager</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=snippets\">Snippets</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=global_variables\">Global Variables</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=sync_templates\">Synchronize Templates</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=template_preferences_manager\">Template Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=global_template_preferences\">Global Preferences</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"parent\" id=\"\"><a href=\"#\">Other</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=email_notification\">Email Notifications</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=user_message\">User Messages</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=system_offline\">Offline Template</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=design&M=member_profile_templates\">Member Profile Templates</a></li></ul></li></ul></li><li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Members</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=view_all_members\">View All</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_group_manager\">Member Groups</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=new_member_form\">New Member</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_validation\">Activate Members</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_banning\">Ban Members</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=ip_search\">IP Search</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=custom_profile_fields\">Member Fields</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=members&M=member_config\">Preferences</a></li></ul></li><li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Add-Ons</a><ul class=\"\"><li class=\"parent\" id=\"modulefolder\"><a href=\"#\">Modules</a><ul class=\"\"><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=automin\">AutoMin</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=cartthrob\">CartThrob</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=comment\">Comment</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=freeform\">Freeform</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=minicp\">Mini CP</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics\">Republic Analytics</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=safecracker\">SafeCracker</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=snippet_editor\">Snippet Editor</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=store\">Store</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=structure\">Structure</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=tagger\">Tagger</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=wygwam\">Wygwam</a></li><li class=\"\" id=\"moduleitem\"><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=zoo_flexible_admin\">Zoo Flexible Admin</a></li><li class=\"bubble_footer\" id=\"\"><a href=\"\"></a></li></ul></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_accessories\">Accessories</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_extensions\">Extensions</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_fieldtypes\">Fieldtypes</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=addons_plugins\">Plugins</a></li></ul></li><li class=\"newlink\"  id=\"\" ><a href=\"manage.php?D=cp&C=addons_modules&M=show_module_cp&module=low_variables\" class=\"first_level\">Settings</a></li><li class=\"parent\"  id=\"\" ><a href=\"#\" class=\"first_level\">Admin</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=channel_management\">Channels</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=field_group_management\">Channel Fields</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=status_group_management\">Statuses</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=category_management\">Categories</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=content_files&M=file_upload_preferences\">File Upload Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=content_files&M=watermark_preferences\">Watermark Preferences</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=global_channel_preferences\">Global Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=general_configuration\">General Configuration</a></li><li class=\"parent\" id=\"\"><a href=\"#\">Content Administration</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=default_ping_servers\">Default Ping Servers</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_content&M=default_html_buttons\">Default HTML Buttons</a></li></ul></li><li class=\"parent\" id=\"\"><a href=\"#\">System Administration</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=localization_settings\">Localization Settings</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=email_configuration\">Email Configuration</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=database_settings\">Database Settings</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=output_debugging_preferences\">Output and Debugging</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=image_resizing_preferences\">Image Resizing Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=emoticon_preferences\">Emoticon Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=search_log_configuration\">Search Log Configuration</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=config_editor\">Config File Editor</a></li></ul></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=localization_settings\">Localization Settings</a></li><li class=\"parent\" id=\"\"><a href=\"#\">Security and Privacy</a><ul class=\"\"><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=security_session_preferences\">Security and Sessions</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=cookie_settings\">Cookie Settings</a></li><li class=\"nav_divider\" id=\"\"><a href=\"#\"></a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=word_censoring\">Word Censoring</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=tracking_preferences\">Tracking Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=captcha_preferences\">CAPTCHA Preferences</a></li><li class=\"\" id=\"\"><a href=\"manage.php?D=cp&C=admin_system&M=throttling_configuration\">Throttling Preferences</a></li></ul></li></ul></li>',1,0,'manage.php?D=cp&C=addons_modules&M=show_module_cp&module=republic_analytics');

/*!40000 ALTER TABLE `ee2_zoo_flexible_admin_menus` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
